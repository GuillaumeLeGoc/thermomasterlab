% Computes distributions of pturn and pflip (defined at the trajectory
% level), rescaled by the temperature-averaged value that corresponds to Y
% in the generic description of navigational parameters selection.
% It is used within the simulations.
% Requires "allsequences.mat" file.

clear
clc

% --- Parameters
%  * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_out0 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ptpfmeanT.mat'];
fn_out1 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ptpfdistY.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Bins
nbins = 80;
method = 'kde';
mode = 'edges';
bturn_n_lim = [0, 4];
bflip_n_lim = [0, 4];
kdeopt_n = {'Bandwidth', 0.08, 'Support', 'Positive'};

% * Threshold
theta_threshold = 10;   % deg

% * Load data
data = load(fn_dataset, 'dtheta');

% --- Prepare arrays & bins
bturn_n = linspace(bturn_n_lim(1), bturn_n_lim(2), nbins + 1)';
bflip_n = linspace(bflip_n_lim(1), bflip_n_lim(2), nbins + 1)';

cdfturn_n = NaN(nbins, numel(T));
cdfflip_n = NaN(nbins, numel(T));
mpturn = NaN(numel(T), 1);
mpflip = NaN(numel(T), 1);

% --- Main loop
for idT = 1:numel(T)
    
    fprintf(['Estimating turn and flip prob. for T = ', num2str(T(idT)) '°C...']); tic
    
    % Get data
    turnangle = data.dtheta{idT};
    nseq = size(turnangle, 2);
    
    allpflip = NaN(nseq, 1);
    allpturn = NaN(nseq, 1);
	toclean = false(nseq, 1);
    
    for seq = 1:nseq
        
        % - Prepare data
        dtheta = turnangle(:, seq);
        dtheta(isnan(dtheta)) = [];
        
        % - Estimate pturn
        nbouts = numel(dtheta);
        nturns = sum(abs(dtheta) > theta_threshold);
        pt = nturns/nbouts;
        allpturn(seq) = pt;
        
        % - Estimate pflip
        dtheta = dtheta - mean(dtheta);  % bias correction
        % Trinarize
        dthetacopy = dtheta;
        dtheta(abs(dthetacopy) <= theta_threshold) = 0;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
        
        % Create lagged vectors
        dthetai = dtheta(1:end  - 1);
        dthetan = dtheta(2:end);
        % Binning
        binedges = [-1, 0, 1, 1 + eps];
        bindti = discretize(dthetai, binedges);                 % regroup in bins
        meandtn = accumarray(bindti, dthetan, [], @mean, NaN);  % average over bins  % average over bins
        
        % skip if there is not 3 kind of bouts in traj.
        if any(isnan(meandtn)) || numel(meandtn) < 3
            toclean(seq) = true;
            continue;
        end
        
        % Fit
        yfit = @(pflip, x) pt.*(1-2*pflip).*x;
        ft = fit((-1:1)', meandtn, yfit, 'StartPoint', 0.25, ...
            'Lower', 0, 'Upper', 1);
        if isequal(ft.pflip, 0) || isequal(ft.pflip, 1)
            toclean(seq) = true;    % ignore failed fit
            continue;
        else
            allpflip(seq) = ft.pflip;
        end
    end
    
    % Cleanup
    allpflip(toclean) = [];
    allpturn(toclean) = [];
    
    mpturn(idT) = mean(allpturn);
    mpflip(idT) = mean(allpflip);
    
    % Rescale
    allpturn_n = allpturn./mpturn(idT);
    allpflip_n = allpflip./mpflip(idT);
    
    % Get pdf & cdf
    [cdfturn_n(:, idT), cturn_n] = computeCDF(bturn_n, allpturn_n, 'method', method, 'mode', mode, 'param', kdeopt_n);
	[cdfflip_n(:, idT), cflip_n] = computeCDF(bflip_n, allpflip_n, 'method', method, 'mode', mode, 'param', kdeopt_n);
    
    fprintf('\t Done (%2.2fs).\n', toc);
   
end

% --- Mean rescaled cdf
mcdfYptr = mean(cdfturn_n, 2);
mcdfYpfp = mean(cdfflip_n, 2);
binsyptr = cturn_n;
binsypfp = cflip_n;

% --- Save
if ~isempty(fn_out0)
    save(fn_out0, 'mpturn', 'mpflip');
end
if ~isempty(fn_out1)
    save(fn_out1, 'binsyptr', 'mcdfYptr', 'binsypfp', 'mcdfYpfp');
end