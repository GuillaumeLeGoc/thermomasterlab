% Analyse output of simulation : MSD, MSR, pflip extraction
% and correlations of means.
% Requires to run simulation with "simulations_generate_baseline.m" script,
% and to compute MSD and MSR with "prepsimu_meansquarex.m" script.
% (Figure 6B-C)

clear
clc

% --- Parameters
T = [18, 22, 26, 30, 33];

nfish = 1000;
blim = 'reflective';
pork = 'p';             % simu with proba or rate ('p' or 'k')

theta_threshold = 10;   % deg
scale_dsp = 1.6;        % rescale factor for turns displacements

% Bins
binsibi = linspace(0, 10, 100);     % bins for interbout interval (s)
binsdsp = linspace(0, 15, 100);     % bins for displacement (mm)
binsreo = linspace(-180, 180, 100); % bins for reorientation (deg)
msx_ndelays = 15;                   % max delays in bouts for MSX

% * Figures
data_color = [0.15, 0.15, 0.15];
simu_color = [0.5, 0.5, 0.5];
temp_color = rainbow(numel(T));
fig_msdb = figure; ax_msdb = gca; hold(ax_msdb, 'on');
fig_msrb = figure; ax_msrb = gca; hold(ax_msrb, 'on');

% * Files
fn_simu = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'nocorr_simu_mv_' pork '_' blim '_' num2str(nfish) '.mat'];
fn_data_msx = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'data_msx.mat'];
fn_data_Tmeans = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MeanOverTemperatures.mat'];
fn_data_meanmat = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix_' pork '.mat'];
fn_data_val = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_data_ptpf = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'ptpfmeanT.mat'];

% * Load data
if ~exist('data_simu', 'var')
    data_simu = load(fn_simu);
    data_real = load(fn_data_val);
end
data_msx = load(fn_data_msx);
data_meanfeatsmat = load(fn_data_meanmat);
data_ptpf = load(fn_data_ptpf);
data_Tmean = load(fn_data_Tmeans);

% --- Processing
fprintf('Analyzing'); tic
for idT = 1:numel(T)
    
    % * Get features for data & simu
    % - interbout intervals
    data_ibi = data_real.interboutintervals{idT};
    data_ibi = data_ibi(:);
    data_ibi(isnan(data_ibi)) = [];
    simu_ibi = fixCellSize(data_simu.alli(idT, :));
    simu_ibi = [simu_ibi{:}];
    data_mibi = data_Tmean.mibi(idT);
    simu_mibi = mean(simu_ibi(:), 'omitnan');
    
    % - displacements
    data_dsp = data_real.displacements{idT};
    data_dsp = data_dsp(:);
    data_dsp(isnan(data_dsp)) = [];
    simu_dsp = fixCellSize(data_simu.alld(idT, :));
    simu_dsp = [simu_dsp{:}];
    data_mdsp = data_Tmean.mdsp(idT);
    
    % - reorientation
    data_reo = data_real.dtheta{idT};
	
    simu_reo = fixCellSize(data_simu.allr(idT, :));
    simu_reo = [simu_reo{:}];
    
    % - reorientation of turns only
    simu_isturn = abs(simu_reo) > theta_threshold;
    data_isturn = abs(data_reo) > theta_threshold;
    data_turn = data_reo;
    data_turn(~data_isturn) = NaN;
    data_reo = data_reo(:);
    data_reo(isnan(data_reo)) = [];
    data_mturn = data_Tmean.mreo(idT);
    
    % Rescale displacement corresponding to turns
    simu_dsp(simu_isturn) = simu_dsp(simu_isturn)./scale_dsp;
    simu_mdsp = mean(simu_dsp(:), 'omitnan');
    
    simu_turn = simu_reo;
    simu_turn(~simu_isturn) = NaN;
    simu_mturn = mean(abs(simu_turn(:)), 'omitnan');
    
    % - pturn by trajectory
    nboutpertraj = sum(~isnan(simu_reo));
    simu_pturn = sum(simu_isturn)./nboutpertraj;
    
    % Mean pturn and std
    simu_mpturn = mean(simu_pturn);
    simu_spturn = std(simu_pturn);
    
    data_mpturn = data_ptpf.mpturn(idT);
    data_spturn = mean(abs(data_ptpf.ci_mpturn(idT, :) - data_mpturn));
    
    % - pflip by trajectory
    simu_trireo = simu_reo;
    simu_trireo(~simu_isturn) = 0;
    simu_trireo(simu_trireo < 0) = -1;
    simu_trireo(simu_trireo > 0) = +1;
    
    % Create lagged vectors & fit
    simu_pflip = NaN(size(simu_trireo, 2), 1);
    for s = 1:size(simu_trireo, 2)
        dtheta = simu_trireo(:, s);
        dtheta(isnan(dtheta)) = [];
        dthetai = dtheta(1:end  - 1);
        dthetan = dtheta(2:end);
        bindti = discretize(dthetai, 3);            % regroup in bins
        meandtn = accumarray(bindti, dthetan, [], @mean, 0);  % average over bins
        
        if numel(meandtn) < 3
            simu_pflip(s) = eps;
            continue
        end
        
        g = @(pflip, x) simu_pturn(s)*(1-2*pflip).*x;
        fitt = fit((-1:1)', meandtn, g, 'StartPoint', 0.25);
        simu_pflip(s) = fitt.pflip;
    end
    
    % Mean pflip and std
    simu_mpflip = mean(simu_pflip);
    simu_spflip = std(simu_pflip);

    data_mpflip = data_ptpf.mpflip(idT);
    data_spflip = mean(abs(data_ptpf.ci_mpflip(idT, :) - data_mpflip));
    
    % * Build feature matrix
    simu_kturn = simu_pturn./simu_mibi;
    simu_kflip = simu_pflip./simu_mibi;
    simu_seqturn = mean(abs(simu_turn), 'omitnan');
    simu_seqturn(isnan(simu_seqturn)) = eps;
    simu_meanmat = [mean(simu_ibi, 'omitnan')', simu_kturn', simu_kflip, simu_seqturn', nanmean(simu_dsp)'];
    data_meanmat = data_meanfeatsmat.feat_mat_pooled{idT};
    
    % * Correlations
    data_R = corrcoef(data_meanmat);
    simu_R = corrcoef(simu_meanmat);
    
    % * Get pdfs
    data_pdfibi = computePDF(binsibi, data_ibi(:), 'method', 'hist', 'mode', 'centers');
    simu_ibi = simu_ibi(:);
    simu_ibi(isnan(simu_ibi)) = [];
    simu_pdfibi = computePDF(binsibi, simu_ibi(:), 'method', 'hist', 'mode', 'centers');
    
    data_pdfdsp = computePDF(binsdsp, data_dsp(:), 'method', 'hist', 'mode', 'centers');
    simu_dsp = simu_dsp(:);
    simu_dsp(isnan(simu_dsp)) = [];
    simu_pdfdsp = computePDF(binsdsp, simu_dsp(:), 'method', 'hist', 'mode', 'centers');
    
    data_pdfreo = computePDF(binsreo, data_reo(:), 'method', 'hist', 'mode', 'centers');
    simu_reo = simu_reo(:);
    simu_reo(isnan(simu_reo)) = [];
    simu_pdfreo = computePDF(binsreo, simu_reo(:), 'method', 'hist', 'mode', 'centers');
    
    % * Get MSD and MSR
    % Get data MSX
    data_msd_delay = data_msx.msd.delay_bout{idT};
    data_msd = data_msx.msd.data_bout{idT};
    data_msd_sem = data_msx.msd.sem_bout{idT};
    
    data_msr_delay = data_msx.msr.delay_bout{idT};
    data_msr = data_msx.msr.data_bout{idT};
    data_msr_sem = data_msx.msr.sem_bout{idT};
    
    % Prepare msdanalyzer inputs
    boutvec = cellfun(@(x) (1:numel(x))', data_simu.allt(idT, :), 'UniformOutput', false);
    spatialseq = cellfun(@(x,y,z) cat(2, x, y, z), boutvec', data_simu.allx(idT, :)', data_simu.ally(idT, :)', 'UniformOutput', false);
    orientaseq = cellfun(@(x, y) cat(2, x, cumsum(y)), boutvec', data_simu.allr(idT, :)', 'UniformOutput', false);
    
    % MSD
    msd = msdanalyzer(2, 'mm', 'bout');
    msd = msd.addAll(spatialseq);
    msd = msd.computeMSD;
    results = msd.getMeanMSD;
    simu_msd_delay = results(1:msx_ndelays, 1);
    simu_msd = results(1:msx_ndelays, 2);
    
    % MSR
    msr = msdanalyzer(1, 'deg', 'bout');
    msr = msr.addAll(orientaseq);
    msr = msr.computeMSD;
    results = msr.getMeanMSD;
    simu_msr_delay = results(1:msx_ndelays, 1);
    simu_msr = results(1:msx_ndelays, 2);
    
    fprintf('.');
    
    % * Display
    % - pdf ibi
    figure; ax = gca; hold(ax, 'on');
    plot(ax, binsibi, data_pdfibi, 'Color', data_color);
    plot(ax, binsibi, simu_pdfibi, 'Color', simu_color);
    plot(ax, [data_mibi, data_mibi], ax.YLim, '--', 'Color', data_color);
    plot(ax, [simu_mibi, simu_mibi], ax.YLim, '--', 'Color', simu_color);
    legend('Data', 'Simu.', 'Mean data', 'Mean simu.');
    xlabel(ax, '\delta{t} [s]');
    ylabel(ax, 'pdf');
    title(['T=' num2str(T(idT)) '°C']);
    
    % - pdf dsp
    figure; ax = gca; hold(ax, 'on');
    plot(ax, binsdsp, data_pdfdsp, 'Color', data_color);
    plot(ax, binsdsp, simu_pdfdsp, 'Color', simu_color);
    plot(ax, [data_mdsp, data_mdsp], ax.YLim, '--', 'Color', data_color);
    plot(ax, [simu_mdsp, simu_mdsp], ax.YLim, '--', 'Color', simu_color);
    legend('Data', 'Simu.', 'Mean data', 'Mean simu.');
    xlabel(ax, 'd [mm]');
    ylabel(ax, 'pdf');
    title(['T=' num2str(T(idT)) '°C']);
    
    % - pdf reo
    figure; ax = gca; hold(ax, 'on'); ax.YScale = 'log';
    plot(ax, binsreo, data_pdfreo, 'Color', data_color);
    plot(ax, binsreo, simu_pdfreo, 'Color', simu_color);
    plot(ax, [data_mturn, data_mturn], ax.YLim, '--', 'Color', data_color);
    plot(ax, [simu_mturn, simu_mturn], ax.YLim, '--', 'Color', simu_color);
    legend('Data', 'Simu.', 'Mean data', 'Mean simu.');
    xlabel(ax, '\delta\theta [deg]');
    ylabel(ax, 'pdf');
    title(['T=' num2str(T(idT)) '°C']);
    
    % - pturn & pflip
    figure; ax = gca; hold(ax, 'on');
    groups = [data_mpturn, simu_mpturn ; data_mpflip, simu_mpflip];
    errors = [data_spturn, simu_spturn ; data_spflip, simu_spflip];
    b = bar(ax, groups);
    
    ngrou = size(groups, 1);
    nbars = size(groups, 2);
    colors = [data_color; simu_color];
    for idb = 1:nbars
        % Colors
        b(idb).FaceColor = 'flat';
        b(idb).CData = repmat(colors(idb, :), [ngrou, 1]);
        
        % Error bars
        groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
        x = [1, 2] - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
        errorbar(x, groups([1, 2], idb), errors(:, idb), '.', 'Color', [0.01, 0.01, 0.01]);
    end
    
    ax.XTick = [1, 2];
    ax.XTickLabel = {'p_{turn}', 'p_{flip}'};
    ylabel(ax, 'Probability');
    legend(ax, 'Data', 'Simu.');
    ylim(ax, [0, 1])
    title(['T=' num2str(T(idT)) '°C']);
    
    % - correlations matrices
    figure;
    ax = subplot(1, 2, 1);
    imagesc(data_R)
    colormap(div_bkr);
    caxis([-.5, .5]);
    c = colorbar;
    title(c, 'R');
    ax.XTick = [1, 2, 3, 4, 5];
    ax.YTick = [1, 2, 3, 4, 5];
    ax.XTickLabel = {'\delta{t}', 'k_{t}', 'k_{f}', '\delta\theta_t', 'd'}';
    ax.YTickLabel = {'\delta{t}', 'k_{t}', 'k_{f}', '\delta\theta_t', 'd'};
    ax.XTickLabelRotation = 45;
    ax.YTickLabelRotation = 45;
    grid(ax, 'off');
    axis(ax, 'square');
    ax.FontSize = 12;
    title('Data');
    
    ax = subplot(1, 2, 2);
    imagesc(simu_R)
    colormap(div_bkr);
    caxis([-.5, .5]);
    c = colorbar;
    title(c, 'R');
    ax.XTick = [1, 2, 3, 4, 5];
    ax.YTick = [1, 2, 3, 4, 5];
    ax.XTickLabel = {'\delta{t}', 'k_{t}', 'k_{f}', '\delta\theta_t', 'd'}';
    ax.YTickLabel = {'\delta{t}', 'k_{t}', 'k_{f}', '\delta\theta_t', 'd'};
    ax.XTickLabelRotation = 45;
    ax.YTickLabelRotation = 45;
    grid(ax, 'off');
    axis(ax, 'square');
    ax.FontSize = 12;
    title('Simu.');
    
    sgtitle(['T=' num2str(T(idT)) '°C']);
    
    % - scatter plot of correlations matrices coefficients
    figure; ax = gca; hold(ax, 'on');
    x = data_R(:);
    x(x == 1) = [];
    y = simu_R(:);
    y(y == 1) = [];
    s0 = scatter(x, y, '+k');
    s0.SizeData = 128;
    plot(x, x, '--k', 'LineWidth', 1);
    xlabel(ax, 'R data');
    ylabel(ax, 'R simu.');
    title(['T=' num2str(T(idT)) '°C']);
    
    % - MSD (bouts)
    e = errorbar(ax_msdb, data_msd_delay, data_msd, data_msd_sem);
    e.Color = temp_color(idT, :);
    e.LineStyle = 'none';
    e.Marker = 'o';
    e.MarkerFaceColor = e.Color;
    p = plot(ax_msdb, simu_msd_delay, simu_msd);
    p.Color = temp_color(idT, :);
    
    % - MSR (bouts)
    e = errorbar(ax_msrb, data_msr_delay, data_msr, data_msr_sem);
    e.Color = temp_color(idT, :);
    e.LineStyle = 'none';
    e.Marker = 'o';
    e.MarkerFaceColor = e.Color;
    p = plot(ax_msrb, simu_msr_delay, simu_msr);
    p.Color = temp_color(idT, :);
end

fprintf('\tDone (%2.2fs).\n', toc);

% - Cosmetic for MSD
dfkplt = errorbar(ax_msdb, [NaN, NaN], [NaN, NaN], [1, 1], 'k', 'DisplayName', 'Data');
sfkplt = plot(ax_msdb, [NaN, NaN], [NaN, NaN], 'k', 'DisplayName', 'Simu.');
xlabel(ax_msdb, 'Delays [bout]');
ylabel(ax_msdb, '<MSD> [mm^2]');
legend([dfkplt, sfkplt], 'Location', 'northwest');
axis(ax_msdb, 'square');

% - Cosmetic for MSR
dfkplt = errorbar(ax_msrb, [NaN NaN], [NaN, NaN], [1, 1], 'k', 'LineStyle', 'none', 'DisplayName', 'Data');
sfkplt = plot(ax_msrb, [NaN, NaN], [NaN, NaN], 'k', 'DisplayName', 'Simu.');
xlabel(ax_msrb, 'Delays [bout]');
ylabel(ax_msrb, '<MSR> [deg^2]');
legend([dfkplt, sfkplt], 'Location', 'northwest');
axis(ax_msrb, 'square');