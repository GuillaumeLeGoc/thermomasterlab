% Computes temperature-averaged parameters for IBI, displacement,
% and turn angle (turn and forwards).
% It is used within the simulations.
% Requires "allsequences.mat" file.

clear
clc

% --- Parameters
% * Files
dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fnout = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MeanOverTemperatures.mat'];

% * Definitions
theta_threshold = 10;    % deg

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Figures
colors = rainbow(numel(T));
msize = 8;  % marker size

% --- Load data
data = load(dataset);

% --- Initialisation
mibi = NaN(numel(T), 1);
mdsp = NaN(numel(T), 1);
mreo = NaN(numel(T), 1);
mfwd = NaN(numel(T), 1);

% --- Processing
fprintf('Processing'); tic
for idT = 1:numel(T)
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
        
    % Pool
    ibi = ibi(:);
    ibi(isnan(ibi)) = [];
    dsp = dsp(:);
    dsp(isnan(dsp)) = [];
    reo = reo(:);
    reo(isnan(reo)) = [];
    fwd = fwd(:);
    fwd(isnan(fwd)) = [];
    
    % Get temperature average
    mibi(idT) = mean(ibi);
    mdsp(idT) = mean(dsp);
    mreo(idT) = mean(reo);
    mfwd(idT) = mean(fwd);
    
    fprintf('.');
end

fprintf('\tDone (%2.2fs).\n', toc);

figure('Name', 'MeansIBI');
ax = gca; hold on
p = plot(T, mibi);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
xlabel('Temperature [°C]');
ylabel('<\delta{t}>_T');
ax.XTick = T;
xlim([17 34])

figure('Name', 'MeansDSP');
ax = gca; hold on
p = plot(T, mdsp);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
xlabel('Temperature [°C]');
ylabel('<d>_T');
ax.XTick = T;
xlim([17 34])

figure('Name', 'MeansREO');
ax = gca; hold on
p = plot(T, mreo);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
xlabel('Temperature [°C]');
ylabel('<\delta\theta_{turn}>_T');
ax.XTick = T;
xlim([17 34])

% --- Save file
if ~isempty(fnout)
    save(fnout, 'mibi', 'mdsp', 'mreo', 'mfwd');
end