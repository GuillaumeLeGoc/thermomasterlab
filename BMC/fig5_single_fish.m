% Single-fish experiments : computes and displays PC coef. and study
% dynamic projection on both first PC to get the autocorrelation of the
% latter.
% (Figure 5A-D)
% Uses "sf_featuresmatrix_pk.mat" and "PCAIntrapooled_pk.mat" (the latter
% can be generated with "BMC/parameters_correlations.m")

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'sf_featuresmatrix_pk_T' num2str(T) '.mat'];
fileallfish = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk.mat'];

% * Settings
timevec = 0:120:7200-1;     % common time vector to interpolate on
ncorrtime = 7200-1;         % max delay for autocorrelation (s)

% * Load file
% single-fish data
ftmp = load(filefeatmat);
featmat = ftmp.feat_mat_pooled;
nfish = numel(featmat);
ntrajs = cellfun(@(x) size(x, 1), featmat);
nfeat = size(featmat{1}, 2);
ttraj = ftmp.ttraj_pooled;
trajduration = ftmp.trajduration_pooled;

% multi-fish pca
ftmp = load(fileallfish);
all26_coefPC1 = ftmp.pc1intrap;
all26_coefPC2 = ftmp.pc2intrap;
all26_projPC1 = ftmp.pc1proj{3};    % 26°C PCA
all26_projPC2 = ftmp.pc2proj{3};
sfac26 = ftmp.sfac;                 % normalization factor

% * Figures
fishtoshow = 13;%1:nfish;
colors = cm20(nfish);

% --- PCA for each fish
% Init.
coeffpc1 = NaN(nfeat, nfish);
coeffpc2 = NaN(nfeat, nfish);
explained = NaN(nfeat, nfish);
pc1proj = cell(nfish, 1);
pc2proj = cell(nfish, 1);
pc1pc2grp = cell(nfish, 1);
dname = cell(nfish + 1, 1);

% * PCA
for idF = 1:nfish
    
    samplefeatmat = featmat{idF};
    
    sfm = samplefeatmat./std(samplefeatmat);
    
    [coeff, ~, ~, ~, explained(:, idF)] = pca(sfm);
    
    if coeff(2, 1) > 0
        coeff(:, 1) = -coeff(:, 1);
    end
    
    coeffpc1(:, idF) = coeff(:, 1);
    coeffpc2(:, idF) = coeff(:, 2);
    
    dname{idF} = ['Fish' num2str(idF)];
    
    % Projection on multi-fish 26 base
    sfmproj = samplefeatmat./sfac26;
    projonpc1 = sfmproj*all26_coefPC1;
    projonpc2 = sfmproj*all26_coefPC2;
    
    pc1proj{idF} = projonpc1;
    pc2proj{idF} = projonpc2;
    pc1pc2grp{idF} = idF.*ones(numel(projonpc1), 1);    % grouping variable
    
end

% * Align traces
dtres = mean(diff(timevec));
pc1projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc1proj, 'UniformOutput', false);
pc1projres = cat(1, pc1projres{:});
pc2projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc2proj, 'UniformOutput', false);
pc2projres = cat(1, pc2projres{:});

% * PCA proj. variance inter & intra
% - All var
mvarall1 = var(cat(1, pc1proj{:}));
evarall1 = bootci(1000, @var, cat(1, pc1proj{:}));
mvarall2 = var(cat(1, pc2proj{:}));
evarall2 = bootci(1000, @var, cat(1, pc2proj{:}));

% - Intra
varof1 = var(pc1projres, [], 2);
varof2 = var(pc2projres, [], 2);
mvof1 = mean(varof1);
svof1 = std(varof1)./sqrt(size(varof1, 1));
evof1 = [mvof1 - svof1; mvof1 + svof1];
mvof2 = mean(varof2);
svof2 = std(varof2)./sqrt(size(varof2, 1));
evof2 = [mvof2 - svof2; mvof2 + svof2];

% - ACF
ncorr = round(ncorrtime./dtres);        % in steps
acfperfish1 = NaN(ncorr + 1, nfish);    % pc1
acfperfish2 = NaN(ncorr + 1, nfish);    % pc2
lagperfish1 = NaN(ncorr + 1, nfish);
lagperfish2 = NaN(ncorr + 1, nfish);

for idF = 1:nfish
    
    pcavec1 = pc1projres(idF, :);
    pcavec1 = pcavec1 - mean(pcavec1, 'omitnan'); % center
    pcavec2 = pc2projres(idF, :);
    pcavec2 = pcavec2 - mean(pcavec2, 'omitnan');
    
    [xco, lag] = xcorr(pcavec1', ncorr, 'normalized');
    selected_inds = find(lag == 0):find(lag == 0) + ncorr;
    acfperfish1(:, idF) = xco(selected_inds);
    lagperfish1(:, idF) = lag(selected_inds).*dtres;

    [xco, lag] = xcorr(pcavec2', ncorr, 'normalized');
    selected_inds = find(lag == 0):find(lag == 0) + ncorr;
    acfperfish2(:, idF) = xco(selected_inds);
    lagperfish2(:, idF) = lag(selected_inds).*dtres;
end

% Mean
mlpf1 = mean(lagperfish1, 2);
mapf1 = mean(acfperfish1, 2);
eapf1 = std(acfperfish1, [], 2)./sqrt(size(acfperfish1, 2));
mlpf2 = mean(lagperfish2, 2);
mapf2 = mean(acfperfish2, 2);
eapf2 = std(acfperfish2, [], 2)./sqrt(size(acfperfish2, 2));

% --- Display

% - Cloud projections
f1 = figure('Name', '2DPCSpaceProj');
tbl = table(cat(1, pc1proj{:}), cat(1, pc2proj{:}), cat(1, pc1pc2grp{:}), 'VariableNames', {'PC1 proj.', 'PC2 proj.', 'Fish'});
scatterhistogram(f1, tbl, 'PC1 proj.', 'PC2 proj.', 'GroupVariable', 'Fish', ...
    'HistogramDisplayStyle', 'smooth', 'LineStyle', '-', ...
    'Color', colors, 'MarkerAlpha', 0.25);
legend;

% - Projection over time in 2D space
for idF = fishtoshow
    
    % 2D
    figure('Name', ['2DProjFish' num2str(idF)]); hold on
    scatter(pc2proj{idF}, pc1proj{idF}, [], ttraj{idF}, 'filled');
    
	c = colorbar;
    title(c, 'Time (s)');
    xlabel('Projection on PC1');
    ylabel('Projection on PC2');
    title(['Fish ' num2str(idF)]);

end

% - Per fish PC1 coefficients
figure('Name', 'PerFishPC1Coeff'); hold on
axmpc1 = gca; axmpc1.ColorOrder = colors;
coeffpc1full = coeffpc1;
dname{end} = 'All 26°C';
axmpc1.ColorOrder(nfish + 1, :) = [0, 0, 0];
bar(axmpc1, coeffpc1full, 'EdgeColor', 'none');
plot(axmpc1, 1:nfeat, all26_coefPC1, 'k');
axmpc1.XTick = 1:nfeat;
axmpc1.XTickLabel = {'\delta\theta_t', '\delta{t}', 'd', 'p_{turn}', 'k_{flip}'};
ylabel(axmpc1, 'PC1 coefficient');
axmpc1.YLim = [-1, 1];
legend(axmpc1, dname, 'Location', 'southeast');

% - Per fish PC2 coefficients
figure('Name', 'PerFishPC2Coeff'); hold on
axmpc2 = gca; axmpc2.ColorOrder = colors;
coeffpc1full = coeffpc2;
dname{end} = 'All 26°C';
axmpc2.ColorOrder(nfish + 1, :) = [0, 0, 0];
bar(axmpc2, coeffpc1full, 'EdgeColor', 'none');
plot(axmpc2, 1:nfeat, all26_coefPC2, 'k');
axmpc2.XTick = 1:nfeat;
axmpc2.XTickLabel = {'\delta\theta_t', '\delta{t}', 'd', 'p_{turn}', 'k_{flip}'};
ylabel(axmpc2, 'PC2 coefficient');
axmpc2.YLim = [-1, 1];
legend(axmpc2, dname, 'Location', 'southeast');

% - Explained variance
figure('Name', 'ExplVar'); hold on
ax = gca;
ax.ColorOrder = colors;
bar(explained, 'EdgeColor', 'none');
ax.XTick = 1:5;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
leg = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
legend(leg, 'AutoUpdate', 'off');
% axis square
ax.XGrid = 'off';
ylabel(ax, 'Explained variance (%)');

% - ACF on PC1 per fish
figure('Name', 'ACFPC1PerFish'); hold on
ax = gca; ax.ColorOrder = colors;

px = plot(lagperfish1, acfperfish1);
px = arrayfun(@(s) setfield(s, 'Color', [s.Color, 0.5]), px);

lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf1, mapf1, eapf1, 'line', lopt, 'patch', sopt);

legs = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
legs{end + 1} = 'Mean';
legend([px; pm], legs);
xlabel('Delays (s)');
ylabel('ACF(Proj. on PC1)');

% - ACF on PC2 per fish
figure('Name', 'ACFPC2PerFish'); hold on
ax = gca; ax.ColorOrder = colors;

px = plot(lagperfish2, acfperfish2);
px = arrayfun(@(s) setfield(s, 'Color', [s.Color, 0.5]), px);

lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf2, mapf2, eapf2, 'line', lopt, 'patch', sopt);

legs = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
legs{end + 1} = 'Mean';
legend([px; pm], legs);
xlabel('Delays (s)');
ylabel('ACF(Proj. on PC2)');

figure('Name', 'VarInterIntra'); hold on; ax = gca;
X = [mvof1, mvarall1; mvof2, mvarall2];
E = cat(3, [evof1'; evof2'], [evarall1'; evarall2']);
cols = lines(7);
ax.ColorOrder = [cols(4, :); cols(5, :)];
b = bar(X, 'EdgeColor', 'none');

[ngroups, nbars] = size(X);
for idb = 1:nbars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, E(:, :, idb), 'Color', [0.01, 0.01, 0.01]);
end

ax.XTick = 1:2;
ax.XTickLabel = {'PC1', 'PC2'};
ylabel(ax, 'Variance');
legend(ax, 'Intra', 'Pooled');