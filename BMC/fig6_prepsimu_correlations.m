% Computes correlations matrix of dtheta, ibi, displacement, pturn and
% pflip to generate copulas in the simulations.
% Requires "allsequences.mat" file.

% close all
clear
clc

% --- Parameters
theta_threshold = 10;   % deg

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix_p'];

fn_out = [pwd filesep 'Data' filesep 'Matfiles' filesep 'corrmat5param_p.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% --- Get data
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;

% --- Processing
Rtraj = NaN(5, 5, 5);
for idT = 1:numel(T)

    % Get Pearson R
    Rtraj(:, :, idT) = corrcoef(featmat{idT});
    
end

% Average over temperature
mRtraj = mean(Rtraj, 3);

% --- Save
if ~isempty(fn_out)
    META = 'mRtraj : temperature average, Rtraj nfeat x nfeat x nTemp';
    DESC = '1. turnamp 2. ibi 3. d 4. pturn 5. pflip';
    save(fn_out, 'mRtraj', 'Rtraj', 'META');
end