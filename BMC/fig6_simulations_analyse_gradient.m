% Further analysis of gradient simulation results with comparison of pool
% lengths.
% Requires pre-processed simulations data (see
% "simulations_process_gradient.m" script).
% (Figure 6D-G)

% close all
clear
clc

% --- Parameters
% * Files
data_path = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, '*_simu*_gradient_p_45_*_18_33_reflective_1000_linearbin.mat'];
files = dir(data_path);

% * Bins
nbinpos = 50;
stationp = 5;      % percent of time from the end to take for stationnary pdf

% * Figures
figstat = figure('Name', 'SimuG_Stationnaire'); axstat = gca; hold on
figmean = figure('Name', 'SimuG_Mean'); axmean = gca; hold on
figskew = figure('Name', 'SimuG_Skewness'); axskew = gca; hold on
figscal = figure('Name', 'SimuG_RescaleL'); axscal = gca; hold on
symbols = {'^', 'o', 's', 'p'};
colors = [0, 0, 0 ; 0.25, 0.25, 0.25 ; 0.5, 0.5, 0.5; 0.75, 0.75, 0.75];

q = cell(numel(files), 1);
r = cell(numel(files), 1);
for idE = 1:numel(files)
    
    % --- Load data
    fname = [files(idE).folder, filesep, files(idE).name];
    D = load(fname);
    
    % --- Definitions
    Tl = D.Tl;
    Tr = D.Tr;
    xbins = linspace(Tl, Tr, nbinpos);
    
    % --- Get data
    bintime = D.bintime;
    grouped = D.grouped;
    
    % --- Stationnary pdf
    ungrouped = cat(1, grouped{:});
    firstpoint = round((1 - stationp/100)*numel(ungrouped));
    datastation = ungrouped(firstpoint:end);
    
    [pdfstat, cbinstat] = computePDF(xbins, datastation, 'mode', 'edges', 'method', 'hist');
    
    % --- First moment
    m1 = NaN(size(grouped, 1), 1);
    m3 = NaN(size(grouped, 1), 1);
    timepdf = NaN(numel(bintime), numel(xbins) - 1);
    for idt = 1:size(grouped, 1)
        m1(idt) = mean(grouped{idt});
        m3(idt) = skewness(grouped{idt});
        [timepdf(idt, :), ctbin] = computePDF(xbins, grouped{idt}, 'mode', 'edges', 'method', 'hist');
    end
    
    m1s = smoothdata(m1, 'sgolay');
    m3s = smoothdata(m3, 'sgolay');
    
    % --- Display
    figure;
    imagesc(timepdf);
    ax = gca; hold(ax, 'on');
    lbl = ax.XTickLabel;
    newxticks = [18, 22, 26, 30, 33];
    xtick = (newxticks - newxticks(1)).*diff(ax.XLim)./abs(Tl - Tr);   % convert x axis to °C
    ax.XTick = xtick;
    ax.XTickLabel = split(num2str(newxticks));
    newyticks = linspace(0, D.ntimes, 6);
    ytick = newyticks.*diff(ax.YLim)./D.ntimes;
    ax.YTick = ytick;
    ax.YTickLabel = split(num2str(newyticks));
    xlabel('Temperature [°C]');
    ylabel('Time [s]');
    cl = colorbar;
    title(cl, 'pdf');
    title(['Simu gradient, L=' num2str(abs(D.xl - D.xr)) 'mm']);
    axis square
    colormap(inferno);
    caxis([0.02 0.14]);
    cl.Ticks = sort([cl.Ticks, 1/(abs(Tr - Tl))]);
    grid(ax, 'off');
    plot((m1 - min(Tl, Tr)).*49/15, 1:numel(m1), 'k');
    
    p = plot(axstat, cbinstat(1:end), pdfstat(1:end));
    p.Color = colors(idE, :);
    p.Marker = symbols{idE};
    p.MarkerSize = 6;
    p.DisplayName = [num2str(abs(Tr - Tl)./abs(D.xr - D.xl)) '°C/mm'];
    xlabel(axstat, 'Temperature [°C]');
    ylabel(axstat, ['presence pdf (last ' num2str(stationp) '% of total time)']);
    
    % Resample for display
    pts = fix(logspace(log10(1), log10(numel(bintime)), 100));
    m1plot = m1(pts);
    bintimeplot = bintime(pts);
    m3plot = m3(pts);
    
    q{idE} = scatter(axmean, bintimeplot, m1plot);
    q{idE}.MarkerEdgeAlpha = 1;
    q{idE}.Marker = symbols{idE};
    q{idE}.CData = colors(idE, :);
    q{idE}.SizeData = 60;
    q{idE}.DisplayName = [num2str(abs(Tr - Tl)./abs(D.xr - D.xl)) '°C/mm'];
    qs = plot(axmean, bintime, m1s);
    qs.Color = colors(idE, :);
    qs.LineWidth = 2;
    
    r{idE} = scatter(axskew, bintimeplot, m3plot);
    r{idE}.MarkerEdgeAlpha = 1;
    r{idE}.Marker = symbols{idE};
    r{idE}.CData = colors(idE, :);
    r{idE}.DisplayName = [num2str(abs(Tr - Tl)./abs(D.xr - D.xl)) '°C/mm'];
    rs = plot(axskew, bintime, m3s);
    rs.Color = colors(idE, :);
    
    s = plot(axscal, bintimeplot./(abs(D.xr - D.xl).^2), m1plot);
%     s.Marker = symbols{idE};
    s.Color = colors(idE, :);
%     s.LineStyle = 'none';
    s.DisplayName = [num2str(abs(Tr - Tl)./abs(D.xr - D.xl)) '°C/mm'];
end

% --- Cosmetic
plot(axstat, [min(Tl, Tr), max(Tl, Tr)], [1/abs(Tr - Tl), 1/abs(Tr - Tl)], ...
    'k--', 'LineWidth', 1.25, 'DisplayName', 'Uniform');
axstat.XLim = [min(Tl, Tr), max(Tl, Tr)];
axstat.XTick = [18, 22, 26, 30, 33];
xlabel(axstat, 'Temperature [°C]');
ylabel(axstat, ['presence pdf (last ' num2str(stationp) '% of total time)']);
xlabel(axmean, 'Time bins [s]');
ylabel(axmean, 'Distribution mean [°C]');
xlabel(axskew, 'Time bins [s]');
ylabel(axskew, 'Distribution skewness');
xlabel(axscal, 'Time bins/L^2');
ylabel(axscal, 'Distribution mean [°C]');
legend(axstat);
legend(axmean, [q{:}]);
legend(axskew, [r{:}], 'Location', 'northwest');
legend(axscal);
axis(axstat, 'square');
axis(axmean, 'square');
axis(axskew, 'square');
axis(axscal, 'square');
axmean.XScale = 'log';
axskew.XScale = 'log';
axscal.XScale = 'log';
xlim(axmean, [10.0005, 216000]);
ylim(axmean, [23.6, 26]);
ylim(axscal, axmean.YLim);