% Plots temperature-averaged probability distribution functions (pdf) of
% kinematics features : interbout interval, displacement, turn angles,
% along with boxplots of distributions means and turning probability.
% (Figure 2)
% Uses the file "allsequences_perbatch.mat".

clear
clc

% --- Definitions
filename = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences_perbatch.mat'];

T = [18, 22, 26, 30, 33];   % tested temperatures (°C)

theta_threshold = 10;       % threshold for forward/turn (°)

% pdf settings
method = 'kde';             % Method for PDF
mode = 'centers';           % Mode for bins
bws = [0.1, 0.1, 0.5];      % Bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'}; % options for KDE
nbins = 250;

% bins
cbin_ibi = linspace(0, 5, nbins);
cbin_dsp = linspace(0, 3, nbins);
cbin_reo = linspace(-180, 180, nbins);

statest = 'kruskalwallis';  % statistical significance test

% figures
colors = rainbow(numel(T));
fignames = {'pdf_ibi', 'pdf_dsp', 'pdf_reo'};
figibi = figure; axibi = axes(figibi); hold(axibi, 'on');
figdsp = figure; axdsp = axes(figdsp); hold(axdsp, 'on');
figreo = figure; axreo = axes(figreo); hold(axreo, 'on');
pltibi = cell(numel(T), 1);
pltdsp = cell(numel(T), 1);
pltreo = cell(numel(T), 1);

lineopt = struct;
lineopt.MarkerSize = 4;
lineopt.LineWidth = 2;
shadopt = struct;
shadopt.FaceAlpha = 0.275;

% --- Load data
data = load(filename);

% --- Initialization
meansibi = cell(numel(T), 1);
meansdsp = cell(numel(T), 1);
meanstrn = cell(numel(T), 1);
pturns = cell(numel(T), 1);

% --- Processing

% Loop over temperatures

fprintf('Computing distributions'); tic;

for idT = 1:numel(T)
    
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    reo = data.dtheta{idT};
    
    nexp = numel(ibi);
    
    % Init.
    pdf_ibi = NaN(nbins, nexp);
    pdf_dsp = NaN(nbins, nexp);
    pdf_reo = NaN(nbins, nexp);
    meansibi{idT} = NaN(nexp, 1);
    meansdsp{idT} = NaN(nexp, 1);
    meanstrn{idT} = NaN(nexp, 1);
    pturns{idT} = NaN(nexp, 1);
    
    % Loop over experiments (batches of 10 fish)
    for ide = 1:nexp
        
        % Get data
        eibi = ibi{ide};
        eibi = eibi(:);
        eibi(isnan(eibi)) = [];
        edsp = dsp{ide};
        edsp = edsp(:);
        edsp(isnan(edsp)) = [];
        ereo = reo{ide};
        ereo = ereo(:);
        ereo(isnan(ereo)) = [];
        
        % Get turns/forward
        isturn = abs(ereo) > theta_threshold;
        
        % Get means for boxplots
        meansibi{idT}(ide) = mean(eibi);
        meansdsp{idT}(ide) = mean(edsp);
        meanstrn{idT}(ide) = mean(abs(ereo(isturn)));
        
        % Get turn fraction
        pturns{idT}(ide) = sum(abs(ereo) > theta_threshold)/numel(ereo);
        
        % Compute PDF
        pdf_ibi(:, ide) = computePDF(cbin_ibi, eibi, 'method', method, 'mode', mode, 'param', kdeopt(1));
        pdf_dsp(:, ide) = computePDF(cbin_dsp, edsp, 'method', method, 'mode', mode, 'param', kdeopt(2));
        pdf_reo(:, ide) = computePDF(cbin_reo, ereo, 'method', method, 'mode', mode, 'param', {'Bandwidth', bws(3)});
        
    end
    
    % Get mean and sem for pdf
    mpdf_ibi = mean(pdf_ibi, 2);
    epdf_ibi = std(pdf_ibi, [], 2)./sqrt(nexp);
    mpdf_dsp = mean(pdf_dsp, 2);
    epdf_dsp = std(pdf_dsp, [], 2)./sqrt(nexp);
    mpdf_reo = mean(pdf_reo, 2);
    epdf_reo = std(pdf_reo, [], 2)./sqrt(nexp);
    
    % Fix negative values for turn angles to show in log scale
    epdf_reo(mpdf_reo - epdf_reo <= 0) = NaN;
    epdf_reo = fillmissing(epdf_reo, 'constant', min(epdf_reo));
    
    % - Display
    % line options
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    
    pltibi{idT} = errorshaded(cbin_ibi, mpdf_ibi, epdf_ibi, 'line', lineopt, 'patch', shadopt, 'ax', axibi);
    pltdsp{idT} = errorshaded(cbin_dsp, mpdf_dsp, epdf_dsp, 'line', lineopt, 'patch', shadopt, 'ax', axdsp);
    pltreo{idT} = errorshaded(cbin_reo, mpdf_reo, epdf_reo, 'line', lineopt, 'patch', shadopt, 'ax', axreo);
    
    fprintf('.');
end

% Figures cosmetics
title(axibi, 'Interbout intervals');
xlabel(axibi, '\delta{t} (s)');
ylabel(axibi, 'pdf');
legend(axibi, [pltibi{:}]);

title(axdsp, 'Displacements');
xlabel(axdsp, 'd (mm)');
ylabel(axdsp, 'pdf');
legend(axdsp, [pltdsp{:}]);

title(axreo, 'Turn angles');
xlabel(axreo, '\delta\theta (deg)');
ylabel(axreo, 'pdf');
legend(axreo, [pltreo{:}]);
axreo.YScale = 'log';
ylim(axreo, [1e-4, 1e-1]);

fprintf('\tDone (%2.2fs).\n', toc);

% --- Box plots of means & proba
maxsize = max(cellfun(@numel, meansibi));   % max number of exp.
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);

% display options
pointopt = struct;
pointopt.Color = colors;
pointopt.jitter = 0.25;
boxopt = struct;
boxopt.BoxColors = colors;

% * Interbout interval
% Group data
meansibi = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), meansibi, 'UniformOutput', false);
g = cell(size(meansibi));
for idx = 1:size(meansibi, 1)
    g{idx} = idx.*ones(numel(meansibi{idx}), 1);
end
g = cat(1, g{:});   % groups data by temperature

boxopt.Labels = labels;

X = cat(1, meansibi{:});

% - Display
figure; hold on
beautifulbox(X, g, pointopt, boxopt);
title('Mean interbout interval');
ylabel('<\delta{t}>_{batch} (s)');
axis([-Inf Inf 0.5 2.5]);

% - Add significance
pairs = {[2, 3]};
addStars(meansibi, 'pairs', pairs, 'test', statest);
axis square

% * Displacements
meansdsp = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), meansdsp, 'UniformOutput', false);
X = cat(1, meansdsp{:});

% - Display
figure; hold on
beautifulbox(X, g, pointopt, boxopt);
title('Mean displacement');
ylabel('<d>_{batch} (mm)');
axis([-Inf Inf 0.5 2]);

% - Add significance
pairs = {[2, 3]};
addStars(meansdsp, 'pairs', pairs, 'test', statest);
axis square

% * Reorientation angles of turn events
meanstrn = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), meanstrn, 'UniformOutput', false);
X = cat(1, meanstrn{:});

% - Display
figure; hold on
beautifulbox(X, g, pointopt, boxopt);
title('Mean turn angle');
ylabel('<|\delta\theta_{turn}|>_{batch} (deg)');
axis([-Inf Inf 20 60]);

% - Add significance
pairs = {[1, 2], [2, 3]};
addStars(meanstrn, 'pairs', pairs, 'test', statest);
axis square

% * Turn fractions (pturn)
pturns = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), pturns, 'UniformOutput', false);
X = cat(1, pturns{:});

% - Display
figure; hold on
beautifulbox(X, g, pointopt, boxopt);
title('Turn fraction');
ylabel('p_{turn}');
axis([-Inf Inf 0 1]);

% - Add significance
pairs = {[1, 2], [2, 3], [3, 4], [4, 5]};
addStars(pturns, 'pairs', pairs, 'test', statest);
axis square