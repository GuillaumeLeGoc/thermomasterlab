% Computes distributions of per-bout parameters rescaled by the
% trajectory-averaged value that corresponds to epsilon in the generic
% description of navigational parameters selection, for IBI, displacement,
% and turn angle (turn and forwards).
% It is used within the simulations.
% Requires "allsequences.mat" file.

% close all
clear
clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_out = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'universaldistributions.mat'];

% * % * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
theta_threshold = 10;   % threshold for turns/forwards

% * Bins
method = 'kde';             % Method for PDF ('kde' or 'hist')
mode = 'edges';             % Mode for bins
bws = [0.1, 0.1, 0.1, 0.1]; % Bandwidths for KDE (ibi, dsp, reo)
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 200;
lim_ibi = [0, 4];
lim_dsp = [0, 4];
lim_reo = [0, 4];
lim_fwd = [0, 4];

% --- Preparations
% * Bin vectors
binsi = linspace(lim_ibi(1), lim_ibi(2), nbins + 1)';
binsd = linspace(lim_dsp(1), lim_dsp(2), nbins + 1)';
binsr = linspace(lim_reo(1), lim_reo(2), nbins + 1)';
binsf = linspace(lim_fwd(1), lim_fwd(2), nbins + 1)';

% * Figures
colors = rainbow(numel(T));
pwid = 2.5;
fitcol = [0.1, 0.1, 0.1];
fitwid = 2;

% * Load data
data = load(fn_dataset);

% * Arrays

cdf_ibi = NaN(nbins, numel(T));
cdf_dsp = NaN(nbins, numel(T));
cdf_reo = NaN(nbins, numel(T));
cdf_fwd = NaN(nbins, numel(T));

% --- Processing
for idT = 1:numel(T)
        
    fprintf(['Computing scaled distributions for T = ', num2str(T(idT)), '°C ']); tic
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
    fwd(fwd == 0) = eps;
    
    % Rescale by trajectory means
    allibi = ibi./mean(ibi, 1, 'omitnan');
    alldsp = dsp./mean(dsp, 1, 'omitnan');
    allreo = reo./mean(reo, 1, 'omitnan');
    allfwd = fwd./mean(fwd, 1, 'omitnan');
    
    % Pool all bouts
    allibi = allibi(:);
    allibi(isnan(allibi)) = [];
    alldsp = alldsp(:);
    alldsp(isnan(alldsp)) = [];
    allreo = allreo(:);
    allreo(isnan(allreo)) = [];
    allfwd = allfwd(:);
    allfwd(isnan(allfwd)) = [];
    
    % Get distributions
    [cdf_ibi(:, idT), cbinsi] = computeCDF(binsi, allibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
    [cdf_dsp(:, idT), cbinsd] = computeCDF(binsd, alldsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
    [cdf_reo(:, idT), cbinsr] = computeCDF(binsr, allreo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
    [cdf_fwd(:, idT), cbinsf] = computeCDF(binsf, allfwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% --- Get mean rescaled pdf and cdf
univcdf_ibi = mean(cdf_ibi, 2);
univcdf_dsp = mean(cdf_dsp, 2);
univcdf_reo = mean(cdf_reo, 2);
univcdf_fwd = mean(cdf_fwd, 2);

% Rename bins
univbin_ibi = cbinsi;
univbin_dsp = cbinsd;
univbin_reo = cbinsr;
univbin_fwd = cbinsf;

% --- Display

% * Plot cdf
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
labels{end + 1} = 'Mean';

figure('Name', 'Epscdfibi'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsi, cdf_ibi);
plot(cbinsi, univcdf_ibi, '--k');
legend(labels);
xlabel('\delta{t}/<\delta{t}>_{traj.}');
ylabel('cdf');
axis(ax, 'square');

figure('Name', 'Epscdfdsp'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsd, cdf_dsp);
plot(cbinsd, univcdf_dsp, '--k');
legend(labels);
xlabel('d/<d>_{traj.}');
ylabel('cdf');
axis(ax, 'square');

figure('Name', 'Epscdfreo'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsr, cdf_reo);
plot(cbinsr, univcdf_reo, '--k');
legend(labels);
xlabel('\delta\theta_{turn}/<\delta\theta_{turn}>_{traj}');
ylabel('cdf');
axis(ax, 'square');

figure('Name', 'Epscdffwd'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsf, cdf_fwd);
plot(cbinsf, univcdf_fwd, '--k');
legend(labels);
xlabel('\delta\theta_{forward}/<\delta\theta_{forward}>_{traj.}');
ylabel('cdf');
axis(ax, 'square');

% --- Save
if ~isempty(fn_out)
    fprintf('Saving...');
    save(fn_out, 'univbin_ibi', 'univbin_dsp', 'univbin_reo', 'univbin_fwd', ...
        'univcdf_ibi', 'univcdf_dsp', 'univcdf_reo', 'univcdf_fwd');
    fprintf('\tDone.\n');
end