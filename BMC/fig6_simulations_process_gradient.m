% Prepare output of gradient simulations : shape them to use in the
% "simulations_analyze_gradient.m". Each settings of the simu. should be
% processed here, typically different pool length.
% Requires output of "simulations_generate_gradient", ran with different
% pool lengths.

% close all
clear
clc

% --- Parameters
% * Files
% simu_path = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu3600_gradient_p_45_100_18_33_reflective_1000.mat'];
% simu_path = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu21600_gradient_p_45_300_18_33_reflective_1000.mat'];
% simu_path = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu43200_gradient_p_45_500_18_33_reflective_1000.mat'];
simu_path = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu216000_gradient_p_45_1000_18_33_reflective_1000.mat'];

[fp, fn] = fileparts(simu_path);

% * Bins
binsizetime = 10;       % s
binmethod = 'linear';   % linear or log

out_path = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, fn, '_' binmethod, 'bin.mat'];

% --- Load data
fprintf('Loading data...'); tic
simu_data = load(simu_path, 'Tl', 'Tr', 'len', 'allx', 'allt', 'ntimes', 'nfish');
fprintf('\tDone (%2.2fs).\n', toc);

% --- Definitions
Tl = simu_data.Tl;
Tr = simu_data.Tr;
xl = simu_data.len(1);
xr = simu_data.len(2);

a = (Tl - Tr)./(xl - xr);
b = (Tr*xl - Tl*xr)./(xl - xr);
hT = @(x) a.*x + b;

% --- Processing
allx = simu_data.allx;
allt = simu_data.allt;
nfish = numel(allx);

nbintime = ceil(simu_data.ntimes/binsizetime);
switch binmethod
    case 'log'
        bintime = logspace(0, log10(simu_data.ntimes + 1), nbintime + 1);
    case 'linear'
        bintime = linspace(0, simu_data.ntimes + 1, nbintime + 1);
end

fprintf('Time binning...'); tic
grouped = cell(nbintime, 1);
fun = @(x) {cat(1, x(:))};
for fish = 1:nfish
    
    x = allx{fish};
    t = allt{fish};
    t = fillmissing(t, 'next');
    t = fillmissing(t, 'previous');
    T = hT(x);
    
    binnedt = discretize(t, bintime);
    
    B = accumarray(binnedt, T, [nbintime, 1], fun);
    grouped = cellfun(@(x, y) cat(1, x, y), grouped, B, 'UniformOutput', false);
end
fprintf('\tDone (%2.2fs).\n', toc);

% --- Save
if ~isempty(out_path)
    ntimes = simu_data.ntimes;
    bintime = edges2centers(bintime);
    save(out_path, 'bintime', 'grouped', 'Tl', 'Tr', 'ntimes', 'nfish', ...
        'xl', 'xr');
end