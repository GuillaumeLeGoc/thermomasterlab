% Computes distributions of trajectory parameters means rescaled by the
% temperature-averaged value that corresponds to Y in the generic
% description of navigational parameters selection, for IBI, displacement,
% and turn angle (turn and forwards).
% It is used within the simulations.
% Requires "allsequences.mat" file.

clear
clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fnout = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'meansdistY.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
theta_threshold = 10;   % threshold for turns/forwards

% * Bins
method = 'kde';                 % Method for PDF ('kde' or 'hist')
mode = 'edges';                 % Mode for bins
bws = [0.2, 0.2, 0.2, 0.2];    % bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'}; % options for KDE
nbins = 100;

% --- Preparations

% Bins for Y
ebinsyibi = linspace(0, 8, nbins + 1)';
ebinsydsp = linspace(0, 4, nbins + 1)';
ebinsyreo = linspace(0, 3, nbins + 1)';
ebinsyfwd = linspace(0, 4, nbins + 1)';

% * Load data
data = load(fn_dataset);

% * Figures
colors = rainbow(numel(T));

% * Arrays
meanspdf_ibi = NaN(nbins, numel(T));
meanspdf_dsp = NaN(nbins, numel(T));
meanspdf_reo = NaN(nbins, numel(T));
meanspdf_fwd = NaN(nbins, numel(T));

meanscdf_ibi = NaN(nbins, numel(T));
meanscdf_dsp = NaN(nbins, numel(T));
meanscdf_reo = NaN(nbins, numel(T));
meanscdf_fwd = NaN(nbins, numel(T));

pdfY_ibi = NaN(nbins, numel(T));
pdfY_dsp = NaN(nbins, numel(T));
pdfY_reo = NaN(nbins, numel(T));
pdfY_fwd = NaN(nbins, numel(T));

errYibi = cell(numel(T), 1);
errYdsp = cell(numel(T), 1);
errYreo = cell(numel(T), 1);
errYfwd = cell(numel(T), 1);

cdfY_ibi = NaN(nbins, numel(T));
cdfY_dsp = NaN(nbins, numel(T));
cdfY_reo = NaN(nbins, numel(T));
cdfY_fwd = NaN(nbins, numel(T));

% --- Processing
for idT = 1:numel(T)
        
    fprintf(['Computing scaled traj. means distributions for T = ', num2str(T(idT)), '°C ']); tic
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
        
    % Get trajectory means
    trajmean_ibi = mean(ibi, 1, 'omitnan');
    trajmean_dsp = mean(dsp, 1, 'omitnan');
    trajmean_reo = mean(reo, 1, 'omitnan');
    trajmean_fwd = mean(fwd, 1, 'omitnan');
    
    % Get temperature means
    tempmean_ibi = mean(ibi, 'all', 'omitnan');
    tempmean_dsp = mean(dsp, 'all', 'omitnan');
    tempmean_reo = mean(reo, 'all', 'omitnan');
    tempmean_fwd = mean(fwd, 'all', 'omitnan');
    
    % Get rescaled trajectory means (Y)
    Y_ibi = trajmean_ibi./tempmean_ibi;
    Y_dsp = trajmean_dsp./tempmean_dsp;
    Y_reo = trajmean_reo./tempmean_reo;
    Y_fwd = trajmean_fwd./tempmean_fwd;
    
    % Get cdf of rescaled trajectory means (Y)
    [cdfY_ibi(:, idT), binsyibi] = computeCDF(ebinsyibi, Y_ibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
    [cdfY_dsp(:, idT), binsydsp] = computeCDF(ebinsydsp, Y_dsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
    [cdfY_reo(:, idT), binsyreo] = computeCDF(ebinsyreo, Y_reo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
    [cdfY_fwd(:, idT), binsyfwd] = computeCDF(ebinsyfwd, Y_fwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% --- Get mean rescaled CDF
mcdfYibi = mean(cdfY_ibi, 2);
mcdfYdsp = mean(cdfY_dsp, 2);
mcdfYreo = mean(cdfY_reo, 2);
mcdfYfwd = mean(cdfY_fwd, 2);

% --- Display
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);

% * Plot CDF of rescaled trajectory means (Y)
labels{end + 1} = 'Mean';
figure('Name', 'Yibicdf'); hold on
axyci = gca;
axyci.ColorOrder = colors;
plot(binsyibi, cdfY_ibi);
plot(axyci, binsyibi, mcdfYibi, '--k');
legend(axyci, labels);
xlabel(axyci, '<\delta{t}>_{traj.}/<\delta{t}>_T');
ylabel(axyci, 'cdf');

figure('Name', 'Ydspcdf'); hold on
axycd = gca;
axycd.ColorOrder = colors;
plot(binsydsp, cdfY_dsp);
plot(axycd, binsydsp, mcdfYdsp, '--k');
legend(axycd, labels);
xlabel(axycd, '<d>_{traj.}/<d>_T');
ylabel(axycd, 'cdf');

figure('Name', 'Yreocdf'); hold on
axycr = gca;
axycr.ColorOrder = colors;
plot(binsyreo, cdfY_reo);
plot(axycr, binsyreo, mcdfYreo, '--k');
legend(axycr, labels);
xlabel(axycr, '<\delta\theta_{t}>_{traj.}/<\delta\theta_{t}>_T');
ylabel(axycr, 'cdf');

figure('Name', 'Yfwdcdf'); hold on
axycf = gca;
axycf.ColorOrder = colors;
plot(binsyfwd, cdfY_fwd);
plot(axycf, binsyfwd, mcdfYfwd, '--k');
legend(axycf, labels);
xlabel(axycf, '<\delta\theta_{f}>_{traj.}/<\delta\theta_{f}>_T');
ylabel(axycf, 'cdf');

% --- Save
fprintf('Saving...');
save(fnout, 'binsyibi', 'mcdfYibi', 'binsydsp', 'mcdfYdsp', ...
    'binsyreo', 'mcdfYreo', 'binsyfwd', 'mcdfYfwd');
fprintf('\tDone.\n');