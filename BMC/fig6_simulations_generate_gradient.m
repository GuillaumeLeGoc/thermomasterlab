% Data-based simulations in a gradient to probe the effect of parameters
% modulation due to temperature.
% Simulation based on generic description X=XbarT*Y*epsilon, with
% correlations of trajectory means, using proba for turns and flips
% Generates files used afterwards in "simulations_process_gradient.m"
% script.
% Requires files created with prepsimu_XXX.mat scripts.
% /!\ Output files are quite large (8GB). Takes a lot of time per 
% temperature (1000 real., 3600s simulated time).
% close all

clear
clc

% --- Settings

% * Temperatures (choose from 18, 22, 26, 30, 33)
% Available temperature
Tavail = [18, 22, 26, 30, 33];
% Gradient
Tl = 18;
Tr = 33;

% * Pool
len = [0, 100];              % Pool length (mm)
wid = [0, 45];               % Pool width (mm)
xbox = len([1 1 2 2 1]);
ybox = wid([1 2 2 1 1]);

% * Experiment
nsavepoint = Inf;      % number of bout to save (downsampling if needed)
nfish = 1000;           % Number of fish (one at a time)
ntimes = 3600;          % Experiement duration (s)
framerate = 10;         % Trajectories sampling rate (Hz)
blim = 'reflective';    % Boundary limits, 'none', 'reflective' or 'startnew'
minnbouts = 15;         % minimum number of bouts to validate trajectory in startnew BL
mintrajtime = 25;       % minimum traj. duration in startnew BL
nsteps = ceil(ntimes*framerate);

% * Output file
outbase = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu' num2str(ntimes) '_gradient_p_' num2str(max(wid)) '_' num2str(max(len)) '_' num2str(Tl) '_' num2str(Tr) '_'];

% * Behavioral constant
theta_threshold = 10;   % Turn definition (°)
dsp_scale = 1.6;

% * Figures
liveplot = 'n';

% --- Available data
% * Paths
cwd = [pwd filesep 'Data' filesep 'Matfiles' filesep];
fname_univcdf = [cwd 'universaldistributions.mat'];
fname_meanscdf = [cwd 'meansdistY.mat'];
fname_ptpfcdf = [cwd 'ptpfdistY.mat'];
fname_covmat = [cwd 'corrmat5param_p.mat'];
fname_Tmeans = [cwd 'MeanOverTemperatures.mat'];
fname_ptpfmeans = [cwd 'ptpfmeanT.mat'];

% Temperature averaged parameters
data_Tmeans = load(fname_Tmeans);
Tmean_ibi = data_Tmeans.mibi;
Tmean_dsp = data_Tmeans.mdsp;
Tmean_reo = data_Tmeans.mreo;
Tmean_fwd = data_Tmeans.mfwd;

% kflip & kturn
data_ptpf = load(fname_ptpfmeans);
Tmean_ptr = data_ptpf.mpturn;
Tmean_pfp = data_ptpf.mpflip;

% universal distributions
tmpdata = load(fname_univcdf);
univbin_ibi = tmpdata.univbin_ibi;
univcdf_ibi = tmpdata.univcdf_ibi;
univbin_dsp = tmpdata.univbin_dsp;
univcdf_dsp = tmpdata.univcdf_dsp;
univbin_reo = tmpdata.univbin_reo;
univcdf_reo = tmpdata.univcdf_reo;
univbin_fwd = tmpdata.univbin_fwd;
univcdf_fwd = tmpdata.univcdf_fwd;

% means distributions
tmpdata = load(fname_meanscdf);
meanbin_ibi = tmpdata.binsyibi;
meancdf_ibi = tmpdata.mcdfYibi;
meanbin_dsp = tmpdata.binsydsp;
meancdf_dsp = tmpdata.mcdfYdsp;
meanbin_reo = tmpdata.binsyreo;
meancdf_reo = tmpdata.mcdfYreo;
meanbin_fwd = tmpdata.binsyfwd;
meancdf_fwd = tmpdata.mcdfYfwd;

tmpdata = load(fname_ptpfcdf);
meanbin_ptr = tmpdata.binsyptr;
meancdf_ptr = tmpdata.mcdfYptr;
meanbin_pfp = tmpdata.binsypfp;
meancdf_pfp = tmpdata.mcdfYpfp;

% prepare cdf for reverseCDF function
[univcdf_ibi, mask] = unique(univcdf_ibi);
univbin_ibi = univbin_ibi(mask);
[univcdf_dsp, mask] = unique(univcdf_dsp);
univbin_dsp = univbin_dsp(mask);
[univcdf_reo, mask] = unique(univcdf_reo);
univbin_reo = univbin_reo(mask);
[univcdf_fwd, mask] = unique(univcdf_fwd);
univbin_fwd = univbin_fwd(mask);
[meancdf_ibi, mask] = unique(meancdf_ibi);
meanbin_ibi = meanbin_ibi(mask);
[meancdf_dsp, mask] = unique(meancdf_dsp);
meanbin_dsp = meanbin_dsp(mask);
[meancdf_reo, mask] = unique(meancdf_reo);
meanbin_reo = meanbin_reo(mask);
[meancdf_fwd, mask] = unique(meancdf_fwd);
meanbin_fwd = meanbin_fwd(mask);
[meancdf_ptr, mask] = unique(meancdf_ptr);
meanbin_ptr = meanbin_ptr(mask);
[meancdf_pfp, mask] = unique(meancdf_pfp);
meanbin_pfp = meanbin_pfp(mask);

% covariance matrix
tmpdata = load(fname_covmat);
covmat = tmpdata.mRtraj;

% --- Preparation
alli = cell(nfish, 1);
alld = cell(nfish, 1);
allr = cell(nfish, 1);
allx = cell(nfish, 1);
ally = cell(nfish, 1);
allt = cell(nfish, 1);

xleft = len(1);
xright = len(2);
ybot = wid(1);
ytop = wid(2);

% Gradient conversion
a = (Tl - Tr)./(xleft - xright);
b = (Tr*xleft - Tl*xright)./(xleft - xright);
T = @(x) a.*x + b;

% --- Generation of correlated means
Z = mvnrnd([0, 0, 0, 0, 0], covmat, nfish);
V = normcdf(Z);
meanIBI = reverseCDFu(meanbin_ibi, meancdf_ibi, V(:, 1));
pturns = reverseCDFu(meanbin_ptr, meancdf_ptr, V(:, 2));
% pflips = reverseCDFu(meanbin_pfp, meancdf_pfp, V(:, 3));
meanREO = reverseCDFu(meanbin_reo, meancdf_reo, V(:, 4));
meanDSP = reverseCDFu(meanbin_dsp, meancdf_dsp, V(:, 5));
meanFWD = reverseCDFu(meanbin_fwd, meancdf_fwd, rand(nfish, 1));

% Display covariances
disp(covmat)
% corrcoef([meanIBI, pturns, pflips, meanREO, meanDSP])
corrcoef([meanIBI, pturns, meanREO, meanDSP])

% --- Trajectories simulation
fprintf('Simulating trajectories of %d fish... ', nfish); tic

parfor fish = 1:nfish
    
    % Trajectory means (rescaled, no units)
    MI = meanIBI(fish);
    MR = meanREO(fish);
    MD = meanDSP(fish);
    MF = meanFWD(fish);
    
    % kturn & kflip (rescaled, no units)
    PT = pturns(fish);
%     PF = pflips(fish);

    % Prepare per-bouts distributions
    pb_ibi = reverseCDFu(univbin_ibi, univcdf_ibi, rand(nsteps, 1));
    pb_dsp = reverseCDFu(univbin_dsp, univcdf_dsp, rand(nsteps, 1));
    pb_reo = reverseCDFu(univbin_reo, univcdf_reo, rand(nsteps, 1));
    pb_fwd = reverseCDFu(univbin_fwd, univcdf_fwd, rand(nsteps, 1));
    
    % Init flags
    isokay = false;
    flagredo = false;
    
	% Start traj. generation
    while ~isokay
        
        % - Initialization
        stopflag = false;
        
        % Arrays
        x = NaN(nsteps, 1);     % x position
        y = NaN(nsteps, 1);     % y position
        o = NaN(nsteps, 1);     % orientation
        s = NaN(nsteps, 1);     % left/right state
        boutloc = false(nsteps, 1);
        time = NaN(nsteps, 1);
        ibis = NaN(nsteps, 1);
        dsps = NaN(nsteps, 1);
        tags = NaN(nsteps, 1);
        
        % Initial internal directional state
        r = rand;
        if r < .5
            si = -1; % Right state
        else
            si = 1;  % Left state
        end
        s(1) = si;
        
        % Initial position
        xi = randitvl(xleft, xright);
        yi = randitvl(ybot, ytop);
        x(1) = xi;
        y(1) = yi;
        
        % Initial orientation
        oi = randitvl(0, 360);
        o(1) = oi;
        
        % - Build trajectory
        p = 0;  % avoid warning in parfor
        switch liveplot
            case 'y'
                f = figure; hold on; ax = gca; p = plot(NaN, NaN);
                axis equal; axis([len wid]);
        end
        
        currentstep = 1;
        itcount = 0;
        while any(isnan(x))
            
            itcount = itcount + 1;
            
            % * Get current temperature and real values
            currT = T(x(currentstep));
            rMI = MI.*interp1(Tavail, Tmean_ibi, currT);
            rMD = MD.*interp1(Tavail, Tmean_dsp, currT);
            rMR = MR.*interp1(Tavail, Tmean_reo, currT);
            rMF = MF.*interp1(Tavail, Tmean_fwd, currT);
            rPT = PT.*interp1(Tavail, Tmean_ptr, currT);
%             rPF = PF.*interp1(Tavail, Tmean_pfp, currT);
            rPF = interp1(Tavail, Tmean_pfp, currT);
            
            % * Pick interbout interval
            ibi_second = pb_ibi(itcount).*rMI;
            ibi = round(ibi_second*framerate);
            
            newstep = currentstep + ibi;
            
            x(currentstep:newstep) = x(currentstep);
            y(currentstep:newstep) = y(currentstep);
            o(currentstep:newstep) = o(currentstep);
            s(currentstep:newstep) = s(currentstep);
            
            % * Pick displacement
            dsp = pb_dsp(itcount).*rMD;
            
            % * Pick new state left/right
            rflip = rand;
            if rflip >= rPF
                % Do not flip
                s(newstep + 1) = s(currentstep);
            else
                % Flip
                s(newstep + 1) = -s(currentstep);
            end
            
            % * Pick new state forward/turn
            rturn = rand;
            tag = 0;        % avoid warning in parallel loop
            if rturn <= rPT
                % Turn
                reoang = pb_reo(itcount).*rMR;
                dsp = dsp*dsp_scale;    % displacement correction
            else
                % Forward
                reoang = pb_fwd(itcount).*rMF;
            end
            
            % Left or right
            if s(newstep + 1) == 1
                % Left
                tag = reoang;
            elseif s(newstep + 1) == -1
                % Right
                tag = -reoang;
            end
            
            % * Get new position & orientation
            newo = wrapTo360(o(currentstep) + tag);
            [dx, dy] = pol2cart(deg2rad(newo), dsp);
            newx = x(currentstep) + dx;
            newy = y(currentstep) + dy;
            
            % * Boundary conditions
            while ~inpolygon(newx, newy, xbox, ybox)
                
                switch blim
                    case 'none'
                        % No boundary limits, continue even if out of box
                        break;
                        
                    case 'reflective'
                        % Get reflected point
                        
                        xE = x(currentstep);
                        yE = y(currentstep);
                        
                        % Avoid warnings in parfor loop
                        xF = NaN;
                        yF = NaN;
                        
                        % Find intersection point
                        [x0, y0, ii] = polyxpoly([xE, newx], [yE, newy], xbox, ybox);
                        
                        % Find angle of incidence on wall and get new mirrored
                        % coordinates
                        OE = sqrt((xE - x0)^2 + (yE - y0)^2);
                        if ii(2) == 1
                            % Left wall
                            incangle = atan((yE-y0)/(xE-x0));
                            xF = cos(incangle)*(dsp-OE) + x0;
                            yF = newy;
                        elseif ii(2) == 2
                            % Top wall
                            incangle = atan((x0-xE)/(y0-yE));
                            xF = newx;
                            yF = -cos(incangle)*(dsp-OE) + y0;
                        elseif ii(2) == 3
                            % Right wall
                            incangle = atan((yE-y0)/(x0-xE));
                            xF = -cos(incangle)*(dsp-OE) + x0;
                            yF = newy;
                        elseif ii(2) == 4
                            % Bottom wall
                            incangle = atan((xE-x0)/(yE-y0));
                            xF = newx;
                            yF = cos(incangle)*(dsp-OE) + y0;
                        end
                        
                        newx = xF;
                        newy = yF;
                        newo = wrapTo360(rad2deg(angle(xF - x0 + 1i*(yF - y0))));
                        
                    case 'startnew'
                        % End this trajectory and start a new one.
                        stopflag = true;
                        % Check if trajectory has enough bouts &
                        % check if three bout types are present
                        chktag = tags;
                        chktag(tags < -theta_threshold) = -1;
                        chktag(tags > theta_threshold) = +1;
                        chktag(abs(tags) < theta_threshold) = 0;
                        chktag(isnan(tags)) = [];
                        lasttime = find(~isnan(time), 1, 'last');
                        condtime = time(lasttime);
                        allconds = numel(unique(chktag)) == 3 && (sum(boutloc) >= minnbouts || (~isempty(condtime) && condtime >= mintrajtime));
                        if allconds
                            isokay = true;
                            %disp('ok');
                        else
                            flagredo = true;
                            %disp('nope');
                        end
                        
                        break;
                end
            end
            
            % * Save picked values
            boutloc(currentstep) = true;
            time(currentstep) = currentstep/framerate;
            ibis(currentstep) = ibi_second;
            dsps(currentstep) = dsp;
            tags(currentstep) = tag;
            
            % Check if the trajectory has to end
            if stopflag
                break;
            end
            
            % * Save new step
            x(newstep + 1) = newx;
            y(newstep + 1) = newy;
            o(newstep + 1) = newo;
            
            % * Go onto next bout
            currentstep = newstep + 1;
            
            switch liveplot
                case 'y'
                    p.XData = x;
                    p.YData = y;
                    axis auto
                    drawnow limitrate
            end
        end
        
        % Cut off & store
        time = time(1:nsteps);
        x = x(1:nsteps);
        y = y(1:nsteps);
        
%         time = time(boutloc);
%         x = x(boutloc);
%         y = y(boutloc);
        tags(isnan(tags)) = [];
        ibis(isnan(ibis)) = [];
        dsps(isnan(dsps)) = [];
        
        pttosave = round(linspace(1, numel(time), min(nsavepoint, numel(time))));
        allt{fish} = time(pttosave) ;
        allx{fish} = x(pttosave);
        ally{fish} = y(pttosave);
        
        if isequal(pttosave, 1:numel(time))
            alli{fish} = ibis;
            alld{fish} = dsps;
            allr{fish} = tags;
        else
            alli{fish} = ibis(pttosave);
            alld{fish} = dsps(pttosave);
            allr{fish} = tags(pttosave);
        end
        
        if ~flagredo
            isokay = true;
        end
    end
end

fprintf('\tDone (%2.2fs).\n', toc)

% --- Save file
outfile = [outbase blim '_' num2str(nfish) '.mat'];
if ~isempty(outfile)
    META = datetime;
    save(outfile, 'META', ...
        'allt', 'allx', 'ally', 'alli', 'alld', 'allr', ...
        'nfish', 'framerate', 'ntimes', 'blim', 'len', 'wid', 'Tl', 'Tr');
end