% Plots bout-based autocorrelation of turn direction, left/right switching
% probability (pflip). Plots time-based autocorrelation and switching rate
% (kflip).
% (Figure 3)
% Uses the file "allsequences_perbatch.mat".

clear
clc

% --- Definitions
filename = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences_perbatch.mat'];

T = [18, 22, 26, 30, 33];   % tested temperatures (°C)

theta_threshold = 10;       % threshold for forward/turn (°)

% Autocorr. settings
ncorr = 500;                % frames

nboots = 1000;              % for bootstraping of IBI mean

% figure
colors = rainbow(numel(T));
fig_reo = figure; ax_reo = axes(fig_reo); hold(ax_reo, 'on');
fig_acf = figure; ax_acf = axes(fig_acf); hold(ax_acf, 'on');
plt_reo = cell(numel(T), 1);
plt_acf = cell(numel(T), 1);
lo_reo = struct;
lo_reo.MarkerSize = 15;
lo_reo.LineStyle = 'none';
so_reo.FaceAlpha = 0.275;
lo_acf = struct;
lo_acf.LineStyle = 'none';
lo_acf.Marker = '.';
lo_acf.MarkerSize = 8;
so_acf = struct;
so_acf.FaceAlpha = 0.275;

% --- Load data
data = load(filename);

% --- Initialization
pflip = NaN(numel(T), 1);
kflip = NaN(numel(T), 1);
pflip_ci = NaN(numel(T), 2);
kflip_ci = NaN(numel(T), 2);
mibi = NaN(numel(T), 1);
eibi = NaN(numel(T), 1);

% --- Processing
fprintf('Extracting pflip'); tic
for idT = 1:numel(T)
        	
    % Get data
    sibi = data.interboutintervals{idT};
    sreo = data.dtheta{idT};
    stime = data.bouttime{idT};
    framerate = mean([data.framerates{idT}{:}]);
    
    % Pool batches
    ibi = fixCellSize(sibi, 1);
    ibi = cat(2, ibi{:});
    reo = fixCellSize(sreo, 1);
    reo = cat(2, reo{:});
    time = fixCellSize(stime, 1);
    time = cat(2, time{:});
    
    % Get temperature-averaged interbout interval
    mibi(idT) = mean(ibi, 'all', 'omitnan');
    ibi_ci = bootci(nboots, @(x) mean(x, 'all', 'omitnan'), ibi);
    eibi(idT) = mean(abs(mibi(idT) - ibi_ci));
    
    % number of tracks
    nseq = size(reo, 2);
	
    % Init.
    pturns = NaN(nseq, 1);  % per seq. pturn to fit afterwards
    dthetai = cell(nseq, 1);
    dthetan = cell(nseq, 1);
    pacf = NaN(ncorr + 1, nseq);
    plag = NaN(ncorr + 1, nseq);
    
    for seq = 1:nseq
        
        % Get sequence
        dtheta = reo(:, seq);
        dtheta(isnan(dtheta)) = [];
        
        % Get pturn
        pturns(seq) = sum(abs(dtheta) > theta_threshold)/numel(dtheta);
        
        % Trinarize
        dtheta = dtheta - mean(dtheta);  % bias correction
        dthetacopy = dtheta;
        dtheta(abs(dthetacopy) <= theta_threshold) = 0;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
                
        % Create lagged vectors
        dthetai{seq} = dtheta(1:end  - 1);
        dthetan{seq} = dtheta((1 + 1):end);
        
        % Reconstruct telegraph time-continuous signal
        dtheta(dtheta == 0) = NaN;      % remove forwards
        t = time(:, seq);
        t(isnan(t)) = [];
        tvec = t(1:end - 1) - t(1);     % start at 0
        fvec = round(tvec*framerate);   % convert in frames
        fr = fvec(1):fvec(end);         % create full time vector
        dthetatime = NaN(size(fr));
        dthetatime(fvec + 1) = dtheta;  % fill with actual values
        dthetatime = fillmissing(dthetatime, 'previous');   % fill missing with previous
        dthetatime = fillmissing(dthetatime, 'next');
        
        % Get ACF
        [xco, lag] = xcorr(dthetatime, ncorr, 'normalized');
        selected_inds = find(lag == 0):find(lag == 0) + ncorr;
        pacf(:, seq) = xco(selected_inds);
        plag(:, seq) = lag(selected_inds)./framerate;
        
    end
	
    % Pool sequences
    dthetai = cat(1, dthetai{:});
    dthetan = cat(1, dthetan{:});
    
    % Binning
    binedges = [-1, 0, 1, 1 + eps];
    bindti = discretize(dthetai, binedges);                 % regroup in bins
    meandtn = accumarray(bindti, dthetan, [], @mean, NaN);  % average over bins
    stddtn = accumarray(bindti, dthetan, [], @std, NaN);    % std
    
    % Get sem within bins
    nelmtsperbin = histcounts(dthetai, binedges);
    semdtn = stddtn./sqrt(nelmtsperbin');
    
    % Fit
    pt = mean(pturns);
    yfit = @(pflip, x) pt.*(1-2*pflip).*x;
    ft_reo = fit((-1:1)', meandtn, yfit, 'StartPoint', 0.25);
    pflip(idT) = ft_reo.pflip;
    dummy = confint(ft_reo);
    pflip_ci(idT, :) = dummy(:, 1)';
    
    % Mean ACF
    mlag = mean(plag, 2, 'omitnan');
    macf = mean(pacf, 2, 'omitnan');
    eacf = std(pacf, [], 2, 'omitnan')./sqrt(nseq);
    
    % Fit ACF with exp. decay
    expfit = @(nu, x) exp(-2*x.*nu);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', 0, 'Upper', Inf, 'StartPoint', 1);
    mdl = fittype(expfit, 'options', fo);
    ft_acf = fit(mlag, macf, mdl);
    kflip(idT) = ft_acf.nu;
    dummy = confint(ft_acf);
    kflip_ci(idT, :) = dummy(:, 1)';
    
    % --- Display
    % Bout direction autocorr.
    lo_reo.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lo_reo.Color = colors(idT, :);
    
    plt_reo{idT} = errorshaded(-1:1, meandtn, semdtn, 'line', lo_reo, 'patch', so_reo, 'ax', ax_reo);
    preo = plot(ax_reo, -1:1, ft_reo(-1:1));
    preo.Color = colors(idT, :);
    
    % Telegraph ACF
    lo_acf.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lo_acf.Color = colors(idT, :);
    
    plt_acf{idT} = errorshaded(mlag, macf, eacf, 'line', lo_acf, 'patch', so_acf, 'ax', ax_acf);
    pacf = plot(ax_acf, mlag, ft_acf(mlag));
    pacf.Color = colors(idT, :);
    
    fprintf('.');
end

% Cosmetic
ax_reo.XLim = [-1.25, 1.25];
ax_reo.YLim = [-0.3, 0.3];
ax_reo.XTick = [-1, 0, 1];
ax_reo.YTick = [-0.2, 0, 0.2];

ax_acf.XLim = [0, ncorr/framerate];
ax_acf.YLim = [-.01, 1];

% Display legends
xlabel(ax_reo, '\Delta_n');
ylabel(ax_reo, '<\Delta_{n+1}>');
title(ax_reo, 'Reorientation direction of bout n+1');

xlabel(ax_acf, 'delay (s)');
ylabel(ax_acf, '<R_{\Delta\Delta}>_{traj.}');
title(ax_acf, 'Time autocorrelation');

% Plot fit legend
preo = plot(ax_reo, [NaN, NaN], [NaN NaN], 'k');
preo.DisplayName = 'Fit';
legend(ax_reo, [plt_reo{:}, preo], 'Location', 'northwest');

pacf = plot(ax_acf, [NaN, NaN], [NaN, NaN], 'k');
pacf.DisplayName = 'Fit';
legend(ax_acf, [plt_acf{:}, pacf]);

fprintf('\t Done (%2.2fs).\n', toc);

% Plot pflip
figure; hold on
neg = pflip - pflip_ci(:, 1);
pos = pflip - pflip_ci(:, 2);
errorbar(T, pflip, neg, pos, 'k');
s = scatter(T, pflip, 75);
s.CData = colors;
s.Marker = 'o';
s.MarkerFaceColor = 'flat';

xlabel('temperature (°C)');
ylabel('p_{flip}');
ylim([0, 0.6]);
ax_reo = gca;
ax_reo.XTick = [18, 22, 26, 30, 33];
ax_reo.YTick = [0, 0.1, 0.2, 0.3, 0.4, 0.5];
plot([15 35], [.5 .5], '--k');
title('Extracted p_{flip}');

% Plot kflip

% pflip/<ibi>
X = pflip./mibi;
errpflip = diff(pflip_ci, 1, 2);
errratio = X.*sqrt( (errpflip./pflip).^2 + (eibi./mibi).^2 );
errX = [X - errratio/2, X + errratio/2];

figure; hold on

col = [0.8, 0, 0];
coltransp = 0.9;

pp = plot(T, X);
pp.Color = [col, coltransp];
pp.DisplayName = 'p_{flip}/<\delta{t}>_T';
manualerrbar(T, errX, 'Color', [col, coltransp]);

neg = kflip - kflip_ci(:, 1);
pos = kflip - kflip_ci(:, 2);
p = errorbar(T, kflip, neg, pos);
p.Marker = 'o';
p.Color = 'k';
p.DisplayName = 'from ACF';

xlabel('Temperature [°C]');
ylab = ylabel('k_{flip} (s^{-1})');
title('Telegraph time constant');

legend([p, pp], 'Location', 'northwest');

ax = gca;
ax.XTick = [18, 22, 26, 30, 33];