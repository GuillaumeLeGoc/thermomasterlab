% Compute and plot mean square displacement and mean square reorientation
% with msdanalyzer package (https://tinevez.github.io/msdanalyzer/).
% Generate a files containing MSD and MSR data to compare to simulations.

% close all
clear
clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
outfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'data_msx.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Opt
nbouts = 15;       % Delays (bouts)
ntimes = 15;       % Delays (seconds)

% * Figures
colors = rainbow(numel(T));

pmsdb = cell(numel(T), 1); fmsdb = figure('Name', 'MSDBout'); hold on;
pmsdt = cell(numel(T), 1); fmsdt = figure('Name', 'MSDTime'); hold on;
pmsrb = cell(numel(T), 1); fmsrb = figure('Name', 'MSRBout'); hold on;
pmsrt = cell(numel(T), 1); fmsrt = figure('Name', 'MSRTime'); hold on;

% * Load data
data = load(fn_dataset);

% * Init. arrays
store_msdb_delay = cell(numel(T), 1);
store_msdb_data = cell(numel(T), 1);
store_msdb_sem = cell(numel(T), 1);
store_msdt_delay = cell(numel(T), 1);
store_msdt_data = cell(numel(T), 1);
store_msdt_sem = cell(numel(T), 1);
store_msrb_delay = cell(numel(T), 1);
store_msrb_data = cell(numel(T), 1);
store_msrb_sem = cell(numel(T), 1);
store_msrt_delay = cell(numel(T), 1);
store_msrt_data = cell(numel(T), 1);
store_msrt_sem = cell(numel(T), 1);

% --- Processing
for idT = 1:numel(T)
    
    fprintf(['Computing MSD and MSR for T = ', num2str(T(idT)), '°C...']); tic
    
    % Get data
    xpos = data.xpos{idT};
    ypos = data.ypos{idT};
    turnangle = data.dtheta{idT};
    time = data.bouttime{idT};
    nseq = size(xpos, 2);
    
    % Prepare arrays
    xy_bout = cell(nseq, 1);    % x,y sequences in bouts
    xy_time = cell(nseq, 1);    % x,y sequences in time
    tu_bout = cell(nseq, 1);    % dtheta sequences in bouts
    tu_time = cell(nseq, 1);    % dtheta sequences in time
    
    for s = 1:nseq
        
        % (x,y) sequences
        x = xpos(:, s);
        x(isnan(x)) = [];
        y = ypos(:, s);
        y(isnan(y)) = [];
        
        b = (0:(size(x, 1) - 1))';
        t = time(:, s);
        t(isnan(t)) = [];
        t = round(t, 4);
        
        xy_bout{s} = [b, x, y];
        xy_time{s} = [t, x, y];
        
        % dtheta sequences
        dtheta = turnangle(:, s);
        dtheta(isnan(dtheta)) = [];
        theta = cumsum(dtheta);
        
        b = (0:size(theta, 1) - 1)';
        t = time(:, s);
        t(isnan(t)) = [];
        t = round(t(1:end - 1), 4);
        
        tu_bout{s} = [b, theta];
        tu_time{s} = [t, theta];
    end
    
    % Init. MSDAnalyser
    msd_bout = msdanalyzer(2, 'mm', 'bouts');
    msd_time = msdanalyzer(2, 'mm', 's');
    msr_bout = msdanalyzer(1, 'deg', 'bouts');
    msr_time = msdanalyzer(1, 'deg', 's');
    
    % Add tracks
    msd_bout = msd_bout.addAll(xy_bout);
    msd_time = msd_time.addAll(xy_time);
    msr_bout = msr_bout.addAll(tu_bout);
    msr_time = msr_time.addAll(tu_time);
    
    % Compute MSD & MSR
    msd_bout = msd_bout.computeMSD;
    msd_time = msd_time.computeMSD;
    msr_bout = msr_bout.computeMSD;
    msr_time = msr_time.computeMSD;
    
    % Get mean & sem
    results_bout = msd_bout.getMeanMSD;
    xy_bouts_delays = results_bout(1:nbouts, 1);
    mmsd_bout = results_bout(1:nbouts, 2);
    emsd_bout = results_bout(1:nbouts, 3)./sqrt(results_bout(1:nbouts, 4));
    
    results_time = msd_time.getMeanMSD;
    results_time(:, 1) = results_time(:, 1);
    [~, stop] = min(abs(results_time(:, 1) - ntimes));
    xy_time_delays = results_time(1:stop, 1);
    mmsd_time = results_time(1:stop, 2);
    emsd_time = results_time(1:stop, 3)./sqrt(results_time(1:stop, 4));
    
    results_bout = msr_bout.getMeanMSD;
    tu_bouts_delays = results_bout(1:nbouts, 1);
    mmsr_bout = results_bout(1:nbouts, 2);
    emsr_bout = results_bout(1:nbouts, 3)./sqrt(results_bout(1:nbouts, 4));
    
    results_time = msr_time.getMeanMSD;
    results_time(:, 1) = results_time(:, 1);
    [~, stop] = min(abs(results_time(:, 1) - ntimes));
    tu_time_delays = results_time(1:stop, 1);
    mmsr_time = results_time(1:stop, 2);
    emsr_time = results_time(1:stop, 3)./sqrt(results_time(1:stop, 4));
    
    % * Store
    store_msdb_delay{idT} = xy_bouts_delays;
    store_msdb_data{idT} = mmsd_bout;
    store_msdb_sem{idT} = emsd_bout;
    store_msdt_delay{idT} = xy_time_delays;
    store_msdt_data{idT} = mmsd_time;
    store_msdt_sem{idT} = emsd_time;
    
    store_msrb_delay{idT} = tu_bouts_delays;
    store_msrb_data{idT} = mmsr_bout;
    store_msrb_sem{idT} = emsr_bout;
    store_msrt_delay{idT} = tu_time_delays;
    store_msrt_data{idT} = mmsr_time;
    store_msrt_sem{idT} = emsr_time;
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.LineStyle = '-';
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    
    % * Bouts
    lineopt.MarkerSize = 16;
    lineopt.LineWidth = 1.25;
    
    % - MSD
    set(0, 'CurrentFigure', fmsdb);
    pmsdb{idT} = errorshaded(xy_bouts_delays, mmsd_bout, emsd_bout, 'line', lineopt, 'patch', shadopt);
    title('Mean square displacement');
    xlabel('Delays [bout]');
    ylabel('<MSD> [mm^2]');
    axis square
    
    % - MSR
    set(0, 'CurrentFigure', fmsrb);
    pmsrb{idT} = errorshaded(tu_bouts_delays, mmsr_bout/10000, emsr_bout/10000, 'line', lineopt, 'patch', shadopt);
    title('Mean square reorientation');
    xlabel('Delays [bout]');
    ylabel('10000\times<MSR> [deg^2]');
    axis square
    
    % * Time
    lineopt.MarkerSize = 2;
    lineopt.LineWidth = 1;
    
    % - MSD
    set(0, 'CurrentFigure', fmsdt);
    pmsdt{idT} = errorshaded(xy_time_delays, mmsd_time, emsd_time, 'line', lineopt, 'patch', shadopt);
    title('Mean square displacement');
    xlabel('Delays [s]');
    ylabel('<MSD> [mm^2]');
    axis square
    
    % - MSR
    set(0, 'CurrentFigure', fmsrt);
    pmsdt{idT} = errorshaded(tu_time_delays, mmsr_time/10000, emsr_time/10000, 'line', lineopt, 'patch', shadopt);
    title('Mean square reorientation');
    xlabel('Delays [s]');
    ylabel('10000\times<MSR> [deg^2]');
    axis square
end

l = legend([pmsdb{:}]);
l.Location = 'northwest';
l = legend([pmsdt{:}]);
l.Location = 'northwest';
l = legend([pmsrb{:}]);
l.Location = 'northwest';
l = legend([pmsrt{:}]);
l.Location = 'northwest';

% --- Save
if ~isempty(outfile)
    msd = struct;
    msd.data_bout = store_msdb_data;
    msd.data_time = store_msdt_data;
    msd.delay_bout = store_msdb_delay;
    msd.delay_time = store_msdt_delay;
    msd.sem_bout = store_msdb_sem;
    msd.sem_time = store_msdt_sem;
    
    msr = struct;
    msr.data_bout = store_msrb_data;
    msr.data_time = store_msrt_data;
    msr.delay_bout = store_msrb_delay;
    msr.delay_time = store_msrt_delay;
    msr.sem_bout = store_msrb_sem;
    msr.sem_time = store_msrt_sem;
    
    META = 'one cell = one temperature';
    
    save(outfile, 'msd', 'msr', 'META');
end