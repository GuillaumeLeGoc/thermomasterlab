% Single-fish experiments : fit ACF with an Ornstein–Uhlenbeck process.
% (Figure 5E-F)
% Uses "sf_featuresmatrix_pk.mat" and "PCAIntrapooled_pk.mat" (the latter
% can be generated with "BMC/parameters_correlations.m")
% /!\ Takes a very long time : ~3s/real. ("exp") on i7-8650U

clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'sf_featuresmatrix_pk_T' num2str(T) '.mat'];
fileallfish = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk.mat'];

% * Settings
dtres = 1;
timevec = 0:dtres:7200-1;
delays = 1:10:7200-1;
ncorrtime = 7200-1;
ncorr = round(ncorrtime./dtres);

% * Simulation settings
nit = 20;   % total number of runs
nexp = 500; % number of simu in one run

% tau = 1/k, k : restoring force term
rangetau1 = linspace(2000, 3000, 1000);     % range to sweep on PC1
rangetau2 = linspace(1900, 2100, 1000);     % PC2
ntime = timevec(end);
% O.U. parameters
mu = 0;     % mean
x0 = 0;     % start
dt = dtres; % use same sampling

% * Load file
ftmp = load(filefeatmat);
featmat = ftmp.feat_mat_pooled;
nfish = numel(featmat);
nfeat = size(featmat{1}, 2);
ttraj = ftmp.ttraj_pooled;
ftmp = load(fileallfish);

all26_coefPC1 = ftmp.pc1intrap;
all26_coefPC2 = ftmp.pc2intrap;
sfac26 = ftmp.sfac;

% Init. simu
bacf1 = NaN(ncorr + 1, nit);
bacf2 = NaN(ncorr + 1, nit);
btau1 = NaN(nit, 1);
btau2 = NaN(nit, 1);
rtau1 = NaN(nit, 1);
rtau2 = NaN(nit, 1);

% --- 1-fish data projections in multi-fish PC space
% Init.
pc1proj = cell(nfish, 1);
pc2proj = cell(nfish, 1);

% * Projection on PC
for idF = 1:nfish
    
    samplefeatmat = featmat{idF};
    
    sfm = samplefeatmat./sfac26;    % normalize data with multi-fish variance
    
    % Projection on all base
    projonpc1 = sfm*all26_coefPC1;
    projonpc2 = sfm*all26_coefPC2;
    
    % Store
    pc1proj{idF} = projonpc1;
    pc2proj{idF} = projonpc2;
end

% Resample proj. on PC1 & PC2
pc1projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc1proj, 'UniformOutput', false);
pc1projres = cat(1, pc1projres{:});
pc2projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc2proj, 'UniformOutput', false);
pc2projres = cat(1, pc2projres{:});

% Select data
pcavec1 = pc1projres;
pcavec2 = pc2projres;
pcavec12 = [pcavec1', pcavec2'];

% * ACF
[apfa, laga] = corOU(pcavec12, ncorr);
apf1 = apfa(:, 1:nfish);
apf2 = apfa(:, (nfish + 1):end);
mlpf1 = laga;
mlpf2 = laga;

% Mean
mapf1 = mean(apf1, 2);
eapf1 = std(apf1, [], 2)./sqrt(nfish);
mapf2 = mean(apf2, 2);
eapf2 = std(apf2, [], 2)./sqrt(nfish);

% * PCA variance over time
vpfa = varOU(delays, pcavec12, dtres);
vpf1 = vpfa(:, 1:nfish);
vpf2 = vpfa(:, (nfish + 1):end);
mvpf1 = mean(vpf1, 2);
evpf1 = std(vpf1, [], 2)./sqrt(nfish);
mvpf2 = mean(vpf2, 2);
evpf2 = std(vpf2, [], 2)./sqrt(nfish);

% --- Simulation of OU process
for it = 1:nit
    
    fprintf('Simulations, iteration %i...\n', it);
    
    fprintf('Fitting ACF to find best tau...'); tic;
    
    % - Tau estimation
    sweepmcorrou1 = NaN(ncorr + 1, numel(rangetau1));
    sweepmcorrou2 = NaN(ncorr + 1, numel(rangetau1));
    sweeplcorrou1 = NaN(ncorr + 1, numel(rangetau1));
    sweeplcorrou2 = NaN(ncorr + 1, numel(rangetau1));
    
    for idtau = 1:numel(rangetau1)
        
        k1 = 1./rangetau1(idtau);
        k2 = 1./rangetau2(idtau);
        
        ousignals1 = generateOU(nexp, ntime, dt, k1, 1, mu, x0);
        ousignals2 = generateOU(nexp, ntime, dt, k2, 1, mu, x0);
        
        ous12 = [ousignals1, ousignals2];
        [apfa, laga] = corOU(ous12, ncorr);
        ape1 = apfa(:, 1:nexp);
        ape2 = apfa(:, (nexp + 1):end);
        
        % Mean
        sweeplcorrou1(:, idtau) = laga;
        sweepmcorrou1(:, idtau) = mean(ape1, 2);
        sweeplcorrou2(:, idtau) = laga;
        sweepmcorrou2(:, idtau) = mean(ape2, 2);
    end
    
    % * Residual sum of square
    rssacf1 = sum( (sweepmcorrou1 - repmat(mapf1, 1, numel(rangetau1))).^2);
    [rtau1(it), rargminacf1] = min(rssacf1);
    bacf1(:, it) = sweepmcorrou1(:, rargminacf1);
    besttau1 = rangetau1(rargminacf1);
    btau1(it) = besttau1;
    
    rssacf2 = sum( (sweepmcorrou2 - repmat(mapf2, 1, numel(rangetau2))).^2);
    [rtau2(it), rargminacf2] = min(rssacf2);
    bacf2(:, it) = sweepmcorrou2(:, rargminacf2);
    besttau2 = rangetau2(rargminacf2);
    btau2(it) = besttau2;
    
    fprintf('\tDone(%2.2fs).\n', toc);
    
end

% --- Get mean +/- sem of best signals and tau & D
facf1 = mean(bacf1, 2);
eacf1 = std(bacf1, [], 2)./sqrt(nit);
facf2 = mean(bacf2, 2);
eacf2 = std(bacf2, [], 2)./sqrt(nit);
ftau1 = mean(btau1);
etau1 = std(btau1)./sqrt(nit);
ftau2 = mean(btau2);
etau2 = std(btau2)./sqrt(nit);

% --- Display
% * ACF PC1
figure; hold on;

% - Data
lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean ACF from data';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf1, mapf1, eapf1, 'line', lopt, 'patch', sopt);

% - Simulation
pf = plot(laga, facf1, 'r');
pf.DisplayName = ['OU simu, \tau=' num2str(ftau1, '%4.0f') '\pm' num2str(etau1, '%2.0f') 's'];

% - Cosmetic
xlabel('Delays (s)');
ylabel('ACF(Proj. on PC1)');
legend([pm, pf]);

% * ACF PC2
figure; hold on;

% - Data
lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean ACF from data';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf2, mapf2, eapf2, 'line', lopt, 'patch', sopt);

% - Simulation
pf = plot(laga, facf2, 'r');
pf.DisplayName = ['OU simu, \tau=' num2str(ftau2, '%4.0f') '\pm' num2str(etau2, '%2.0f') 's'];

xlabel('Delays (s)');
ylabel('ACF(Proj. on PC2)');
legend([pm, pf]);