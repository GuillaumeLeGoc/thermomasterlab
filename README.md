# ThermoMasterLab

This repository contains codes to analyse larval zebrafish behavior in the so called ThermoMaster rig in [Laboratoire Jean Perrin](https://www.labojeanperrin.fr/?lang=en).  
It essentially uses raw tracking data from [FastTrack](https://fasttrack.sh), and derive distributions, models and simulations.  
A lot of utilities codes from the [Tools repository](https://gitlab.com/GuillaumeLeGoc/tools) are used within scripts and functions.

## Publication
This repo contains all codes used to perform analyses presented in the following paper :
Le Goc, G., Lafaye, J., Karpenko, S. et al. Thermal modulation of Zebrafish exploratory statistics reveals constraints on individual behavioral variability. BMC Biol 19, 208 (2021). [https://doi.org/10.1186/s12915-021-01126-w](https://doi.org/10.1186/s12915-021-01126-w)

## Figures reproduction
The "BMC" folder pools scripts to generate figures shown in the aforementioned paper, from the pre-processed data available on Dryad : [doi:10.5061/dryad.3r2280ggw](https://doi.org/10.5061/dryad.3r2280ggw). 
