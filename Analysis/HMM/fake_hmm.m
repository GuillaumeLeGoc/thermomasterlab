% Hidden Markov model. Estimates model parameters from a sequence with
% unknown hidden states.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [22 26 30];

% * Established probabilities, median (sorted by temperatures)
pturns = [0.54, 0.66 0.76];

% * HMM parameters
% Use file rather than computing transition matrix
file = '';
% Guess transition matrix
trguess = (1/3).*ones(3, 3);
% Guess emission matrix
emitguess = eye(3);

% Maximum iterations
maxitr = 1e4;

% * Sequence generations
seq_length = 1000;
n_seq = 300;
n_corr = 20;
colors = getColors(3);

% * Filters
filt.seq_length = 60;       % min sequence length for each fish in seconds
filt.bout_freq = 0.1;       % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Threshold
theta_threshold = 6;   % deg

% * Preparation
if ~exist(file, 'file')
    Mtransition_mat = cell(numel(T), 1);
    Etransition_mat = cell(numel(T), 1);
    storeMtrans = cell(numel(T), 1);
else
    Mtransition_mat = load(file);
    Mtransition_mat = Mtransition_mat.Mtransition_mat;
end

figure; hold on

% --- Main loop
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    if ~exist(file, 'file')
        % Find tracks file
        [ftrack, fstamp, nfish] = getExperimentList(temperature);
        
        % Loop over experiments
        trans_mat_exp = NaN(4, 4, numel(ftrack));
        allsequences = cell(0);
        for idx_exp = 1:numel(ftrack)
            
            % Get data
            [~, ~, ~, turnangle_exp] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
            
            % Prepare output
            pooledsequences = cell(size(turnangle_exp, 2), 1);
            good = 0;
            for trajid = 1:size(turnangle_exp, 2)
                
                turnangle = turnangle_exp(:, trajid);
                turnangle(isnan(turnangle)) = [];
                
                % --- Prepare data for HMM
                % - Create the sequence
                % N observable states : R, L, F
                sequence = cell(size(turnangle));
                for i = 1:numel(sequence)
                    
                    if turnangle(i) > theta_threshold
                        sequence{i} = 'L';
                    elseif turnangle(i) < -theta_threshold
                        sequence{i} = 'R';
                    else
                        sequence{i} = 'F';
                    end
                end
                
                % For one per experiment
                pooledsequences{trajid} = sequence';
                
            end
            
            allsequences{idx_exp} = pooledsequences;
            
            % --- HMM (one per experiment)
            %         try
            %             [tm, est_emis] = hmmtrain(pooledsequences, trguess, emitguess, 'Symbols', {'R';'L';'F'}, 'maxiterations', maxitr);
            %         catch
            %             fprintf('T = %i: Algorithm did not converge, not using this experiment.\n', T(idT));
            %             continue;
            %         end
            %
            %         if ~isequal(est_emis, emitguess)
            %             disp(est_emis)
            %             fprintf('T = %i: Bad estimation of emission matrix, ignoring experiment...\n', T(idT));
            %         else
            %             trans_mat_exp(:, :, idx_exp) = tm;
            %             good = good + 1;
            %             fprintf('T = %i: OK\n', T(idT));
            %         end
        end
        
        % --- HMM (one for all)
        allsequences = cat(1, allsequences{:});
        [trans_mat_exp, est_emis] = hmmtrain(allsequences, trguess, emitguess, 'Symbols', {'R';'L';'F'}, 'maxiterations', maxitr);
        
        storeMtrans{idT} = trans_mat_exp;
        Mtransition_mat{idT} = nanmean(trans_mat_exp, 3);
        Etransition_mat{idT} = nanstd(trans_mat_exp, [], 3)./sqrt(good);
        
    end
    
    % --- Display
    subplot(2, 2, idT);
    imagesc(Mtransition_mat{idT});
    ax = gca;
    ax.XTick = 1:3;
    ax.YTick = 1:3;
    ax.XTickLabel = {'L'; 'R'; 'F'};
    ax.YTickLabel = {'L'; 'R'; 'F'};
    colorbar;
    grid off
    caxis([0 .5]);
    title(['T = ' num2str(T(idT)) '°C']);
end
sgtitle('non hidden MM Transition matrix');
colormap(jet);

% -------------------------------------------------------------------------

% --- Flip probability estimation

pflips = NaN(1, numel(T)*4);
c = 0;
for idT = 1:numel(T)
    
    TM = Mtransition_mat{idT};
    pt = pturns(idT);
    
    pflips(c + 1) = 1 - TM(1, 1)/pt;
    pflips(c + 2) = TM(1, 2)/pt;
    pflips(c + 3) = TM(2, 1)/pt;
    pflips(c + 4) = 1 - TM(2, 2)/pt;
    
    c = c + 4;
end

labels = {[num2str(T(1)) '°C'], [num2str(T(2)) '°C'], [num2str(T(3)) '°C']};
g = [ones(1, 4), 2*ones(1, 4), 3*ones(1, 4)];

figure; hold on
opt = struct;
opt.MarkerFaceColor = 'same';
boxplot_withpoints(pflips, g, 0.15, opt, 'Labels', labels);
ax = gca;
ax.Children(1).Children(9).Color = colors(1, :);
ax.Children(1).Children(9).LineWidth = 1.5;
ax.Children(1).Children(6).LineWidth = 1.25;

ax.Children(1).Children(8).Color = colors(2, :);
ax.Children(1).Children(8).LineWidth = 1.5;
ax.Children(1).Children(5).LineWidth = 1.25;

ax.Children(1).Children(7).Color = colors(3, :);
ax.Children(1).Children(7).LineWidth = 1.5;
ax.Children(1).Children(4).LineWidth = 1.25;

title('Non hidden MM : p_{flip} estimation from TM');
ylabel('p_{flip}');
axis([-Inf Inf 0 1]);

% -------------------------------------------------------------------------

% --- Generate new trajectories, compute autocorr
figure; hold on
pflipsauto = NaN(numel(T), 1);
p = cell(numel(T)+1, 1);
for idT = 1:numel(Mtransition_mat)
    
    tr = Mtransition_mat{idT};
    
    mean_xco = 0;
    for seq = 1:n_seq
        
        dtheta = hmmgenerate(seq_length, tr, emitguess, 'Symbols', [-1 1 0]);
        
        [xco, lag] = xcorr(dtheta, n_corr, 'biased');
        selected_inds = find(lag == 0):find(lag == 0) + n_corr;
        
        mean_xco = mean_xco + xco(selected_inds)./max(xco(selected_inds));
        
    end
    
    % Mean
    mean_xco = mean_xco./n_seq;
    
    % Fit
    x = 1:n_corr;
    pt = pturns(idT);
    g = fittype(@(pflip, x) pt^2.*(1 - 2*pflip).^x);
    fitt = fit(x', mean_xco(2:end)', g, 'StartPoint', 0.2);
    
    pflipsauto(idT) = fitt.pflip;
    
    p{idT} = plot(1:n_corr, mean_xco(2:end));
    p{idT}.Marker = '+';
    p{idT}.LineStyle = 'none';
    p{idT}.Color = colors(idT, :);
    p{idT}.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    pf = plot(1:0.01:n_corr, fitt(1:0.01:n_corr));
    pf.Color = p{idT}.Color;
end
p{4} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Fit', 'LineWidth', 1);
legend([p{:}]);
xlabel('# Bout');
ylabel('Autocorrelation of \Delta{\theta}');
title('Autocorrelation on synthetic data from non hidden MM');

a = num2str(T(1));
b = num2str(pflipsauto(1));
c = num2str(T(2));
d = num2str(pflipsauto(2));
e = num2str(T(3));
f = num2str(pflipsauto(3));
txt = {'p_{flip} from autocorr :' ; ...
    ['T = ' a '°C : ' b ] ; ...
    ['T = ' c '°C : ' d ] ; ...
    ['T = ' e '°C : ' f]};
text([5 5 5 5], [0.2 0.175 0.150 0.125], txt);