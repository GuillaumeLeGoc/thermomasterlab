% Generate HMM set from Sophia's parameters and use this sequence as input
% of the other code for hmmtrain.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = 22;

% * Established probabilities, median (sorted by temperatures)
pturn = 0.5;

% * Data generation
syn_nexp = 1;
syn_nseq = 300;
syn_seqlength = 70;
syn_ptr = 0.5;
syn_pfw = 1 - syn_ptr;
syn_pfp = 0.2;

syn_tm = [syn_pfw*(1 - syn_pfp), syn_ptr*(1 - syn_pfp), syn_pfw*syn_pfp, syn_ptr*syn_pfp ;
    syn_pfw*(1 - syn_pfp) , syn_ptr*(1 - syn_pfp), syn_pfw*syn_pfp, syn_ptr*syn_pfp ;
    syn_pfw*syn_pfp, syn_ptr*syn_pfp, syn_pfw*(1 - syn_pfp) , syn_ptr*(1 - syn_pfp) ;
    syn_pfw*syn_pfp, syn_ptr*syn_pfp, syn_pfw*(1 - syn_pfp) , syn_ptr*(1 - syn_pfp)];

syn_emit = [0 , 0 , 1 ;
    0 , 1 , 0 ;
    0 , 0 , 1 ;
    1 , 0 , 0];

% *HMM options 
% Maximum iterations
maxitr = 10e3;
emitguess = [0 , 0 , 1 ;
    0 , 1 , 0 ;
    0 , 0 , 1 ;
    1 , 0 , 0];
trguess = 0.25.*ones(4, 4);

% * Sequence generations for autocorrelations
seq_length = 60;
n_seq = 300;
n_corr = 20;
sigscoot = [3.06, 3.62, 3.94];
sigturn = [34.66, 41.17 46.39];
colors = getColors(3);

% * Filters
filt.seq_length = 60;       % min sequence length for each fish in seconds
filt.bout_freq = 0.1;       % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Preparation
customwarning = warning('error', 'stats:hmmtrain:NoConvergence');
Mtransition_mat = cell(numel(T), 1);
Etransition_mat = cell(numel(T), 1);
storeMtrans = cell(numel(T), 1);
figure; hold on

% --- Create data
syn_data = cell(syn_nexp, 1);
for idx_exp = 1:syn_nexp
    syn_data{idx_exp} = NaN(syn_seqlength, syn_nseq);
    for idx_seq = 1:syn_nseq
        syn_data{idx_exp}(:, idx_seq) = hmmgenerate(syn_seqlength, syn_tm, syn_emit);
    end
end

% --- Use synthetic data
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    % Loop over experiments
    trans_mat_exp = NaN(4, 4, syn_nexp);
    for idx_exp = 1:syn_nexp
        
        % Get data
        turnangle_exp = syn_data{idx_exp};
        
        % Prepare output
        pooledsequences = cell(size(turnangle_exp, 2), 1);
        good = 0;
        for trajid = 1:size(turnangle_exp, 2)
            
            turnangle = turnangle_exp(:, trajid);
            turnangle(isnan(turnangle)) = [];
            
            % --- Prepare data for HMM
            % - Create the sequence
            % N observable states : R, L, F
            sequence = cell(size(turnangle));
            for i = 1:numel(sequence)
                
                if turnangle(i) == 1
                    sequence{i} = 'R';
                elseif turnangle(i) == 2
                    sequence{i} = 'L';
                else
                    sequence{i} = 'F';
                end
            end
            pooledsequences{trajid} = sequence';
                        
        end
        
        % --- HMM
        try
            [tm, est_emis] = hmmtrain(pooledsequences, trguess, emitguess, 'Symbols', {'R';'L';'F'}, 'maxiterations', maxitr);
        catch
            fprintf('T = %i: Algorithm did not converge, not using this experiment.\n', T(idT));
            continue;
        end
        
        if ~isequal(est_emis, emitguess)
            disp(est_emis)
            fprintf('T = %i: Bad estimation of emission matrix, ignoring experiment...\n', T(idT));
        else
            trans_mat_exp(:, :, idx_exp) = tm;
            good = good + 1;
            fprintf('T = %i: OK\n', T(idT));
        end
    end
    
    storeMtrans{idT} = trans_mat_exp;
    Mtransition_mat{idT} = nanmean(trans_mat_exp, 3);
    Etransition_mat{idT} = nanstd(trans_mat_exp, [], 3)./sqrt(good);
    
    % --- Display
    subplot(2, 2, idT);
    imagesc(Mtransition_mat{idT});
    ax = gca;
    ax.XTick = 1:4;
    ax.YTick = 1:4;
    ax.XTickLabel = {'LF'; 'LT'; 'RF'; 'RT'};
    ax.YTickLabel = {'LF'; 'LT'; 'RF'; 'RT'};
    colorbar;
    grid off
    caxis([0 .5]);
    title(['T = ' num2str(T(idT)) '°C']);
end
sgtitle('HMM Transition matrix on synthetic data');
colormap(jet);

% -------------------------------------------------------------------------

% --- Flip probability estimation

% * Method 1 : one for each transition
pflips = NaN(1, numel(T)*4);
c = 0;
for idT = 1:numel(T)
    
    TM = Mtransition_mat{idT};
    %     pflips1 = 1 - [TM(1, 1), TM(2, 1), TM(3, 3), TM(4, 3)]./(1 - pturn(idT));
    %     pflips2 = 1 - [TM(1, 2), TM(2, 2), TM(3, 4), TM(4, 4)]./pturn(idT);
    %     pflips3 = [TM(3, 1), TM(4, 1), TM(1, 3), TM(2, 3)]./(1 - pturn(idT));
    %     pflips4 = [TM(3, 2), TM(4, 2), TM(1, 4), TM(2, 4)]./pturn(idT);
    
    pflips1 = 1 - [TM(2, 2) TM(4, 4)]./pturn(idT);
    pflips2 = [TM(2, 4), TM(4, 2)]./pturn(idT);
    
    %     pflips(c + 1:c + 8) = [pflips1, pflips2, pflips3, pflips4];
    pflips(c + 1:c + 4) = [pflips1, pflips2];
    c = c + 4;
end

labels = {[num2str(T(1)) '°C']};
g = ones(1, 4);

figure; hold on
opt = struct;
opt.MarkerFaceColor = 'same';
boxplot_withpoints(pflips, g, 0.15, opt, 'Labels', labels);
ax = gca;
ax.Children(1).Children(3).Color = colors(1, :);
ax.Children(1).Children(4).LineWidth = 1.5;
ax.Children(1).Children(5).LineWidth = 1.25;

title('p_{flip} estimation from transitions of synthetic data');
ylabel('p_{turn}');
axis([-Inf Inf 0 1]);

% -------------------------------------------------------------------------

% --- Generate new trajectories, compute autocorr
figure; hold on
pflipsauto = NaN(numel(T), 1);
p = cell(numel(T)+1, 1);
for idT = 1:numel(Mtransition_mat)
    
    tr = Mtransition_mat{idT};
    
    mean_xco = 0;
    for seq = 1:n_seq
        
        dtheta = hmmgenerate(seq_length, tr, emitguess, 'Symbols', [-1 1 0]);
        
        [xco, lag] = xcorr(dtheta, n_corr, 'biased');
        selected_inds = find(lag == 0):find(lag == 0) + n_corr;
        
        mean_xco = mean_xco + xco(selected_inds)./max(xco(selected_inds));
        
    end
    
    % Mean
    mean_xco = mean_xco./n_seq;
    
    % Fit
    x = 1:n_corr;
    pt = 1 - mean([tr(1,1) tr(2,1) tr(3,3) tr(4,3)]);
    ps = mean([tr(1,1) tr(2,1) tr(3,3) tr(4,3)]);
    st = sigturn(idT);
    ss = sigscoot(idT);
    g = fittype(@(pflip, x) pt^2.*(1 - 2*pflip).^x);
    fitt = fit(x', mean_xco(2:end)', g, 'StartPoint', 0.2);
    
    pflipsauto(idT) = fitt.pflip;
    
    p{idT} = plot(1:n_corr, mean_xco(2:end));
    p{idT}.Marker = '+';
    p{idT}.LineStyle = 'none';
    p{idT}.Color = colors(idT, :);
    p{idT}.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    pf = plot(1:0.01:n_corr, fitt(1:0.01:n_corr));
    pf.Color = p{idT}.Color;
end
p{4} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Fit', 'LineWidth', 1);
legend([p{:}]);
xlabel('# Bout');
ylabel('Autocorrelation of \Delta{\theta}');
title('Autocorrelation on synthetic data from HMM (synthetic data)');

a = num2str(T(1));
b = num2str(pflipsauto(1));
txt = {'p_{flip} from autocorr :' ; ...
    ['T = ' a '°C : ' b ] };
text([5 5], [0.2 0.175], txt);

warning(customwarning);