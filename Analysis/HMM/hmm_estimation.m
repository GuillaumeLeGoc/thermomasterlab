% Hidden Markov model. Estimates model parameters from a sequence with
% known hidden states.

% close all
clear
clc

% --- Parameters
% * Definitions
cwd = strcat(pwd, filesep, 'Data', filesep);
exp_list = fullfile(cwd, 'Uniform_list.txt');
stamps_name = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');
tracks_name = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');

% * Sequence generations
seq_length = 100;
n_seq = 300;
n_corr = 20;
sigscoot = [3.06, 3.62, 3.94];
sigturn = [34.66, 41.17 46.39];
colors = getColors(3);

supposed_emis = [0 , 0 , 1 ;
    0 , 1 , 0 ;
    0 , 0 , 1 ;
    1 , 0 , 0];

% * Temperatures
T = [22, 26, 30];

% * Filters
filt.seq_length = 60;       % min sequence length for each fish in seconds
filt.bout_freq = 0.1;       % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Threshold
theta_threshold = 5.8245;   % deg

% Loop over temperatures
Mtransition_mat = cell(numel(T), 1);
Etransition_mat = cell(numel(T), 1);
storeMtrans = cell(numel(T), 1);
stot = 0;
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    % Find tracks file
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Loop over experiments
    trans_mat_exp = NaN(4, 4, numel(ftrack));
    for idx_exp = 1:numel(ftrack)
        
        % Get data
        [~, ~, ~, turnangle_exp] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        % Prepare output
        trans_mat = zeros(4, 4);
        good = 0;
        bad = 0;
        for trajid = 1:size(turnangle_exp, 2)
            
            turnangle = turnangle_exp(:, trajid);
            turnangle(isnan(turnangle)) = [];
            
            % --- Prepare data for HMM
            
            % - Create the sequence
            % 3 observable states : R, L, F
            sequence = cell(size(turnangle));
            for i = 1:numel(sequence)
                
                if turnangle(i) < -theta_threshold
                    sequence{i} = 'R';
                elseif turnangle(i) > theta_threshold
                    sequence{i} = 'L';
                else
                    sequence{i} = 'F';
                end
            end
            sequence = sequence';
            
            % - Create the states : LF, LT, RF, RT
            
            % Numeric : 1 = LF, 2 = LT, 3 = RF, 4 = RT
            sta = NaN(size(turnangle));
            sta(turnangle > theta_threshold) = 2;
            sta(turnangle < -theta_threshold) = 4;
            states = sta;
            % If first bout is forward, randomly choose if it is LF or RF
            if isnan(states(1))
                r = rand;
                if r < .5
                    states(1) = 1;
                else
                    states(1) = 3;
                end
            end
            
            % Convert to string cell array
            fstates = cell(size(states));
            if states(1) == 1
                fstates{1} = 'LF';
            elseif states(1) == 2
                fstates{1} = 'LT';
            elseif states(1) == 3
                fstates{1} = 'RF';
            elseif states(1) == 4
                fstates{1} = 'RT';
            end
            
            for s = 2:size(states, 1)
                
                % Left turns
                if states(s) == 2
                    fstates{s} = 'LT';
                    % Right turns
                elseif states(s) == 4
                    fstates{s} = 'RT';
                    % Forward : determine what was the last turning bout
                elseif isnan(states(s))
                    subseq = states(1:s);
                    m = find(~isnan(subseq), 1, 'last');
                    
                    % Last turn was left
                    if states(m) == 2
                        fstates{s} = 'LF';
                        % Last turn was right
                    elseif states(m) == 4
                        fstates{s} = 'RF';
                        % Last turn was a... forward (happens if several
                        % forward bouts at beginning)
                    elseif ismember(states(m), [1, 3])
                        fstates{s} = fstates{m};
                    end
                end
            end
            fstates = fstates';
            
            % --- HMM
            [est_trans, est_emis] = hmmestimate(sequence, fstates, 'Symbols', {'R';'L';'F'},...
                'Statenames', {'LF','LT','RF','RT'});
            
            if ~isequal(est_emis, supposed_emis)
                fprintf('Bad estimation of emission matrix, ignoring sequence...\n');
                bad = bad + 1;
            else
                good = good + 1;
                stot = stot + 1;
                trans_mat = trans_mat + est_trans;
            end
        end
        
        % Mean probabilities
        trans_mat_exp(:, :, idx_exp) = trans_mat./good;
        
        % Verbose
        fprintf('Ignored <strong>%2.1f%%</strong> of sequences\n', bad/trajid);
    end
    
    storeMtrans{idT} = trans_mat_exp;
    Mtransition_mat{idT} = mean(trans_mat_exp, 3);
    Etransition_mat{idT} = std(trans_mat_exp, [], 3)./sqrt(numel(ftrack));
end
fprintf('Total sequences used : <strong>%i</strong>\n', stot);

% --- Generate new trajectories, compute autocorr
figure; hold on
pflips = NaN(numel(T), 1);
p = cell(numel(T)+1, 1);
for idT = 1:numel(Mtransition_mat)
    
    tr = Mtransition_mat{idT};
    
    mean_xco = 0;
    for seq = 1:n_seq
        
        dtheta = hmmgenerate(seq_length, tr, supposed_emis, 'Symbols', [-1 1 0]);
        
        [xco, lag] = xcorr(dtheta, n_corr, 'biased');
        selected_inds = find(lag == 0):find(lag == 0) + n_corr;
        
        mean_xco = mean_xco + xco(selected_inds)./max(xco(selected_inds));
        
    end
    
    % Mean
    mean_xco = mean_xco./n_seq;
    
    % Fit
    x = 1:n_corr;
    pt = 1 - mean([tr(1,1) tr(2,1) tr(3,3) tr(4,3)]);
    ps = mean([tr(1,1) tr(2,1) tr(3,3) tr(4,3)]);
    st = sigturn(idT);
    ss = sigscoot(idT);
    g = fittype(@(pflip, x) ((2/pi).*(1-2*pflip).^x).*((pt*st)^2)./(pt*st.^2 + ps*ss.^2));
    fitt = fit(x', mean_xco(2:end)', g, 'StartPoint', 0.2);
    
    pflips(idT) = fitt.pflip;
    
    p{idT} = plot(1:n_corr, mean_xco(2:end));
    p{idT}.Marker = '+';
    p{idT}.LineStyle = 'none';
    p{idT}.Color = colors(idT, :);
    p{idT}.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    pf = plot(1:0.01:n_corr, fitt(1:0.01:n_corr));
    pf.Color = p{idT}.Color;
end
p{4} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Fit', 'LineWidth', 1);
legend([p{:}]);
xlabel('# Bout');
ylabel('Autocorrelation of \Delta{\theta}');
title('Autocorrelation on synthetic data from HMM');