% Hidden Markov model. Estimates model parameters from a sequence with
% unknown hidden states.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Established probabilities, median (sorted by temperatures)
fn_pturn = [pwd '/Data/Matfiles/pturn_'];

% * HMM parameters
% Use file rather than computing transition matrix
% file = '';
file = '/home/ljp/Science/Projects/ThermoMaster/Data/Matfiles/HMM_Temperatures_AllPooled.mat';

% Guess transition matrix
trguess = 0.25.*ones(4, 4);
% Guess emission matrix
emitguess = [0 , 0 , 1 ;
    0 , 1 , 0 ;
    0 , 0 , 1 ;
    1 , 0 , 0];

% Maximum iterations
maxitr = 1e4;

% * Sequence generations
seq_length = 1000;
n_seq = 1000;
n_corr = 10;
colors = rainbow(numel(T));

% * Filters
filt.seq_length = 60;       % min sequence length for each fish in seconds
filt.bout_freq = 0.1;       % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Threshold
theta_threshold = 6;   % deg

% * Preparation
customwarning = warning('error', 'stats:hmmtrain:NoConvergence');

if ~exist(file, 'file')
    Mtransition_mat = cell(numel(T), 1);
    Etransition_mat = cell(numel(T), 1);
    storeMtrans = cell(numel(T), 1);
else
    Mtransition_mat = load(file);
    Mtransition_mat = Mtransition_mat.Mtransition_mat;
end
pturns = NaN(numel(T), 1);

figure; hold on

% --- Main loop
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    if ~exist(file, 'file')
        % Find tracks file
        [ftrack, fstamp, nfish] = getExperimentList(temperature);
        
        % Loop over experiments
%         trans_mat_exp = NaN(4, 4, numel(ftrack));
        allsequences = cell(0);
        for idx_exp = 1:numel(ftrack)
            
            % Get data
            [~, ~, ~, turnangle_exp] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
            
            % Prepare output
            pooledsequences = cell(size(turnangle_exp, 2), 1);
            pooledsequences_mirror = cell(size(turnangle_exp, 2), 1);
            good = 0;
            for trajid = 1:size(turnangle_exp, 2)
                
                turnangle = turnangle_exp(:, trajid);
                turnangle(isnan(turnangle)) = [];
                
                % --- Prepare data for HMM
                % - Create the sequence
                % N observable states : R, L, F
                sequence = NaN(size(turnangle));
                for i = 1:numel(sequence)
                    
                    if turnangle(i) < -theta_threshold
                        sequence(i) = -1;   % Right
                    elseif turnangle(i) > theta_threshold
                        sequence(i) = 1;    % Left
                    else
                        sequence(i) = 0;    % Forward
                    end
                end
                
                % For one per experiment
                pooledsequences{trajid} = sequence';
                % Symetrize
                pooledsequences_mirror{trajid} = -sequence';
            end
            
            allsequences{idx_exp} = [pooledsequences ; pooledsequences_mirror];
            
            % --- HMM (one per experiment)
            %         try
            %             [tm, est_emis] = hmmtrain(pooledsequences, trguess, emitguess, 'Symbols', {'R';'L';'F'}, 'maxiterations', maxitr);
            %         catch
            %             fprintf('T = %i: Algorithm did not converge, not using this experiment.\n', T(idT));
            %             continue;
            %         end
            %
            %         if ~isequal(est_emis, emitguess)
            %             disp(est_emis)
            %             fprintf('T = %i: Bad estimation of emission matrix, ignoring experiment...\n', T(idT));
            %         else
            %             trans_mat_exp(:, :, idx_exp) = tm;
            %             good = good + 1;
            %             fprintf('T = %i: OK\n', T(idT));
            %         end
        end
        
        % --- HMM (one for all)
        allsequences = cat(1, allsequences{:});
        [trans_mat_exp, est_emis] = hmmtrain(allsequences, trguess, emitguess, 'Symbols', [1, -1, 0], 'maxiterations', maxitr);
                
        storeMtrans{idT} = trans_mat_exp;
        Mtransition_mat{idT} = nanmean(trans_mat_exp, 3);
        Etransition_mat{idT} = nanstd(trans_mat_exp, [], 3)./sqrt(good);
        
    end
    
    % --- Display
    subplot(2, 3, idT);
    imagesc(Mtransition_mat{idT});
    ax = gca;
    ax.XTick = 1:4;
    ax.YTick = 1:4;
    ax.XTickLabel = {'LF'; 'LT'; 'RF'; 'RT'};
    ax.YTickLabel = {'LF'; 'LT'; 'RF'; 'RT'};
    colorbar;
    grid off
    caxis([0 .5]);
    title(['T = ' num2str(T(idT)) '°C']);
    axis square
    
    % --- Load pturns for later
    file_pturn = [fn_pturn 'T' num2str(T(idT)) '.mat'];
    data_pturn = load(file_pturn);
    pturns(idT) = mean(data_pturn.pt);
end
sgtitle('HMM Transition matrix');
colormap(jet);

% -------------------------------------------------------------------------

% --- Generate new trajectories, compute autocorr
figure; hold on
pflipsauto = NaN(numel(T), 1);
conf_int = NaN(numel(T), 2);

p = cell(numel(T) + 1, 1);
for idT = 1:numel(Mtransition_mat)
    
    tr = Mtransition_mat{idT};
    
    pxco = NaN(n_seq, n_corr + 1);
    nturns = 0;
    for seq = 1:n_seq
        
        dtheta = hmmgenerate(seq_length, tr, emitguess, 'Symbols', [-1 1 0]);
        
        [xco, lag] = xcorr(dtheta, n_corr, 'biased');
        selected_inds = find(lag == 0):find(lag == 0) + n_corr;
        
        pxco(seq, :) = xco(selected_inds)./max(xco(selected_inds));
        
        nturns = nturns + sum(abs(dtheta) > 0);
    end
    
    % Mean
    mxco = nanmean(pxco);
    exco = nanstd(pxco)./sqrt(n_seq);
    
    % Fit
    x = 1:n_corr;
    pt = nturns./(n_seq*seq_length);
    g = fittype(@(pflip, x) pt^2.*(1 - 2*pflip).^x);
    fitt = fit(x', mxco(2:end)', g, 'StartPoint', 0.5);
    
    pflipsauto(idT) = fitt.pflip;
    dummy = confint(fitt);
    conf_int(idT, :) = dummy(:, 1)';
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.Marker = '.';
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = 'none';
    shadopt.FaceAlpha = 0.275;
    
    p{idT} = errorshaded(1:n_corr, mxco(2:end), exco(2:end), 'line', lineopt, 'patch', shadopt);
    f = plot(1:0.01:n_corr, fitt(1:0.01:n_corr));
    f.Color = colors(idT, :);
    f.LineWidth = 1.75;
end
p{end} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Fit', 'LineWidth', 1);
legend([p{:}]);
xlabel('# Bout');
ylabel('Autocorrelation of \Delta{\theta}');
title('Autocorrelation on synthetic data from HMM');
axis square

% - Plot extracted pflip values
figure; hold on
neg = pflipsauto - conf_int(:, 1);
pos = pflipsauto - conf_int(:, 2);
e = errorbar(T, pflipsauto, neg, pos);
e.Color = 'k';
xlabel('Temperature [°C]');
ylab = ylabel('p_{flip}');
title('Flipping prob. from ACF on HMM');
axis square
axis([15 35 0 0.6]);
plot([15 35], [.5 .5], '--k');

warning(customwarning);