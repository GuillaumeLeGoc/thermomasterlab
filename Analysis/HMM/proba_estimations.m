% Probabilities estimation with a transition matrix between 4 states.
% The 4 states are LF (1), LT (2), RF (3), RT (4).

% close all
clear
clc

% --- Parameters
% * Definitions
cwd = strcat(pwd, filesep, 'Data', filesep);
exp_list = fullfile(cwd, 'Uniform_list.txt');
stamps_name = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');
tracks_name = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');

% Temporary sequence to use
dat = '2019-03-13';
run = 2;
idx_exp = 5;
trajid = 30;

% * Temperatures
% T = [22, 26, 30];
temperature = 26;

% * Filters
filt.seq_length = 30;       % min sequence length for each fish in seconds
filt.bout_freq = 0.1;       % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Threshold
theta_threshold = 5.8245;   % deg

% Find tracks file
[ftrack, fstamp, nfish] = getExperimentList(temperature);

% Get data
[~, ~, ~, turnangle] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
turnangle = turnangle(:, trajid);
turnangle(isnan(turnangle)) = [];

% Build the sequence with ids 1, 2, 3, 4
seq = NaN(size(turnangle));
% Right and left turns (RT, LT)
seq(turnangle < -theta_threshold) = 2;
seq(turnangle > theta_threshold) = 4;

% We don't have simple forward state so the sequence can't begin with a 
% forward bout. Therefore we assign a random left or right state.
if isnan(seq(1))
    r = rand;
    if r < .5
        seq(1) = 1;
    else
        seq(1) = 3;
    end
end

fseq = seq;
for s = 2:size(seq, 1)
    
    if isnan(seq(s))
        subseq = seq(1:s);
        m = find(~isnan(subseq), 1, 'last');
        
        if subseq(m) == 2
            fseq(s) = 1;
        elseif subseq(m) == 4
            fseq(s) = 3;
        else
            warning('Bug in the MATRIX');
        end
    end
end

% Counting
n = numel(fseq);        % number of bouts
n1 = sum(fseq == 1);    % number of forward after a turn left
n2 = sum(fseq == 2);    % number of left turn
n3 = sum(fseq == 3);    % number of forward after a turn right
n4 = sum(fseq == 4);    % number of right turn

% We should get : (all *nbouts)
% n1 = pfwd
% n2 = pturn(1-pflip)
% n3 = pfwd
% n4 = pturn(1-pflip)
% SO WHAT