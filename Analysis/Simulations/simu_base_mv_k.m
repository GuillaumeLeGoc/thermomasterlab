% Simulation based on generic description X=XbarT Y epsilon, with
% correlations of trajectory means, using rates for turn and flips

% close all
clear
clc

% --- Settings

% * Temperatures (choose from 18, 22, 26, 30, 33)
T = [18, 22, 26, 30, 33];

% * Output file
outbase = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu_mv_k_'];

% * Pool
len = [0, 100];              % Pool length (mm)
wid = [0, 45];               % Pool width (mm)
xbox = len([1 1 2 2 1]);
ybox = wid([1 2 2 1 1]);

% * Experiment
nfish = 1000;           % Number of fish (one at a time)
ntimes = 300;           % Experiement duration (s)
framerate = 25;         % Trajectories sampling rate (Hz)
blim = 'reflective';          % Boundary limits, 'none', 'reflective' or 'startnew'
nbouts = 15;            % minimum number of bouts to validate trajectory in startnew BL
mintrajtime = 25;       % minimum traj. duration in startnew BL
nsteps = ceil(ntimes*framerate);

% * Behavioral constant
theta_threshold = 10;   % Turn definition (°)
dsp_scale = 1.6;

% * Figures
liveplot = 'n';
colors = rainbow(numel(T));

% --- Available data
% * Paths
cwd = [pwd filesep 'Data' filesep 'Matfiles' filesep];
fname_univcdf = [cwd 'universaldistributions.mat'];
fname_meanscdf = [cwd 'meansdistY.mat'];
fname_ktkfcdf = [cwd 'ktkfdistY.mat'];
fname_covmat = [cwd 'corrmat5param_k.mat'];
fname_Tmeans = [cwd 'MeanOverTemperatures.mat'];
fname_ktkfmeans = [cwd 'ktkfmeanT.mat'];
fname_ptpfmeans = [cwd 'ptpfmeanT.mat'];

% Temperature averaged parameters
data_Tmeans = load(fname_Tmeans);
Tmean_ibi = data_Tmeans.mibi;
Tmean_dsp = data_Tmeans.mdsp;
Tmean_reo = data_Tmeans.mreo;
Tmean_fwd = data_Tmeans.mfwd;

% kflip & kturn
data_ktkf = load(fname_ktkfmeans);
Tmean_ktr = data_ktkf.mkturn;
% Tmean_kfp = data_ktkf.mkflip;
data_ptpf = load(fname_ptpfmeans);
Tmean_pfp = data_ptpf.mpflip;

% universal distributions
tmpdata = load(fname_univcdf);
univbin_ibi = tmpdata.univbin_ibi;
univcdf_ibi = tmpdata.univcdf_ibi;
univbin_dsp = tmpdata.univbin_dsp;
univcdf_dsp = tmpdata.univcdf_dsp;
univbin_reo = tmpdata.univbin_reo;
univcdf_reo = tmpdata.univcdf_reo;
univbin_fwd = tmpdata.univbin_fwd;
univcdf_fwd = tmpdata.univcdf_fwd;

% means distributions
tmpdata = load(fname_meanscdf);
meanbin_ibi = tmpdata.binsyibi;
meancdf_ibi = tmpdata.mcdfYibi;
meanbin_dsp = tmpdata.binsydsp;
meancdf_dsp = tmpdata.mcdfYdsp;
meanbin_reo = tmpdata.binsyreo;
meancdf_reo = tmpdata.mcdfYreo;
meanbin_fwd = tmpdata.binsyfwd;
meancdf_fwd = tmpdata.mcdfYfwd;

tmpdata = load(fname_ktkfcdf);
meanbin_ktr = tmpdata.binsyktr;
meancdf_ktr = tmpdata.mcdfYktr;
meanbin_kfp = tmpdata.binsykfp;
meancdf_kfp = tmpdata.mcdfYkfp;

% prepare cdf for reverseCDFu function
[univcdf_ibi, mask] = unique(univcdf_ibi);
univbin_ibi = univbin_ibi(mask);
[univcdf_dsp, mask] = unique(univcdf_dsp);
univbin_dsp = univbin_dsp(mask);
[univcdf_reo, mask] = unique(univcdf_reo);
univbin_reo = univbin_reo(mask);
[univcdf_fwd, mask] = unique(univcdf_fwd);
univbin_fwd = univbin_fwd(mask);
[meancdf_ibi, mask] = unique(meancdf_ibi);
meanbin_ibi = meanbin_ibi(mask);
[meancdf_dsp, mask] = unique(meancdf_dsp);
meanbin_dsp = meanbin_dsp(mask);
[meancdf_reo, mask] = unique(meancdf_reo);
meanbin_reo = meanbin_reo(mask);
[meancdf_fwd, mask] = unique(meancdf_fwd);
meanbin_fwd = meanbin_fwd(mask);
[meancdf_ktr, mask] = unique(meancdf_ktr);
meanbin_ktr = meanbin_ktr(mask);
[meancdf_kfp, mask] = unique(meancdf_kfp);
meanbin_kfp = meanbin_kfp(mask);

% covariance matrix
tmpdata = load(fname_covmat);
covmat = tmpdata.mRtraj;

% --- Preparation
alli = cell(numel(T), nfish);
alld = cell(numel(T), nfish);
allr = cell(numel(T), nfish);
allx = cell(numel(T), nfish);
ally = cell(numel(T), nfish);
allt = cell(numel(T), nfish);

xleft = len(1);
xright = len(2);
ybot = wid(1);
ytop = wid(2);

% --- Processing
for idT = 1:numel(T)
    
    % --- Generation of correlated means
    Z = mvnrnd([0, 0, 0, 0, 0], covmat, nfish);
    V = normcdf(Z);
    meanIBI = reverseCDFu(meanbin_ibi, meancdf_ibi, V(:, 1));
    kturns = reverseCDFu(meanbin_ktr, meancdf_ktr, V(:, 2));
%     kflips = reverseCDFu(meanbin_kfp, meancdf_kfp, V(:, 3));
    meanREO = reverseCDFu(meanbin_reo, meancdf_reo, V(:, 4));
    meanDSP = reverseCDFu(meanbin_dsp, meancdf_dsp, V(:, 5));
    
    meanFWD = reverseCDFu(meanbin_fwd, meancdf_fwd, rand(nfish, 1));
    
    % Display covariances
    disp(covmat)
%     corrcoef([meanIBI, kturns, kflips, meanREO, meanDSP])
    corrcoef([meanIBI, kturns, meanREO, meanDSP])
    
    % Rescale to temperature
    meanIBI = meanIBI.*Tmean_ibi(idT);
    kturns = kturns.*Tmean_ktr(idT);
%     kflips = kflips.*Tmean_kfp(idT);
    meanREO = meanREO.*Tmean_reo(idT);
    meanDSP = meanDSP.*Tmean_dsp(idT);
    meanFWD = meanFWD.*Tmean_fwd(idT);
    
%     KF = Tmean_kfp(idT);
    PF = Tmean_pfp(idT);
    
    % --- Trajectories simulation
    fprintf('Simulating trajectories of %d fish... ', nfish);
    fprintf('%4d/%4d', 0, nfish); tic;
    
    parfor fish = 1:nfish
        
        % Trajectory means
        MI = meanIBI(fish);
        MR = meanREO(fish);
        MD = meanDSP(fish);
        MF = meanFWD(fish);
        
        % kturn & kflip
        KT = kturns(fish);
%         PF = KF.*MI;
%         KF = kflips(fish);
        
        % Generate underlying orientational signal
%         lrrts = generateRTS(ntimes, framerate, KF, [-1, 1]);
        
        % Generate forward/turn probability
        nturns = round(KT*ntimes*framerate);
        fts = zeros(ntimes*framerate, 1);
        fts(1:nturns) = 1;
        fts = shuffleArray(fts);
        
        % Prepare per-bouts distributions
        pb_ibi = reverseCDFu(univbin_ibi, univcdf_ibi, rand(nsteps, 1));
        pb_dsp = reverseCDFu(univbin_dsp, univcdf_dsp, rand(nsteps, 1));
        pb_reo = reverseCDFu(univbin_reo, univcdf_reo, rand(nsteps, 1));
        pb_fwd = reverseCDFu(univbin_fwd, univcdf_fwd, rand(nsteps, 1));
        
        % Init flags
        isokay = false;
        flagredo = false;
        
        % Start traj. generation
        while ~isokay
            
            % - Initialization
            stopflag = false;
            
            % Arrays
            x = NaN(nsteps, 1);     % x position
            y = NaN(nsteps, 1);     % y position
            o = NaN(nsteps, 1);     % orientation
            lrrts = NaN(nsteps, 1); % left/right state
            
            boutloc = false(nsteps, 1);
            time = NaN(nsteps, 1);
            ibis = NaN(nsteps, 1);
            dsps = NaN(nsteps, 1);
            tags = NaN(nsteps, 1);
            
            % Initial position
            xi = randitvl(xleft, xright);
            yi = randitvl(ybot, ytop);
            x(1) = xi;
            y(1) = yi;
            
            % Initial orientation
            oi = randitvl(0, 360);
            o(1) = oi;
            
            % Initial left/right state
            rd = rand;
            if rd < .5
                lrrts(1) = -1;
            else
                lrrts(1) = +1;
            end
            
            % - Build trajectory
            p = 0;  % avoid warning in parfor
            switch liveplot
                case 'y'
                    f = figure; hold on; ax = gca; p = plot(NaN, NaN);
                    axis equal; axis([0 100 0 45]);
            end
            
            currentstep = 1;
            itcount = 0;
            while any(isnan(x))
                
                itcount = itcount + 1;
                
                % * Pick interbout interval
                ibi_second = pb_ibi(itcount).*MI;
                ibi = round(ibi_second*framerate);
                
                newstep = currentstep + ibi;
                
                x(currentstep:newstep) = x(currentstep);
                y(currentstep:newstep) = y(currentstep);
                o(currentstep:newstep) = o(currentstep);
                
                % * Check for end of experiment
                if ~any(isnan(x))
                    break;
                end
                
                % * Pick displacement
                dsp = pb_dsp(itcount).*MD;
                
                % * Pick new state forward/turn
                if fts(currentstep) == 1
                    % Turn
                    reoang = pb_reo(itcount).*MR;
                    dsp = dsp*dsp_scale;    % displacement correction
                else
                    % Forward
                    reoang = pb_fwd(itcount).*MF;
                end
                
                % * Pick new state left/right
                rflip = rand;
                if rflip >= PF
                    % Do not flip
                    lrrts(newstep + 1) = lrrts(currentstep);
                else
                    % Flip
                    lrrts(newstep + 1) = -lrrts(currentstep);
                end
                
                tag = 0;    % avoid warning in parfor
                if lrrts(currentstep) == 1
                    % Left
                    tag = reoang;
                elseif lrrts(currentstep) == -1
                    % Right
                    tag = -reoang;
                end
                
                % * Get new position & orientation
                newo = wrapTo360(o(currentstep) + tag);
                [dx, dy] = pol2cart(deg2rad(newo), dsp);
                newx = x(currentstep) + dx;
                newy = y(currentstep) + dy;
                
                % * Boundary conditions
                while ~inpolygon(newx, newy, xbox, ybox)
                    
                    switch blim
                        case 'none'
                            % No boundary limits, continue even if out of box
                            break;
                            
                        case 'reflective'
                            % Get reflected point
                            
                            xE = x(currentstep);
                            yE = y(currentstep);
                            
                            % Avoid warnings in parfor loop
                            xF = NaN;
                            yF = NaN;
                            
                            % Find intersection point
                            [x0, y0, ii] = polyxpoly([xE, newx], [yE, newy], xbox, ybox);
                            
                            % Find angle of incidence on wall and get new mirrored
                            % coordinates
                            OE = sqrt((xE - x0)^2 + (yE - y0)^2);
                            if ii(2) == 1
                                % Left wall
                                incangle = atan((yE-y0)/(xE-x0));
                                xF = cos(incangle)*(dsp-OE) + x0;
                                yF = newy;
                            elseif ii(2) == 2
                                % Top wall
                                incangle = atan((x0-xE)/(y0-yE));
                                xF = newx;
                                yF = -cos(incangle)*(dsp-OE) + y0;
                            elseif ii(2) == 3
                                % Right wall
                                incangle = atan((yE-y0)/(x0-xE));
                                xF = -cos(incangle)*(dsp-OE) + x0;
                                yF = newy;
                            elseif ii(2) == 4
                                % Bottom wall
                                incangle = atan((xE-x0)/(yE-y0));
                                xF = newx;
                                yF = cos(incangle)*(dsp-OE) + y0;
                            end
                            
                            newx = xF;
                            newy = yF;
                            newo = wrapTo360(rad2deg(angle(xF - x0 + 1i*(yF - y0))));
                            
                        case 'startnew'
                            % End this trajectory and start a new one.
                            stopflag = true;
                            % Check if trajectory has enough bouts &
                            % check if three bout types are present
                            chktag = tags;
                            chktag(tags < -theta_threshold) = -1;
                            chktag(tags > theta_threshold) = +1;
                            chktag(abs(tags) < theta_threshold) = 0;
                            chktag(isnan(tags)) = [];
                            lasttime = find(~isnan(time), 1, 'last');
                            condtime = time(lasttime);
                            allconds = numel(unique(chktag)) == 3 && (sum(boutloc) >= nbouts || (~isempty(condtime) && condtime >= mintrajtime));
                            if allconds
                                isokay = true;
                                %                                 disp('ok');
                            else
                                flagredo = true;
                                %                                 disp('nope');
                            end
                            
                            break;
                    end
                end
                
                % * Save picked values
                boutloc(currentstep) = true;
                time(currentstep) = currentstep/framerate;
                ibis(currentstep) = ibi_second;
                dsps(currentstep) = dsp;
                tags(currentstep) = tag;
                
                % Check if the trajectory has to end
                if stopflag
                    break;
                end
                
                % * Save new step
                x(newstep + 1) = newx;
                y(newstep + 1) = newy;
                o(newstep + 1) = newo;
                
                % * Go onto next bout
                currentstep = newstep + 1;
                
                switch liveplot
                    case 'y'
                        p.XData = x;
                        p.YData = y;
                        axis auto
                        drawnow limitrate
                end
            end
            
            % Cut off & store
            time = time(1:nsteps);
            x = x(1:nsteps);
            y = y(1:nsteps);
            
            time = time(boutloc);
            x = x(boutloc);
            y = y(boutloc);
            tags(isnan(tags)) = [];
            ibis(isnan(ibis)) = [];
            dsps(isnan(dsps)) = [];
            
            allt{idT, fish} = time;
            allx{idT, fish} = x;
            ally{idT, fish} = y;
            alli{idT, fish} = ibis;
            alld{idT, fish} = dsps;
            allr{idT, fish} = tags;
            
            if ~flagredo
                isokay = true;
            end
        end
    end
    
    fprintf('\b\b\b\b\b\b\b\b\bDone (%2.2fs).\n', toc)
    
end

% --- Save file
outfile = [outbase blim '_' num2str(nfish) '.mat'];
if ~isempty(outfile)
    META = datetime;
    save(outfile, 'META', ...
        'allt', 'allx', 'ally', 'alli', 'alld', 'allr', ...
        'nfish', 'framerate', 'ntimes', 'blim');
end