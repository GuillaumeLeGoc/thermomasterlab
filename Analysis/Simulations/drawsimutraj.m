% Draws some trajectories from simulation

% close all
clear
clc

% --- Parameters
% * Settings
T = [18, 26, 33];
allT = [18, 22, 26, 30, 33];

ntrials = 20;
nbouts = 40;

% * Files
file = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'simu_mv_p_reflective_1000.mat'];

% * Figures
colors = rainbow(numel(allT));
basemarkersize = 32;

% --- Load data
data = load(file, 'alli', 'allx', 'ally');

for id = 1:ntrials
    
    % * Prepare figure
    fig = figure; hold on
    ax = gca;
    axis equal
    grid off
    box off
    axis off
    
    for idT = 1:numel(T)
        
        % Pick random iteration
        idexp = randi(size(data.allx, 2), 1);
        
        % - Get data
        x = data.allx{T(idT) == allT, idexp};
        y = data.ally{T(idT) == allT, idexp};
        i = data.alli{T(idT) == allT, idexp};
        
        % Pick random bouts range
        idstart = randi(numel(x) - nbouts, 1);
        idstop = idstart + nbouts;
        
        x = x(idstart:idstop);
        y = y(idstart:idstop);
        i = i(idstart:idstop);
        
        color = colors(T(idT) == allT, :);
        p = plot(ax, x, y);
        p.Color = [0.5, 0.5, 0.5, 0.5];
        p.LineWidth = 1;
        s = scatter(ax, x, y, i*16, color, 'filled');
        plot(ax, x(1), y(1), '.k');   % start point
        
    end
    % add legend
    scatter(5, 5, 1*basemarkersize, 'filled', 'MarkerFaceColor', 'k');
    str = '1 s';
    tt = text(2, 3, str);
    
    plot([10, 20], [10 10], 'k');
    str = '10mm';
    ttt = text(12, 8, str);
end