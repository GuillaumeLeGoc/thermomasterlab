% Plot old data densities

clear
clc

% --- Definitions
basepath = [pwd, filesep, 'Data', filesep, 'Datasets', filesep];

% Temperatures
T1 = 20;
T2 = 36;

% Pool
xl = 0;
xr = 100;

% Bins
nbins = 50;          % number of bins
Tbins = linspace(T1, T2, nbins + 1);
xbins = linspace(0, 100, nbins + 1);

nboots = 1000;

% Figures
nicered = [195, 49, 36]./255;
niceblk = [11, 23, 40]./255;

% Get list of files
listfile1 = dir([basepath 'gradient_' num2str(T1) '_' num2str(T2) '_*.mat']);
listfile2 = dir([basepath 'gradient_' num2str(T2) '_' num2str(T1) '_*.mat']);

% --- Get data
bl_allX1 = cell(numel(listfile1), 1);
gr_allT1 = cell(numel(listfile1), 1);
bl_pdf1 = NaN(nbins, numel(listfile1));
gr_pdf1 = NaN(nbins, numel(listfile1));
for ide = 1:numel(listfile1)

    D = load([basepath listfile1(ide).name]);
    
    % - Get temperatures
    Tl = D.Experiment.Tleft;
    Tr = D.Experiment.Tright;
    
    a = (Tl - Tr)./(xl - xr);
    b = (Tr*xl - Tl*xr)./(xl - xr);
    hT = @(x) a.*x + b;
    
    % Baseline
%     allX1 = D.xPosition_full_bl;
    allX1 = D.xPosition_bl;
    allX1 = cat(1, allX1{:});
    allX1(isnan(allX1)) = [];
    bl_allX1{ide} = allX1;
    
    % Gradient
%     allT1 = D.xPosition_full_gr;
    allT1 = D.xPosition_gr;
    allT1 = cat(1, allT1{:});
    allT1(isnan(allT1)) = [];
    allT1 = hT(allT1);
    gr_allT1{ide} = allT1;
    
    % Get pdf
    bl_pdf1(:, ide) = computePDF(xbins, allX1, 'mode', 'edges', 'method', 'hist');
    gr_pdf1(:, ide) = computePDF(Tbins, allT1, 'mode', 'edges', 'method', 'hist');
    
end

bl_allX2 = cell(numel(listfile2), 1);
gr_allT2 = cell(numel(listfile2), 1);
bl_pdf2 = NaN(nbins, numel(listfile2));
gr_pdf2 = NaN(nbins, numel(listfile2));
for ide = 1:numel(listfile2)
    
    D = load([basepath listfile2(ide).name]);
    
    % - Get temperatures
    Tl = D.Experiment.Tleft;
    Tr = D.Experiment.Tright;
    
    a = (Tl - Tr)./(xl - xr);
    b = (Tr*xl - Tl*xr)./(xl - xr);
    hT = @(x) a.*x + b;
    
    % Baseline
%     allX2 = D.xPosition_full_bl;
    allX2 = D.xPosition_bl;
    allX2 = cat(1, allX2{:});
    allX2(isnan(allX2)) = [];
    bl_allX2{ide} = allX2;
    
    % Gradient
%     allT2 = D.xPosition_full_gr;
    allT2 = D.xPosition_gr;
    allT2 = cat(1, allT2{:});
    allT2(isnan(allT2)) = [];
    allT2 = hT(allT2);
    gr_allT2{ide} = allT2;
    
    % Get pdf
    bl_pdf2(:, ide) = computePDF(xbins, allX2, 'mode', 'edges', 'method', 'hist');
    gr_pdf2(:, ide) = computePDF(xbins, allT2, 'mode', 'edges', 'method', 'hist');
    
end

% --- Pool data
allX = cat(1, bl_allX1{:}, bl_allX2{:});
allT = cat(1, gr_allT1{:}, gr_allT2{:});
bl_pdf = [bl_pdf1, bl_pdf2];
gr_pdf = [gr_pdf1, gr_pdf2];

% --- Pooled pdf
% bl_mpdf = mean(bl_pdf, 2);
% bl_epdf = std(bl_pdf, [], 2)./sqrt(size(bl_pdf, 2));
% gr_mpdf = mean(gr_pdf, 2);
% gr_epdf = std(gr_pdf, [], 2)./sqrt(size(gr_pdf, 2));

xcbins = edges2centers(xbins);
Tcbins = edges2centers(Tbins);

bl_mpdf = computePDF(xbins, allX, 'mode', 'edges', 'method', 'hist');
gr_mpdf = computePDF(Tbins, allT, 'mode', 'edges', 'method', 'hist');

% 95% confidence interval bootstrap
fun = @(x) computePDF(xbins, x, 'mode', 'edges', 'method', 'hist');
bl_epdf = bootci(nboots, {fun, allX});
fun = @(x) computePDF(Tbins, x, 'mode', 'edges', 'method', 'hist');
gr_epdf = bootci(nboots, {fun, allT});

% --- Pooled cdf
[blcdf, xbl] = ecdf(allX);
[grcdf, xgr] = ecdf(allT);

% --- Display

lineopt = struct;
lineopt.Color = niceblk;
lineopt.Marker = 'none';
% lineopt.MarkerSize = 4;
lineopt.LineWidth = 2;
lineopt.DisplayName = 'Baseline';
shadopt = struct;
shadopt.FaceAlpha = 0.275;
shadopt.DisplayName = 's.e.m.';

% PDF
figure;

% Baseline
axbl = axes; hold(axbl, 'on');
grid(axbl, 'off');

[eb, pb] = bierrorshaded(xcbins, bl_mpdf, bl_epdf(1, :), bl_epdf(2, :), ...
    'line', lineopt, 'patch', shadopt);

% pb = plot(axbl, xcbins, bl_mpdf);
% pb.LineWidth = 2;
% pb.Color = niceblk;
% pb.DisplayName = 'Baseline';

pub = plot(axbl, [xl, xr], [1/(xr - xl), 1/(xr - xl)], '--', ...
    'DisplayName', 'Uniform');
pub.Color = niceblk;
pub.LineWidth = 1.25;

convfac = (1/100)*16;

xlim(axbl, [0, 100]);
ylim(axbl, [0, 0.15*convfac]);
xlabel(axbl, 'position (mm)');
ylabel(axbl, '<pdf>_{batch} (baseline)');
axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;
axbl.XTick = [0, 50, 100];
axis(axbl, 'square');

% Gradient
axgr = axes; hold(axgr, 'on');
axgr.Position = axbl.Position;
axgr.XAxisLocation = 'top';
axgr.YAxisLocation = 'right';
axgr.Color = 'none';
axis(axgr, 'square');
grid(axgr, 'off');

lineopt.Color = nicered;
lineopt.DisplayName = 'Gradient';

[eg, pg] = bierrorshaded(Tcbins, gr_mpdf, gr_epdf(1, :), gr_epdf(2, :), ...
    'line', lineopt, 'patch', shadopt);
% pg = plot(Tcbins, gr_mpdf);
% pg.LineWidth = 2;
% pg.Color = nicered;
% pg.DisplayName = 'Gradient';

% pug = plot([Tl, Tr], [1/(Tr - Tl), 1/(Tr - Tl)], '--', ...
%     'DisplayName', 'Uniform gradient');
% pug.Color = nicered;
% pug.LineWidth = 1.25;

xlim(axgr, [T1, T2]);
ylim(axgr, [0, 0.15]);
xlabel(axgr, 'temperature (°C)');
ylabel(axgr, '<pdf>_{batch} (gradient)');
axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;

legend(axbl, [eb, pub], 'Location', 'northeast');
legend(axgr, eg, 'Location', 'northwest');

% CDF
[axbl, axgr] = make2axes;

plot(axbl, [0, 100], [0, 1], 'k--', 'LineWidth', 0.75);
plot(axgr, [T1, T2], [0.5, 0.5], 'k-.', 'LineWidth', 1);

p = plot(axbl, xbl, blcdf);
p.LineWidth = 3;
p.Color = niceblk;

q = plot(axgr, xgr, grcdf);
q.LineWidth = 3;
q.Color = nicered;

axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;
xlabel(axbl, 'position (mm)');
ylabel(axbl, 'cdf');
axis(axbl, 'square');

axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;
xlabel(axgr, 'temperature (°C)');
ylabel(axgr, 'cdf');
xlim(axgr, [T1, T2]);
axis(axgr, 'square');