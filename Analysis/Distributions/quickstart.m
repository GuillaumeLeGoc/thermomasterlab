% Quick plot of tracking data

% close all
clear
clc

filename = '/home/guillaume/Science/Projects/ThermoMaster/Data/2020-12-17/Run 02/Tracking_Result/tracking.txt';
metaname = '/home/guillaume/Science/Projects/ThermoMaster/Data/2020-12-17/Run 02/stamps.mat';

T = readtable(filename);
M = load(metaname);

framerate = 1/mean(diff(M.t));
pix_size = 0.09;

n_seq = max(unique(T.id));

pooled_x = cell(n_seq + 1, 1);
pooled_dangles = cell(n_seq + 1, 1);
pooled_ibi = cell(n_seq + 1, 1);
pooled_disp = cell(n_seq + 1, 1);

for id = 0:n_seq
    
    % Get features
    x = T.xBody(T.id == id);
    y = T.yBody(T.id == id);
    angles = T.tBody(T.id == id);
    
    if numel(x) < 10*framerate
        continue;
    end
    
    % Bouts detection
    B = Bouts.find(x, y, framerate, struct);
    
    if numel(B{:}) < 0.1*numel(x)/framerate
        continue;
    end
    
    % x position
    x_b = Bouts.limit(x, B);
    x_b(isnan(x_b)) = [];
    pooled_x{id + 1} = x_b;
    
    % Interbout interval
    ibi = B{1};
    pooled_ibi{id + 1} = diff(ibi);
    
    % Displacement
    y_b = Bouts.limit(y, B);
    y_b(isnan(y_b)) = [];
    dx = diff(x_b);
    dy = diff(y_b);
    pooled_disp{id + 1} = sqrt(dx.^2 + dy.^2);
    
    % Angles difference
    angles = rad2deg(wrapToPi(angles));
    angles = Bouts.limit(angles, B);
    angles(isnan(angles)) = [];
    
    dangles = NaN(size(angles, 1) - 1, 1);
    for idco = 1:size(angles, 1) - 1
        source = angles(idco);
        target = angles(idco + 1);
        delta = target - source;
        % Take minimal angle
        if delta > 180
            delta = delta - 360;
        elseif delta < 180
            delta = delta + 360;
        end
        dangles(idco) = wrapTo180(delta);
    end
   
    pooled_dangles{id + 1} = dangles;
    
end

pooled_x = cat(1, pooled_x{:});
pooled_ibi = cat(1, pooled_ibi{:});
pooled_disp = cat(1, pooled_disp{:});
pooled_dangles = cat(1, pooled_dangles{:});

figure;
histogram(pooled_x);
title('X position');

figure;
histogram(pooled_ibi/framerate, 0:0.1:6);
xlabel('Interbout interval [s]');
title('Interbout interval');

figure;
histogram(pooled_disp*pix_size);
xlabel('Displacement [mm]');
title('Displacement');

figure;
histogram(pooled_dangles);
xlabel('\Delta{\theta} [deg]');
title('\Delta{\Theta}');

figure; hold on
p = plot(M.t, M.T_right);
p.Color(4) = 0.5;
p = plot(M.t, M.T_left);
p.Color(4) = 0.5;
xlabel('Time [s]');
ylabel('Temperature [°C]');
title('Temperature');
legend('Right', 'Left');