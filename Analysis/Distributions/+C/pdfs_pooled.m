% Compare distributions with different chemicals

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Temperature & chemicals
T = 26;
chemicals = {'E3', 'Ethanol', 'Menthol'};
concentrations = [1, 0.2, 0.2];

% * Reference PDFs
fname_pdf = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pooled_pdf_T' num2str(T) '.mat'];

% * Filters
filt.seq_length = 20;    % sequence length for each fish in seconds
filt.bout_freq = .01;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Bins
method = 'kde';        % Method for PDF
mode = 'edges';         % Mode for bins
bws = [1, 0.1, 0.1, 1];	% Bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 200;

e.x = [100*pixsize, 1100*pixsize]; % bins centers for x (mm)
e.ibi = [0, 10];                    % bins for interbout interval (s)
e.disp = [0, 15];                   % bins for displacement (mm)
e.turn = [-180, 180];              % bins for turn angle (°, wrapped to 180)

% * Figures names
fignames = {'Xpositions', 'InterboutIntervals', 'Displacements', 'TurnAngles'};

% --- Prepare bin vectors
switch mode
    case 'edges'
        bx = linspace(e.x(1), e.x(2), nbins+1)';
        bi = linspace(e.ibi(1), e.ibi(2), nbins+1)';
        bd = linspace(e.disp(1), e.disp(2), nbins+1)';
        bt = linspace(e.turn(1), e.turn(2), 120+1)';
    case 'centers'
        bx = linspace(e.x(1), e.x(2), nbins)';
        bi = linspace(e.ibi(1), e.ibi(2), nbins)';
        bd = linspace(e.disp(1), e.disp(2), nbins)';
        bt = linspace(e.turn(1), e.turn(2), 120)';
end

% --- Prepare figures
colors = paired(2*numel(chemicals));
colors = colors(2:2:end, :);
figs = cell(4, 1);
for idx_fig = 1:4
    figs{idx_fig} = figure; hold on
end

% --- Prepare arrays
Xp = cell(numel(chemicals), 1);
Ib = cell(numel(chemicals), 1);
Di = cell(numel(chemicals), 1);
Tu = cell(numel(chemicals), 1);

% --- Loop over each temperatures and compute features PDF
for idC = 1:numel(chemicals)
    
    chem = chemicals{idC};
    conc = concentrations(idC);
    
    fprintf(['Computing distributions for ', chem]); tic
    
    % Build file names
    [ftrack, fstamp, units, nfish] = getChemicalsList(T, chem, conc);
    
    % Get features from each experiment
    x = cell(size(ftrack, 1), 1);
    i = cell(size(ftrack, 1), 1);
    d = cell(size(ftrack, 1), 1);
    t = cell(size(ftrack, 1), 1);
    f = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [x{idx_exp}, i{idx_exp}, d{idx_exp}, t{idx_exp}, ~, ~, f{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        fprintf('.');
    end
    
    % Gather features
    framerate = mean(cat(1, f{:}));
    allx = cat(1, x{:}).*pixsize;
    alli = cat(1, i{:})./framerate;
    alld = cat(1, d{:}).*pixsize;
    allt = cat(1, t{:});
    
    % Get PDF
    switch method
        case 'hist'
            [pdfx, cx] = computePDF(bx, allx, 'method', method, 'mode', mode);
            [pdfi, ci] = computePDF(bi, alli, 'method', method, 'mode', mode);
            [pdfd, cd] = computePDF(bd, alld, 'method', method, 'mode', mode);
            [pdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode);
        case 'kde'
            [pdfx, cx] = computePDF(bx, allx, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(1)});
            [pdfi, ci] = computePDF(bi, alli, 'method', method, 'mode', mode, 'param', kdeopt(2));
            [pdfd, cd] = computePDF(bd, alld, 'method', method, 'mode', mode, 'param', kdeopt(3));
            [pdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(4)});
    end
    
    % Get CDF
    switch method
        case 'hist'
            [cdfx, ccx] = computeCDF(bx, allx, 'method', method, 'mode', mode);
            [cdfi, cci] = computeCDF(bi, alli, 'method', method, 'mode', mode);
            [cdfd, ccd] = computeCDF(bd, alld, 'method', method, 'mode', mode);
            [cdft, cct] = computeCDF(bt, allt, 'method', method, 'mode', mode);
        case 'kde'
            [cdfx, ccx] = computeCDF(bx, allx, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(1)});
            [cdfi, cci] = computeCDF(bi, alli, 'method', method, 'mode', mode, 'param', kdeopt(2));
            [cdfd, ccd] = computeCDF(bd, alld, 'method', method, 'mode', mode, 'param', kdeopt(3));
            [cdft, cct] = computeCDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(4)});
    end
    
    % Get CDF
%     [cdfx, ccx] = ecdf(allx);
%     [cdfi, cci] = ecdf(alli);
%     [cdfd, ccd] = ecdf(alld);
%     [cdft, cct] = ecdf(allt);
    
    % Store all data
    Xp{idC} = allx;
    Ib{idC} = alli;
    Di{idC} = alld;
    Tu{idC} = allt;
    
    % Get statistics
    mx = trapz(cx, cx.*pdfx);
    mi = trapz(ci, ci.*pdfi);
    md = trapz(cd, cd.*pdfd);
    mt = trapz(ct, ct.*pdft);
    
    nbouts = num2str(numel(alli));
    nfish = num2str(sum(cell2mat(nfish)));
    fprintf('\t Done (%s bouts, %2.2fs).\n', nbouts, toc);
    
    % --- Display
    dispname = [chem ' ' num2str(conc) units{1}];

    set(0, 'CurrentFigure', figs{1})
    lineopt = plot(cx, pdfx);
    lineopt.DisplayName = dispname;
    lineopt.Color = colors(idC, :);
    lineopt.LineWidth = 1.5;
    erb{1}{idC} = lineopt;
    xlabel('Position [mm]');
    ylabel('PDF');
    ax = gca;
    plot([mx mx], ax.YLim, 'Color', colors(idC, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{2});
    lineopt = plot(ci, pdfi);
    lineopt.DisplayName = dispname;
    lineopt.Color = colors(idC, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.5;
    erb{2}{idC} = lineopt;
    xlabel('Interbout interval [s]');
    ylabel('PDF');
    ax = gca;
    plot([mi mi], ax.YLim, 'Color', colors(idC, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{3});
    lineopt = plot(cd, pdfd);
    lineopt.DisplayName = dispname;
    lineopt.Color = colors(idC, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.75;
    erb{3}{idC} = lineopt;
    xlabel('Displacement [mm]');
    ylabel('PDF');
    ax = gca;
    plot([md md], ax.YLim, 'Color', colors(idC, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{4});
    lineopt = plot(ct, pdft);
    lineopt.DisplayName = dispname;
    lineopt.Color = colors(idC, :);
    lineopt.MarkerSize = 5;
    lineopt.LineWidth = 1.5;
    erb{4}{idC} = lineopt;
    xlabel('\delta{\theta} [deg]');
    ylabel('PDF');
    ax = gca; ax.YScale = 'log';
    ax.XLim = [e.turn(1), e.turn(2)];
    ax.YLim = [1e-4, 1e-1];
    plot([mt, mt], ax.YLim, 'Color', colors(idC, :), 'LineStyle', '--', 'LineWidth', .4);
    
end

% --- Add reference PDF & legends
mat_pdfs = matfile(fname_pdf);
ref_bins = {mat_pdfs.cx, mat_pdfs.ci, mat_pdfs.cd, mat_pdfs.ct};
ref_pdfs = {mat_pdfs.pdfx, mat_pdfs.pdfi, mat_pdfs.pdfd, mat_pdfs.pdft};

fkm = plot(NaN, NaN, 'k--', 'DisplayName', 'Mean', 'LineWidth', .6);
for idx_fig = 1:numel(figs)
    set(0, 'CurrentFigure', figs{idx_fig});
    ref_plot = plot(ref_bins{idx_fig}, ref_pdfs{idx_fig});
    ref_plot.DisplayName = ['Reference T=' num2str(T) '°C'];
    ref_plot.Color = [0.1, 0.1, 0.1];
    legend([erb{idx_fig}{:}, ref_plot, fkm]);
    figs{idx_fig}.Name = fignames{idx_fig};
    axis square
end