% Estimate variance of turn angles of turns and forwards.

% close all
clear
clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
% filt.bout_freq = .1;    % minimum average bout frequency
filt.n_bouts = 10;
filt.minx = 0;
filt.maxx = Inf;

% * Statistical test
statest = 'kruskalwallis';

theta_threshold = 10;

colors = rainbow(numel(T));

% --- Processing
allmturn = cell(numel(T), 1);
allmfowd = cell(numel(T), 1);
for idT = 1:numel(T)
        
    temperature = T(idT);
    
    fprintf(['Computing turn angle variances for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    allmturn{idT} = NaN(size(ftrack, 1), 1);
    allmfowd{idT} = NaN(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangles] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, true);
        
        turns = turnangles(abs(turnangles) > theta_threshold);
        fowds = turnangles(abs(turnangles) < theta_threshold);
        
%         allvarturn{idT}(idx_exp) = var(turns);
%         allvarfowd{idT}(idx_exp) = var(fowds);
        allmturn{idT}(idx_exp) = mean(abs(turns));
        allmfowd{idT}(idx_exp) = mean(abs(fowds));
        
        fprintf('.');
    end
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
end

% --- Display
% * Turns
maxsize = max(cellfun(@numel, allmturn));
allmturn = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), allmturn, 'UniformOutput', false);
g = cell(size(allmturn));
for idx = 1:size(allmturn, 1)
    g{idx} = idx.*ones(numel(allmturn{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);
X = cat(1, allmturn{:});

f = figure; hold on
f.Name = 'MeanTurn';
opt = struct;
opt.Color = colors;
boxopt = struct;
boxopt.BoxColors = colors;
boxopt.Labels = labels;
beautifulbox(deg2rad(X), g, opt, boxopt);

title('Mean of turn bouts');
ylabel('<|\delta{\theta_{turn}}|>_{batch} (rad)');
% axis([-Inf Inf 20 60]);
xlim([-Inf Inf]);
ax = gca;
% ax.YTick = [20, 40, 60];

pairs = {[1, 2], [2, 3], [3, 4], [4, 5]};
addStars(allmturn, 'pairs', pairs, 'test', statest);
axis square
ax.YGrid = 'off';
ax.FontSize = 16;

% * Forwards
maxsize = max(cellfun(@numel, allmfowd));
allmfowd = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), allmfowd, 'UniformOutput', false);
X = cat(1, allmfowd{:});

f = figure; hold on
f.Name = 'MeanForward';
beautifulbox(X, g, opt, boxopt);
title('Mean of forward bouts');
ylabel('<\delta{\theta_{forward}}>_{batch} [deg]');
axis([-Inf Inf 0 5]);

pairs = {[2, 3], [4, 5]};
addStars(allmfowd, 'pairs', pairs, 'test', statest);
axis square