% Turn angles autocorrelation where absolute value of turn angle is taken
% to check if there is a persistence between forward/turn states.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

theta_threshold = 6;   % deg

% * Window
n_corr = 20;            % in bouts
n_corr_time = 500;      % in frames

% * Filters
filt.seq_length = 20;	% min sequence length for each fish in seconds
filt.bout_freq = .01;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
figs.bouts = figure; hold on
figs.bouts.Name = 'TurnAngleACFBoutAbs';
figs.time = figure; hold on
figs.time.Name = 'TurnAngleACFTimeAbs';

% --- Processing
e = cell(numel(T), 1);
et = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing autocorrelations for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get mean autocorrelation for each experiment
    
    pooled_xco = cell(size(ftrack, 1), 1);
    pooled_xco_time = cell(size(ftrack, 1), 1);
    pooled_lagstime = cell(size(ftrack, 1), n_corr_time + 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        pxco = NaN(n_seq, n_corr + 1);
        pxco_time = NaN(n_seq, n_corr_time + 1);
        lagstime = NaN(n_seq, n_corr_time + 1);
        counter = 0;
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            % Binarize turn angles
            dthetacopy = dtheta;
            dtheta(abs(dthetacopy) > theta_threshold) = 1;
            dtheta(abs(dthetacopy) < theta_threshold) = 0;
            
            if numel(dtheta) < 2*n_corr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            [xco, lag] = xcorr(dtheta, n_corr, 'biased');
            selected_inds = find(lag == 0):find(lag == 0) + n_corr;
            pxco(seq, :) = xco(selected_inds)./max(xco(selected_inds));  % norm
            
            % Time
            afr = allframes(:, seq) + 1;
            afr(isnan(afr)) = [];
            dthetatime = NaN(size(afr));
            fr = frames(:, seq) + 1;
            fr(isnan(fr)) = [];
            fr = fr - fr(1);
            fr(1) = [];
            dthetatime(fr) = dtheta;
            dthetatime = fillmissing(dthetatime, 'previous');
            dthetatime = fillmissing(dthetatime, 'next');
            [xcotime, lagtime] = xcorr(dthetatime, 'biased');
            selected_inds = find(lagtime == 0):find(lagtime == 0) + n_corr_time;
            pxco_time(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));  % norm
            lagstime(seq, :) = lagtime(selected_inds)./framerate;
        end
        
        pooled_xco{idx_exp} = pxco;
        pooled_xco_time{idx_exp} = pxco_time;
        pooled_lagstime{idx_exp} = lagstime;
        
        fprintf('.');
    end
    
    % Pool
    pooled_xco = cat(1, pooled_xco{:});
    pooled_xco_time = cat(1, pooled_xco_time{:});
    pooled_lagstime = cat(1, pooled_lagstime{:});
    
    % Compute mean across batch
    mxco = nanmean(pooled_xco, 1);
    exco = nanstd(pooled_xco, [], 1)/sqrt(size(pooled_xco, 1));
    mxcot = nanmean(pooled_xco_time, 1);
    excot = nanstd(pooled_xco_time, [], 1)/sqrt(size(pooled_xco, 1));
    lxcot = nanmean(pooled_lagstime, 1);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = 'none';
    shadopt.FaceAlpha = 0.275;
    
    % Bouts
    set(0, 'CurrentFigure', figs.bouts);
    e{idT} = errorshaded(1:n_corr, mxco(2:end), exco(2:end), 'line', lineopt, 'patch', shadopt);
    xlabel('# Bout');
    ylabel('Autocorrelation of \Delta{\theta}');
    title('Bout autocorrelations');
    
    % Time
    set(0, 'CurrentFigure', figs.time);
    lineopt.MarkerSize = 4;
    et{idT} = errorshaded(lxcot, mxcot, excot, 'line', lineopt);
    xlabel('Time [s]');
    ylabel('Autocorrelation of \Delta{\theta}');
    title('Time autocorrelations');
    
    fprintf('\t Done (%2.2fs).\n', toc);
end

% Display legends
set(0, 'CurrentFigure', figs.bouts);
axis square
legend([e{:}]);
set(0, 'CurrentFigure', figs.time);
legend([et{:}]);
axis square