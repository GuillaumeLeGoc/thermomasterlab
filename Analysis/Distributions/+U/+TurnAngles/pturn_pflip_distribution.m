% pturn and pflip distributions & rescaling

% close all
clear
clc

% --- Parameters
%  * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_out0 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ptpfmeanT.mat'];
fn_out1 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ptpfdistY.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

usepar = true;  % use parralel for bootstrap

% * Bins
nbins = 80;
method = 'kde';
mode = 'edges';
bturn_lim = [0, 1];
bflip_lim = [0, 1];
bturn_n_lim = [0, 4];
bflip_n_lim = [0, 4];
kdeopt = {'Bandwidth', 0.08, 'Support', 'Positive'};
kdeopt_n = {'Bandwidth', 0.08, 'Support', 'Positive'};

% * Confidence interval
nboots = 1000;

% * Threshold
theta_threshold = 10;   % deg

% * Load data
data = load(fn_dataset, 'dtheta');

% --- Prepare arrays & bins
switch mode
    case 'edges'
        bturn = linspace(bturn_lim(1), bturn_lim(2), nbins + 1)';
        bflip = linspace(bflip_lim(1), bflip_lim(2), nbins + 1)';
        bturn_n = linspace(bturn_n_lim(1), bturn_n_lim(2), nbins + 1)';
        bflip_n = linspace(bflip_n_lim(1), bflip_n_lim(2), nbins + 1)';
    case 'centers'
        bturn = linspace(bturn_lim(1), bturn_lim(2), nbins)';
        bflip = linspace(bflip_lim(1), bflip_lim(2), nbins)';
        bturn_n = linspace(bturn_n_lim(1), bturn_n_lim(2), nbins)';
        bflip_n = linspace(bflip_n_lim(1), bflip_n_lim(2), nbins)';
end
cdfturn_n = NaN(nbins, numel(T));
cdfflip_n = NaN(nbins, numel(T));
mpturn = NaN(numel(T), 1);
mpflip = NaN(numel(T), 1);
ci_mpturn = NaN(numel(T), 2);
ci_mpflip = NaN(numel(T), 2);
allpturn_n = cell(numel(T), 1);
allpflip_n = cell(numel(T), 1);

% --- Prepare figures
figt = figure; axt = gca; hold on
figf = figure; axf = gca; hold on
figtn = figure; axtn = gca; hold on
figtf = figure; axfn = gca; hold on
figctn = figure; axctn = gca; hold on
figcfn = figure; axcfn = gca; hold on

colors = rainbow(numel(T));
msize = 8;

p = cell(numel(T), 1);
pn = cell(numel(T), 1);
q = cell(numel(T), 1);
qn = cell(numel(T), 1);

lineopt = struct;
lineopt.MarkerSize = 8;
lineopt.LineWidth = 1.25;
shadopt = struct;
shadopt.FaceAlpha = 0.275;

% --- Main loop
for idT = 1:numel(T)
    
    fprintf(['Estimating turn and flip prob. for T = ', num2str(T(idT)) '°C...']); tic
    
    % Get data
    turnangle = data.dtheta{idT};
    nseq = size(turnangle, 2);
    
    allpflip = NaN(nseq, 1);
    allpturn = NaN(nseq, 1);
	toclean = false(nseq, 1);
    
    for seq = 1:nseq
        
        % - Prepare data
        dtheta = turnangle(:, seq);
        dtheta(isnan(dtheta)) = [];
        
        % - Estimate pturn
        nbouts = numel(dtheta);
        nturns = sum(abs(dtheta) > theta_threshold);
        pt = nturns/nbouts;
        allpturn(seq) = pt;
        
        % - Estimate pflip
        dtheta = dtheta - mean(dtheta);  % bias correction
        % Trinarize
        dthetacopy = dtheta;
        dtheta(abs(dthetacopy) <= theta_threshold) = 0;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
        
        % Create lagged vectors
        dthetai = dtheta(1:end  - 1);
        dthetan = dtheta(2:end);
        % Binning
        binedges = [-1, 0, 1, 1 + eps];
        bindti = discretize(dthetai, binedges);                 % regroup in bins
        meandtn = accumarray(bindti, dthetan, [], @mean, NaN);  % average over bins  % average over bins
        
        if any(isnan(meandtn)) || numel(meandtn) < 3
            toclean(seq) = true;
            continue;
        end
        
        % Fit
        yfit = @(pflip, x) pt.*(1-2*pflip).*x;
        ft = fit((-1:1)', meandtn, yfit, 'StartPoint', 0.25, ...
            'Lower', 0, 'Upper', 1);
        if isequal(ft.pflip, 0) || isequal(ft.pflip, 1)
            toclean(seq) = true;    % ignore failed fit
            continue;
        else
            allpflip(seq) = ft.pflip;
        end
    end
    
    % Cleanup
    allpflip(toclean) = [];
    allpturn(toclean) = [];
    
    mpturn(idT) = mean(allpturn);
    mpflip(idT) = mean(allpflip);
    ci_mpturn(idT, :) = bootci(nboots, {@mean, allpturn}, 'Options', statset('UseParallel', usepar))';
    ci_mpflip(idT, :) = bootci(nboots, {@mean, allpflip}, 'Options', statset('UseParallel', usepar))';
    
    % Rescale
    allpturn_n{idT} = allpturn./mpturn(idT);
    allpflip_n{idT} = allpflip./mpflip(idT);
    
    % Get pdf & cdf
    switch method
        case 'hist'
            [pdfturn, cturn] = computePDF(bturn, allpturn, 'method', method, 'mode', mode);
            [pdfflip, cflip] = computePDF(bflip, allpflip, 'method', method, 'mode', mode);
    
            [pdfturn_n, cturn_n] = computePDF(bturn_n, allpturn_n{idT}, 'method', method, 'mode', mode);
            [pdfflip_n, cflip_n] = computePDF(bflip_n, allpflip_n{idT}, 'method', method, 'mode', mode);
            
            cdfturn_n(:, idT) = computeCDF(bturn_n, allpturn_n{idT}, 'method', method, 'mode', mode);
            cdfflip_n(:, idT) = computeCDF(bflip_n, allpflip_n{idT}, 'method', method, 'mode', mode);
        case 'kde'
            [pdfturn, cturn] = computePDF(bturn, allpturn, 'method', method, 'mode', mode, 'param', kdeopt);
            [pdfflip, cflip] = computePDF(bflip, allpflip, 'method', method, 'mode', mode, 'param', kdeopt);
    
            [pdfturn_n, cturn_n] = computePDF(bturn_n, allpturn_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
            [pdfflip_n, cflip_n] = computePDF(bflip_n, allpflip_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
            
            cdfturn_n(:, idT) = computeCDF(bturn_n, allpturn_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
            cdfflip_n(:, idT) = computeCDF(bflip_n, allpflip_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
    end
    
    % Get conf. int.
    fun = @(x) computePDF(bturn_n, x, 'method', method, 'mode', mode, 'param', kdeopt);
    errturn_n = bootci(nboots, {fun, allpturn_n{idT}}, 'Options', statset('UseParallel', usepar));
    errturn_n(1, isnan(errturn_n(1, :))) = min(errturn_n(1, :));
    errturn_n(2, isnan(errturn_n(2, :))) = min(errturn_n(2, :));
    
    fun = @(x) computePDF(bflip_n, x, 'method', method, 'mode', mode, 'param', kdeopt);
    errflip_n = bootci(nboots, {fun, allpflip_n{idT}}, 'Options', statset('UseParallel', usepar));
    
    % --- Display
    lineopt.DisplayName = ['T=' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    
    p{idT} = plot(axt, cturn, pdfturn);
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    p{idT}.Color = colors(idT, :);
    plot(axt, [mpturn(idT), mpturn(idT)], axt.YLim, '--', 'Color', colors(idT, :));
    
    pn{idT} = bierrorshaded(cturn_n, pdfturn_n, errturn_n(2, :), errturn_n(1, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axtn);
    
    q{idT} = plot(axf, cflip, pdfflip);
    q{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    q{idT}.Color = colors(idT, :);
    plot(axf, [mpflip(idT), mpflip(idT)], axf.YLim, '--', 'Color', colors(idT, :));
    
    qn{idT} = bierrorshaded(cflip_n, pdfflip_n, errflip_n(2, :), errflip_n(1, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axfn);
    
    fprintf('\t Done (%2.2fs).\n', toc);
   
end

% --- Mean rescaled cdf
mcdfYptr = mean(cdfturn_n, 2);
mcdfYpfp = mean(cdfflip_n, 2);
binsyptr = cturn_n;
binsypfp = cflip_n;

% --- Display cdf
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);

axctn.ColorOrder = colors;
plot(axctn, cturn_n, cdfturn_n);
legend(axctn, labels, 'Location', 'southeast');
plot(axctn, binsyptr, mcdfYptr, 'k--', 'DisplayName', 'Pool');
xlabel(axctn, 'p_{turn}/<p_{turn}>_T');
ylabel(axctn, 'cdf');
axis(axctn, 'square');

axcfn.ColorOrder = colors;
plot(axcfn, cflip_n, cdfflip_n);
legend(axcfn, labels, 'Location', 'southeast');
plot(axcfn, binsypfp, mcdfYpfp, 'k--', 'DisplayName', 'Pool');
xlabel(axcfn, 'p_{flip}/<p_{flip}>_T');
ylabel(axcfn, 'cdf');
axis(axcfn, 'square');

% Cosmetic
axis(axt, 'square');
xlabel(axt, 'p_{turn}');
ylabel(axt, 'pdf');
legend(axt, [p{:}]);

axis(axtn, 'square');
xlabel(axtn, 'p_{turn}/<p_{turn}>_T');
ylabel(axtn, 'pdf');
legend(axtn, [pn{:}]);

axis(axf, 'square');
xlabel(axf, 'p_{flip}');
ylabel(axf, 'pdf');
legend(axf, [q{:}]);

axis(axfn, 'square');
xlabel(axfn, 'p_{flip}/<p_{flip}>_T');
ylabel(axfn, 'pdf');
legend(axfn, [qn{:}]);

figure('Name', 'MeansPTR');
ax = gca; hold on
p = plot(T, mpturn);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mpturn, 'k');
xlabel('Temperature [°C]');
ylabel('<p_{turn}>_T');
ax.XTick = T;
xlim([17 34])
axis square

figure('Name', 'MeansPFP');
ax = gca; hold on
p = plot(T, mpflip);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mpflip, 'k');
xlabel('Temperature [°C]');
ylabel('<p_{flip}>_T');
ax.XTick = T;
xlim([17 34])
axis square

% --- Save
if ~isempty(fn_out0)
    save(fn_out0, 'mpturn', 'ci_mpturn', 'mpflip', 'ci_mpflip');
end
if ~isempty(fn_out1)
    save(fn_out1, 'binsyptr', 'mcdfYptr', 'binsypfp', 'mcdfYpfp');
end