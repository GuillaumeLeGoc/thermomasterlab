% Plots autocorrelation of turn bouts versus the interbout interval to
% extract a temporal pflip. One curve for one experiment.

% close all
clear
clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
theta_threshold = 6;   % deg
ncorr = 10;
nbins = 50;

% * Filters
filt.seq_length = 20;	% min sequence length for each fish in seconds
filt.bout_freq = .01;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Figures
colors = rainbow(numel(T));
figs = cell(numel(T), 1);
fig = figure; hold on
fig.Name = 'AutocorrIBI';

% --- Processing
tau = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Prep
    tau{idT} = NaN(size(ftrack, 1), 1);
    figs{idT} = figure; hold on
    xlabel('\Delta{T} [s]');
    ylabel('<\delta{\theta}(t).\delta{\theta}(t+\Delta{t})>');
    title(['Autocorrelation binned by IBI, T = ' num2str(temperature) '°C']);
    axis square
    
    % Process
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        n_seq = size(turnangle, 2);
        
        % Build sequences
        deltat = cell(n_seq, 1);
        autocorr = cell(n_seq, 1);
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            timebout = frames(:, seq)./framerate;
            timebout(isnan(timebout)) = [];
            timebout = (timebout - timebout(1));
            
            % Trinarize turn angles
            dthetacopy = dtheta;
            %             dtheta(dthetacopy > theta_threshold) = 1;
            %             dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = 0;
%             timebout(abs(dthetacopy) < theta_threshold) = [];
            
            if numel(dtheta) < 2*ncorr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            var1 = NaN(numel(dtheta) - 1, ncorr);
            var2 = NaN(numel(dtheta) - 1, ncorr);
            
            % Manual autocorrelation
            for idx = 1:ncorr
                
                dtheta_slide1 = dtheta(1:(end - idx));
                dtheta_slide2 = dtheta((1 + idx):end);
                correl_dtheta = dtheta_slide1.*dtheta_slide2;
                absprod_dtheta = abs(dtheta_slide1).*abs(dtheta_slide2);
                
                var1(1:(end - idx + 1), idx) = timebout((1 + idx):(end - 1)) - timebout(1:(end - idx - 1));
                var2(1:(end - idx + 1), idx) = correl_dtheta./absprod_dtheta;
                
            end
            
            % Linearize & store
            deltat{seq} = var1(:);
            autocorr{seq} = var2(:);
            
        end
        
        % Pool all events
        deltat = cat(1, deltat{:});
        autocorr = cat(1, autocorr{:});
        
        % Bins with same number of elements per bins
        [bins_deltat, elmts_per_bins, v2bin1] = BinsWithEqualNbofElements(deltat, autocorr, nbins, nbins + 3);
        
        meanautocorr = mean(v2bin1,2);
        errorautocorr = std(v2bin1,1,2)./sqrt(elmts_per_bins);
        
        % - Fit
        g = fittype(@(tau, x) exp(-x/(2*tau)));
        fitt = fit(bins_deltat, meanautocorr, g, 'StartPoint', 5, 'Lower', 0, 'Upper', 20);
        
        tau{idT}(idx_exp) = fitt.tau;
        
        % - Display
        set(0, 'CurrentFigure', figs{idT});
        
        lineopt = struct;
        lineopt.Color = colors(idT, :);
        lineopt.MarkerSize = 8;
        lineopt.LineWidth = 2;
        lineopt.LineStyle = 'none';
        lineopt.Marker = '.';
        shadopt = struct;
        shadopt.FaceAlpha = 0.275;
        erb = errorshaded(bins_deltat, meanautocorr, errorautocorr, 'line', lineopt, 'patch', shadopt);
        p = plot(bins_deltat, fitt(bins_deltat));
        p.Color = colors(idT, :);
    end
    
end

% --- Display
maxsize = max(cellfun(@numel, tau));
tau = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), tau, 'UniformOutput', false);
g = cell(size(tau));
for idx = 1:size(tau, 1)
    g{idx} = idx.*ones(numel(tau{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);
X = cat(1, tau{:});

set(0, 'CurrentFigure', fig);
lineopt = struct;
lineopt.Color = colors;
lineopt.MarkerFaceColor = 'same';
boxopt = struct;
boxopt.BoxColors = colors;
beautifulbox(X, g, lineopt, boxopt, 'Labels', labels);

title('\tau est. from autocorrelation fit');
ylabel('\tau [s]');
axis([-Inf Inf 0 1.5]);
axis square