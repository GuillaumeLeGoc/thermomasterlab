% Compute time intervals before left/right switch.

clear
clc

% --- Definitions
filename = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences_perbatch.mat'];

T = [18, 22, 26, 30, 33];   % tested temperatures (°C)

theta_threshold = 10;       % threshold for forward/turn (°)

% Bins
nbins = 75;
edges = linspace(0, 30, nbins);
bw = 0.5;

% Figure
colors = rainbow(numel(T));
f = figure('Name', 'LR_intervals'); ax = axes(f); hold(ax, 'on');

% --- Load data
data = load(filename);

% --- Processing
fprintf('Computing intervals bewteen changes'); tic

for idT = 1:numel(T)
        	
    % Get data
    sreo = data.dtheta{idT};
    stime = data.bouttime{idT};
    framerate = mean([data.framerates{idT}{:}]);
    
    % Pool batches
    reo = fixCellSize(sreo, 1);
    reo = cat(2, reo{:});
    time = fixCellSize(stime, 1);
    time = cat(2, time{:});
   
    % number of tracks
    nseq = size(reo, 2);
	
    % Init.
    intervals = cell(nseq, 1);
    
    for seq = 1:nseq
        
        % Get sequence
        dtheta = reo(:, seq);
        dtheta(isnan(dtheta)) = [];
        
        % Trinarize
        dtheta = dtheta - mean(dtheta);  % bias correction
        dthetacopy = dtheta;
        dtheta(abs(dthetacopy) <= theta_threshold) = 0;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
        
        % Reconstruct telegraph time-continuous signal
        dtheta(dtheta == 0) = NaN;      % remove forwards
        t = time(:, seq);
        t(isnan(t)) = [];
        tvec = t(1:end - 1) - t(1);     % start at 0
        fvec = round(tvec*framerate);   % convert in frames
        fr = fvec(1):fvec(end);         % create full time vector
        dthetatime = NaN(size(fr));
        dthetatime(fvec + 1) = dtheta;  % fill with actual values
        dthetatime = fillmissing(dthetatime, 'previous');   % fill missing with previous
        dthetatime = fillmissing(dthetatime, 'next');
        
        % Check if fish changes direction
        if numel(unique(dthetatime)) == 1
            continue;
        end

        % Compute intervals between flips
        dthetatime(dthetatime == -1) = 0;
        ds = diff(dthetatime);
        flipsloc = find(ds);
        restime = [flipsloc(1), diff(flipsloc)];
        restime = restime./framerate;

        % Store
        intervals{seq} = restime;
        
    end
	
    % Pool data
    intervals(cellfun(@isempty, intervals)) = [];
    pintervals = cat(2, intervals{:});

    % Compute pdf
%     [pdfintervals, centers] = computePDF(edges, pintervals, 'mode', 'edges', 'method', 'kde', 'param', {'Bandwidth', bw});
[pdfintervals, centers] = computePDF(edges, pintervals, 'mode', 'edges', 'method', 'hist');

    % --- Display
    plot(centers, pdfintervals, 'Color', colors(idT, :), ...
        'DisplayName', num2str(T(idT)), ...
        'Linewidth', 1.5);
    
    fprintf('.');
end

fprintf('\t(Done %2.2fs)\n', toc);

% --- Cosmetic
xlabel(ax, 'intervals between changes (s)');
ylabel(ax, 'pdf');
ax.XGrid = 'off';
lg = legend;
title(lg, 'temperature (°C)');
lg.Box = 'off';