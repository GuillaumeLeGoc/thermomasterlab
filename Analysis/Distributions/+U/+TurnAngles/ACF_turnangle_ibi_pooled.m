% Plots autocorrelation of turn bouts versus the interbout interval to
% extract a temporal pflip. All events pooled across experiments.

% close all
clear
clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
theta_threshold = 6;   % deg
ncorr = 10;
nbins = 50;

% * Filters
filt.seq_length = 20;	% min sequence length for each fish in seconds
filt.bout_freq = .01;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Figures
fig = figure; hold on
fig.Name = 'AutocorrIBI_pooled';
colors = rainbow(numel(T));
erb = cell(numel(T), 1);

% --- Processing
tau = NaN(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    
    pooled_deltat = cell(size(ftrack, 1), 1);
    pooled_autocorr = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        n_seq = size(turnangle, 2);
        
        % Build sequences
        deltat = cell(n_seq, 1);
        autocorr = cell(n_seq, 1);
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            timebout = frames(:, seq)./framerate;
            timebout(isnan(timebout)) = [];
            timebout = (timebout - timebout(1));
            
            % Trinarize turn angles
            dthetacopy = dtheta;
            %             dtheta(dthetacopy > theta_threshold) = 1;
            %             dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = [];
            timebout(abs(dthetacopy) < theta_threshold) = [];
            
            if numel(dtheta) < 2*ncorr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            var1 = NaN(numel(dtheta) - 1, ncorr);
            var2 = NaN(numel(dtheta) - 1, ncorr);
            
            % Manual autocorrelation
            for idx = 1:ncorr
                
                dtheta_slide1 = dtheta(1:(end - idx));
                dtheta_slide2 = dtheta((1 + idx):end);
                correl_dtheta = dtheta_slide1.*dtheta_slide2;
                absprod_dtheta = abs(dtheta_slide1).*abs(dtheta_slide2);
                
                var1(1:(end - idx + 1), idx) = timebout((1 + idx):(end - 1)) - timebout(1:(end - idx - 1));
                var2(1:(end - idx + 1), idx) = correl_dtheta./absprod_dtheta;
                
            end
            
            % Linearize & store
            deltat{seq} = var1(:);
            autocorr{seq} = var2(:);
            
        end
        
        % Pool all events
        pooled_deltat{idx_exp} = cat(1, deltat{:});
        pooled_autocorr{idx_exp} = cat(1, autocorr{:});
        
    end
    
    % Bins with same number of elements per bins
    pooled_deltat = cat(1, pooled_deltat{:});
    pooled_autocorr = cat(1, pooled_autocorr{:});
    [bins_deltat, elmts_per_bins, v2bin1] = BinsWithEqualNbofElements(pooled_deltat, pooled_autocorr, nbins, nbins + 3);
    
    meanautocorr = mean(v2bin1,2);
    errorautocorr = std(v2bin1,1,2)./sqrt(elmts_per_bins);
    
    % - Fit
    g = fittype(@(tau, x) exp(-x/(2*tau)));
	fitt = fit(bins_deltat, meanautocorr, g, 'StartPoint', 5);
    
    tau(idT) = fitt.tau;
    
    % - Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 8;
    lineopt.LineWidth = 2;
    lineopt.LineStyle = 'none';
    lineopt.Marker = '.';
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    erb{idT} = errorshaded(bins_deltat, meanautocorr, errorautocorr, 'line', lineopt, 'patch', shadopt);
    p = plot(bins_deltat, fitt(bins_deltat));
    p.Color = colors(idT, :);
end
legend([erb{:}]);
xlabel('\delta{t} [s]');
ylabel('<\delta{\theta}(t).\delta{\theta}(t+\Delta{t})>');
ax = gca;
ax.XLim = [0 20];
axis square