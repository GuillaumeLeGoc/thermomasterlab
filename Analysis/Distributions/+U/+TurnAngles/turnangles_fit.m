% Fit deltatheta distributions with something

% close all
clear
clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 20;     % sequence length for each fish in seconds
filt.bout_freq = 0.01;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

thetalim = 180;         % Limit analysis to |dtheta| = thetalim

% * Bins
method = 'kde';         % Method for PDF
mode = 'edges';         % Mode for bins
bw = 1;                % Bandwidth for KDE
nbins = 2*thetalim;
e.turn = [-thetalim, thetalim];                 % bins for turn angle (°, wrapped to 180)

% --- Prepare bin vectors
switch mode
    case 'edges'
        bt = linspace(e.turn(1), e.turn(2), nbins+1)';
    case 'centers'
        bt = linspace(e.turn(1), e.turn(2), nbins)';
end

colors = getColors;

% --- Processing
thetacrossmean = NaN(numel(T), 1);
for idT = 1:numel(T)
        
    temperature = T(idT);
    
    fprintf('Fitting distribution for T = %i°C...\n', temperature); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    t = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, t{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
    end
    
    % Gather features
    allt = cat(1, t{:});
    
    % Compute PDF
    switch method
        case 'hist'
            [mpdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode);
        case 'kde'
            [mpdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bw});
    end
    
    % --- Fitting
    % * Gauss2
    normfactor = 1./sqrt(2*pi);
    gfwd = @(sfwd, x) (1./sfwd).*normfactor.*exp(- 0.5*(x./sfwd).^2);
    gtrnleft = @(strn, mtrn, x) (1./strn).*normfactor.*exp(- 0.5*((x-mtrn)./strn).^2);
    gtrnright = @(strn, mtrn, x) (1./strn).*normfactor.*exp(- 0.5*((x+mtrn)./strn).^2);
    fun = @(p, sfwd, strn, mtrn, x) p.*gfwd(sfwd, x) + (1-p).*(gtrnleft(strn, mtrn, x) + gtrnright(strn, mtrn, x));
    g = fittype(fun);
    
    aft = fit(ct, mpdft, g, 'StartPoint', [0.5, 5, 50, 25]);
    
    fprintf('----------------------\n\n');
    disp(aft);
    fprintf('----------------------\n\n');
    % - Display
    f = figure; hold on
    f.Name = ['GaussianFitT=' num2str(T(idT))];
    plot(ct, mpdft, 'k.');
    plot(ct, aft(ct));
    
    ax = gca;
    ax.YScale = 'log';
    legend('Data', 'Fit');
    xlabel('Turn angle [deg]');
    ylabel('PDF');
    title(['Fit, T = ' num2str(temperature) '°C']);
    axis([-100 100 5e-5 1e-1]);
    
    fprintf('\tDone (%2.2fs).\n', toc);
end