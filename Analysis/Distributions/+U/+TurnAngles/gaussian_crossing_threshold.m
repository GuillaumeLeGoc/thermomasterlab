% Get turn angle distribution and fit with gaussian. Establish the turn
% threshold as the crossing between the two.

% close all
clear
clc

% --- Parameters
% * Destination file, leave empty to not save
prefix_fit = [pwd '/Data/Matfiles/gauss2fit_'];
% prefix_fit = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 30;     % sequence length for each fish in seconds
filt.bout_freq = 0.1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

thetalim = 180;         % Limit analysis to |theta| = thetalim

% * Bins
n_bins = 2*thetalim;
e.turn = [-thetalim, thetalim];                 % bins for turn angle (°, wrapped to 180)

% --- Prepare bin vectors
bt = linspace(e.turn(1), e.turn(2), n_bins);
colors = lines;

% --- Processing
thetacrossmean = NaN(numel(T), 1);
for idT = 1:numel(T)
        
    temperature = T(idT);
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    t = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, t{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
    end
    
    % Gather features
    allt = cat(1, t{:});
    
    % Compute PDF
    [mpdft, ct] = computePDF(bt', allt);
    
    % --- Fitting
    % * Gauss2
    normfactor = 1./sqrt(2*pi);
    gauss_fwd = @(sfwd, x) (1./sfwd).*normfactor.*exp(- 0.5*(x./sfwd).^2);
    gauss_trn = @(strn, x) (1./strn).*normfactor.*exp(- 0.5*(x./strn).^2);
    gauss_full = @(p, sfwd, strn, x) p.*gauss_fwd(sfwd, x) + (1-p).*gauss_trn(strn, x);
    g = fittype(gauss_full);
    
    gauss_fit = fit(ct, mpdft, g, 'StartPoint', [0.5, 5, 50]);
    
    % - Find intersection
    gauss_dif = @(x) gauss_fit.p.*gauss_fwd(gauss_fit.sfwd, x) - (1-gauss_fit.p).*gauss_trn(gauss_fit.strn, x);
    if sign(gauss_dif(0)) ~= sign(gauss_dif(thetalim))
        thetacross = fzero(gauss_dif, [0 thetalim]);
    else
        thetacross = NaN;
    end
    thetacrossmean(idT) = thetacross;
    
    % - Get parameters
    sig_fwd = gauss_fit.sfwd;
    sig_tur = gauss_fit.strn;
    fprintf('Gauss2, T = %i°C :\n----------------\n sigma_forward = %2.2f°\n sigma_turn = %2.2f°\n pforward = %0.2f\n pturn = %0.2f\n', ...
        temperature, sig_fwd, sig_tur, gauss_fit.p, 1-gauss_fit.p);
    
    fprintf('<strong>Crossing theta</strong> = %2.6f°\n', thetacross);
    
    % --- Display
    f = figure; hold on
    % - Style
    ax = gca;
    ax.YScale = 'log';
    xlabel('Turn angle [deg]');
    ylabel('PDF');
    title(['Gauss2, T = ' num2str(temperature) '°C']);
    axis([-100 100 5e-5 1e-1]);
    axis square
    % - Patches
    ptop = ax.YLim(2);
    pbot = ax.YLim(1);
    tleft1 = ax.XLim(1);
    tright1 = -thetacross;
    tleft2 = thetacross;
    tright2 = ax.XLim(2);
    fleft = -thetacross;
    fright = thetacross;
    tp1 = patch([tleft1, tright1, tright1, tleft1], [pbot, pbot, ptop, ptop], colors(4, :));
    tp1.FaceAlpha = 0.15;
    tp1.LineStyle = 'none';
    tp1.DisplayName = 'Turn';
    tp2 = patch([tleft2, tright2, tright2, tleft2], [pbot, pbot, ptop, ptop], colors(4, :));
    tp2.FaceAlpha = 0.15;
    tp2.LineStyle = 'none';
    fp = patch([fleft, fright, fright, fleft], [pbot, pbot, ptop, ptop], colors(5, :));
    fp.FaceAlpha = 0.15;
    fp.LineStyle = 'none';
    fp.DisplayName = 'Forward';
    % - Curves
    f.Name = ['GaussianFitT=' num2str(T(idT))];
    pdata = plot(ct, mpdft, 'k', 'DisplayName', 'Data');
    pfwd = plot(ct, gauss_fit.p.*gauss_fwd(gauss_fit.sfwd, ct), 'Color', colors(5, :), 'LineStyle', '--', 'DisplayName', 'Fit forward');
    ptrn = plot(ct, (1-gauss_fit.p).*gauss_trn(gauss_fit.strn, ct) ,'Color', colors(5, :), 'LineStyle', '--', 'DisplayName', 'Fit turn');
    pful = plot(ct, gauss_fit(ct), 'Color', colors(5, :), 'DisplayName', 'Sum');
    % - Crossing points
    plot([-thetacross thetacross], gauss_fit.p.*gauss_fwd(gauss_fit.sfwd, [-thetacross thetacross]), 'o');
    % - Legend
    legend([pdata, pfwd, ptrn, pful, fp, tp1]);
    
    % --- Save file
    if ~isempty(prefix_fit)
        fname = [prefix_fit 'T' num2str(T(idT)) '.mat'];
        save(fname, 'gauss_fit', 'gauss_fwd', 'gauss_trn', 'gauss_full');
    end
    
    fprintf('\n');
end

mthetacross = mean(thetacrossmean);
sthetacross = std(thetacrossmean)./sqrt(numel(thetacrossmean));