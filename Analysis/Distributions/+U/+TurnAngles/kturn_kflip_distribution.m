% kturn and kflip distributions & rescaling

% close all
clear
clc

% --- Parameters
%  * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_ibi = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MeanOverTemperatures.mat'];
fn_out0 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ktkfmeanT.mat'];
fn_out1 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ktkfdistY.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

usepar = true;  % use parralel for bootstrap

% * Bins
nbins = 80;
method = 'kde';
mode = 'edges';
bturn_lim = [0, 1];
bflip_lim = [0, 1];
bturn_n_lim = [0, 4];
bflip_n_lim = [0, 4];
kdeopt = {'Bandwidth', 0.08, 'Support', 'Positive'};
kdeopt_n = {'Bandwidth', 0.08, 'Support', 'Positive'};

% * Confidence interval
nboots = 1000;

% * Threshold
theta_threshold = 10;   % deg

% * Load data
data = load(fn_dataset, 'dtheta');
mibi = load(fn_ibi, 'mibi');
mibi = mibi.mibi;

% --- Prepare arrays & bins
switch mode
    case 'edges'
        bturn = linspace(bturn_lim(1), bturn_lim(2), nbins + 1)';
        bflip = linspace(bflip_lim(1), bflip_lim(2), nbins + 1)';
        bturn_n = linspace(bturn_n_lim(1), bturn_n_lim(2), nbins + 1)';
        bflip_n = linspace(bflip_n_lim(1), bflip_n_lim(2), nbins + 1)';
    case 'centers'
        bturn = linspace(bturn_lim(1), bturn_lim(2), nbins)';
        bflip = linspace(bflip_lim(1), bflip_lim(2), nbins)';
        bturn_n = linspace(bturn_n_lim(1), bturn_n_lim(2), nbins)';
        bflip_n = linspace(bflip_n_lim(1), bflip_n_lim(2), nbins)';
end
cdfturn_n = NaN(nbins, numel(T));
cdfflip_n = NaN(nbins, numel(T));
mkturn = NaN(numel(T), 1);
mkflip = NaN(numel(T), 1);
ci_mkturn = NaN(numel(T), 2);
ci_mkflip = NaN(numel(T), 2);
allkturn_n = cell(numel(T), 1);
allkflip_n = cell(numel(T), 1);

% --- Prepare figures
figt = figure; axt = gca; hold on
figf = figure; axf = gca; hold on
figtn = figure; axtn = gca; hold on
figtf = figure; axfn = gca; hold on
figctn = figure; axctn = gca; hold on
figcfn = figure; axcfn = gca; hold on

colors = rainbow(numel(T));
msize = 8;

p = cell(numel(T), 1);
pn = cell(numel(T), 1);
q = cell(numel(T), 1);
qn = cell(numel(T), 1);

lineopt = struct;
lineopt.MarkerSize = 8;
lineopt.LineWidth = 1.25;
shadopt = struct;
shadopt.FaceAlpha = 0.275;

% --- Main loop
for idT = 1:numel(T)
    
    fprintf(['Estimating turn and flip rate for T = ', num2str(T(idT)) '°C...']); tic
    
    % Get data
    turnangle = data.dtheta{idT};
    nseq = size(turnangle, 2);
    
    allkflip = NaN(nseq, 1);
    allkturn = NaN(nseq, 1);
	toclean = false(nseq, 1);
    
    for seq = 1:nseq
        
        % - Prepare data
        dtheta = turnangle(:, seq);
        dtheta(isnan(dtheta)) = [];
        
        % - Estimate kturn
        nbouts = numel(dtheta);
        nturns = sum(abs(dtheta) > theta_threshold);
        pt = nturns/nbouts;
        allkturn(seq) = pt./mibi(idT);
        
        % - Estimate kflip
        dtheta = dtheta - mean(dtheta);  % bias correction
        % Trinarize
        dthetacopy = dtheta;
        dtheta(abs(dthetacopy) <= theta_threshold) = 0;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
        
        % Create lagged vectors
        dthetai = dtheta(1:end  - 1);
        dthetan = dtheta(2:end);
        % Binning
        binedges = [-1, 0, 1, 1 + eps];
        bindti = discretize(dthetai, binedges);                 % regroup in bins
        meandtn = accumarray(bindti, dthetan, [], @mean, NaN);  % average over bins  % average over bins
        
        if any(isnan(meandtn)) || numel(meandtn) < 3
            toclean(seq) = true;
            continue;
        end
        
        % Fit
        yfit = @(pflip, x) pt.*(1-2*pflip).*x;
        ft = fit((-1:1)', meandtn, yfit, 'StartPoint', 0.25, ...
            'Lower', 0, 'Upper', 1);
        if isequal(ft.pflip, 0) || isequal(ft.pflip, 1)
            toclean(seq) = true;    % ignore failed fit
            continue;
        else
            allkflip(seq) = ft.pflip./mibi(idT);
        end
    end
    
    % Cleanup
    allkflip(toclean) = [];
    allkturn(toclean) = [];
    
    mkturn(idT) = mean(allkturn);
    mkflip(idT) = mean(allkflip);
    ci_mkturn(idT, :) = bootci(nboots, {@mean, allkturn}, 'Options', statset('UseParallel', usepar))';
    ci_mkflip(idT, :) = bootci(nboots, {@mean, allkflip}, 'Options', statset('UseParallel', usepar))';
    
    % Rescale
    allkturn_n{idT} = allkturn./mkturn(idT);
    allkflip_n{idT} = allkflip./mkflip(idT);
    
    % Get pdf & cdf
    switch method
        case 'hist'
            [pdfturn, cturn] = computePDF(bturn, allkturn, 'method', method, 'mode', mode);
            [pdfflip, cflip] = computePDF(bflip, allkflip, 'method', method, 'mode', mode);
    
            [pdfturn_n, cturn_n] = computePDF(bturn_n, allkturn_n{idT}, 'method', method, 'mode', mode);
            [pdfflip_n, cflip_n] = computePDF(bflip_n, allkflip_n{idT}, 'method', method, 'mode', mode);
            
            cdfturn_n(:, idT) = computeCDF(bturn_n, allkturn_n{idT}, 'method', method, 'mode', mode);
            cdfflip_n(:, idT) = computeCDF(bflip_n, allkflip_n{idT}, 'method', method, 'mode', mode);
        case 'kde'
            [pdfturn, cturn] = computePDF(bturn, allkturn, 'method', method, 'mode', mode, 'param', kdeopt);
            [pdfflip, cflip] = computePDF(bflip, allkflip, 'method', method, 'mode', mode, 'param', kdeopt);
    
            [pdfturn_n, cturn_n] = computePDF(bturn_n, allkturn_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
            [pdfflip_n, cflip_n] = computePDF(bflip_n, allkflip_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
            
            cdfturn_n(:, idT) = computeCDF(bturn_n, allkturn_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
            cdfflip_n(:, idT) = computeCDF(bflip_n, allkflip_n{idT}, 'method', method, 'mode', mode, 'param', kdeopt_n);
    end
    
    % Get conf. int.
    fun = @(x) computePDF(bturn_n, x, 'method', method, 'mode', mode, 'param', kdeopt);
    errturn_n = bootci(nboots, {fun, allkturn_n{idT}}, 'Options', statset('UseParallel', usepar));
    errturn_n(1, isnan(errturn_n(1, :))) = min(errturn_n(1, :));
    errturn_n(2, isnan(errturn_n(2, :))) = min(errturn_n(2, :));
    
    fun = @(x) computePDF(bflip_n, x, 'method', method, 'mode', mode, 'param', kdeopt);
    errflip_n = bootci(nboots, {fun, allkflip_n{idT}}, 'Options', statset('UseParallel', usepar));
    
    % --- Display
    lineopt.DisplayName = ['T=' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    
    p{idT} = plot(axt, cturn, pdfturn);
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    p{idT}.Color = colors(idT, :);
    plot(axt, [mkturn(idT), mkturn(idT)], axt.YLim, '--', 'Color', colors(idT, :));
    
    pn{idT} = bierrorshaded(cturn_n, pdfturn_n, errturn_n(2, :), errturn_n(1, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axtn);
    
    q{idT} = plot(axf, cflip, pdfflip);
    q{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    q{idT}.Color = colors(idT, :);
    plot(axf, [mkflip(idT), mkflip(idT)], axf.YLim, '--', 'Color', colors(idT, :));
    
    qn{idT} = bierrorshaded(cflip_n, pdfflip_n, errflip_n(2, :), errflip_n(1, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axfn);
    
    fprintf('\t Done (%2.2fs).\n', toc);
   
end

% --- Mean rescaled cdf
mcdfYktr = mean(cdfturn_n, 2);
mcdfYkfp = mean(cdfflip_n, 2);
binsyktr = cturn_n;
binsykfp = cflip_n;

% --- Display cdf
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);

axctn.ColorOrder = colors;
plot(axctn, cturn_n, cdfturn_n);
legend(axctn, labels, 'Location', 'southeast');
plot(axctn, binsyktr, mcdfYktr, 'k--', 'DisplayName', 'Pool');
xlabel(axctn, 'k_{turn}/<k_{turn}>_T');
ylabel(axctn, 'cdf');
axis(axctn, 'square');

axcfn.ColorOrder = colors;
plot(axcfn, cflip_n, cdfflip_n);
legend(axcfn, labels, 'Location', 'southeast');
plot(axcfn, binsykfp, mcdfYkfp, 'k--', 'DisplayName', 'Pool');
xlabel(axcfn, 'k_{flip}/<k_{flip}>_T');
ylabel(axcfn, 'cdf');
axis(axcfn, 'square');

% Cosmetic
axis(axt, 'square');
xlabel(axt, 'k_{turn}');
ylabel(axt, 'pdf');
legend(axt, [p{:}]);

axis(axtn, 'square');
xlabel(axtn, 'k_{turn}/<k_{turn}>_T');
ylabel(axtn, 'pdf');
legend(axtn, [pn{:}]);

axis(axf, 'square');
xlabel(axf, 'k_{flip}');
ylabel(axf, 'pdf');
legend(axf, [q{:}]);

axis(axfn, 'square');
xlabel(axfn, 'k_{flip}/<k_{flip}>_T');
ylabel(axfn, 'pdf');
legend(axfn, [qn{:}]);

figure('Name', 'MeansKTR');
ax = gca; hold on
p = plot(T, mkturn);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mkturn, 'k');
xlabel('Temperature [°C]');
ylabel('<k_{turn}>_T');
ax.XTick = T;
xlim([17 34])
axis square

figure('Name', 'MeansKFP');
ax = gca; hold on
p = plot(T, mkflip);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mkflip, 'k');
xlabel('Temperature [°C]');
ylabel('<k_{flip}>_T');
ax.XTick = T;
xlim([17 34])
axis square

% --- Save
if ~isempty(fn_out0)
    save(fn_out0, 'mkturn', 'ci_mkturn', 'mkflip', 'ci_mkflip');
end
if ~isempty(fn_out1)
    save(fn_out1, 'binsyktr', 'mcdfYktr', 'binsykfp', 'mcdfYkfp');
end