% Fractions of turns and fractions of forwards.

% close all
clear
% clc

% --- Parameters
% Destination file, leave empty to not save
savefileprefix = [pwd '/Data/Matfiles/pturns.mat'];
% savefileprefix = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Threshold
theta_threshold = 10;   % deg

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
% filt.bout_freq = .1;    % minimum number of bouts per seconds
filt.n_bouts = 10;      % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Statisical test
statest = 'kruskalwallis';

% --- Prepare figures
fig = figure; hold on
fig.Name = 'pTurn';
colors = rainbow(numel(T));

% --- Main loop
all_pturn = cell(numel(T), 1);
all_pscoot = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Estimating forward/turn for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    pooled_pturn = cell(size(ftrack, 1), 1);
    pooled_pscoot = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
%         pturn = NaN(n_seq, 1);
%         pscoot = NaN(n_seq, 1);
        bouts = 0;
        turns = 0;
        scoots = 0;
        for seq = 1:n_seq
            
            % Prepare data
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            
            % Estimate probabilities
            n_bouts = numel(dtheta);
            n_turns = sum(abs(dtheta) > theta_threshold);
            n_scoots = sum(abs(dtheta) <= theta_threshold);
            % Per sequence
%             pturn(seq) = n_turns/n_bouts;
%             pscoot(seq) = n_scoots/n_bouts;
            turns = turns + n_turns;
            scoots = scoots + n_scoots;
            bouts = bouts + n_bouts;
        end
        
        % Per experiment
        pturn = turns./bouts;
        pscoot = scoots./bouts;
        
        pooled_pturn{idx_exp} = pturn';
        pooled_pscoot{idx_exp} = pscoot';

        fprintf('.');
    end
    
    all_pturn{idT} = [pooled_pturn{:}]';
    all_pscoot{idT} = [pooled_pscoot{:}]';
     
    fprintf('\t Done (%2.2fs).\n', toc);
   
end

% --- Display
maxsize = max(cellfun(@numel, all_pturn));
all_pturn = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), all_pturn, 'UniformOutput', false);
g = cell(size(all_pturn));
for idx = 1:size(all_pturn, 1)
    g{idx} = idx.*ones(numel(all_pturn{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);
X = cat(1, all_pturn{:});

% - Display
set(0, 'CurrentFigure', fig);
pointopt = struct;
pointopt.Color = colors;
boxopt = struct;
boxopt.BoxColors = colors;
boxopt.Labels = labels;
beautifulbox(X, g, pointopt, boxopt);

% - Significance
pairs = {[1, 2], [2, 3], [3, 4], [4, 5]};
h = addStars(all_pturn, 'pair', pairs, 'test', statest);

% - Style
axis square
title('Turn probability estimation');
ylabel('p_{turn}');
axis([-Inf Inf 0 1.0]);
ax = gca;
ax.YTick = [0, 0.5, 1];
ax.YTickLabel = {'0', '0.5', '1'};
ax.FontSize = 16;

% --- Save
if ~isempty(savefileprefix)
    fname = savefileprefix;
    save(fname, 'all_pturn');
end