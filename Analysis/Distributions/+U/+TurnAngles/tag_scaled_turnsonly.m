% Find a fit for turn angles (only turns).

% close all
clear
clc

% * Paths
fn_dtheta = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Bins
binlim = [0, 100];
method = 'hist';         % Method for PDF
mode = 'edges';         % Mode for bins
kdeopt = {'BandWidth', 0.5};
nbins = 100;
xfit = linspace(binlim(1), binlim(2), 1500);
binscale = linspace(0, 200, nbins);

% * Fit
theta_threshold = 6;
gausswid = 4;
gamshape = 10;

% * Figure
colors = rainbow(numel(T));

% --- Prepare bin vectors
switch mode
    case 'edges'
        bins = linspace(binlim(1), binlim(2), nbins+1)';
    case 'centers'
        bins = linspace(binlim(1), binlim(2), nbins)';
end

% --- Load data
dtheta = load(fn_dtheta, 'Tu');
dtheta = dtheta.Tu;

% --- Processing
pdf_dtheta = NaN(nbins, numel(T));
cdf_dtheta = NaN(nbins, numel(T));

for idT = 1:numel(T)
    
    du = abs(dtheta{idT});
    du(du < theta_threshold) = [];
    
    % - Get PDF
    switch method
        case 'hist'
            [pdft, cbin] = computePDF(bins, du, 'method', method, 'mode', mode);
            scaled_cdfd = computeCDF(bins, du, 'method', method, 'mode', mode);
        case 'kde'
            [pdft, cbin] = computePDF(bins, du, 'method', method, 'mode', mode, 'param', kdeopt);
            scaled_cdfd = computeCDF(bins, du, 'method', method, 'mode', mode, 'param', kdeopt);
    end
    
    % - Fit
    [cbin, pdft] = prepareCurveData(cbin, pdft);
    fun = @(pturn, gamscale, x) (1 - pturn)*2/gausswid/sqrt(2*pi)*exp(-x.^2/2/gausswid^2) + pturn*gampdf(x, gamscale, gamshape);
    ft = fittype(fun);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0, 0], 'Upper', [1, 10], 'StartPoint', [0.7, 4.5]);
    
    f = fit(cbin, pdft, ft, fo);
    
    % Rescale data by their variance
%     duscale = du./(f.pturn);
    
%     % - Get normalized pdf
%     switch method
%         case 'hist'
%             [pdftscale, cbinscale] = computePDF(binscale, duscale, 'method', method, 'mode', mode);
%             scaled_cdfd = computeCDF(bins, du, 'method', method, 'mode', mode);
%         case 'kde'
%             [pdftscale, cbinscale] = computePDF(binscale, duscale, 'method', method, 'mode', mode, 'param', kdeopt);
%             scaled_cdfd = computeCDF(bins, du, 'method', method, 'mode', mode, 'param', kdeopt);
%     end
%     
%     pn = plot(axnorm, cbinscale, pdftscale);
%     pn.Color = colors(idT, :);
%     xlabel('\delta\theta [a.u.]');
%     ylabel('pdf');
    
    % - Display
    figure('Name', ['dthetaGamFitT' num2str(T(idT))]); hold on
    p = plot(cbin, pdft);
    p.Color = colors(idT, :);
    g = plot(xfit, f(xfit));
    g.Color = 'r';
    ax = gca;
    ax.YScale = 'log';
    ax.XLabel.String = '\delta\theta [deg]';
    ax.YLabel.String = 'pdf';
    ax.YLim = [1e-5, 1];
end