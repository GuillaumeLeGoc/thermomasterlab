% Fit the turn angles distributions to extract parameters to plot
% versus temperatures.

% close all
clear
clc

% --- Parameters

% * Temperatures
T = [18, 22, 26, 30, 33];

n = 1;                  % order for fit : x^n.exp(-x/tau)

% * Filters
filt.seq_length = 20;	% sequence length for each fish in seconds
filt.bout_freq = .01;   % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Bins
method = 'kde';         % Method for PDF
mode = 'edges';         % Method for bins
bw = 0.25;              % Bandwidth for KDE
nbins = 360;            % Number of bins
limbins = [0, 180];        % bins for interbout interval (s)

% --- Prepare bin vector (center)
bt = linspace(limbins(1), limbins(2), nbins)';

% --- Loop over each temperatures and compute features PDF
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Fitting turn angle distribution for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    t = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, t{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        fprintf('.');
    end
    
    % Gather features
    allt = cat(1, t{:});
    
    % Fold distribution
    allt(allt < 0) = abs(allt(allt < 0));
    
    % Get PDF & CDF
    switch method
        case 'hist'
            [pdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode);
            cdfi = computeCDF(bt, allt, 'method', method, 'mode', mode);
        case 'kde'
            [pdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bw});
            cdfi = computeCDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bw});
    end
    
    % Create functions
    normfactor = 1./sqrt(2*pi);
    gaussfwd = @(sfwd, x)(1./sfwd).*normfactor.*exp(-0.5*(x./sfwd).^2);
    gammapdf = @(a, b, x) (b.^a./(gamma(a))).*x.^(a-1).*exp(-b.*x);
    fulldist = @(p, sfwd, a, b, x) p.*gaussfwd(sfwd, x) + (1-p).*gammapdf(a, b, x);
    
    % - Fit
    l0 = [0, 0, 0, 0];
    u0 = [1, 10, 60, 10];
    sp = [0.5, 3, 1, 1];
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', l0, 'Upper', u0, 'StartPoint', sp);
    mdl = fittype(fulldist, 'options', fo);
    ft = fit(ct, pdft, mdl);
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Display
    
    % - PDF
    figure; hold on
    plot(ct, pdft, '.');
    plot(ct, ft(ct), 'r');
    plot(ct, ft.p.*gaussfwd(ft.sfwd, ct));
    plot(ct, (1-ft.p).*gammapdf(ft.a, ft.b, ct));
    
    % - CDF
    figure; hold on
    
end