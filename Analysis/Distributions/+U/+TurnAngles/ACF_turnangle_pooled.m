% Turn angles autocorrelation and pflip extraction from fit. All sequences
% are pooled.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters file
fn_gfit = [pwd '/Data/Matfiles/gauss2fit_'];

% * Window
n_corr = 20;            % in bouts
n_corr_time = 500;      % in frames

% * Filters
filt.seq_length = 20;	% min sequence length for each fish in seconds
filt.bout_freq = .01;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
figs.bouts = figure; hold on
figs.bouts.Name = 'TurnAngleACFBout';
figs.time = figure; hold on
figs.time.Name = 'TurnAngleACFTime';

% --- Loop over each temperatures and compute features PDF
pflips = NaN(numel(T), 1);
conf_int = NaN(numel(T), 2);
e = cell(numel(T) + 1, 1);
et = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing autocorrelations for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get mean autocorrelation for each experiment
    
    pooled_xco = cell(size(ftrack, 1), 1);
    pooled_xco_time = cell(size(ftrack, 1), 1);
    pooled_lagstime = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        mean_xco = NaN(n_seq, n_corr + 1);
        mean_xco_time = NaN(n_seq, n_corr_time + 1);
        lagstime = NaN(n_seq, n_corr_time + 1);
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta); % bias correction
            if numel(dtheta) < 2*n_corr
                continue;
            end
            [xco, lag] = xcorr(dtheta, n_corr, 'biased');
            selected_inds = find(lag == 0):find(lag == 0) + n_corr;
            mean_xco(seq, :) = xco(selected_inds)./max(xco(selected_inds));  % norm
            
            % Time
            afr = allframes(:, seq) + 1;
            afr(isnan(afr)) = [];
            dthetatime = NaN(size(afr));
            fr = frames(:, seq) + 1;
            fr(isnan(fr)) = [];
            fr = fr - fr(1);
            fr(1) = [];
            dthetatime(fr) = dtheta;
            dthetatime = fillmissing(dthetatime, 'previous');
            dthetatime = fillmissing(dthetatime, 'next');
            [xcotime, lagtime] = xcorr(dthetatime, 'biased');
            selected_inds = find(lagtime == 0):find(lagtime == 0) + n_corr_time;
            mean_xco_time(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));  % norm
            lagstime(seq, :) = lagtime(selected_inds)./framerate;
        end
        
        pooled_xco{idx_exp} = mean_xco;
        pooled_xco_time{idx_exp} = mean_xco_time;
        pooled_lagstime{idx_exp} = lagstime;
        
        fprintf('.');
    end
    
    % Pool
    pooled_xco = cat(1, pooled_xco{:});
    pooled_xco_time = cat(1, pooled_xco_time{:});
    pooled_lagstime = cat(1, pooled_lagstime{:});
    
    % Compute mean
    mxco = nanmean(pooled_xco, 1);
    exco = nanstd(pooled_xco, [], 1)/sqrt(size(pooled_xco, 1));
    mxcot = nanmean(pooled_xco_time, 1);
    excot = nanstd(pooled_xco_time, [], 1)/sqrt(size(pooled_xco_time, 1));
    lxcot = nanmean(pooled_lagstime, 1);
    
    % --- Analytical fit
    file_gaussfit = [fn_gfit 'T' num2str(T(idT)) '.mat'];
    datafit = load(file_gaussfit);
    x = 1:n_corr;
    ps = datafit.gauss_fit.p;
    pt = 1 - ps;
    ss = datafit.gauss_fit.strn;
    st = datafit.gauss_fit.strn;
    g = fittype(@(pflip, x) ((2/pi).*(1-2*pflip).^x).*((pt*st)^2)./(pt*st.^2 + ps*ss.^2));
    fitt = fit(x', mxco(2:end)', g, 'StartPoint', 0.5);
    
    pflips(idT) = fitt.pflip;
    dummy = confint(fitt);
    conf_int(idT, :) = dummy(:, 1)';
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 14;
    lineopt.LineStyle = 'none';
    
    % Bouts
    set(0, 'CurrentFigure', figs.bouts);
    e{idT} = errorshaded(1:n_corr, mxco(2:end), exco(2:end), 'line', lineopt);
    
    p = plot(1:0.01:n_corr, fitt(1:0.01:n_corr));
    p.Color = colors(idT, :);
    p.LineWidth = 1.75;
    xlabel('# Bout');
    ylabel('Autocorrelation of \Delta{\theta}');
    title('Bout autocorrelations');
    
    % Time
    set(0, 'CurrentFigure', figs.time);
    lineopt.MarkerSize = 5;
    et{idT} = errorshaded(lxcot, mxcot, excot, 'line', lineopt);
    xlabel('Time [s]');
    ylabel('Autocorrelation of \Delta{\theta}');
    title('Time autocorrelations');
    
    fprintf('\t Done (%2.2fs).\n', toc);
end

fprintf('pflip from fit :\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n', ...
    T(1), pflips(1), T(2), pflips(2), T(3), pflips(3), T(4), pflips(4), T(5), pflips(5));

% Display legends & pflips values
set(0, 'CurrentFigure', figs.bouts);
e{numel(T) + 1} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Fit', 'LineWidth', 1);
legend([e{:}]);
axis square

set(0, 'CurrentFigure', figs.time);
legend([et{:}]);
axis square

% - Plot extracted pflip values
f = figure; hold on
f.Name = 'pflipFromPooledACF';
neg = pflips - conf_int(:, 1);
pos = pflips - conf_int(:, 2);
e = errorbar(T, pflips, neg, pos);
e.Color = 'k';
xlabel('Temperature [°C]');
ylab = ylabel('p_{flip}');
title('Flipping probability from autocorr. fit');
axis square
axis([15 35 0 0.6]);
plot([15 35], [.5 .5], '--k');