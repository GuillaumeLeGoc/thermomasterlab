% Squared turn angles n+1 as a function of n to check if there is a 
% persistence between forward/turn states.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Window
n_corr = 20;            % Minumum number of bouts
nbins = 20;

% * Filters
filt.seq_length = 30;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
fig = figure; hold on
fig.Name = 'ReorientationAmplitudeSquared';
e = cell(numel(T), 1);

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Getting turn amplitudes for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get turn amplitude of next bout given the preceding one
	c = 1;
    dthetai = cell(0, 1);
    dthetan = cell(0, 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        for seq = 1:n_seq
            
            % Get sequence
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            dtheta2 = abs(dtheta);              % abs
            
            if numel(dtheta2) < n_corr
                continue;
            end
            
            % Create lagged vectors
            dthetai{end + 1, 1} = dtheta2(1:end  - 1);
            dthetan{end + 1, 1} = dtheta2(2:end);
        end
        
        fprintf('.');
    end
    
    % Pad sequences with nan so that all are the same size
    maxnum = max(cellfun(@numel, dthetai));
    fun = @(C) padarray(C, maxnum - numel(C), NaN, 'post');
    dthetai = cellfun(fun, dthetai, 'UniformOutput', false);
    dthetan = cellfun(fun, dthetan, 'UniformOutput', false);
    
    % Pool sequences
    dthetai = cat(2, dthetai{:})';
    dthetan = cat(2, dthetan{:})';
    
    % Normalize
    var1 = dthetai;
    var2 = dthetan;
    
    % Make bins with equal number of elements
    [binvals, elmts_per_bin, v2bin] = BinsWithEqualNbofElements(var1, var2, nbins, nbins + 6);
    
    % Get mean & sem
    meanamp = mean(v2bin, 2);
	erroamp = std(v2bin, 1, 2)./sqrt(elmts_per_bin);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = 'none';
    shadopt.FaceAlpha = 0.275;
    
    % Bouts
    set(0, 'CurrentFigure', fig);
    e{idT} = errorshaded(binvals, meanamp, erroamp, 'line', lineopt, 'patch', shadopt);
    
    fprintf('\t Done (%2.2fs).\n', toc);
end

% Display legends
xlabel('|\delta{\theta}_n| [deg]');
ylabel('<|\delta{\theta}_{n+1}|> [deg]');
title('Reorientation of bout n+1');
axis square
set(0, 'CurrentFigure', fig);
axis square
legend([e{:}], 'Location', 'southeast');