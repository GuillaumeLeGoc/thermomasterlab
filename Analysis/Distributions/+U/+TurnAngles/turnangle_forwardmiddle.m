% Find turn sequences with two turns adjacent to a forward to see if this
% resets the hidden turning state left/right.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters
theta_threshold = 6;   % deg

% * Filters
filt.seq_length = 20;	% min sequence length for each fish in seconds
filt.bout_freq = .01;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Patterns to find in turning sequences
patterns = [1, 0, -1; ...
    1, 0, 1; ...
    -1, 0, 1; ...
    -1, 0, -1];
counting = zeros(numel(T), size(patterns, 1));

% --- Prepare figures

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Counting patterns in sequences for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Count occurences of patterns in each sequences
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        for seq = 1:n_seq
            
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            % Binarize turn angles
            dthetacopy = dtheta;
            dtheta(dthetacopy > theta_threshold) = 1;
            dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = 0;
            
            % Count occurences of patterns
            for idxpat = 1:size(patterns, 1)
                
                pat = patterns(idxpat, :);
                
                found = strfind(dtheta', pat);
                counting(idT, idxpat) = counting(idT, idxpat) + numel(found);
                
            end
            
        end
        
        
        fprintf('.');
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
end

% --- Scale
counting = counting./sum(counting, 2)*100;

% --- Display
figure; hold on
bar(counting);
legend('LFR', 'LFL', 'RFL', 'RFR');
ax = gca;
lbl = split(num2str(T));
lbl = cellfun(@(x) [x '°C'], lbl, 'UniformOutput', false);
ax.XTick = 1:numel(T);
ax.XTickLabel = lbl;
ylabel('Fraction of pattern [%]');
ax.XLim = [0.5, 5.5];
ax.YLim = [0, 40];
axis square