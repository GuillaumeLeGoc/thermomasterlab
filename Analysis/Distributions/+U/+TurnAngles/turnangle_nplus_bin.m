% Turn angles n+1 as a function of n to check if there is a 
% persistence between right/left. Trinarized

% close all
clear
% clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_pturn = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'ptpfmeanT.mat'];
fn_out = [pwd, filesep, 'Data', filesep, 'Matfiles' filesep, 'pflips_meanreorientation_bin.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

theta_threshold = 10;
order = 1;

% * Load data
data = load(fn_dataset);
pturns = load(fn_pturn, 'mpturn');
pturns = pturns.mpturn;

% --- Prepare figures
colors = rainbow(numel(T));
fig = figure; hold on
fig.Name = 'ReorientationAmplitude';
e = cell(numel(T), 1);

% --- Processing
pflip = NaN(numel(T), 1);
conf_int = NaN(numel(T), 2);
for idT = 1:numel(T)
        
    fprintf(['Getting turn amplitudes for T = ', num2str(T(idT)) '°C...']); tic
	
    % Get data
    turnangle = data.dtheta{idT};
    nseq = size(turnangle, 2);
	
    dthetai = cell(nseq, 1);
    dthetan = cell(nseq, 1);
    for seq = 1:nseq
        
        % Get sequence
        dtheta = turnangle(:, seq);
        dtheta(isnan(dtheta)) = [];
        dtheta = dtheta - mean(dtheta);  % bias correction
        % Trinarize
        dthetacopy = dtheta;
        dtheta(abs(dthetacopy) <= theta_threshold) = 0;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
                
        % Create lagged vectors
        dthetai{seq} = dtheta(1:end  - order);
        dthetan{seq} = dtheta((order + 1):end);
    end
	
    % Pool sequences
    dthetai = cat(1, dthetai{:});
    dthetan = cat(1, dthetan{:});
    
    % Binning
    binedges = [-1, 0, 1, 1 + eps];
    bindti = discretize(dthetai, binedges);                 % regroup in bins
    meandtn = accumarray(bindti, dthetan, [], @mean, NaN);  % average over bins
    stddtn = accumarray(bindti, dthetan, [], @std, NaN);    % std
    % Get sem
    nelmtsperbin = histcounts(dthetai, binedges);
    semdtn = stddtn./sqrt(nelmtsperbin');
    
    % Fit
    pt = pturns(idT);
    yfit = @(pflip, x) pt^order.*(1-2*pflip)^order.*x;
    ft = fit((-1:1)', meandtn, yfit, 'StartPoint', 0.25);
    pflip(idT) = ft.pflip;
    dummy = confint(ft);
    conf_int(idT, :) = dummy(:, 1)';
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = 'none';
    shadopt.FaceAlpha = 0.275;
    
    % Bouts
    set(0, 'CurrentFigure', fig);
    e{idT} = errorshaded(-1:1, meandtn, semdtn, 'line', lineopt, 'patch', shadopt);
    p = plot(-1:1, ft(-1:1));
    p.Color = colors(idT, :);
    
    fprintf('\t Done (%2.2fs).\n', toc);
end

% Cosmetic
ax = gca;
ax.XLim = [-1.25, 1.25];
ax.YLim = [-0.3, 0.3];
ax.XTick = [-1, 0, 1];
ax.YTick = [-0.2, 0, 0.2];
axis square;

if ~isempty(fn_out)
    save(fn_out, 'pflip', 'conf_int');
end

% Display legends
xlabel('\delta{\theta}_n [deg]');
ylabel(['<\delta{\theta}_{n+' num2str(order) '}> [deg]']);
title('Reorientation of bout n+m');
axis square
set(0, 'CurrentFigure', fig);
axis square
p = plot([NaN, NaN], [NaN NaN], 'k');
p.DisplayName = 'Fit';
legend([e{:}, p], 'Location', 'northwest');

figure('Name', 'pflipFromMeanReorientation'); hold on
neg = pflip - conf_int(:, 1);
pos = pflip - conf_int(:, 2);
errorbar(T, pflip, neg, pos, 'k');
s = scatter(T, pflip, 75);
s.CData = colors;
s.Marker = 'o';
s.MarkerFaceColor = 'flat';

xlabel('Temperature [°C]');
ylabel('p_{flip}');
ylim([0, 0.6]);
ax = gca;
ax.XTick = [18, 22, 26, 30, 33];
ax.YTick = [0, 0.1, 0.2, 0.3, 0.4, 0.5];
title(sprintf('n+%i', order));
plot([15 35], [.5 .5], '--k');

axis square