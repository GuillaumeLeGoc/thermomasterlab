% Turn angles autocorrelation and pflip extraction from fit. All sequences
% are pooled. Turn angles are trinarized.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% Destination file (leave empty to not save)
savefileprefix = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pflips_autocorrbin_pooled'];
% savefileprefix = '';

% * Fit parameters
theta_threshold = 6;   % deg
fn_pturn = [pwd '/Data/Matfiles/pturn_'];
fn_pflip = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pflips_meanreorientation_bin.mat'];

% * Settings
ncorr = 30;            % in bouts
ncorr_time = 500;      % in frames
fwdisnan = 'n';        % replace forward by nan (y) or zeros (n) or remove (rm)

% * Filters
filt.seq_length = 30;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
figs.bouts = figure; hold on
figs.bouts.Name = 'TurnAngleACFBoutBin';
figs.time = figure; hold on
figs.time.Name = 'TurnAngleACFTimeBin';

% --- Processing
pflips = NaN(numel(T), 1);
conf_int = NaN(numel(T), 2);
e = cell(numel(T) + 1, 1);
et = cell(numel(T), 1);
fpflip = load(fn_pflip);
pflips = fpflip.pflip;

for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing autocorrelations for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get mean autocorrelation for each experiment
    
    pooled_xco = cell(size(ftrack, 1), 1);
    pooled_xco_time = cell(size(ftrack, 1), 1);
    pooled_lagstime = cell(size(ftrack, 1), ncorr_time + 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        pxco = NaN(n_seq, ncorr + 1);
        pxco_time = NaN(n_seq, ncorr_time + 1);
        lagstime = NaN(n_seq, ncorr_time + 1);
        counter = 0;
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            % Binarize turn angles
            dthetacopy = dtheta;
            dtheta(dthetacopy > theta_threshold) = 1;
            dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = 0;
            
            if numel(dtheta) < 2*ncorr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            switch fwdisnan
                case 'n'
                    [xco, lag] = xcorr(dtheta, ncorr, 'biased');
                case 'y'
                    dtheta_nan = dtheta;
                    dtheta_nan(dtheta_nan == 0) = NaN;
                    [xco, lag] = xcorr_withnan(dtheta_nan, ncorr);
                case 'rm'
                    dtheta_rm = dtheta;
                    dtheta_rm(dtheta_rm == 0) = [];
                    [xco, lag] = xcorr(dtheta_rm, ncorr, 'biased');
            end
            
            selected_inds = find(lag == 0):find(lag == 0) + ncorr;
            pxco(seq, :) = xco(selected_inds)./max(xco(selected_inds));  % norm
            
            % Time
            afr = allframes(:, seq) + 1;
            afr(isnan(afr)) = [];
            dthetatime = NaN(size(afr));
            fr = frames(:, seq) + 1;
            fr(isnan(fr)) = [];
            fr = fr - fr(1);
            fr(1) = [];
            dthetatime(fr) = dtheta;
            dthetatime = fillmissing(dthetatime, 'previous');
            dthetatime = fillmissing(dthetatime, 'next');
            [xcotime, lagtime] = xcorr(dthetatime, 'biased');
            selected_inds = find(lagtime == 0):find(lagtime == 0) + ncorr_time;
            pxco_time(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));  % norm
            lagstime(seq, :) = lagtime(selected_inds)./framerate;
        end
        
        pooled_xco{idx_exp} = pxco;
        pooled_xco_time{idx_exp} = pxco_time;
        pooled_lagstime{idx_exp} = lagstime;
        
        fprintf('.');
    end
    
    % Pool
    pooled_xco = cat(1, pooled_xco{:});
    pooled_xco_time = cat(1, pooled_xco_time{:});
    pooled_lagstime = cat(1, pooled_lagstime{:});
    
    % Compute mean across batch
    mxco = nanmean(pooled_xco, 1);
    exco = nanstd(pooled_xco, [], 1)/sqrt(size(pooled_xco, 1));
    mxcot = nanmean(pooled_xco_time, 1);
    excot = nanstd(pooled_xco_time, [], 1)/sqrt(size(pooled_xco, 1));
    lxcot = nanmean(pooled_lagstime, 1);
    
    % --- Analytical fit
    file_pturn = [fn_pturn 'T' num2str(T(idT)) '.mat'];
    data_pturn = load(file_pturn, 'pt');
    pt = mean(data_pturn.pt);
    pf = pflips(idT);
    g = @(x) (pt^2).*(1 - 2*pf).^x;
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = 'none';
    shadopt.FaceAlpha = 0.275;
    
    % Bouts
    set(0, 'CurrentFigure', figs.bouts);
    e{idT} = errorshaded(1:ncorr, mxco(2:end), exco(2:end), 'line', lineopt, 'patch', shadopt);
    
    p = plot(1:0.01:ncorr, g(1:0.01:ncorr));
    p.Color = colors(idT, :);
    p.LineWidth = 1.75;
    xlabel('# Bout');
    ylabel('<ACF(\delta{\theta})>_{traj}');
    title('Bout autocorrelations');
    
    % Time
    set(0, 'CurrentFigure', figs.time);
    lineopt.MarkerSize = 4;
    et{idT} = errorshaded(lxcot, mxcot, excot, 'line', lineopt);
    xlabel('Time [s]');
    ylabel('<ACF(\delta{\theta}>_{traj.}');
    title('Time autocorrelations');
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Save
    if ~isempty(savefileprefix)
        fname = [savefileprefix 'T' num2str(T(idT)) '.mat'];
        pf = pflips(idT);
        save(fname, 'pf');
    end
end

% Display legends
set(0, 'CurrentFigure', figs.bouts);
e{end} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Analytical', 'LineWidth', 1.75);
axis square
legend([e{:}]);
set(0, 'CurrentFigure', figs.time);
legend([et{:}]);
axis square