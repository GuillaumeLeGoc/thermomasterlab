% Compare bout autocorrelations where turn angles are trinarized -1,1,0.
% Fit each mean from batch and boxplot pflips

% close all
clear
% clc

% --- Parameters
% Destination file (leave empty to not save)
savefileprefix = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pflips_autocorrbin_'];
% savefileprefix = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters
theta_threshold = 6;   % deg
pturn = [0.4, 0.46, 0.58, 0.68, 0.81];

% * Window
n_corr = 20;            % in bouts

% * Filters
filt.seq_length = 30;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Statistical test
statest = 'kruskalwallis';

% --- Prepare figures
colors = rainbow(numel(T));
fig = figure; hold on
fig.Name = 'pFlipACFBin';
figs = cell(numel(T), 1);

% --- Processing
pflips = cell(numel(T), 1);
e = cell(numel(T), 1);
et = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    pt = pturn(idT);
    figs{idT} = figure; hold on;
    xlabel('# Bouts');
    ylabel('Autocorrelation of \delta{\theta}');
    title(['T = ' num2str(temperature) '°C']);
    
    fprintf(['Computing autocorrelations for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get mean autocorrelation for each experiment
    pflips{idT} = NaN(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        mean_xco = NaN(n_corr + 1, n_seq);
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            % Binarize turn angles
            dthetacopy = dtheta;
            dtheta(dthetacopy > theta_threshold) = 1;
            dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = 0;
            
            if numel(dtheta) < 2*n_corr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            [xco, lag] = xcorr(dtheta, n_corr, 'biased');
            selected_inds = find(lag == 0):find(lag == 0) + n_corr;

            mean_xco(:, seq) = xco(selected_inds)./max(xco(selected_inds));  % norm
            
        end
        
        mxco = nanmean(mean_xco, 2);
        exco = nanstd(mean_xco, [], 2)./sqrt(n_seq);
        
        % --- Analytical fit
        x = 1:n_corr;
        g = fittype(@(pflip, x) (pt^2).*(1 - 2*pflip).^x);
        fitt = fit(x', mxco(2:end), g, 'StartPoint', 0.2);
        
        pflips{idT}(idx_exp) = fitt.pflip;
        
        % - Display
        set(0, 'CurrentFigure', figs{idT});
        
        lineopt = struct;
        lineopt.Color = colors(idT, :);
        lineopt.MarkerSize = 8;
        lineopt.LineWidth = 2;
        lineopt.LineStyle = 'none';
        lineopt.Marker = '.';
        shadopt = struct;
        shadopt.FaceAlpha = 0.275;
        erb = errorshaded(x, mxco(2:end), exco(2:end), 'line', lineopt, 'patch', shadopt);
        xoversample = 1:0.01:n_corr;
        p = plot(xoversample, fitt(xoversample));
        p.Color = colors(idT, :);
        
        fprintf('.');
    end
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Save
    if ~isempty(savefileprefix)
        fname = [savefileprefix 'T' num2str(T(idT)) '.mat'];
        pf = pflips{idT};
        save(fname, 'pf');
    end
end

% --- Display
maxsize = max(cellfun(@numel, pflips));
pflips = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), pflips, 'UniformOutput', false);
g = cell(size(pflips));
for idx = 1:size(pflips, 1)
    g{idx} = idx.*ones(numel(pflips{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);
X = cat(1, pflips{:});

set(0, 'CurrentFigure', fig);
lineopt = struct;
lineopt.Color = colors;
lineopt.MarkerFaceColor = 'same';
boxopt = struct;
boxopt.BoxColors = colors;
beautifulbox(X, g, lineopt, boxopt, 'Labels', labels);

title('p_{flip} est. from ACF fit');
ylabel('p_{flip}');
axis([-Inf Inf 0 0.8]);

% - Add significance
pairs = {[2, 3], [3, 4], [4, 5]};
addStars(pflips, 'pairs', pairs, 'test', statest);
axis square