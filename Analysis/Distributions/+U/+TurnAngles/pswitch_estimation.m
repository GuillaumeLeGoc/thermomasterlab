% Estimate p_switch looking at typical time and typical bout numbers the
% fish flips its turning angle. A flip is defined as change of left/right
% heading direction even if there is forward bouts in between.

% close all
clear
clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Threshold
theta_threshold = 6;   % deg

% * Filters
filt.seq_length = 20;	% min sequence length for each fish in seconds
filt.bout_freq = .01;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
figs.bouts_trn = figure; hold on
figs.bouts_trn.Name = 'pFlipBouts_Turns';
figs.bouts_bou = figure; hold on
figs.bouts_bou.Name = 'pFlipBouts_Bouts';
figs.time = figure; hold on
figs.time.Name = 'pFlipTime';

% --- Main loop
all_pflip_trn = cell(numel(T), 1);
all_pflip_bou = cell(numel(T), 1);
all_pflip_time = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Estimating pflip for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Estimate flip probabilities
    pooled_pflip_trn = cell(size(ftrack, 1), 1);
    pooled_pflip_bou = cell(size(ftrack, 1), 1);
    pooled_pflip_time = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        pflip = NaN(n_seq, 1);
        pflip_time = NaN(n_seq, 1);
        flips = 0;
        bouts = 0;
        turns = 0;
        seqlength = 0;
        for seq = 1:n_seq
            
            % Prepare data
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            fr = frames(:, seq)./framerate;
            fr(isnan(fr)) = [];
            
            % Binarize left/forward/right
            dtheta(abs(dtheta) < theta_threshold) = 0;
            dtheta = sign(dtheta);
            
            % Find flips with flip = left to right or right to left even if
            % there are forwards bouts in between.
            for idx_bout = 2:numel(dtheta)
                
                subseq = dtheta(1:idx_bout);
                m = find(subseq, 2, 'last');
                if numel(m) > 1
                    m = m(1);
                    
                    if subseq(m)*subseq(end) < 0
                        flips = flips + 1;
                    end
                end
                
            end
            
            % * For one pflip per sequence
            % -- p = nflip/nturns :
%             pflip(seq) = flips/sum(abs(dtheta));
            % -- p = nflip/nbouts :
%             pflip(seq) = flips/numel(dtheta);
            % -- p = nflip/sequence length
%             pflip_time(seq) = flips/(fr(end) - fr(1));

            turns = turns + sum(abs(dtheta));
            bouts = bouts + numel(dtheta);
            seqlength = seqlength + (fr(end) - fr(1));
        end
        
        % * For one pflip per experiment
        % - nflips/nbouts
        pooled_pflip_trn{idx_exp} = flips./bouts;
        % - nflips/nturns
        pooled_pflip_bou{idx_exp} = flips./turns;
        % - nflips/sequence length
        pooled_pflip_time{idx_exp} = flips./seqlength;
        
        fprintf('.');
    end
    
    all_pflip_trn{idT} = [pooled_pflip_trn{:}]';
    all_pflip_bou{idT} = [pooled_pflip_bou{:}]';
    all_pflip_time{idT} = [pooled_pflip_time{:}]';
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
end

% --- Display
opt = struct;
opt.Color = colors;
opt.MarkerFaceColor = 'same';
boxopt = struct;
boxopt.BoxColors = colors;

% * Bouts (turns)
maxsize = max(cellfun(@numel, all_pflip_trn));
all_pflip_trn = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), all_pflip_trn, 'UniformOutput', false);
g = cell(size(all_pflip_trn));
for idx = 1:size(all_pflip_trn, 1)
    g{idx} = idx.*ones(numel(all_pflip_trn{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);
X = cat(1, all_pflip_trn{:});

set(0, 'CurrentFigure', figs.bouts_trn);
beautifulbox(X, g, opt, boxopt, 'Labels', labels);
title('p = n_{flip}/n_{turns}');
ylabel('p_{switch}');
axis([-Inf Inf 0 0.8]);
axis square
% ---

% * Bouts (bouts)
all_pflip_bou = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), all_pflip_bou, 'UniformOutput', false);
X = cat(1, all_pflip_bou{:});

set(0, 'CurrentFigure', figs.bouts_bou);
beautifulbox(X, g, opt, boxopt, 'Labels', labels);
title('p = n_{flip}/n_{bouts}');
ylabel('p_{switch}');
axis([-Inf Inf 0 0.8]);
axis square

% ---

% * Time
all_pflip_time = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), all_pflip_time, 'UniformOutput', false);
X = cat(1, all_pflip_time{:});

set(0, 'CurrentFigure', figs.time);
beautifulbox(X, g, opt, boxopt, 'Labels', labels);
title('p = n_{flip}/seq. length');
ylabel('p_{switch}[s^{-1}]');
axis([-Inf Inf 0 0.8]);
axis square