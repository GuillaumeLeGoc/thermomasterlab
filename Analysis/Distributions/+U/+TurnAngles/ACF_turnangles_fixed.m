% Manual ACF on turn angles to fix mean estimation problem. Trinarized
% trajectories.

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% Destination file (leave empty to not save)
savefileprefix = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pflips_autocorrbin_pooled'];
% savefileprefix = '';

% * Fit parameters
theta_threshold = 6;   % deg
fn_pturn = [pwd '/Data/Matfiles/pturn_'];

% * Settings
ncorr = 20;            % in bouts
ncorr_time = 500;      % in frames
fwdisnan = 'n';        % replace forward by nan (y) or zeros (n) or remove (rm)

% * Filters
filt.seq_length = 30;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
figs.bouts = figure; hold on
figs.bouts.Name = 'TurnAngleACFBoutBin';

% --- Processing
pflips = NaN(numel(T), 1);
conf_int = NaN(numel(T), 2);
e = cell(numel(T) + 1, 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing autocorrelations for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Build the turn angles sequences matrix
    alltagseq = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        tagseq = cell(n_seq, 1);
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            
            if numel(dtheta) < ncorr
                continue
            end
            
            % Binarize turn angles
%             dthetacopy = dtheta;
%             dtheta(dthetacopy > theta_threshold) = 1;
%             dtheta(dthetacopy < -theta_threshold) = -1;
%             dtheta(abs(dthetacopy) < theta_threshold) = 0;
            
            tagseq{seq} = dtheta';
        end
        
        % Fill trajectories with nans
        tagseq(cellfun(@isempty, tagseq)) = [];
        maxsize = max(cellfun(@numel, tagseq));
        tagseq = cellfun(@(x) [x, NaN(1, maxsize - numel(x))], tagseq, 'UniformOutput', false);
        
        alltagseq{idx_exp} = cat(1, tagseq{:});
        
        fprintf('.');
    end
    
    % Fill trajectories with nans
    maxsize = max(cellfun(@(x) size(x, 2), alltagseq));
    alltagseq = cellfun(@(x) [x, NaN(size(x, 1), maxsize - size(x, 2))], alltagseq, 'UniformOutput', false);
    alltagseq = cat(1, alltagseq{:});
    
    % --- Compute ACF
    dx_norm = alltagseq - nanmean(alltagseq(:)); % center
    dx_norm = dx_norm/nanstd(dx_norm(:));        % normalize
    mxco = NaN(1, ncorr);
    exco = NaN(1, ncorr);
    % Manual ACF
    for n = 1:ncorr
        Deltadx = dx_norm(:, n:end).*dx_norm(:, 1:end-n+1);
        mxco(n) = nanmean(Deltadx(:));
        exco(n) = nanstd(Deltadx(:))./sqrt(numel(Deltadx));
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Analytical fit
    file_pturn = [fn_pturn 'T' num2str(T(idT)) '.mat'];
    data_pturn = load(file_pturn, 'pt');
    pt = mean(data_pturn.pt);
    x = 1:ncorr-1;
    g = fittype(@(pflip, x) (pt^2).*(1 - 2*pflip).^x);
    fitt = fit(x', mxco(2:end)', g, 'StartPoint', 0.5);

    pflips(idT) = fitt.pflip;
    dummy = confint(fitt);
    conf_int(idT, :) = dummy(:, 1)';
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = 'none';
    shadopt.FaceAlpha = 0.275;
    
    % Bouts
    set(0, 'CurrentFigure', figs.bouts);
    e{idT} = errorshaded(x, mxco(2:end), exco(2:end), 'line', lineopt, 'patch', shadopt);
    
    p = plot(1:0.01:ncorr, fitt(1:0.01:ncorr));
    p.Color = colors(idT, :);
    p.LineWidth = 1.75;
    xlabel('# Bout');
    ylabel('<ACF(\delta{\theta})>_{traj}');
    title('Bout autocorrelations');
    
end

fprintf('pflip from fit :\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n\tT = %i°C : %0.4f\n', ...
    T(1), pflips(1), T(2), pflips(2), T(3), pflips(3), T(4), pflips(4), T(5), pflips(5));

% Display legends
set(0, 'CurrentFigure', figs.bouts);
e{end} = plot(NaN, NaN, 'Color', 'k', 'DisplayName', 'Fit', 'LineWidth', 1.75);
axis square
legend([e{:}]);

% - Plot extracted pflip values
f = figure; hold on
f.Name = 'pflipFromPooledACFBin';
neg = pflips - conf_int(:, 1);
pos = pflips - conf_int(:, 2);
e = errorbar(T, pflips, neg, pos);
e.Color = 'k';
xlabel('Temperature [°C]');
ylab = ylabel('p_{flip}');
title('Flipping probability from autocorr. fit');
axis square
axis([15 35 0 0.6]);
plot([15 35], [.5 .5], '--k');
ax = gca;
ax.XTick = [18, 22, 26, 30, 33];