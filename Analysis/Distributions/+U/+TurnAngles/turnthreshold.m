% Fit the turn angles distribution with a gaussian + gamma distributions
% to find threshold for turns

% close all
clear
clc

% * Paths
fn_dtheta = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Bins
binlim = [0, 100];
bintscalelim = [0, 6];
binfscalelim = [0, 4];
method = 'hist';         % Method for PDF
mode = 'edges';         % Mode for bins
kdeopt = {'BandWidth', 0.5};
nbins = 100;
xfit = linspace(binlim(1), binlim(2), 1500);
bintscale = linspace(bintscalelim(1), bintscalelim(2), nbins);
binfscale = linspace(binfscalelim(1), binfscalelim(2), nbins);

% * Fit
gausswid = 4;
gamscale = 10;

% * Figure
colors = rainbow(numel(T));
fig = figure; ax = axes(fig); hold(ax, 'on');
ftscale = figure; axtnorm = axes(ftscale); hold(axtnorm, 'on');
ffscale = figure; axfnorm = axes(ffscale); hold(axfnorm, 'on');
p = cell(numel(T), 1);

% --- Prepare bin vectors
switch mode
    case 'edges'
        bins = linspace(binlim(1), binlim(2), nbins+1)';
    case 'centers'
        bins = linspace(binlim(1), binlim(2), nbins)';
end

% --- Load data
dtheta = load(fn_dtheta, 'dtheta');
dtheta = dtheta.dtheta;

% --- Processing
pturns = NaN(numel(T), 1);
gamshapes = NaN(numel(T), 1);
TCROSS = NaN(numel(T), 1);
for idT = 1:numel(T)
    
    du = dtheta{idT};
    du(isnan(du)) = [];
    du = abs(du);    
    
    % - Get PDF
    switch method
        case 'hist'
            [pdft, cbin] = computePDF(bins, du, 'method', method, 'mode', mode);
            scaled_cdfd = computeCDF(bins, du, 'method', method, 'mode', mode);
        case 'kde'
            [pdft, cbin] = computePDF(bins, du, 'method', method, 'mode', mode, 'param', kdeopt);
            scaled_cdfd = computeCDF(bins, du, 'method', method, 'mode', mode, 'param', kdeopt);
    end
    
    % - Fit
    gam = @(gamshape, x) gampdf(x, gamshape, gamscale);
    gau = @(x) 2/gausswid/sqrt(2*pi)*exp(-x.^2/2/gausswid^2);
    fun = @(pturn, gamshape, x) (1 - pturn)*gau(x) + pturn*gam(gamshape, x);
    ft = fittype(fun);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0, 0], 'Upper', [1, 10], 'StartPoint', [0.7, 4.5]);
    
    f = fit(cbin, pdft', ft, fo);
    
    % Find crossing
    distdiff = @(x) (1 - f.pturn)*gau(x) - f.pturn*gam(f.gamshape, x);
    thetacross = fzero(distdiff, binlim)
    
    TCROSS(idT) = thetacross;
    
    pturns(idT) = f.pturn;
    gamshapes(idT) = f.gamshape;
    
    % - Get normalized pdf
    dutrnscale = du;
    dutrnscale(dutrnscale < thetacross) = [];
    dutrnscale = dutrnscale./(mean(dutrnscale));
    
    dufwdscale = du;
    dufwdscale(dufwdscale >= thetacross) = [];
    dufwdscale = dufwdscale./(mean(dufwdscale));
    
    switch method
        case 'hist'
            [pdftscale, cbintscale] = computePDF(bintscale, dutrnscale, 'method', method, 'mode', mode);
            
            [pdffscale, cbinfscale] = computePDF(binfscale, dufwdscale, 'method', method, 'mode', mode);
        case 'kde'
            [pdftscale, cbintscale] = computePDF(bintscale, dutrnscale, 'method', method, 'mode', mode, 'param', kdeopt);
            
            [pdffscale, cbinfscale] = computePDF(binfscale, dufwdscale, 'method', method, 'mode', mode, 'param', kdeopt);
    end
    
    % - Display
    % PDF
    p{idT} = plot(ax, cbin, pdft);
    p{idT}.Color = colors(idT, :);
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    g = plot(ax, xfit, f(xfit));
    g.Color = 'r';
    
    % Rescaled
    pn = plot(axtnorm, cbintscale, pdftscale);
    pn.Color = colors(idT, :);
    pn.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    pn = plot(axfnorm, cbinfscale, pdffscale);
    pn.Color = colors(idT, :);
    pn.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
end

% - Cosmetic
fkplt = plot(ax, [NaN NaN], [NaN NaN], 'k', 'DisplayName', 'Fit');
ax.YScale = 'log';
ax.XLabel.String = '\delta\theta [deg]';
ax.YLabel.String = 'pdf';
ax.YLim = [1e-5, 1];
legend(ax, [p{:}, fkplt]);

axtnorm.YScale = 'log';
xlabel(axtnorm, '\delta\theta_{turn}/<\delta\theta_{turn}>');
ylabel(axtnorm, 'pdf');
legend(axtnorm);
axis(axtnorm, 'square');

axfnorm.YScale = 'log';
xlabel(axfnorm, '\delta\theta_{forward}/<\delta\theta_{forward}>_T');
ylabel(axfnorm, 'pdf');
axis(axfnorm, 'square');
legend(axfnorm);