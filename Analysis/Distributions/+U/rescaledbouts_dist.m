% Prepare scaled distributions for multivariate simulations.
% Produce universal distributions for interbout intervals, displacements
% and reorientations of turn angles. They are scaled by the mean of each
% trajectory. This corresponds to epsilon

% close all
clear
clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_out = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'universaldistributions.mat'];

% * % * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
theta_threshold = 10;   % threshold for turns/forwards

% * Bins
method = 'kde';             % Method for PDF ('kde' or 'hist')
mode = 'edges';             % Mode for bins
bws = [0.1, 0.1, 0.1, 0.1]; % Bandwidths for KDE (ibi, dsp, reo)
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 200;
lim_ibi = [0, 4];
lim_dsp = [0, 4];
lim_reo = [0, 4];
lim_fwd = [0, 4];

% --- Preparations
% * Bin vectors
switch mode
    case 'edges'
        binsi = linspace(lim_ibi(1), lim_ibi(2), nbins + 1)';
        binsd = linspace(lim_dsp(1), lim_dsp(2), nbins + 1)';
        binsr = linspace(lim_reo(1), lim_reo(2), nbins + 1)';
        binsf = linspace(lim_fwd(1), lim_fwd(2), nbins + 1)';
    case 'centers'
        binsi = linspace(lim_ibi(1), lim_ibi(2), nbins)';
        binsd = linspace(lim_dsp(1), lim_dsp(2), nbins)';
        binsr = linspace(lim_reo(1), lim_reo(2), nbins)';
        binsf = linspace(lim_fwd(1), lim_fwd(2), nbins)';
        binsi = centers2edges(binsi);
        binsd = centers2edges(binsd);
        binsr = centers2edges(binsr);
        binsf = centers2edges(binsf);
end

% * Figures
colors = rainbow(numel(T));
pwid = 2.5;
fitcol = [0.1, 0.1, 0.1];
fitwid = 2;

% * Load data
data = load(fn_dataset);

% * Arrays
pdf_ibi = NaN(nbins, numel(T));
pdf_dsp = NaN(nbins, numel(T));
pdf_reo = NaN(nbins, numel(T));
pdf_fwd = NaN(nbins, numel(T));

cdf_ibi = NaN(nbins, numel(T));
cdf_dsp = NaN(nbins, numel(T));
cdf_reo = NaN(nbins, numel(T));
cdf_fwd = NaN(nbins, numel(T));

erreibi = cell(numel(T), 1);
erredsp = cell(numel(T), 1);
errereo = cell(numel(T), 1);
errefwd = cell(numel(T), 1);

pooledibi = cell(numel(T), 1);
pooleddsp = cell(numel(T), 1);
pooledreo = cell(numel(T), 1);
pooledfwd = cell(numel(T), 1);

% --- Processing
for idT = 1:numel(T)
        
    fprintf(['Computing scaled distributions for T = ', num2str(T(idT)), '°C ']); tic
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
    fwd(fwd == 0) = eps;
    
    % Rescale by trajectory means
    allibi = ibi./nanmean(ibi, 1);
    alldsp = dsp./nanmean(dsp, 1);
    allreo = reo./nanmean(reo, 1);
    allfwd = fwd./nanmean(fwd, 1);
    
    % Pool all bouts
    allibi = allibi(:);
    allibi(isnan(allibi)) = [];
    alldsp = alldsp(:);
    alldsp(isnan(alldsp)) = [];
    allreo = allreo(:);
    allreo(isnan(allreo)) = [];
    allfwd = allfwd(:);
    allfwd(isnan(allfwd)) = [];
    
    pooledibi{idT} = allibi;
	pooleddsp{idT} = alldsp;
    pooledreo{idT} = allreo;
    pooledfwd{idT} = allfwd;
    
    % Get distributions
    switch method
        case 'hist'
            [pdf_ibi(:, idT), cbinsi] = computePDF(binsi, allibi, 'mode', 'edges', 'method', method);
            [pdf_dsp(:, idT), cbinsd] = computePDF(binsd, alldsp, 'mode', 'edges', 'method', method);
            [pdf_reo(:, idT), cbinsr] = computePDF(binsr, allreo, 'mode', 'edges', 'method', method);
            [pdf_fwd(:, idT), cbinsf] = computePDF(binsf, allfwd, 'mode', 'edges', 'method', method);
            
            cdf_ibi(:, idT) = computeCDF(binsi, allibi, 'mode', 'edges', 'method', method);
            cdf_dsp(:, idT) = computeCDF(binsd, alldsp, 'mode', 'edges', 'method', method);
            cdf_reo(:, idT) = computeCDF(binsr, allreo, 'mode', 'edges', 'method', method);
            cdf_fwd(:, idT) = computeCDF(binsf, allfwd, 'mode', 'edges', 'method', method);
            
        case 'kde'
            [pdf_ibi(:, idT), cbinsi] = computePDF(binsi, allibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
            [pdf_dsp(:, idT), cbinsd] = computePDF(binsd, alldsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
            [pdf_reo(:, idT), cbinsr] = computePDF(binsr, allreo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
            [pdf_fwd(:, idT), cbinsf] = computePDF(binsf, allfwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
            
            cdf_ibi(:, idT) = computeCDF(binsi, allibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
            cdf_dsp(:, idT) = computeCDF(binsd, alldsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
            cdf_reo(:, idT) = computeCDF(binsr, allreo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
            cdf_fwd(:, idT) = computeCDF(binsf, allfwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
    end
    
    % Get error with bootstrapping
%     fun = @(y) computePDF(binsi, y, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
%     erreibi{idT} = bootci(nboots, {fun, allibi}, 'Options', statset('UseParallel', usepar));
    erreibi{idT} = [pdf_ibi(:, idT), pdf_ibi(:, idT)]';
    
%     fun = @(y) computePDF(binsd, y, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
%     erredsp{idT} = bootci(nboots, {fun, alldsp}, 'Options', statset('UseParallel', usepar));
    erredsp{idT} = [pdf_dsp(:, idT), pdf_dsp(:, idT)]';
    
%     fun = @(y) computePDF(binsr, y, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
%     errereo{idT} = bootci(nboots, {fun, allreo}, 'Options', statset('UseParallel', usepar));
    errereo{idT} = [pdf_reo(:, idT), pdf_reo(:, idT)]';
    
%     fun = @(y) computePDF(binsf, y, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
%     errefwd{idT} = bootci(nboots, {fun, allfwd}, 'Options', statset('UseParallel', usepar));
    errefwd{idT} = [pdf_fwd(:, idT), pdf_fwd(:, idT)]';
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% --- Get mean rescaled pdf and cdf
univcdf_ibi = mean(cdf_ibi, 2);
univcdf_dsp = mean(cdf_dsp, 2);
univcdf_reo = mean(cdf_reo, 2);
univcdf_fwd = mean(cdf_fwd, 2);

% Rename bins
univbin_ibi = cbinsi;
univbin_dsp = cbinsd;
univbin_reo = cbinsr;
univbin_fwd = cbinsf;

% --- Fitting rescaled distributions
pooledibi = cat(1, pooledibi{:});
pooleddsp = cat(1, pooleddsp{:});
pooledreo = cat(1, pooledreo{:});
pooledfwd = cat(1, pooledfwd{:});

pdibi = fitdist(pooledibi(pooledibi<2), 'Gamma');
pddsp = fitdist(pooleddsp, 'Gamma');
pdreo = fitdist(pooledreo, 'Gamma');
pdfwd = fitdist(pooledfwd, 'Gamma');

binsibi = linspace(0, cbinsi(end), 1000);
binsdsp = linspace(0, cbinsd(end), 1000);
binsreo = linspace(0, cbinsr(end), 1000);
binsfwd = linspace(0, cbinsf(end), 1000);

gamibi = pdibi.pdf(binsibi);
gamdsp = pddsp.pdf(binsdsp);
gamreo = pdreo.pdf(binsreo);
gamfwd = pdfwd.pdf(binsfwd);

% --- Display

% * Plot pdf with shaded error bars
% Prepare figures
figure('Name', 'Epspdfibi'); axibi = gca; hold(axibi, 'on');
figure('Name', 'Epspdfdsp'); axdsp = gca; hold(axdsp, 'on');
figure('Name', 'Epspdfreo'); axreo = gca; hold(axreo, 'on');
figure('Name', 'Epspdffwd'); axfwd = gca; hold(axfwd, 'on');

lineopt = struct;
lineopt.LineWidth = pwid;
lineopt.Marker = 'none';
shadopt = struct;
shadopt.FaceAlpha = 0.275;

pibi = cell(numel(T), 1);
pdsp = cell(numel(T), 1);
preo = cell(numel(T), 1);
pfwd = cell(numel(T), 1);
for idT = 1:numel(T)
    
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    
    pibi{idT} = bierrorshaded(cbinsi, pdf_ibi(:, idT), ...
        erreibi{idT}(1, :), erreibi{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axibi);
    
    pdsp{idT} = bierrorshaded(cbinsd, pdf_dsp(:, idT), ...
        erredsp{idT}(1, :), erredsp{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axdsp);
    
    preo{idT} = bierrorshaded(cbinsr, pdf_reo(:, idT), ...
        errereo{idT}(1, :), errereo{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axreo);
    
    pfwd{idT} = bierrorshaded(cbinsf, pdf_fwd(:, idT), ...
        errefwd{idT}(1, :), errefwd{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axfwd);
    
end

% Add fits
p = plot(axibi, binsibi, gamibi);
p.Color = fitcol;
p.LineWidth = fitwid;
p.DisplayName = 'Gamma fit';
q = plot(axdsp, binsdsp, gamdsp);
q.Color = fitcol;
q.LineWidth = fitwid;
q.DisplayName = 'Gamma fit';
r = plot(axreo, binsreo, gamreo);
r.Color = fitcol;
r.LineWidth = fitwid;
r.DisplayName = 'Gamma fit';
s = plot(axfwd, binsreo, gamfwd);
s.Color = fitcol;
s.LineWidth = fitwid;
s.DisplayName = 'Gamma fit';

% Add legends
legend(axibi, [pibi{:}, p]);
legend(axdsp, [pdsp{:}, q]);
legend(axreo, [preo{:}, r]);
legend(axfwd, [pfwd{:}, s]);

% Add labels
xlabel(axibi, '\delta{t}/<\delta{t}>_{traj.}');
ylabel(axibi, 'pdf');
axis(axibi, 'square');
xlabel(axdsp, 'd/<d>_{traj.}');
ylabel(axdsp, 'pdf');
axis(axdsp, 'square');
xlabel(axreo, '\delta\theta_t/<\delta\theta_t>_{traj.}');
ylabel(axreo, 'pdf');
axis(axreo, 'square');
ylim(axreo, [1e-5, 1.2]);
xlabel(axfwd, '\delta\theta_f/<\delta\theta_f>_{traj.}');
ylabel(axfwd, 'pdf');
axis(axfwd, 'square');

% * Plot cdf
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
labels{end + 1} = 'Mean';

figure('Name', 'Epscdfibi'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsi, cdf_ibi);
plot(cbinsi, univcdf_ibi, '--k');
legend(labels);
xlabel('\delta{t}/<\delta{t}>_{traj.}');
ylabel('cdf');
axis(ax, 'square');

figure('Name', 'Epscdfdsp'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsd, cdf_dsp);
plot(cbinsd, univcdf_dsp, '--k');
legend(labels);
xlabel('d/<d>_{traj.}');
ylabel('cdf');
axis(ax, 'square');

figure('Name', 'Epscdfreo'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsr, cdf_reo);
plot(cbinsr, univcdf_reo, '--k');
legend(labels);
xlabel('\delta\theta_{turn}/<\delta\theta_{turn}>_{traj}');
ylabel('cdf');
axis(ax, 'square');

figure('Name', 'Epscdffwd'); ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(cbinsf, cdf_fwd);
plot(cbinsf, univcdf_fwd, '--k');
legend(labels);
xlabel('\delta\theta_{forward}/<\delta\theta_{forward}>_{traj.}');
ylabel('cdf');
axis(ax, 'square');

% --- Save
if ~isempty(fn_out)
    fprintf('Saving...');
    save(fn_out, 'univbin_ibi', 'univbin_dsp', 'univbin_reo', 'univbin_fwd', ...
        'univcdf_ibi', 'univcdf_dsp', 'univcdf_reo', 'univcdf_fwd');
    fprintf('\tDone.\n');
end