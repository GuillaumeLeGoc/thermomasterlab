% Plot residence time in left or right state distributions & PSD

% close all
clear
% clc

% --- Parameters
% * File names
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn_ibi = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MeanOverTemperatures.mat'];
% fn_ibi = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MedianOverTemperatures.mat'];
% fn_ibi = @(n) [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'statistics_T' num2str(n) '.mat'];
fn_pfl = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pflips_meanreorientation_bin.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters
theta_threshold = 10;   % deg

% * Bins
nbins = 250;
freq = logspace(log10(5e-2), log10(1), nbins)';
ncorr = 500;            % frames

% * Get data
data = load(fn_dataset);

% --- Get interbout intervals
% Mean
fntmp = load(fn_ibi, 'mibi', 'ci_mibi');
mibi = fntmp.mibi;
ci_mibi = fntmp.ci_mibi;
morm = '<\delta{t}>';

% Median
% fntmp = load(fn_ibi, 'medibi', 'ci_medibi');
% mibi = fntmp.medibi;
% ci_mibi = fntmp.ci_medibi;
% morm = 'median(\delta{t})';

% --- Prepare figures
figs = cell(2, 1);
figs{1} = figure; hold on
figs{1}.Name = 'TelegraphACFPooledBehaviour';
figs{2} = figure; hold on
figs{2}.Name = 'TelegraphPSDPooledBehaviour';
colors = rainbow(numel(T));
plt_acf = cell(numel(T), 1);
plt_psd = cell(numel(T), 1);

% --- Processing
nu_acf = NaN(numel(T), 1);
nu_psd = NaN(numel(T), 1);
conf_int_psd = NaN(numel(T), 2);
conf_int_acf = NaN(numel(T), 2);
tmp = load(fn_pfl);
pflips = tmp.pflip;
err = tmp.conf_int;
errpflips = diff(err, 1, 2);
errmibi = NaN(numel(T), 1);
for idT = 1:numel(T)
        
    fprintf(['Computing ACF of RTS for T = ', num2str(T(idT)), '°C...']); tic
    
    % Get data
    turnangle = data.dtheta{idT};
    time = data.bouttime{idT};
    framerate = data.framerates{idT};
    nseq = size(turnangle, 2);
    
    restime = cell(nseq, 1);
    pooled_pxx = NaN(nseq, numel(freq));
    pooled_acf = NaN(nseq, ncorr + 1);
    pooled_plg = NaN(nseq, ncorr + 1);
    toclean = false(nseq, 1);
    for seq = 1:nseq
        
        % Bouts
        dtheta = turnangle(:, seq);
        dtheta(isnan(dtheta)) = [];
        dtheta = dtheta - mean(dtheta);
        
        % Binarize turn angles
        dthetacopy = dtheta;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
        dtheta(abs(dthetacopy) < theta_threshold) = NaN;
        
        % Create the telegraph signal
        t = time(:, seq);
        t(isnan(t)) = [];
        tvec = t(1:end - 1) - t(1);
        fvec = round(tvec*framerate);
        fr = fvec(1):fvec(end);
        dthetatime = NaN(size(fr));
        dthetatime(fvec + 1) = dtheta;
        dthetatime = fillmissing(dthetatime, 'previous');
        dthetatime = fillmissing(dthetatime, 'next');
        
        if numel(unique(dthetatime)) < 2
            toclean(seq) = true;
            continue
        end
        
%         dthetatime = dthetatime - mean(dthetatime);
%         dthetatime = dthetatime./std(dthetatime);

        % Get ACF
        [xco, lag] = xcorr(dthetatime, ncorr, 'normalized');
        selected_inds = find(lag == 0):find(lag == 0) + ncorr;
        pooled_acf(seq, :) = xco(selected_inds);%./max(xco(selected_inds));  % norm
        pooled_plg(seq, :) = lag(selected_inds)./framerate;
        
        % Get PSD
        pooled_pxx(seq, :) = periodogram(dthetatime, hamming(length(dthetatime)), freq, framerate);
    end
	
    pooled_plg(toclean, :) = [];
    pooled_acf(toclean, :) = [];
    pooled_pxx(toclean, :) = [];
    
    % - ACF
    macf = nanmean(pooled_acf);
    mplg = nanmean(pooled_plg);
    eacf = nanstd(pooled_acf, [], 1)./sqrt(size(pooled_acf, 1));
    
    % - PSD
    mpxx = nanmean(pooled_pxx, 1);
    epxx = nanstd(pooled_pxx, [], 1)./sqrt(size(pooled_pxx, 1));
    
    % --- Fit
    % - ACF
    expfit = @(nu, x) exp(-2*x.*nu);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', 0, 'Upper', Inf, 'StartPoint', 1);
    mdl = fittype(expfit, 'options', fo);
    ft_acf = fit(mplg', macf', mdl);
    
    % - PXX
    lorenfit = @(nu, I, x) (I*4*nu)./(4*nu^2 + (2*pi*x).^2);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0 0], 'Upper', [10 100], 'StartPoint', [0.25 1]);
    mdl = fittype(lorenfit, 'options', fo);
    ft_psd = fit(freq, mpxx', mdl);
    
	nu_acf(idT) = ft_acf.nu;
    nu_psd(idT) = ft_psd.nu;
    dummy = confint(ft_psd);
    conf_int_psd(idT, :) = dummy(:, 1)';
    dummy = confint(ft_acf);
    conf_int_acf(idT, :) = dummy(:, 1)';
    
    % --- Error on ibi
    errmibi(idT) = mean(abs(mibi(idT) - ci_mibi(idT, :)));
    
    % - Display
    lineopt = struct;
    lineopt.Color = colors(idT, :);
    lineopt.LineStyle = 'none';
    lineopt.Marker = '.';
    lineopt.MarkerSize = 8;
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    set(0, 'CurrentFigure', figs{1});
    plt_acf{idT} = errorshaded(mplg(1:4:end), macf(1:4:end), eacf(1:4:end), 'line', lineopt, 'patch', shadopt);
%     plt_pdf{idT} = plot(mplg(1:3:end), macf(1:3:end));
%     plt_pdf{idT}.Color = colors(idT, :);
%     plt_pdf{idT}.LineStyle = 'none';
%     plt_pdf{idT}.Marker = '.';
%     plt_pdf{idT}.MarkerSize = 8;
%     plt_pdf{idT}.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    pfit = plot(mplg, ft_acf(mplg));
    pfit.Color = colors(idT, :);
    
    set(0, 'CurrentFigure', figs{2});
    plt_psd{idT} = plot(freq, 10*log10(mpxx));
    plt_psd{idT}.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    plt_psd{idT}.Color = colors(idT, :);
    plt_psd{idT}.LineStyle = 'none';
    plt_psd{idT}.Marker = '.';
    plt_psd{idT}.MarkerSize = 8;
    qfit = plot(freq, 10*log10(ft_psd(freq)));
    qfit.Color = colors(idT, :);
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% Errors on pflip/ibi
X = pflips./mibi;
errratio = X.*sqrt( (errpflips./pflips).^2 + (errmibi./mibi).^2 );
errX = [X - errratio/2, X + errratio/2];

% Q10 fit
pairs = nchoosek(1:numel(X), 2);    
ratiorate = NaN(size(pairs, 1), 1);
diffT = NaN(size(pairs, 1), 1);
for k = 1:size(pairs, 1)
    ratiorate(k) = X(pairs(k, 2))/X(pairs(k, 1));
    diffT(k) = T(pairs(k, 2)) - T(pairs(k, 1));
end

fn = @(q10, x) q10.^(x/10);
[x, y] = prepareCurveData(diffT, ratiorate);
ft = fit(x, y, fn, 'StartPoint', 2.5);
xos = linspace(0, 15, 1000);
yos = ft(xos);
intconf = predint(ft, xos, 0.95, 'observation', 'off');
emin = intconf(:, 1);
emax = intconf(:, 2);

% - Cosmetic
set(0, 'CurrentFigure', figs{1});
fkfit = plot([NaN, NaN], [NaN, NaN], 'k');
fkfit.DisplayName = 'Fit';
legend([plt_acf{:}, fkfit]);
xlabel('Delays [s]');
ylabel('ACF');
xlim([0, 20.25]);

axis square

set(0, 'CurrentFigure', figs{2});
fkfit = plot([NaN, NaN], [NaN, NaN], 'k');
fkfit.DisplayName = 'Fit';
legend([plt_psd{:}, fkfit], 'Location', 'southwest');
ax = gca;
ax.XScale = 'log';
axis square
xlabel('frequency (Hz)');
ylabel('<power>_T (dB/Hz)');

% - Plot extracted nu values
f = figure; hold on
f.Name = 'kFlipFromPooledPSD';
col = [0.8, 0, 0];
coltransp = 0.9;
pp = plot(T, X);
pp.Color = [col, coltransp];
pp.DisplayName = ['p_{flip}/' morm '_T'];
manualerrbar(T, errX, 'Color', [col, coltransp]);
neg = nu_psd - conf_int_psd(:, 1);
pos = nu_psd - conf_int_psd(:, 2);
e = errorbar(T, nu_psd, neg, pos);
e.DisplayName = 'k_{flip} from PSD';
e.Marker = 'diamond';
e.Color = [0.5 0.5 0.5];
xlabel('Temperature [°C]');
ylab = ylabel('k_{flip} [s^{-1}]');
title('Telegraph time constant');
neg = nu_acf - conf_int_acf(:, 1);
pos = nu_acf - conf_int_acf(:, 2);
p = errorbar(T, nu_acf, neg, pos);
p.Marker = 'o';
p.Color = 'k';
p.DisplayName = 'k_{flip} from ACF';
legend([e, p, pp], 'Location', 'southeast', 'FontSize', 14);
axis square
ax = gca;
ax.XTick = [18, 22, 26, 30, 33];

% - Plot Q10 fit
f = figure; hold on
f.Name = 'kflip_Q10';
lineopt = struct;
lineopt.DisplayName = ['Fit Q10=' sprintf('%1.2f', ft.q10)];
lineopt.Color = 'r';
lineopt.Marker = 'none';
lineopt.LineWidth = 1.25;
shadopt = struct;
shadopt.FaceAlpha = 0.175;
shadopt.DisplayName = '95% conf. int.';
[p, e] = bierrorshaded(xos, yos, emin, emax, 'line', lineopt, 'patch', shadopt);
s = scatter(diffT, ratiorate, 20, 'filled', 'DisplayName', 'Data');
s.MarkerFaceColor = [0.2, 0.2, 0.2];
legend([s, p, e], 'Location', 'northwest');
axis square
xlabel('\Delta{T} = T_j - T_i [°C]');
ylabel('k_{flip,j}/k_{flip,i}');
axis([1, 15, 0, 4.5]);