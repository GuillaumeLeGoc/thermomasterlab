% Plot residence time in left or right state distributions & PSD

% close all
clear
clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters
theta_threshold = 10;   % deg

% * Window
n_corr = 20;            % in bouts

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * File
outfname = [pwd filesep 'Data' filesep 'Matfiles' filesep 'kflip_psd_perexp.mat'];

% * Bins
freq = logspace(log10(5e-2), log10(1), 100)';
nw = 4;

% * Statisical test
statest = 'kruskalwallis';

% * Figures
colors = rainbow(numel(T));
% --- Prepare figures
figspec = figure; hold on;
figspec.Name = 'spectrumByExp';
fignu = figure; hold on
fignu.Name = 'tau0PSDFit';
erb = cell(numel(T), 1);

% --- Processing
kflip_psd = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing residence time distributions for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get PSD from telegraph signal
    kflip_psd{idT} = NaN(size(ftrack, 1), 1);
    apxx = NaN(size(ftrack, 1), numel(freq));
    ftplot = NaN(size(ftrack, 1), numel(freq));
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        restime = cell(n_seq, 1);
        pxx = NaN(n_seq, numel(freq));
        
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            % Binarize turn angles
            dthetacopy = dtheta;
            dtheta(dthetacopy > theta_threshold) = 1;
            dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = NaN;
            
            if numel(dtheta) < 2*n_corr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            % Create the telegraph signal
            afr = allframes(:, seq) + 1;
            afr(isnan(afr)) = [];
            dthetatime = NaN(size(afr));
            fr = frames(:, seq) + 1;
            fr(isnan(fr)) = [];
            fr = fr - fr(1);
            fr(1) = [];
            dthetatime(fr) = dtheta;
            dthetatime = fillmissing(dthetatime, 'previous');
            dthetatime = fillmissing(dthetatime, 'next');
            
            % Get PSD
%             pxx(seq, :) = periodogram(dthetatime, hamming(length(dthetatime)), freq, framerate);
            pxx(seq, :) = pmtm(dthetatime, nw, freq, framerate);
        end
        
        % Average PSDs
        mpxx = nanmean(pxx, 1);
        
        % --- Fit
        % - PXX
        lorenfit = @(nu, I, x) (I*4*nu)./(4*nu^2 + (2*pi*x).^2);
        fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0 0], 'Upper', [10 100], 'StartPoint', [0.25 1]);
        mdl = fittype(lorenfit, 'options', fo);
        ft_psd = fit(freq, mpxx', mdl);
       
       	kflip_psd{idT}(idx_exp) = ft_psd.nu;
        
        apxx(idx_exp, :) = mpxx;
        ftplot(idx_exp, :) = ft_psd(freq)';
        
        fprintf('.');
        
    end
    
    mapxx = mean(apxx, 1);
    eapxx = std(apxx, [], 1)./sqrt(size(apxx, 1));
    mftpl = mean(ftplot, 1);
    
    errorlogged = (eapxx./mapxx).*10.*log10(mapxx);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 6;
    lineopt.Marker = '.';
    lineopt.LineStyle = 'none';
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    
    set(0, 'CurrentFigure', figspec)
    erb{idT} = errorshaded(freq, 10*log10(mapxx), errorlogged, 'line', lineopt, 'patch', shadopt);
    p = plot(freq, 10*log10(mftpl), 'Color', colors(idT, :));
    p.LineWidth = 1;
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

ax = gca; ax.XScale = 'log';    
xlabel(ax, 'Frequency [Hz]');
ylabel(ax, '<Power>_{T} [dB/Hz]');
axis(ax, 'square');
legend(ax, [erb{:}], 'Location', 'southwest');
ax.XTick = [0.1, 1];
ylim(ax, [-25, 5]);
pp = plot([NaN NaN], [NaN, NaN], 'k');
pp.DisplayName = 'Fit';

% --- Display
maxsize = max(cellfun(@numel, kflip_psd));
kflip_psd = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), kflip_psd, 'UniformOutput', false);
g = cell(size(kflip_psd));
for idx = 1:size(kflip_psd, 1)
    g{idx} = idx.*ones(numel(kflip_psd{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);
X = cat(1, kflip_psd{:});

set(0, 'CurrentFigure', fignu);
pointopt = struct;
pointopt.Color = colors;
pointopt.jitter = 0;
boxopt = struct;
boxopt.BoxColors = colors;
boxopt.Labels = labels;

beautifulbox(X, g, pointopt, boxopt);

t = title('k_{flip} from PSD fit');
yl = ylabel('k_{flip} (s^{-1})');
axis([-Inf Inf 0 1.1]);
axis square

% - Significance
% pairs = {[2, 3], [3, 4]};
% addStars(kflip_psd, 'pair', pairs, 'test', statest);
% axis square

% --- Save file
if ~isempty(outfname)
    save(outfname, 'kflip_psd');
end