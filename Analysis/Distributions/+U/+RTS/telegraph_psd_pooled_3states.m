% Plot residence time in left or right state distributions & PSD with
% forwards

% close all
clear
% clc

% --- Parameters
% * File names
fn_ibi = [pwd '/Data/Matfiles/statistics_T'];
fn_pfl = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pflips_meanreorientation_bin.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters
theta_threshold = 6;   % deg

% * Window
n_corr = 20;            % in bouts

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Bins
nbins = 50;
bins = linspace(0.3, 60, nbins);
freq = logspace(log10(5e-2), log10(1), 250)';
ncorr = 500;            % frames

% --- Prepare figures
figs = cell(2, 1);
figs{1} = figure; hold on
figs{1}.Name = 'TelegraphACFPooled';
figs{2} = figure; hold on
figs{2}.Name = 'TelegraphPSDPooled3states';
colors = rainbow(numel(T));
plt_pdf = cell(numel(T), 1);
plt_psd = cell(numel(T), 1);

% --- Processing
nu_acf = NaN(numel(T), 1);
nu_psd = NaN(numel(T), 1);
conf_int_psd = NaN(numel(T), 2);
conf_int_acf = NaN(numel(T), 2);
tmp = load(fn_pfl);
pflips = tmp.pflip;
err = tmp.conf_int;
errpflips = diff(err, 1, 2);
medibi = NaN(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing residence time distributions for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get residence times
    pooled_restime = cell(size(ftrack, 1), 1);
    pooled_pxx = cell(size(ftrack, 1), 1);
    pooled_acf = cell(size(ftrack, 1), 1);
    pooled_plg = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, ~, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        restime = cell(n_seq, 1);
        pxx = NaN(n_seq, numel(freq));
        acf = NaN(n_seq, ncorr + 1);
        plg = NaN(n_seq, ncorr + 1);
        for seq = 1:n_seq
            
            % Bouts
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            
            % Binarize turn angles
            dthetacopy = dtheta;
            dtheta(dthetacopy > theta_threshold) = 1;
            dtheta(dthetacopy < -theta_threshold) = -1;
            dtheta(abs(dthetacopy) < theta_threshold) = 0;
            
            if numel(dtheta) < 2*n_corr
                continue;
            elseif ~any(dtheta)
                continue;
            end
            
            % Create the telegraph signal
            afr = allframes(:, seq) + 1;
            afr(isnan(afr)) = [];
            dthetatime = NaN(size(afr));
            fr = frames(:, seq) + 1;
            fr(isnan(fr)) = [];
            fr = fr - fr(1);
            fr(1) = [];
            dthetatime(fr) = dtheta;
            dthetatime = fillmissing(dthetatime, 'previous');
            dthetatime = fillmissing(dthetatime, 'next');
            
            % Get residence time
            ds = diff(dthetatime);
            flipsloc = find(ds);
            if isempty(flipsloc)
                % Never switched, ignore
                continue;
            end
            rs = [flipsloc(1); diff(flipsloc)]./framerate;
            restime{seq} = rs;
            
            dthetatime(dthetatime < 0) = 0;     % Rescale data
            
            % Get ACF
            [xco, lag] = xcorr(dthetatime - mean(dthetatime), ncorr, 'normalized');
            selected_inds = find(lag == 0):find(lag == 0) + ncorr;
            acf(seq, :) = xco(selected_inds);%./max(xco(selected_inds));  % norm
            plg(seq, :) = lag(selected_inds)./framerate;
            
            % Get PSD
            pxx(seq, :) = periodogram(dthetatime, hamming(length(dthetatime)), freq, framerate);
        end
        
        % Pool data
        pooled_pxx{idx_exp} = pxx;
        pooled_restime{idx_exp} = cat(1, restime{:});
        pooled_acf{idx_exp} = acf;
        pooled_plg{idx_exp} = plg;
        
        fprintf('.');
    end
    
    % - ACF
    pooled_acf = cat(1, pooled_acf{:});
    pooled_plg = cat(1, pooled_plg{:});
    macf = nanmean(pooled_acf);
    mplg = nanmean(pooled_plg);
    eacf = nanstd(pooled_acf, [], 1)./sqrt(size(pooled_acf, 1));
    
    % - PSD
    pooled_pxx = cat(1, pooled_pxx{:});
    mpxx = nanmean(pooled_pxx, 1);
    epxx = nanstd(pooled_pxx, [], 1)./sqrt(size(pooled_pxx, 1));
    
    % --- Fit
    % - ACF
    expfit = @(nu, x) exp(-2*x.*nu);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', 0, 'Upper', Inf, 'StartPoint', 1);
    mdl = fittype(expfit, 'options', fo);
    ft_acf = fit(mplg', macf', mdl);
    % - PXX
%     lorenfit = @(nu, I, x) I*(1/nu)./(4 + (2*pi*(1/nu)*x).^2);
%     lorenfit = @(nu, I, x) (I*4*nu)./(4*nu^2 + (2*pi*x).^2);
    A = @(I, nu, x) I*nu./(nu^2 + (nu - 2*pi.*x).^ 2);
	B = @(I, nu, x) I*nu./(nu^2 + (nu + 2*pi.*x).^ 2);
	lorenfit = @(I, nu, x) A(I, nu, x) + B(I, nu, x);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0 0], 'Upper', [10 100], 'StartPoint', [0.25 1]);
    mdl = fittype(lorenfit, 'options', fo);
    ft_psd = fit(freq, mpxx', mdl);
    
	nu_acf(idT) = ft_acf.nu;
    nu_psd(idT) = ft_psd.nu;
    dummy = confint(ft_psd);
    conf_int_psd(idT, :) = dummy(:, 1)';
    dummy = confint(ft_acf);
    conf_int_acf(idT, :) = dummy(:, 1)';
    
    % - Display
    set(0, 'CurrentFigure', figs{1});
    plt_pdf{idT} = plot(mplg(1:3:end), macf(1:3:end));
    plt_pdf{idT}.Color = colors(idT, :);
    plt_pdf{idT}.LineStyle = 'none';
    plt_pdf{idT}.Marker = '.';
    plt_pdf{idT}.MarkerSize = 8;
    plt_pdf{idT}.DisplayName = ['T = ' num2str(temperature) '°C'];
    pfit = plot(mplg, ft_acf(mplg));
    pfit.Color = colors(idT, :);
    
    set(0, 'CurrentFigure', figs{2});
    plt_psd{idT} = plot(freq, 10*log10(mpxx));
    plt_psd{idT}.DisplayName = ['T = ' num2str(temperature) '°C'];
    plt_psd{idT}.Color = colors(idT, :);
    plt_psd{idT}.LineStyle = 'none';
    plt_psd{idT}.Marker = '.';
    plt_psd{idT}.MarkerSize = 8;
    qfit = plot(freq, 10*log10(ft_psd(freq)));
    qfit.Color = colors(idT, :);
    
    % - Get median interbout interval
    ftmp = load([fn_ibi num2str(T(idT)) '.mat'], 'median_ibi');
    medibi(idT) = mean(ftmp.median_ibi);
    errmedibi = std(ftmp.median_ibi)/sqrt(numel(ftmp.median_ibi));
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% Errors on pflip/ibi
X = pflips./medibi;
errratio = X.*sqrt( (errpflips./pflips).^2 + (errmedibi./medibi).^2 );
errX = [X - errratio/2, X + errratio/2];

% - Cosmetic
set(0, 'CurrentFigure', figs{1});
fkfit = plot([NaN, NaN], [NaN, NaN], 'k');
fkfit.DisplayName = 'Fit';
legend([plt_pdf{:}, fkfit]);
xlabel('Delays [s]');
ylabel('ACF');
axis square

set(0, 'CurrentFigure', figs{2});
fkfit = plot([NaN, NaN], [NaN, NaN], 'k');
fkfit.DisplayName = 'Fit';
legend([plt_psd{:}, fkfit], 'Location', 'southwest');
ax = gca;
ax.XScale = 'log';
axis square
xlabel('Frequency [Hz]');
ylabel('Power [dB/Hz]');

% - Plot extracted nu values
f = figure; hold on
f.Name = 'kFlipFromPooledPSD';
col = [0.8, 0, 0];
coltransp = 0.9;
pp = plot(T, X);
pp.Color = [col, coltransp];
pp.DisplayName = 'p_{flip}/median(\delta{t})';
manualerrbar(T, errX, 'Color', [col, coltransp]);
neg = nu_psd - conf_int_psd(:, 1);
pos = nu_psd - conf_int_psd(:, 2);
e = errorbar(T, nu_psd, neg, pos);
e.DisplayName = 'k_{flip} from PSD';
e.Marker = 'diamond';
e.Color = 'k';
xlabel('Temperature [°C]');
ylab = ylabel('k_{flip} [s^-1]');
title('Telegraph time constant');
neg = nu_acf - conf_int_acf(:, 1);
pos = nu_acf - conf_int_acf(:, 2);
p = errorbar(T, nu_acf, neg, pos);
p.Marker = 'o';
p.Color = [0.5 0.5 0.5];
p.DisplayName = 'k_{flip} from ACF';
legend([e, p, pp], 'Location', 'southeast', 'FontSize', 14);
axis square
ax = gca;
ax.XTick = [18, 22, 26, 30, 33];