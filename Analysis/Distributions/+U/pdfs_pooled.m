% Distributions of positions, interbout intervals, displacements and turn
% angles. All sequences are pooled together.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% Destination file, leave empty to not save
savefileprefix = [pwd '/Data/Matfiles/pdfpooled_'];
% savefileprefix = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;    % sequence length for each fish in seconds
% filt.bout_freq = .1;    % minimum average bout frequency
filt.n_bouts = 10;
filt.minx = 0;
filt.maxx = Inf;

% * Displacement fix
theta_threshold = 10;    % deg
displ_factor = 1.6;

% * Bins
method = 'kde';        % Method for PDF
mode = 'centers';         % Mode for bins
bws = [1, 0.1, 0.1, 1];	% Bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 200;

e.x = [100*pixsize, 1100*pixsize]; % bins centers for x (mm)
e.ibi = [0, 10];                    % bins for interbout interval (s)
e.disp = [0, 15];                   % bins for displacement (mm)
e.turn = [-180, 180];              % bins for turn angle (°, wrapped to 180)

% * Figures names
fignames = {'Xpositions', 'InterboutIntervals', 'Displacements', 'TurnAngles'};

% --- Prepare bin vectors
switch mode
    case 'edges'
        bx = linspace(e.x(1), e.x(2), nbins+1)';
        bi = linspace(e.ibi(1), e.ibi(2), nbins+1)';
        bd = linspace(e.disp(1), e.disp(2), nbins+1)';
        bt = linspace(e.turn(1), e.turn(2), 120+1)';
    case 'centers'
        bx = linspace(e.x(1), e.x(2), nbins)';
        bi = linspace(e.ibi(1), e.ibi(2), nbins)';
        bd = linspace(e.disp(1), e.disp(2), nbins)';
        bt = linspace(e.turn(1), e.turn(2), 120)';
end

% --- Prepare figures
colors = rainbow(numel(T));
figs = cell(4, 1);
for idx_fig = 1:4
    figs{idx_fig} = figure; hold on
end

% --- Prepare arrays
Xp = cell(numel(T), 1);
Ib = cell(numel(T), 1);
Di = cell(numel(T), 1);
Tu = cell(numel(T), 1);

% --- Loop over each temperatures and compute features PDF
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing distributions for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    x = cell(size(ftrack, 1), 1);
    i = cell(size(ftrack, 1), 1);
    d = cell(size(ftrack, 1), 1);
    t = cell(size(ftrack, 1), 1);
    f = cell(size(ftrack, 1), 1);
    
    for idexp = 1:size(ftrack, 1)
        
        [x{idexp}, i{idexp}, d{idexp}, t{idexp}, ~, ~, f{idexp}] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt);
        
        isturn = abs(t{idexp}) > theta_threshold;
        d{idexp}(isturn) = d{idexp}(isturn)./displ_factor;
        
        fprintf('.');
    end
    
    % Gather features
    framerate = mean(cat(1, f{:}));
    allx = cat(1, x{:}).*pixsize;
    alli = cat(1, i{:})./framerate;
    alld = cat(1, d{:}).*pixsize;
    allt = cat(1, t{:});
    
    % Get PDF
    switch method
        case 'hist'
            [pdfx, cx] = computePDF(bx, allx, 'method', method, 'mode', mode);
            [pdfi, ci] = computePDF(bi, alli, 'method', method, 'mode', mode);
            [pdfd, cd] = computePDF(bd, alld, 'method', method, 'mode', mode);
            [pdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode);
        case 'kde'
            [pdfx, cx] = computePDF(bx, allx, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(1)});
            [pdfi, ci] = computePDF(bi, alli, 'method', method, 'mode', mode, 'param', kdeopt(2));
            [pdfd, cd] = computePDF(bd, alld, 'method', method, 'mode', mode, 'param', kdeopt(3));
            [pdft, ct] = computePDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(4)});
    end
    
    % Get CDF
    switch method
        case 'hist'
            [cdfx, ccx] = computeCDF(bx, allx, 'method', method, 'mode', mode);
            [cdfi, cci] = computeCDF(bi, alli, 'method', method, 'mode', mode);
            [cdfd, ccd] = computeCDF(bd, alld, 'method', method, 'mode', mode);
            [cdft, cct] = computeCDF(bt, allt, 'method', method, 'mode', mode);
        case 'kde'
            [cdfx, ccx] = computeCDF(bx, allx, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(1)});
            [cdfi, cci] = computeCDF(bi, alli, 'method', method, 'mode', mode, 'param', kdeopt(2));
            [cdfd, ccd] = computeCDF(bd, alld, 'method', method, 'mode', mode, 'param', kdeopt(3));
            [cdft, cct] = computeCDF(bt, allt, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(4)});
    end
    
    % Get CDF
%     [cdfx, ccx] = ecdf(allx);
%     [cdfi, cci] = ecdf(alli);
%     [cdfd, ccd] = ecdf(alld);
%     [cdft, cct] = ecdf(allt);
    
    % Store all data
    Xp{idT} = allx;
    Ib{idT} = alli;
    Di{idT} = alld;
    Tu{idT} = allt;
    
    % Get statistics
    mx = trapz(cx, cx.*pdfx);
    mi = trapz(ci, ci.*pdfi);
    md = trapz(cd, cd.*pdfd);
    mt = trapz(ct, ct.*pdft);
    
    nbouts = num2str(numel(alli));
    nfish = num2str(sum(cell2mat(nfish)));
    fprintf('\t Done (%s bouts, %2.2fs).\n', nbouts, toc);
    
    % --- Display

    set(0, 'CurrentFigure', figs{1})
    lineopt = plot(cx, pdfx);
%     lineopt.DisplayName = ['T = ' num2str(temperature) '°C (' nfish ' fish, ' nbouts ' bouts)'];
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.LineWidth = 1.5;
    erb{1}{idT} = lineopt;
    xlabel('Position [mm]');
    ylabel('PDF');
    ax = gca;
    plot([mx mx], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{2});
    lineopt = plot(ci, pdfi);
%     lineopt.DisplayName = ['T = ' num2str(temperature) '°C (' nfish ' fish, ' nbouts ' bouts)'];
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.5;
    erb{2}{idT} = lineopt;
    xlabel('Interbout interval [s]');
    ylabel('PDF');
    ax = gca;
    ax.XLim = [0, 5];
    ax.YLim = [0, 2];
    plot([mi mi], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{3});
    lineopt = plot(cd, pdfd);
%     lineopt.DisplayName = ['T = ' num2str(temperature) '°C (' nfish ' fish, ' nbouts ' bouts)'];
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.75;
    erb{3}{idT} = lineopt;
    xlabel('Displacement [mm]');
    ylabel('PDF');
    ax = gca;
    ax.XLim = [0, 8];
    ax.YLim = [0, 1];
    plot([md md], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{4});
    lineopt = plot(ct, pdft);
%     lineopt.DisplayName = ['T = ' num2str(temperature) '°C (' nfish ' fish, ' nbouts ' bouts)'];
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 5;
    lineopt.LineWidth = 1.5;
    erb{4}{idT} = lineopt;
    xlabel('\delta{\theta} [deg]');
    ylabel('PDF');
    ax = gca; ax.YScale = 'log';
    ax.XLim = [e.turn(1), e.turn(2)];
    ax.YLim = [1e-4, 1e-1];
    plot([mt, mt], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .4);
    
    % --- Save file
    if ~isempty(savefileprefix)
        fname = [savefileprefix 'pdf_T' num2str(T(idT)) '.mat'];
        save(fname, 'cx', 'ci', 'cd', 'ct', 'pdfx', 'pdfi', 'pdfd', 'pdft');
        fname = [savefileprefix 'cdf_T' num2str(T(idT)) '.mat'];
        save(fname, 'ccx', 'cci', 'ccd', 'cct', 'cdfx', 'cdfi', 'cdfd', 'cdft');
    end 
end

% --- Save all features
if ~isempty(savefileprefix)
    fname = [savefileprefix 'features.mat'];
    save(fname, 'Xp', 'Ib', 'Tu', 'Di');
end

% Display legends
fkm = plot(NaN, NaN, 'k--', 'DisplayName', 'Mean', 'LineWidth', .6);
for idx_fig = 1:numel(figs)
    set(0, 'CurrentFigure', figs{idx_fig});
    legend([erb{idx_fig}{:} fkm]);
    figs{idx_fig}.Name = fignames{idx_fig};
    axis square
end