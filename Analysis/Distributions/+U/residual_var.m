% Compare residual variability of means, intra and inter trajectory.
% Intra : distribution of X after rescaling values by the mean of the
% trajectory
% Inter : distribution of means rescaled by the mean across temperature

% close all
clear
clc

% --- Parameters
% * % * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum number of bouts per seconds
filt.minx = 0;
filt.maxx = Inf;

pixsize = 0.09;         % pixel size in mm
theta_threshold = 10;   % threshold for turns/forwards
displ_factor = 1.6;     % scaling factor for displacements of turns

% * Bins
method = 'hist';         % Method for PDF ('kde' or 'hist')
mode = 'centers';       % Mode for bins
bws = [0.2, 0.2, 0.2, 0.2];  % Bandwidths for KDE (ibi, dsp, reo)
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 100;
lim_ibi = [0, 8];
lim_dsp = [0, 8];
lim_reo = [0, 4];
lim_fwd = [0, 4];

% --- Preparations
% * Bin vectors
switch mode
    case 'edges'
        binsi = linspace(lim_ibi(1), lim_ibi(2), nbins + 1)';
        binsd = linspace(lim_dsp(1), lim_dsp(2), nbins + 1)';
        binsr = linspace(lim_reo(1), lim_reo(2), nbins + 1)';
        binsf = linspace(lim_fwd(1), lim_fwd(2), nbins + 1)';
        cbinsi = edges2centers(binsi);
        cbinsd = edges2centers(binsd);
        cbinsr = edges2centers(binsr);
        cbinsf = edges2centers(binsf);
    case 'centers'
        cbinsi = linspace(lim_ibi(1), lim_ibi(2), nbins)';
        cbinsd = linspace(lim_dsp(1), lim_dsp(2), nbins)';
        cbinsr = linspace(lim_reo(1), lim_reo(2), nbins)';
        cbinsf = linspace(lim_fwd(1), lim_fwd(2), nbins)';
end

% * Figures
colors = rainbow(numel(T));
fibi_intra = figure; aibi_intra = axes(fibi_intra); hold(aibi_intra, 'on');
fibi_inter = figure; aibi_inter = axes(fibi_inter); hold(aibi_inter, 'on');
fdsp_intra = figure; adsp_intra = axes(fdsp_intra); hold(adsp_intra, 'on');
fdsp_inter = figure; adsp_inter = axes(fdsp_inter); hold(adsp_inter, 'on');
freo_intra = figure; areo_intra = axes(freo_intra); hold(areo_intra, 'on');
freo_inter = figure; areo_inter = axes(freo_inter); hold(areo_inter, 'on');
ffwd_intra = figure; afwd_intra = axes(ffwd_intra); hold(afwd_intra, 'on');
ffwd_inter = figure; afwd_inter = axes(ffwd_inter); hold(afwd_inter, 'on');

% * Arrays
pibi_intra = NaN(nbins, numel(T));
pdsp_intra = NaN(nbins, numel(T));
preo_intra = NaN(nbins, numel(T));
pfwd_intra = NaN(nbins, numel(T));
pibi_inter = NaN(nbins, numel(T));
pdsp_inter = NaN(nbins, numel(T));
preo_inter = NaN(nbins, numel(T));
pfwd_inter = NaN(nbins, numel(T));
cibi_intra = NaN(nbins, numel(T));
cdsp_intra = NaN(nbins, numel(T));
creo_intra = NaN(nbins, numel(T));
cfwd_intra = NaN(nbins, numel(T));
cibi_inter = NaN(nbins, numel(T));
cdsp_inter = NaN(nbins, numel(T));
creo_inter = NaN(nbins, numel(T));
cfwd_inter = NaN(nbins, numel(T));

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing scaled distributions for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Pool parameters from all experiments
    allibi_intra = cell(size(ftrack, 1), 1);
    allibi_inter = cell(size(ftrack, 1), 1);
    alldsp_intra = cell(size(ftrack, 1), 1);
    alldsp_inter = cell(size(ftrack, 1), 1);
    allreo_intra = cell(size(ftrack, 1), 1);
    allreo_inter = cell(size(ftrack, 1), 1);
    allfwd_intra = cell(size(ftrack, 1), 1);
    allfwd_inter = cell(size(ftrack, 1), 1);
    for idexp = 1:size(ftrack, 1)
        
        % Get parameters
        [~, ibi, dsp, reo, ~, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        ibi = ibi./framerate;                   % convert to s
        ibi_intra = ibi./nanmean(ibi);          % rescale by the mean of each traj.
        ibi_inter = nanmean(ibi);
        ibi_intra = ibi_intra(:);
        ibi_intra(isnan(ibi_intra)) = [];
        ibi_inter = ibi_inter(:);
        allibi_intra{idexp} = ibi_intra;
        allibi_inter{idexp} = ibi_inter;
        
        dsp = dsp.*pixsize;                     % convert to mm
        reo = abs(reo);
        isturn = reo > theta_threshold;         % detect turns
        dsp(isturn) = dsp(isturn)./displ_factor;% rescale displacements of turns
        dsp_intra = dsp./nanmean(dsp);
        dsp_inter = nanmean(dsp);
        dsp_intra = dsp_intra(:);
        dsp_intra(isnan(dsp_intra)) = [];
        dsp_inter = dsp_inter(:);
        alldsp_intra{idexp} = dsp_intra;
        alldsp_inter{idexp} = dsp_inter;
        
        reot = reo;
        reot(~isturn) = NaN;
        reo_intra = reot./nanmean(reot);
        reo_inter = nanmean(reot);
        reo_intra = reo_intra(:);
        reo_intra(isnan(reo_intra)) = [];
        reo_inter = reo_inter(:);
        allreo_intra{idexp} = reo_intra;
        allreo_inter{idexp} = reo_inter;
        
        reof = reo;
        reof(isturn) = NaN;
        fwd_intra = reof./nanmean(reof);
        fwd_inter = nanmean(reof);
        fwd_intra = fwd_intra(:);
        fwd_intra(isnan(fwd_intra)) = [];
        fwd_inter = fwd_inter(:);
        allfwd_intra{idexp} = fwd_intra;
        allfwd_inter{idexp} = fwd_inter;
        
        fprintf('.');
    end
    
    % Pool data
    allibi_intra = cat(1, allibi_intra{:});
    alldsp_intra = cat(1, alldsp_intra{:});
    allreo_intra = cat(1, allreo_intra{:});
    allfwd_intra = cat(1, allfwd_intra{:});
    allibi_inter = cat(1, allibi_inter{:});
    alldsp_inter = cat(1, alldsp_inter{:});
    allreo_inter = cat(1, allreo_inter{:});
    allfwd_inter = cat(1, allfwd_inter{:});
    
    % Scale by mean over temperature
    allibi_inter = allibi_inter./nanmean(allibi_inter);
    alldsp_inter = alldsp_inter./nanmean(alldsp_inter);
    allreo_inter = allreo_inter./nanmean(allreo_inter);
    allfwd_inter = allfwd_inter./nanmean(allfwd_inter);
    
    % Get distributions
    switch method
        case 'hist'
            pibi_intra(:, idT) = computePDF(cbinsi, allibi_intra, 'method', method);
            pdsp_intra(:, idT) = computePDF(cbinsd, alldsp_intra, 'method', method);
            preo_intra(:, idT) = computePDF(cbinsr, allreo_intra, 'method', method);
            pfwd_intra(:, idT) = computePDF(cbinsf, allfwd_intra, 'method', method);
            
            pibi_inter(:, idT) = computePDF(cbinsi, allibi_inter, 'method', method);
            pdsp_inter(:, idT) = computePDF(cbinsd, alldsp_inter, 'method', method);
            preo_inter(:, idT) = computePDF(cbinsr, allreo_inter, 'method', method);
            pfwd_inter(:, idT) = computePDF(cbinsf, allfwd_inter, 'method', method);
            
            cibi_intra(:, idT) = computeCDF(cbinsi, allibi_intra, 'method', method);
            cdsp_intra(:, idT) = computeCDF(cbinsd, alldsp_intra, 'method', method);
            creo_intra(:, idT) = computeCDF(cbinsr, allreo_intra, 'method', method);
            cfwd_intra(:, idT) = computeCDF(cbinsf, allfwd_intra, 'method', method);
            
            cibi_inter(:, idT) = computeCDF(cbinsi, allibi_inter, 'method', method);
            cdsp_inter(:, idT) = computeCDF(cbinsd, alldsp_inter, 'method', method);
            creo_inter(:, idT) = computeCDF(cbinsr, allreo_inter, 'method', method);
            cfwd_inter(:, idT) = computeCDF(cbinsf, allfwd_inter, 'method', method);
            
        case 'kde'
            pibi_intra(:, idT) = computePDF(cbinsi, allibi_intra, 'method', method, 'param', kdeopt(1));
            pdsp_intra(:, idT) = computePDF(cbinsd, alldsp_intra, 'method', method, 'param', kdeopt(2));
            preo_intra(:, idT) = computePDF(cbinsr, allreo_intra, 'method', method, 'param', kdeopt(3));
            pfwd_intra(:, idT) = computePDF(cbinsf, allfwd_intra, 'method', method, 'param', kdeopt(4));
            
            pibi_inter(:, idT) = computePDF(cbinsi, allibi_inter, 'method', method, 'param', kdeopt(1));
            pdsp_inter(:, idT) = computePDF(cbinsd, alldsp_inter, 'method', method, 'param', kdeopt(2));
            preo_inter(:, idT) = computePDF(cbinsr, allreo_inter, 'method', method, 'param', kdeopt(3));
            pfwd_intra(:, idT) = computePDF(cbinsf, allfwd_inter, 'method', method, 'param', kdeopt(4));
            
            cibi_intra(:, idT) = computeCDF(cbinsi, allibi_intra, 'method', method, 'param', kdeopt(1));
            cdsp_intra(:, idT) = computeCDF(cbinsd, alldsp_intra, 'method', method, 'param', kdeopt(2));
            creo_intra(:, idT) = computeCDF(cbinsr, allreo_intra, 'method', method, 'param', kdeopt(3));
            cfwd_intra(:, idT) = computeCDF(cbinsf, allfwd_intra, 'method', method, 'param', kdeopt(4));
            
            cibi_inter(:, idT) = computeCDF(cbinsi, allibi_inter, 'method', method, 'param', kdeopt(1));
            cdsp_inter(:, idT) = computeCDF(cbinsd, alldsp_inter, 'method', method, 'param', kdeopt(2));
            creo_inter(:, idT) = computeCDF(cbinsr, allreo_inter, 'method', method, 'param', kdeopt(3));
            cfwd_inter(:, idT) = computeCDF(cbinsf, allfwd_inter, 'method', method, 'param', kdeopt(4));
            
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Display
    % * Intra pdf
    p = plot(aibi_intra, cbinsi, pibi_intra(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
	
    p = plot(adsp_intra, cbinsd, pdsp_intra(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    p = plot(areo_intra, cbinsr, preo_intra(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    p = plot(afwd_intra, cbinsf, pfwd_intra(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    % * Inter pdf
    p = plot(aibi_inter, cbinsi, pibi_inter(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    p = plot(adsp_inter, cbinsd, pdsp_inter(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    p = plot(areo_inter, cbinsr, preo_inter(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
    p = plot(afwd_inter, cbinsf, pfwd_inter(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
    
end

% * Axes labels & legends
xlabel(aibi_intra, '\delta{t}/<\delta{t}>_{traj.}');
ylabel(aibi_intra, 'pdf');
legend(aibi_intra);

xlabel(adsp_intra, 'd/<d>_{traj}');
ylabel(adsp_intra, 'pdf');
legend(adsp_intra);

xlabel(areo_intra, '\delta\theta_{turn}/<\delta\theta_{turn}>_{traj.}');
ylabel(areo_intra, 'pdf');
legend(areo_intra);

xlabel(afwd_intra, '\delta\theta_{forward}/<\delta\theta_{forward}>_{traj.}');
ylabel(afwd_intra, 'pdf');
legend(afwd_intra);

xlabel(aibi_inter, '<\delta{t}>_{traj.}/<<\delta{t}>>_T');
ylabel(aibi_inter, 'pdf');
legend(aibi_inter);

xlabel(adsp_inter, '<d>_{traj.}/<<d>>_T');
ylabel(adsp_inter, 'pdf');
legend(adsp_inter);

xlabel(areo_inter, '<\delta\theta_{turn}>_{traj.}/<<\delta\theta_{turn}>>_T');
ylabel(areo_inter, 'pdf');
legend(areo_inter);

xlabel(afwd_inter, '<\delta\theta_{forward}>_{traj.}/<<\delta\theta_{forward}>>_T');
ylabel(afwd_inter, 'pdf');
legend(afwd_inter);