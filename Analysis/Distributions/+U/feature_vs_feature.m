% Plots one feature versus another.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Features
feat1 = 'turn';          % First feature, either 'xpos', 'ibi', 'disp', 'turn'
feat2 = 'displ';         % Second feature

theta_threshold = 6;     % turn definition (deg)
displ_factor = 1.6;       % correction for displacement 

% * Display
fig = figure; hold on
% fig.Name = 'Displacement_IBI_binned';
fig.Name = 'TurnAngle_Displacement_binned';
colors = rainbow(numel(T));
erb = cell(numel(T), 1);

% * Bins
nbins = 50;

% * Filters
filt.seq_length = 20;	% sequence length for each fish in seconds
filt.bout_freq = .01;   % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Plotting comparison for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get 2 requested features from each experiment
    f1 = cell(size(ftrack, 1), 1);
    f2 = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        switch feat1
            case 'xpos'
                f1{idx_exp} = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                f2{idx_exp} = f1{idx_exp}.*pixsize;
                f1label = 'X position [mm]';
            case 'ibi'
                [~, f1{idx_exp}, ~, ~, ~, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                f1{idx_exp} = f1{idx_exp}./framerate;
                f1label = 'Interbout interval [s]';
            case {'disp', 'displ'}
                [~, ~, tmpf1, tag] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                tmpf1 = tmpf1.*pixsize;
                isturn = abs(tag) > theta_threshold;
                tmpf1(isturn) = tmpf1(isturn)./displ_factor;
                f1{idx_exp} = tmpf1;
                f1label = 'Displacement [mm]';
            case 'turn'
                [~, ~, ~, f1{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                f1label = 'Turn angle [deg]';
        end
        
        switch feat2
            case 'xpos'
                f2{idx_exp} = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                f2{idx_exp} = f2{idx_exp}.*pixsize;
                f2label = 'X position [mm]';
            case 'ibi'
                [~, f2{idx_exp}, ~, ~, ~, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                f2{idx_exp} = f2{idx_exp}./framerate;
                f2label = 'Interbout interval [s]';
            case {'disp', 'displ'}
                [~, ~, tmpf2, tag] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                tmpf2 = tmpf2.*pixsize;
                isturn = abs(tag) > theta_threshold;
                tmpf2(isturn) = tmpf2(isturn)./displ_factor;
                f2{idx_exp} = tmpf2;
                f2label = 'Displacement [mm]';
            case 'turn'
                [~, ~, ~, f2{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
                f2label = 'Turn angle [deg]';
        end
        
        fprintf('.');
    end
    
    % Gather features
    allf1 = cat(1, f1{:});
    allf2 = cat(1, f2{:});
    
    % Bin
    [bins_feat1, elmts_per_bins, bins_feat2] = BinsWithEqualNbofElements(allf1, allf2, nbins, nbins + 3);
    
    mean_feat2 = mean(bins_feat2, 2);
    erro_feat2 = std(bins_feat2, 1, 2)./sqrt(elmts_per_bins);
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Display
    set(0, 'CurrentFigure', fig);
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 8;
    lineopt.LineWidth = 2;
    lineopt.LineStyle = 'none';
    lineopt.Marker = '.';
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    erb{idT} = errorshaded(bins_feat1, mean_feat2, erro_feat2, 'line', lineopt, 'patch', shadopt);
    xlabel(f1label);
    ylabel(['<' f2label '>']);
    axis square
end

legend([erb{:}]);