% Plots mean velocities autocorrelations

% close all
clear
clc

% --- Parameters
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Opt
nbouts = 15;       % Delays (bouts)

% * MSDAnalyzer
dim = 2;

% Prepare figure
colors = rainbow(numel(T));
fig = figure; hold on
fig.Name = 'VCorr';
plt = cell(numel(T), 1);

% * Filters
filt.seq_length = 30;	% sequence length for each fish in seconds
filt.n_bouts = nbouts + 1;
filt.minx = 0;
filt.maxx = Inf;

% --- Main loop
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Building sequences for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Build sequences from all experiments
    pooled_sequences = cell(size(ftrack, 1), 1);   % Bouts
    for idx_exp = 1:size(ftrack, 1)
        
        % Get data
        [xpos, ~, ~, ~, frames, ~, framerate, ypos] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        n_seq = size(xpos, 2);
        sequences = cell(n_seq, 1);
        for s = 1:n_seq
            
            x = xpos(:, s).*pixsize;
            x(isnan(x)) = [];
            y = ypos(:, s).*pixsize;
            y(isnan(y)) = [];
            t = frames(:, s)./framerate;
            t(isnan(t)) = [];
            b = 0:size(x, 1) - 1;
            
            sequences{s} = [b', x, y];
        end
        
        % Pool sequences
        pooled_sequences{idx_exp} = sequences;
        
        fprintf('.');
    end
    
    fprintf('\n');
    
    all_sequences = cat(1, pooled_sequences{:});
    
    % Init MSDAnalyser
    ma = msdanalyzer(dim, 'mm', 'bouts');
    
    % Add tracks
    ma = ma.addAll(all_sequences);
    
    % Compute velocities autocorrelations
    ma = ma.computeVCorr;
    
    % Get mean & sem
    results = ma.getMeanVCorr;
    bouts_delays = results(1:nbouts, 1);
    mvc = results(1:nbouts, 2);
    evc = results(1:nbouts, 3)./sqrt(results(1:nbouts, 4));
    
    fprintf('Done (%2.2fs).\n', toc);
    
    % --- Display
    set(0, 'CurrentFigure', fig);
    
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1;
    lineopt.LineStyle = '-';
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    
    plt{idT} = errorshaded(bouts_delays, mvc, evc, 'line', lineopt, 'patch', shadopt);
    title('Velocity autocorrelation');
    xlabel('Delays [bout]');
    ylabel('<Velocity corr.>');
    axis square
end

legend([plt{:}]);