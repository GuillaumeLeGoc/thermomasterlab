% Fit the interbout interval distributions to extract parameters to plot
% versus temperatures.

% close all
clear
clc

% --- Parameters

% * Temperatures
T = [18, 22, 26, 30, 33];

n = 1;                  % order for fit : x^n.exp(-x/tau)

% * Filters
filt.seq_length = 20;	% sequence length for each fish in seconds
filt.bout_freq = .01;   % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Bins
method = 'kde';         % Method for PDF
mode = 'edges';         % Method for bins
bw = 0.25;              % Bandwidth for KDE
nbins = 250;            % Number of bins
e.ibi = [0, 15];        % bins for interbout interval (s)

% --- Prepare bin vector (center)
bi = linspace(e.ibi(1), e.ibi(2), nbins)';

% --- Loop over each temperatures and compute features PDF
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Fitting interbout interval distribution for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    i = cell(size(ftrack, 1), 1);
    f = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, i{idx_exp}, ~, ~, ~, ~, f{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        fprintf('.');
    end
    
    % Gather features
    framerate = mean(cat(1, f{:}));
    alli = cat(1, i{:})./framerate;
    
    % Get PDF & CDF
    switch method
        case 'hist'
            [pdfi, ci] = computePDF(bi, alli, 'method', method, 'mode', mode);
            cdfi = computeCDF(bi, alli, 'method', method, 'mode', mode);
        case 'kde'
            [pdfi, ci] = computePDF(bi, alli, 'method', method, 'mode', mode, 'param', {'BandWidth', bw, 'Support', 'Positive'});
            cdfi = computeCDF(bi, alli, 'method', method, 'mode', mode, 'param', {'BandWidth', bw, 'Support', 'Positive'});
    end
    
    % Get statistics
    mi = mean(alli);
    
    % - Fit
    % Analytical
    an_ci = linspace(e.ibi(1), e.ibi(2), nbins*1000);
    [anapdf, anacdf] = expdistrib(an_ci, 2/mi, 1);
    
    % Fit exp(x-x0)
    pdfexp = @(tau, x0, x) expdistrib(x - x0, 2/tau, n);
    cdfexp = @(tau, x0, x) getNthOutput(@expdistrib, 2, x - x0, 2/tau, n);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0 0], 'Upper', [Inf Inf], 'StartPoint', [1 1]);
    mdl = fittype(pdfexp, 'options', fo);
    fitexppdf = fit(ci, pdfi, mdl);
    
    % Gamma
    phat = gamfit(alli);
    fitgampdf = gampdf(an_ci, phat(1), phat(2));
    fitgamcdf = gamcdf(an_ci, phat(1), phat(2));
    
    % Lognormal
    phat = lognfit(alli);
    fitlogpdf = lognpdf(an_ci, phat(1), phat(2));
    fitlogcdf = logncdf(an_ci, phat(1), phat(2));
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Display
    
    % - PDF
    figure; hold on
    
    % Data
    p = plot(ci, pdfi);
    p.Color = [0 0 0];
    p.Marker = '.';
    p.MarkerSize = 8;
    p.LineWidth = 1.5;
    p.DisplayName = 'Data';
    
    % Exp
    q = plot(an_ci, anapdf);
    q.Color = [1 0 0 0.5];
    q.DisplayName = 'x*exp(-x)';
    
    % Fit exp
    r = plot(an_ci, pdfexp(fitexppdf.tau, fitexppdf.x0, an_ci));
    r.Color = [0 1 0 0.5];
    r.DisplayName = 'Fit x*exp(-x+x0)';
    
    % Gamma
    s = plot(an_ci, fitgampdf);
    s.Color = [0 0 1 0.5];
    s.DisplayName = 'Fit gamma';
    
    % Lognormal
    t = plot(an_ci, fitlogpdf);
    t.Color = [0.85 0.75 0 0.5];
    t.DisplayName = 'Fit lognormal';
    
    % Cosmetic
    xlabel('Interbout interval [s]');
    ylabel('PDF');
    title(['T = ' num2str(T(idT)) '°C']);
    legend;
    
    % - CDF
    figure; hold on
    
    % Data
    p = plot(ci, cdfi);
    p.Color = [0 0 0];
    p.Marker = '.';
    p.MarkerSize = 8;
    p.LineWidth = 1.5;
    p.DisplayName = 'Data';
    
    % Exp
    q = plot(an_ci, anacdf);
    q.Color = [1 0 0 0.5];
    q.DisplayName = 'x*exp(-x)';
    
    % Fit exp
    r = plot(an_ci, cdfexp(fitexppdf.tau, fitexppdf.x0, an_ci));
    r.Color = [0 1 0 0.5];
    r.DisplayName = 'Fit x*exp(-x+x0)';
    
    % Gamma
    s = plot(an_ci, fitgamcdf);
    s.Color = [0 0 1 0.5];
    s.DisplayName = 'Fit gamma';
    
    % Lognormal
    t = plot(an_ci, fitlogcdf);
    t.Color = [0.85 0.75 0 0.5];
    t.DisplayName = 'Fit lognormal';
    
    % Cosmetic
    xlabel('Interbout interval [s]');
    ylabel('CDF');
    ax = gca;
    ax.XLim = [e.ibi(1) e.ibi(2)];
    title(['T = ' num2str(T(idT)) '°C']);
    legend;
end