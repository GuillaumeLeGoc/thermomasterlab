% The interbout interval distributions scaled by their means by
% trajectories

% close all
clear
clc

% --- Parameters
% * Destination file, leave empty to not save
prefix_pdf = [pwd '/Data/Matfiles/normalized_ibi_pdf_'];
prefix_cdf = [pwd '/Data/Matfiles/normalized_ibi_cdf_'];
% prefix_pdf = '';
% prefix_cdf = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;    % sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Bins
method = 'kde';         % Method for PDF
mode = 'centers';         % Mode for bins
kdeopt = {'BandWidth', 0.1, 'Support', 'positive'}; % options for KDE
nbins = 250;

e.ibi = [0, 0.3];                    % bins for interbout interval (s)

% --- Prepare bin vectors
switch mode
    case 'edges'
        bi = linspace(e.ibi(1), e.ibi(2), nbins+1)';
    case 'centers'
        bi = linspace(e.ibi(1), e.ibi(2), nbins)';
end

% --- Prepare figures
figname_pdf = 'scaledPDF_IBI_traj';
figname_cdf = 'scaledCDF_IBI_traj';
colors = rainbow(numel(T));
plt_pdf = cell(numel(T), 1);
plt_cdf = cell(numel(T), 1);
fig_pdf = figure; hold on
fig_cdf = figure; hold on

% --- Loop over each temperatures and compute features PDF
pooledi_scaled = cell(numel(T), 1);
pooledi = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing scaled interbout interval for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    pdfi = NaN(nbins, size(ftrack, 1));
    cdfi = NaN(nbins, size(ftrack, 1));
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, tmp, ~, ~, ~, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        tmp = tmp./framerate;
        tmp = tmp./nanmean(tmp);
        
        i(isnan(i)) = [];
        
        % Get scaled PDF
        switch method
            case 'hist'
                [pdfi(:, idx_exp), scaled_ci] = computePDF(bi, i, 'method', method, 'mode', mode);
                cdfi(:, idx_exp) = computeCDF(bi, i, 'method', method, 'mode', mode);
            case 'kde'
                [pdfi(:, idx_exp), scaled_ci] = computePDF(bi, i, 'method', method, 'mode', mode, 'param', kdeopt);
                cdfi(:, idx_exp) = computeCDF(bi, i, 'method', method, 'mode', mode, 'param', kdeopt);
        end
        
        fprintf('.');
    end
    
    % Get mean pdf & cdf
    mpdf = mean(pdfi, 2);
    epdf = std(pdfi, [], 2)./sqrt(size(pdfi, 2));
    mcdf = mean(cdfi, 2);
    ecdf = std(cdfi, [], 2)./sqrt(size(cdfi, 2));
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 4;
    lineopt.LineWidth = 2;
    lineopt.LineStyle = 'none';
    lineopt.Marker = '.';
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    
    % * PDF
    set(0, 'CurrentFigure', fig_pdf);
    plt_pdf{idT} = errorshaded(scaled_ci, mpdf, epdf, 'line', lineopt, 'patch', shadopt);
    xlabel('\delta{t}/<\delta{t}>_{traj} [norm]');
    ylabel('pdf');
    title('Scaled pdf');
    ax_pdf = gca;
    
    % * CDF
    set(0, 'CurrentFigure', fig_cdf);
    plt_cdf{idT} = errorshaded(scaled_ci, mcdf, ecdf, 'line', lineopt, 'patch', shadopt);
    xlabel('\delta{t}/<\delta{t}>_{traj} [norm]');
    ylabel('cdf');
    title('Scaled cdf');
    ax_cdf = gca;
    
end

% Display legends
set(0, 'CurrentFigure', fig_pdf);
legend(ax_pdf, [plt_pdf{:}]);
fig_pdf.Name = figname_pdf;
axis square
set(0, 'CurrentFigure', fig_cdf);
legend(ax_cdf, [plt_cdf{:}]);
fig_cdf.Name = figname_cdf;
axis square