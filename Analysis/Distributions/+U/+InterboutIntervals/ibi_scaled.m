% The interbout interval distributions scaled by their means over
% temperature

% close all
clear
clc

% --- Parameters
% * Destination file, leave empty to not save
prefix_pdf = [pwd '/Data/Matfiles/normalized_ibi_pdf_'];
prefix_cdf = [pwd '/Data/Matfiles/normalized_ibi_cdf_'];
% prefix_pdf = '';
% prefix_cdf = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;    % sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;
cutoff = Inf;

% * Bins
method = 'kde';         % Method for PDF
mode = 'centers';         % Mode for bins
kdeopt = {'BandWidth', 0.1, 'Support', 'positive'}; % options for KDE
nbins = 500;

e.ibi = [0, 10];                    % bins for interbout interval (s)

% --- Prepare bin vectors
switch mode
    case 'edges'
        bi = linspace(e.ibi(1), e.ibi(2), nbins+1)';
    case 'centers'
        bi = linspace(e.ibi(1), e.ibi(2), nbins)';
end

% --- Prepare figures
figname_pdf = 'scaledPDF_IBI';
figname_cdf = 'scaledCDF_IBI';
colors = rainbow(numel(T));
plt = cell(numel(T), 1);
fig_pdf = figure; hold on
fig_cdf = figure; hold on

% --- Loop over each temperatures and compute features PDF
pooledi_scaled = cell(numel(T), 1);
pooledi = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing scaled interbout interval for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    i = cell(size(ftrack, 1), 1);
    f = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, i{idx_exp}, ~, ~, ~, ~, f{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        fprintf('.');
    end
    
    % Gather feature
    framerate = mean(cat(1, f{:}));
    alli = cat(1, i{:})./framerate;
    alli(alli > cutoff) = [];
    
    % Get statistics
    mi = mean(alli);
    scaled_i = alli./mi;
    
    % Get scaled PDF
%     scaled_bi = bi./mi;
    scaled_bi = bi;
    switch method
        case 'hist'
            [scaled_pdfi, scaled_ci] = computePDF(scaled_bi, scaled_i, 'method', method, 'mode', mode);
            scaled_cdfi = computeCDF(scaled_bi, alli./mi, 'method', method, 'mode', mode);
        case 'kde'
            [scaled_pdfi, scaled_ci] = computePDF(scaled_bi, scaled_i, 'method', method, 'mode', mode, 'param', kdeopt);
            scaled_cdfi = computeCDF(scaled_bi, alli./mi, 'method', method, 'mode', mode, 'param', kdeopt);
    end
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % Store
    pooledi_scaled{idT} = scaled_i;
    pooledi{idT} = alli;
    
    % --- Display
    % * PDF
    set(0, 'CurrentFigure', fig_pdf);
    lineopt = plot(scaled_ci, scaled_pdfi);
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.5;
    plt{idT} = lineopt;
    xlabel('\delta{t}/<\delta{t}>_T [norm]');
    ylabel('pdf');
    title('Scaled pdf');
    ax_pdf = gca;
    
    % * CDF
    set(0, 'CurrentFigure', fig_cdf);
    lineopt = plot(scaled_ci, scaled_cdfi);
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.5;
    plt{idT} = lineopt;
    xlabel('\delta{t}/<\delta{t}>_T [norm]');
    ylabel('cdf');
    title('Scaled cdf');
    ax_cdf = gca;
    
    % --- Save file
    if ~isempty(prefix_pdf)
        filename = [prefix_pdf 'T' num2str(T(idT)) '.mat'];
        save(filename, 'scaled_pdfi', 'scaled_ci');
        filename = [prefix_cdf 'T' num2str(T(idT)) '.mat'];
        save(filename, 'scaled_cdfi', 'scaled_ci');
    end
end

% Display legends
set(0, 'CurrentFigure', fig_pdf);
legend(ax_pdf, [plt{:}]);
fig_pdf.Name = figname_pdf;
axis square
set(0, 'CurrentFigure', fig_cdf);
legend(ax_cdf, [plt{:}]);
fig_cdf.Name = figname_cdf;
axis square

% --- Fit
pooledi_scaled = cat(1, pooledi_scaled{:});

bins = linspace(0, max(pooledi_scaled), sqrt(numel(pooledi_scaled)));
[pdi, ci] = computePDF(bins, pooledi_scaled, 'mode', 'edges', 'method', 'kde');

pdbt = NaN(numel(bins)-1, numel(T));
cit = NaN(numel(bins)-1, numel(T));
for idT = 1:numel(T) 
    [pdbt(:, idT), cit(:, idT)] = computePDF(bins, pooledi{idT}, 'mode', 'edges');
end
    
[ci, pdi] = prepareCurveData(ci, pdi);

gamdist = @(a, b, x) (1/(b^a.*gamma(a))).*(x).^(a - 1).*exp(-(x)/b);

manualfit = fit(ci, pdi, gamdist, ...
    'Upper', [Inf, Inf], ...
    'Lower', [0, 0], ...
    'StartPoint', [3, 0.4]);

autofit = fitdist(pooledi_scaled, 'Gamma');

binsos = linspace(bins(1), bins(end), 1000);
manualpdf = manualfit(binsos);
autopdf = autofit.pdf(binsos);

figure('Name', 'scaledIBIGammaFit'); hold on
plot(ci, pdi, 'k-o', 'LineWidth', 1);
plot(binsos, manualpdf, 'DisplayName', 'Manual fit');
plot(binsos, autopdf, 'DisplayName', 'Fitdist');

xlabel('Normalized \delta{t} [u.a.]');
ylabel('Count');
axis square