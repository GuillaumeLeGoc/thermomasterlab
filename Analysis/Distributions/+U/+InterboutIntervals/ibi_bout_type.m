% Look at the interbout intervals given the nature of bout performed (turn
% or forward).

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

% * Fit parameters
theta_threshold = 6;   % deg

% * Filters
filt.seq_length = 30;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Bins
nbins = 50;
bins = linspace(0, 6, nbins);

% --- Prepare figures

% --- Processing
ibit_T = cell(numel(T), 1);
ibif_T = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Getting IBI for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Count occurences of patterns in each sequences
    ibit_exp = cell(size(ftrack, 1), 1);
    ibif_exp = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, intbout, ~, turnangle, ~, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        n_seq = size(turnangle, 2);
        
        ibit = cell(n_seq, 1);
        ibif = cell(n_seq, 1);
        for seq = 1:n_seq
            
            dtheta = turnangle(:, seq);
            dtheta(isnan(dtheta)) = [];
            dtheta = dtheta - mean(dtheta);     % bias correction
            ibi = intbout(:, seq)./framerate;
            ibi(isnan(ibi)) = [];
            
            % Find turns & forwards
            isturns = abs(dtheta) > theta_threshold;
            isforwd = abs(dtheta) < theta_threshold;
            
            ibit{seq} = ibi(isturns);
            ibif{seq} = ibi(isforwd);
        end
        
        ibit_exp{idx_exp} = cat(1, ibit{:});
        ibif_exp{idx_exp} = cat(1, ibif{:});
        
        fprintf('.');
    end
    
    ibit_T{idT} = cat(1, ibit_exp{:});
    ibif_T{idT} = cat(1, ibif_exp{:});
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
end

% Pool
ibit_pooled = cat(1, ibit_T{:});
ibif_pooled = cat(1, ibif_T{:});

% --- Get PDF
pdfibit = computePDF(bins, ibit_pooled);
pdfibif = computePDF(bins, ibif_pooled);

% --- Display
fig = figure; hold on
fig.Name = 'IBIFwdTurns';
p = plot(bins, pdfibif);
p.DisplayName = 'Forward';
p = plot(bins, pdfibit);
p.DisplayName = 'Turns';
legend;
xlabel('Interbout interval [s]');
ylabel('PDF');
axis square