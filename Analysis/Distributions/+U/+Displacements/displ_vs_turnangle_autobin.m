% Plots mean displacement versus turn angles in polar plot.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
theta_threshold = 10;        % turn definition (deg)
displ_factor = 1;           % correction for displacement 

% * Display
fig = figure; pax = polaraxes; hold(pax, 'on');
fig.Name = 'TurnAngle_Displacement_binned';
colors = rainbow(numel(T));
pp = cell(numel(T), 1);

% * Bins
nbins = 30;

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Plotting comparison for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get 2 requested features from each experiment
    poold = cell(size(ftrack, 1), 1);
    poolt = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)

        [~, ~, dis, tag] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        dis = dis.*pixsize;
        isturn = abs(tag) > theta_threshold;
        dis(isturn) = dis(isturn)./displ_factor;
        poold{idx_exp} = dis;
        poolt{idx_exp} = tag;
        
        fprintf('.');
    end
    
    % Gather features
    alld = cat(1, poold{:});
    allt = cat(1, poolt{:});
    alld_n = alld./mean(alld);
    
    % Bin
    [bins_t, elmts_per_bins, bins_d] = BinsWithEqualNbofElements(allt, alld, nbins, nbins + 3);
    [bins_t_n, elmts_per_bins_n, bins_d_n] = BinsWithEqualNbofElements(allt, alld_n, nbins, nbins + 3);
    
    mean_d = mean(bins_d, 2);
    erro_d = std(bins_d, 1, 2)./sqrt(elmts_per_bins);
    mean_d_n = mean(bins_d_n, 2);
    erro_d_n = std(bins_d_n, 1, 2)./sqrt(elmts_per_bins_n);          
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Display
    pp{idT} = polarplot(pax, deg2rad(bins_t), mean_d);
    pp{idT}.LineStyle = '-';
    pp{idT}.Marker = '.';
    pp{idT}.MarkerSize = 7.5;
    pp{idT}.Color = colors(idT, :);
    pp{idT}.DisplayName = ['T=' num2str(temperature) '°C'];
end

pax.RTick = [0, 1, 2];
pax.ThetaZeroLocation = 'top';
pax.RAxisLocation = 180;
pax.RAxis.Label.String = '<d> [mm]';
pax.ThetaAxis.Label.String = '\delta{\theta} [deg]';
pax.FontSize = 16;
pax.ThetaTick = [0, 30, 60, 90, 120, 180, 240, 270, 300, 330];
pax.ThetaTickLabel = {'0', '30', '60', '90', '120', '180' ,'-120', '-90', '-60', '-30'};
pax.FontName = 'DejaVu Math TeX Gyre';