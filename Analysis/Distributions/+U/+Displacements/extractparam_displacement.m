% Fit displacement distributions to extract parameters to plot versus
% temperatures.

% close all
clear
clc

% --- Parameters
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 20;	% sequence length for each fish in seconds
filt.bout_freq = .01;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

theta_threshold = 6;
scalefac = 1.6;

% * Bins
method = 'kde';        % Method for PDF
mode = 'edges';         % Method for bins
bw = 0.5;
nbins = 100;
e.d = [0, 8];       % bins for displacement (mm)

% --- Prepare bin vector (center)
bd = linspace(e.d(1), e.d(2), nbins)';

% --- Loop over each temperatures and compute features PDF
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Fitting displacement distribution for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    d = cell(size(ftrack, 1), 1);
    t = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, d{idx_exp}, t{idx_exp}] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        fprintf('.');
    end
    
    % Gather features
    alld = cat(1, d{:}).*pixsize;
    allt = cat(1, t{:});
    alld(abs(allt) > theta_threshold) = alld(abs(allt) > theta_threshold)./scalefac;
    
    % Get PDF
    switch method
        case 'hist'
            [pdfd, cd] = computePDF(bd, alld, 'method', method, 'mode', mode);
        case 'kde'
            [pdfd, cd] = computePDF(bd, alld, 'method', method, 'mode', mode, 'param', {'BandWidth', bw});
    end
    
    % Get CDF
    [cdfd, cdd] = ecdf(alld);
    
    % Get statistics
    md = trapz(cd, cd.*pdfd);
    
    % - Fit
    % Analytical
    cddd = linspace(e.d(1), e.d(2), nbins*1000);
    [anapdf, anacdf] = expdistrib(cddd, 2/md, 1);
    
    % Exp
    expfit = @(tau, x) expdistrib(x, 2/tau, 1);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', 0, 'Upper', Inf, 'StartPoint', 1);
    mdl = fittype(expfit, 'options', fo);
    ft_exp = fit(cd, pdfd, mdl);
    
    % Gamma
    phat = gamfit(alld);
    fitgampdf = gampdf(cddd, phat(1), phat(2));
    fitgamcdf = gamcdf(cddd, phat(1), phat(2));
    
    % Lognormal
    phat = lognfit(alld);
    fitlogpdf = lognpdf(cddd, phat(1), phat(2));
    fitlogcdf = logncdf(cddd, phat(1), phat(2));
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Display
    
    % - PDF
    figure; hold on
    
    % Data
    p = plot(cd, pdfd);
    p.Color = [0 0 0];
    p.Marker = '.';
    p.MarkerSize = 5;
    p.LineWidth = 1.5;
    p.DisplayName = 'Data';
    
    % Exp
    q = plot(cddd, anapdf);
    q.Color = [1 0 0 0.5];
    q.DisplayName = 'x*exp(-x)';
    
    % Fit exp
    r = plot(cddd, expfit(ft_exp.tau, cddd));
    r.Color = [0 1 0 0.5];
    r.DisplayName = 'Fit x*exp(-x)';
    
    % Gamma
    s = plot(cddd, fitgampdf);
    s.Color = [0 0 1 0.5];
    s.DisplayName = 'Fit gamma';
    
    % Lognormal
    t = plot(cddd, fitlogpdf);
    t.Color = [0.85 0.75 0 0.5];
    t.DisplayName = 'Fit lognormal';
    
    % Cosmetic
    xlabel('Displacement [mm]');
    ylabel('PDF');
    title(['T = ' num2str(T(idT)) '°C']);
    legend;
    
    % - CDF
    figure; hold on
    
    % Data
    p = plot(cdd, cdfd);
    p.Color = [0 0 0];
    p.Marker = '.';
    p.MarkerSize = 5;
    p.LineWidth = 1.5;
    p.DisplayName = 'Data';
    
    % Exp
    q = plot(cddd, anacdf);
    q.Color = [1 0 0 0.5];
    q.DisplayName = 'x*exp(-x)';
    
    % Gamma
    s = plot(cddd, fitgamcdf);
    s.Color = [0 0 1 0.5];
    s.DisplayName = 'Fit gamma';
    
    % Lognormal
    t = plot(cddd, fitlogcdf);
    t.Color = [0.85 0.75 0 0.5];
    t.DisplayName = 'Fit lognormal';
    
    % Cosmetic
    xlabel('Displacement [mm]');
    ylabel('CDF');
    ax = gca;
    ax.XLim = [e.d(1) e.d(2)];
    title(['T = ' num2str(T(idT)) '°C']);
    legend;
end