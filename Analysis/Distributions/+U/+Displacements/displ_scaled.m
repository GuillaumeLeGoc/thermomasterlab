% The displacement distributions scaled by their means

% close all
clear
clc

% --- Parameters
pixsize = 0.09;         % mm

% * Destination file, leave empty to not save
prefix_pdf = [pwd '/Data/Matfiles/normalized_displ_pdf_'];
prefix_cdf = [pwd '/Data/Matfiles/normalized_displ_cdf_'];
% prefix_pdf = '';
% prefix_cdf = '';

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;    % sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

theta_threshold = 10;
scalefactor = 1.6;

% * Bins
method = 'kde';         % Method for PDF
mode = 'centers';         % Mode for bins
kdeopt = {'BandWidth', 0.1};
nbins = 500;

e.disp = [0, 10];                    % bins limit

% --- Prepare bin vectors
switch mode
    case 'edges'
        bd = linspace(e.disp(1), e.disp(2), nbins+1)';
    case 'centers'
        bd = linspace(e.disp(1), e.disp(2), nbins)';
end

% --- Prepare figures
figname_pdf = 'scaledPDF_Disp';
figname_cdf = 'scaledCDF_Disp';
colors = rainbow(numel(T));
plt = cell(numel(T), 1);
fig_pdf = figure; hold on
fig_cdf = figure; hold on

% --- Loop over each temperatures and compute features PDF
pooledd = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing scaled displacement for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    d = cell(size(ftrack, 1), 1);
    isturn = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, d{idx_exp}, turnangles] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        isturn{idx_exp} = abs(turnangles) > theta_threshold;
        
        fprintf('.');
    end
    
    % Gather feature
    alld = cat(1, d{:}).*pixsize;
    isturn = cat(1, isturn{:});
    alld(isturn) = alld(isturn)./scalefactor;
    
    % Get statistics
    md = mean(alld);
    
    % Get scaled PDF
%     scaled_bd = bd./md;
    scaled_bd = bd;
    scaled_d = alld./md;
    switch method
        case 'hist'
            [scaled_pdfd, scaled_cd] = computePDF(scaled_bd, scaled_d, 'method', method, 'mode', mode);
            scaled_cdfd = computeCDF(scaled_bd, scaled_d, 'method', method, 'mode', mode);
        case 'kde'
            [scaled_pdfd, scaled_cd] = computePDF(scaled_bd, scaled_d, 'method', method, 'mode', mode, 'param', kdeopt);
            scaled_cdfd = computeCDF(scaled_bd, scaled_d, 'method', method, 'mode', mode, 'param', kdeopt);
    end
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % Store
    pooledd{idT} = scaled_d;
    
    % --- Display
    
    % * PDF
    set(0, 'CurrentFigure', fig_pdf);
    lineopt = plot(scaled_cd, scaled_pdfd);
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.5;
    plt{idT} = lineopt;
    xlabel('d/<d>_T [norm]');
    ylabel('pdf');
    title('Scaled pdf');
    ax_pdf = gca;
    
    % * CDF
    set(0, 'CurrentFigure', fig_cdf);
    lineopt = plot(scaled_cd, scaled_cdfd);
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 10;
    lineopt.LineWidth = 1.5;
    plt{idT} = lineopt;
    xlabel('d/<d>_T [norm]');
    ylabel('cdf');
    title('Scaled cdf');
    ax_cdf = gca;
    
    % --- Save file
    if ~isempty(prefix_pdf)
        filename = [prefix_pdf 'T' num2str(T(idT)) '.mat'];
        save(filename, 'scaled_pdfd', 'scaled_cd');
        filename = [prefix_cdf 'T' num2str(T(idT)) '.mat'];
        save(filename, 'scaled_cdfd', 'scaled_cd');
    end
end

% Display legends
set(0, 'CurrentFigure', fig_pdf);
legend(ax_pdf, [plt{:}]);
fig_pdf.Name = figname_pdf;
axis square
set(0, 'CurrentFigure', fig_cdf);
legend(ax_cdf, [plt{:}]);
fig_cdf.Name = figname_cdf;
axis square

% --- Fit
pooledd = cat(1, pooledd{:});
figure('Name', 'scaledDispGammaFit'); hold on
histfit(pooledd, nbins, 'Gamma');
xlabel('Normalized d [u.a.]');
ylabel('Count');
legend('Data', 'Gamma fit');
axis square