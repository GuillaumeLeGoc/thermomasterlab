% Plots mean displacement versus turn angles in polar plot.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
theta_threshold = 6;        % turn definition (deg)
displ_factor = 1;         % correction for displacement 

% * Display
fig = figure; pax = polaraxes; hold(pax, 'on');
fig.Name = 'TurnAngle_Displacement_binned';
fig_n = figure; pax_n = polaraxes; hold(pax_n, 'on');
colors = rainbow(numel(T));
pp = cell(numel(T), 1);
pp_n = cell(numel(T), 1);

% * Bins
nbins = 20;
binedges = linspace(-100, 100, nbins + 1);
bincenters = binedges(1:end-1) + diff(binedges)/2;  % centers of bins

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Plotting comparison for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get 2 requested features from each experiment
    poold = cell(size(ftrack, 1), 1);
    poolt = cell(size(ftrack, 1), 1);
    
    for idx_exp = 1:size(ftrack, 1)

        [~, ~, dis, tag] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        dis = dis.*pixsize;
        isturn = abs(tag) > theta_threshold;
        dis(isturn) = dis(isturn)./displ_factor;
        poold{idx_exp} = dis;
        poolt{idx_exp} = tag;
        
        fprintf('.');
    end
    
    % Gather features
    alld = cat(1, poold{:});
    allt = cat(1, poolt{:});
    alld_n = alld./mean(alld);
    
    % Binning
    bint = discretize(allt, nbins);                 % regroup in bins
    mean_d = accumarray(bint, alld, [], @mean, NaN);% average over bins
    mean_d_n = accumarray(bint, alld_n, [], @mean, NaN);          
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Display
    pp{idT} = polarplot(pax, deg2rad(bincenters), mean_d);
    pp{idT}.LineStyle = '-';
    pp{idT}.Marker = '.';
    pp{idT}.MarkerSize = 7.5;
    pp{idT}.Color = colors(idT, :);
    pp{idT}.DisplayName = ['T=' num2str(temperature) '°C'];
    
    pp_n{idT} = polarplot(pax_n, deg2rad(bincenters), mean_d_n);
    pp_n{idT}.LineStyle = '-';
    pp_n{idT}.Marker = '.';
    pp_n{idT}.MarkerSize = 7.5;
    pp_n{idT}.Color = colors(idT, :);
    pp_n{idT}.DisplayName = ['T=' num2str(temperature) '°C'];
end

pax.RTick = [0, 1, 2];
pax.ThetaZeroLocation = 'top';
pax.RAxisLocation = 180;
pax.RAxis.Label.String = '<d> [mm]';
pax.ThetaAxis.Label.String = '\delta{\theta} [deg]';
pax.FontSize = 16;
pax.ThetaTick = [0, 30, 60, 90, 120, 180, 240, 270, 300, 330];
pax.ThetaTickLabel = {'0', '30', '60', '90', '120', '180' ,'-120', '-90', '-60', '-30'};
pax.FontName = 'DejaVu Math TeX Gyre';

pax_n.RTick = [0, 1, 2];
pax_n.ThetaZeroLocation = 'top';
pax_n.RAxisLocation = 180;
pax_n.RAxis.Label.String = '<d> [mm]';
pax_n.ThetaAxis.Label.String = '\delta{\theta} [deg]';
pax_n.FontSize = 16;
pax_n.ThetaTick = [0, 30, 60, 90, 120, 180, 240, 270, 300, 330];
pax_n.ThetaTickLabel = {'0', '30', '60', '90', '120', '180' ,'-120', '-90', '-60', '-30'};
pax_n.FontName = 'DejaVu Math TeX Gyre';