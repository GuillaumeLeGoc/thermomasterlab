% Plot distance travelled during forward bouts and turn bouts

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;
theta_threshold = 10;    % deg

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
filt.bout_freq = .1;   % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Figures
colors = rainbow(numel(T));

% --- Prepare arrays
dfwd = cell(numel(T), 1);
dtrn = cell(numel(T), 1);

% --- Loop over each temperatures
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Getting displacements for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    dfwd{idT} = cell(size(ftrack, 1), 1);
    dtrn{idT} = cell(size(ftrack, 1), 1);
    for idx_exp = 1:size(ftrack, 1)
        
        [~, ~, displ, turnangle] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
        
        displ_fwd = displ.*pixsize;
        displ_trn = displ.*pixsize;
        displ_fwd(abs(turnangle) > theta_threshold) = [];
        displ_trn(abs(turnangle) < theta_threshold) = [];
        
        dfwd{idT}{idx_exp} = displ_fwd;
        dtrn{idT}{idx_exp} = displ_trn;
        
        fprintf('.');
    end
    
    dfwd{idT} = cat(1, dfwd{idT}{:});
    dtrn{idT} = cat(1, dtrn{idT}{:});
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
end

% --- Average
mdispl_fwd = cellfun(@mean, dfwd);
edispl_fwd = cellfun(@(x) std(x)./sqrt(numel(x)), dfwd);
mdispl_trn = cellfun(@mean, dtrn);
edispl_trn = cellfun(@(x) std(x)./sqrt(numel(x)), dtrn);
mratio = mdispl_trn./mdispl_fwd;
eratio = mratio.*sqrt( (edispl_fwd./mdispl_fwd).^2 + (edispl_trn./mdispl_trn).^2);

% --- Display
f = figure; hold on
f.Name = 'DisplacementFwdTrn';
errorbar(T, mdispl_fwd, edispl_fwd, 'DisplayName', 'Forward');
errorbar(T, mdispl_trn, edispl_trn, 'DisplayName', 'Turn');
xlabel('Temperature [°C]');
ylabel('<d> [mm]');
axis([-Inf Inf 0.5 2.5])
l = legend;
l.Location = 'northwest';
axis square

f = figure;
f.Name = 'DsplTurnOnDsplFwd';
p = errorbar(T, mratio, eratio, 'k');
p.Marker = 'o';
xlabel('Temperature [°C]');
ylabel('d_{turn}/d_{forward}');
axis([-Inf, Inf, 1, 2]);
axis square
ax = gca;
ax.XTick = [18, 22, 26, 30, 33];