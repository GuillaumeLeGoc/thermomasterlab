% pdf of parameters given age

% close all
clear
clc

% --- Parameters
% * Temperatures
ages = [5,6,7];
T = 26;

% --- Prepare figures
colors = lines(numel(ages));
figure; hold on; axibi = gca;
figure; hold on; axdsp = gca;
figure; hold on; axreo = gca;
figure; hold on; axkfp = gca;
figure; hold on; axktr = gca;


for ida = 1:numel(ages)
    
    age = ages(ida);
    
    % * Files
%     fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences1fishT' num2str(T) '_' num2str(age) 'dpf.mat'];
    fn_all26 = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep 'allsequences' num2str(age) 'dpf.mat'];
%     fn_k = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix1fish_k_T' num2str(T) '_' num2str(age) 'dpf.mat'];
    % fn_p = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix1fish_p_T26.mat'];
    fn_all26k = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix_k_' num2str(age) 'dpf.mat'];
    % fn_all26p = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix_p.mat'];
    
    % * Settings
    theta_threshold = 10;    % deg
    scale_turn_disp = 1.6;  % turn displacement scaling factor
    ncorr = 10;
    
    % * Bins
    method = 'kde';         % Method for PDF
    mode = 'centers';         % Mode for bins
    bws = [0.1, 0.1, 0.5, 0.05, 0.05]; % Bandwidths for KDE
    kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
    nbins = 250;
    nbinst = 100;
    
    cibi = linspace(0, 3, nbins);                         % bins for interbout interval (s)
    cdisp = linspace(0, 6, nbins);                        % bins for displacement (mm)
    cturn = linspace(-180, 180, nbins);                   % bins for turn angle (°, wrapped to 180)
    ckf = linspace(0, 1.5, nbinst);
    ckt = linspace(0, 1.5, nbinst);
    
    % * Load data
%     data = load(fn_dataset);
%     nfish = numel(data.xpos);
    all26data = load(fn_all26);
%     kfkp = load(fn_k);
    all26kfkp = load(fn_all26k);
    
    % --- Processing (single fish)
    
%     for idF = 1:nfish
%         
%         % Get data
%         intboutint = data.interboutintervals{idF}(:);
%         turnangle = data.dtheta{idF}(:);
%         displacement = data.displacements{idF}(:);
%         
%         intboutint(isnan(intboutint)) = [];
%         turnangle(isnan(turnangle)) = [];
%         displacement(isnan(displacement)) = [];
%         
%         kf = kfkp.feat_mat_pooled{idF}(:, 3);
%         kt = kfkp.feat_mat_pooled{idF}(:, 2);
%         
%         % Get pdf
%         tmpibi = computePDF(cibi, intboutint, 'mode', mode, 'method', method, 'param', kdeopt(1));
%         tmpdsp = computePDF(cdisp, displacement, 'mode', mode, 'method', method, 'param', kdeopt(2));
%         tmptag = computePDF(cturn, turnangle, 'mode', mode, 'method', method, 'param', {'Bandwidth', bws(3)});
%         tmpkfp = computePDF(ckf, kf, 'mode', mode, 'method', method, 'param', kdeopt(4));
%         tmpktr = computePDF(ckt, kt, 'mode', mode, 'method', method, 'param', kdeopt(5));
%         
%         p = plot(axibi, cibi, tmpibi);
%         p.Color = colors(idF, :);
%         p.DisplayName = ['Fish ' num2str(idF)];
%         p = plot(axdsp, cdisp, tmpdsp);
%         p.Color = colors(idF, :);
%         p.DisplayName = ['Fish ' num2str(idF)];
%         p = plot(axreo, cturn, tmptag);
%         p.Color = colors(idF, :);
%         p.DisplayName = ['Fish ' num2str(idF)];
%         p = plot(axkfp, ckf, tmpkfp);
%         p.Color = colors(idF, :);
%         p.DisplayName = ['Fish ' num2str(idF)];
%         p = plot(axktr, ckt, tmpktr);
%         p.Color = colors(idF, :);
%         p.DisplayName = ['Fish ' num2str(idF)];
%     end
    
%     % Pooled
%     % Get data
%     intboutint = cellfun(@(x) x(:), data.interboutintervals, 'UniformOutput', false);
%     intboutint = cat(1, intboutint{:});
%     turnangle = cellfun(@(x) x(:), data.dtheta, 'UniformOutput', false);
%     turnangle = cat(1, turnangle{:});
%     displacement = cellfun(@(x) x(:), data.displacements, 'UniformOutput', false);
%     displacement = cat(1, displacement{:});
%     
%     intboutint(isnan(intboutint)) = [];
%     turnangle(isnan(turnangle)) = [];
%     displacement(isnan(displacement)) = [];
%     
%     kf = kfkp.feat_mat_pooled;
%     kf = cellfun(@(x) x(:, 3), kf, 'UniformOutput', false);
%     kf = cat(1, kf{:});
%     kt = kfkp.feat_mat_pooled;
%     kt = cellfun(@(x) x(:, 2), kt, 'UniformOutput', false);
%     kt = cat(1, kt{:});
    
%     % Get pdf
%     pdfibi = computePDF(cibi, intboutint, 'mode', mode, 'method', method, 'param', kdeopt(1));
%     pdfdsp = computePDF(cdisp, displacement, 'mode', mode, 'method', method, 'param', kdeopt(2));
%     pdftag = computePDF(cturn, turnangle, 'mode', mode, 'method', method, 'param', {'Bandwidth', bws(3)});
%     pdfkfp = computePDF(ckf, kf, 'mode', mode, 'method', method, 'param', kdeopt(4));
%     pdfktr = computePDF(ckt, kt, 'mode', mode, 'method', method, 'param', kdeopt(5));
%     
    % --- Processing (all 26)
    % Get data
    intboutint = all26data.interboutintervals{3}(:);
    turnangle = all26data.dtheta{3}(:);
    displacement = all26data.displacements{3}(:);
    
    intboutint(isnan(intboutint)) = [];
    turnangle(isnan(turnangle)) = [];
    displacement(isnan(displacement)) = [];
    
    kf = all26kfkp.feat_mat_pooled{3}(:, 3);
    kt = all26kfkp.feat_mat_pooled{3}(:, 2);
    
    dname = ['All 26°C ' num2str(age) 'dpf'];
    
    % Get pdf
    apdfibi = computePDF(cibi, intboutint, 'mode', mode, 'method', method, 'param', kdeopt(1));
    apdfdsp = computePDF(cdisp, displacement, 'mode', mode, 'method', method, 'param', kdeopt(2));
    apdftag = computePDF(cturn, turnangle, 'mode', mode, 'method', method, 'param', {'Bandwidth', bws(3)});
    apdfkfp = computePDF(ckf, kf, 'mode', mode, 'method', method, 'param', kdeopt(4));
    apdfktr = computePDF(ckt, kt, 'mode', mode, 'method', method, 'param', kdeopt(5));
    
    % --- Display
%     plot(axibi, cibi, pdfibi, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
    plot(axibi, cibi, apdfibi, 'Color', colors(ida, :), 'LineWidth', 2, 'DisplayName', dname);
    xlabel(axibi, '\delta{t} (s)');
    ylabel(axibi, 'pdf');
    legend(axibi);
    title([num2str(age) 'dpf']);
    
%     plot(axdsp, cdisp, pdfdsp, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
    plot(axdsp, cdisp, apdfdsp, 'Color', colors(ida, :), 'LineWidth', 2, 'DisplayName', dname);
    xlabel(axdsp, 'd (mm)');
    ylabel(axdsp, 'pdf');
    legend(axdsp);
    title([num2str(age) 'dpf']);
    
%     plot(axreo, cturn, pdftag, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
    plot(axreo, cturn, apdftag, 'Color', colors(ida, :), 'LineWidth', 2, 'DisplayName', dname);
    legend(axreo);
    axreo.YScale = 'log';
    xlabel(axreo, '\delta\theta (deg)');
    ylabel(axreo, 'pdf');
    ylim(axreo, [1e-4, 1e-1]);
    title([num2str(age) 'dpf']);
    
%     plot(axkfp, ckf, pdfkfp, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
    plot(axkfp, ckf, apdfkfp, 'Color', colors(ida, :), 'LineWidth', 2, 'DisplayName', dname);
    legend(axkfp);
    xlabel(axkfp, 'k_{flip}');
    ylabel(axkfp, 'pdf');
    title([num2str(age) 'dpf']);
    
%     plot(axktr, ckt, pdfktr, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
    plot(axktr, ckt, apdfktr, 'Color', colors(ida, :), 'LineWidth', 2, 'DisplayName', dname);
    legend(axktr);
    xlabel(axktr, 'k_{turn}');
    ylabel(axktr, 'pdf');
    title([num2str(age) 'dpf']);
    
end