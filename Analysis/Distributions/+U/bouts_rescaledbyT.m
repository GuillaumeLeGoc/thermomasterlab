% Rescale distributions of ibi, displacement and reorientation by the mean
% over temperatures.

% close all
clear
clc

% --- Parameters
% * Files
dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];

% * Definitions
pixsize = 0.09;
theta_threshold = 10;    % deg
displ_factor = 1.6;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Bins
nbins = 200;
method = 'kde';
bws = [0.1, 0.1, 0.1];
kdeopt = @(n) {'Bandwidth', bws(n), 'Support', 'positive'};
% kdeopt = @(n) {};
binsibi = linspace(0, 6, nbins);
binsdsp = linspace(0, 3, nbins);
binsreo = linspace(0, 5, 150);
binsfwd = linspace(0, 4, nbins);

% * Confidence interval
nboots = 1000;

% * Figures
colors = rainbow(numel(T));

% --- Load data
data = load(dataset);

% --- Initialisation
rspdf_ibi = NaN(numel(binsibi), numel(T));
rspdf_dsp = NaN(numel(binsdsp), numel(T));
rspdf_reo = NaN(numel(binsreo), numel(T));
rspdf_fwd = NaN(numel(binsfwd), numel(T));
mibi = NaN(numel(T), 1);
mdsp = NaN(numel(T), 1);
mreo = NaN(numel(T), 1);
mfwd = NaN(numel(T), 1);
allrsibi = cell(numel(T), 1);
allrsdsp = cell(numel(T), 1);
allrsreo = cell(numel(T), 1);
allrsfwd = cell(numel(T), 1);

% --- Processing
for idT = 1:numel(T)
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
    
    dsp(isturn) = dsp(isturn)./displ_factor;
    
    % Pool
    ibi = ibi(:);
    ibi(isnan(ibi)) = [];
    dsp = dsp(:);
    dsp(isnan(dsp)) = [];
    reo = reo(:);
    reo(isnan(reo)) = [];
    fwd = fwd(:);
    fwd(isnan(fwd)) = [];
    
    % Get temperature average
    mibi(idT) = mean(ibi);
    mdsp(idT) = mean(dsp);
    mreo(idT) = mean(reo);
    mfwd(idT) = mean(fwd);
    
    % Rescale data
    rsibi = ibi./mibi(idT);
    rsdsp = dsp./mdsp(idT);
    rsreo = reo./mreo(idT);
    rsfwd = fwd./mfwd(idT);
    
    % Get pdf
    rspdf_ibi(:, idT) = computePDF(binsibi, rsibi, 'method', method, 'param', kdeopt(1));
    rspdf_dsp(:, idT) = computePDF(binsdsp, rsdsp, 'method', method, 'param', kdeopt(2));
    rspdf_reo(:, idT) = computePDF(binsreo, rsreo, 'method', 'hist');
    rspdf_fwd(:, idT) = computePDF(binsfwd, rsfwd, 'method', 'hist');
    
    % Store
    allrsibi{idT} = rsibi;
    allrsdsp{idT} = rsdsp;
    allrsreo{idT} = rsreo;
    allrsfwd{idT} = rsfwd;
end

% Gamma fit
allrsibi = cat(1, allrsibi{:});
allrsdsp = cat(1, allrsdsp{:});
allrsreo = cat(1, allrsreo{:});
allrsfwd = cat(1, allrsfwd{:});

% pd_rsibi = fitdist(allrsibi, 'NegativeBinomial');
% pd_rsdsp = fitdist(allrsdsp, 'Gamma');
% pd_rsreo = fitdist(allrsreo, 'Gamma');
% pd_rsfwd = fitdist(allrsfwd, 'Gamma');
 
% --- Display
legs = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);

figure('Name', 'Trescale_IBI');
ax = gca; hold on
ax.ColorOrder = colors;
plot(binsibi, rspdf_ibi);
xlabel(ax, '\delta{t}/<\delta{t}>_T');
ylabel(ax, 'pdf');
axis(ax, 'square');
legend(legs);

figure('Name', 'Trescale_DSP');
ax = gca; hold on
ax.ColorOrder = colors;
plot(binsdsp, rspdf_dsp);
xlabel(ax, 'd/<d>_T');
ylabel(ax, 'pdf');
axis(ax, 'square');
legend(legs);

figure('Name', 'Trescale_REO');
ax = gca; hold on
ax.ColorOrder = colors;
plot(binsreo, rspdf_reo);
xlabel(ax, '\delta\theta_{turn}/<\delta\theta_{turn}>_T');
ylabel(ax, 'pdf');
axis(ax, 'square');
legend(legs);
ax.YScale = 'log';
ax.XLim = [0, 5];

figure('Name', 'Trescale_FWD');
ax = gca; hold on
ax.ColorOrder = colors;
plot(binsfwd, rspdf_fwd);
xlabel(ax, '\delta\theta_{fwd}/<\delta\theta_{fwd}>_T');
ylabel(ax, 'pdf');
axis(ax, 'square');
legend(legs);