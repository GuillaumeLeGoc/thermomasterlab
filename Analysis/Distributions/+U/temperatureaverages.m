% IBI, DSP and REO means and medians for each temperature

% close all
clear
clc

% --- Parameters
% * Files
dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fnout0 = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MeanOverTemperatures.mat'];
fnout1 = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'MedianOverTemperatures.mat'];

% * Definitions
theta_threshold = 10;    % deg

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Confidence interval
nboots = 1;
opt = statset('UseParallel', true); % true to use parallel

% * Figures
colors = rainbow(numel(T));
msize = 8;  % marker size

% --- Load data
data = load(dataset);

% --- Initialisation
mibi = NaN(numel(T), 1);
mdsp = NaN(numel(T), 1);
mreo = NaN(numel(T), 1);
mfwd = NaN(numel(T), 1);
ci_mibi = NaN(numel(T), 2);
ci_mdsp = NaN(numel(T), 2);
ci_mreo = NaN(numel(T), 2);
ci_mfwd = NaN(numel(T), 2);
medibi = NaN(numel(T), 1);
meddsp = NaN(numel(T), 1);
medreo = NaN(numel(T), 1);
medfwd = NaN(numel(T), 1);
ci_medibi = NaN(numel(T), 2);
ci_meddsp = NaN(numel(T), 2);
ci_medreo = NaN(numel(T), 2);
ci_medfwd = NaN(numel(T), 2);

% --- Processing
fprintf('Processing'); tic
for idT = 1:numel(T)
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
        
    % Pool
    ibi = ibi(:);
    ibi(isnan(ibi)) = [];
    dsp = dsp(:);
    dsp(isnan(dsp)) = [];
    reo = reo(:);
    reo(isnan(reo)) = [];
    fwd = fwd(:);
    fwd(isnan(fwd)) = [];
    
    % Get temperature average
    mibi(idT) = mean(ibi);
    mdsp(idT) = mean(dsp);
    mreo(idT) = mean(reo);
    mfwd(idT) = mean(fwd);
    
    % Get confidence interval for mean
    ci_mibi(idT, :) = bootci(nboots, {@mean, ibi}, 'Options', opt)';
    ci_mdsp(idT, :) = bootci(nboots, {@mean, dsp}, 'Options', opt)';
    ci_mreo(idT, :) = bootci(nboots, {@mean, reo}, 'Options', opt)';
    ci_mfwd(idT, :) = bootci(nboots, {@mean, fwd}, 'Options', opt)';
    
    % Get temperature median
    medibi(idT) = median(ibi);
    meddsp(idT) = median(dsp);
    medreo(idT) = median(reo);
    medfwd(idT) = median(fwd);
    
    % Get confidence interval for median
    ci_medibi(idT, :) = bootci(nboots, {@median, ibi}, 'Options', opt)';
    ci_meddsp(idT, :) = bootci(nboots, {@median, dsp}, 'Options', opt)';
    ci_medreo(idT, :) = bootci(nboots, {@median, reo}, 'Options', opt)';
    ci_medfwd(idT, :) = bootci(nboots, {@median, fwd}, 'Options', opt)';
    
    fprintf('.');
end

fprintf('\tDone (%2.2fs).\n', toc);

figure('Name', 'MeansIBI');
ax = gca; hold on
p = plot(T, mibi);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mibi, 'k');
xlabel('Temperature [°C]');
ylabel('<\delta{t}>_T');
ax.XTick = T;
xlim([17 34])
axis square

figure('Name', 'MeansDSP');
ax = gca; hold on
p = plot(T, mdsp);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mdsp, 'k');
xlabel('Temperature [°C]');
ylabel('<d>_T');
ax.XTick = T;
xlim([17 34])
axis square

figure('Name', 'MeansREO');
ax = gca; hold on
p = plot(T, mreo);
p.Color = 'k';
p.Marker = 'o';
p.MarkerFaceColor = 'k';
p.MarkerSize = msize;
p.LineWidth = 2;
manualerrbar(T, ci_mreo, 'k');
xlabel('Temperature [°C]');
ylabel('<\delta\theta_{turn}>_T');
ax.XTick = T;
xlim([17 34])
axis square

% --- Save file
if ~isempty(fnout0)
    save(fnout0, 'mibi', 'mdsp', 'mreo', 'mfwd', ...
        'ci_mibi', 'ci_mdsp', 'ci_mreo', 'ci_mfwd');
end
if ~isempty(fnout1)
    save(fnout1, 'medibi', 'meddsp', 'medreo', 'medfwd', ...
        'ci_medibi', 'ci_meddsp', 'ci_medreo', 'ci_medfwd');
end