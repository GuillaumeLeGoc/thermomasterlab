% Distributions of means of parameters by trajectories (Y). Saves them to be
% used in the simulation.
% Plots the Y (<X>_traj/<X>_T).

% close all
clear
clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];
fn1_out = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'meansdistributions.mat'];
fn2_out = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'meansdistY.mat'];

usepar = true;  % use parallel for bootstrapping

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
theta_threshold = 10;   % threshold for turns/forwards

% * Bins
nboots = 1000;
method = 'kde';                 % Method for PDF ('kde' or 'hist')
mode = 'edges';                 % Mode for bins
bws = [0.2, 0.2, 0.2, 0.2];     % Bandwidths for KDE (ibi, dsp, reo, fwd)
bwsY = [0.2, 0.2, 0.2, 0.2];
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
kdeYopt = @(n) {'BandWidth', bwsY(n), 'Support', 'positive'};
nbins = 100;
lim_ibi = [0, 10];
lim_dsp = [0, 8];
lim_reo = [0, 120];
lim_fwd = [0, 10];

% --- Preparations
% * Bin vectors
switch mode
    case 'edges'
        binsi = linspace(lim_ibi(1), lim_ibi(2), nbins + 1)';
        binsd = linspace(lim_dsp(1), lim_dsp(2), nbins + 1)';
        binsr = linspace(lim_reo(1), lim_reo(2), nbins + 1)';
        binsf = linspace(lim_fwd(1), lim_fwd(2), nbins + 1)';
    case 'centers'
        binsi = linspace(lim_ibi(1), lim_ibi(2), nbins)';
        binsd = linspace(lim_dsp(1), lim_dsp(2), nbins)';
        binsr = linspace(lim_reo(1), lim_reo(2), nbins)';
        binsf = linspace(lim_fwd(1), lim_fwd(2), nbins)';
        binsi = centers2edges(binsi);
        binsd = centers2edges(binsd);
        binsr = centers2edges(binsr);
        binsf = centers2edges(binsf);
end

% Bins for Y
ebinsyibi = linspace(0, 8, nbins + 1)';
ebinsydsp = linspace(0, 4, nbins + 1)';
ebinsyreo = linspace(0, 3, nbins + 1)';
ebinsyfwd = linspace(0, 4, nbins + 1)';

% * Load data
data = load(fn_dataset);

% * Figures
colors = rainbow(numel(T));

% * Arrays
meanspdf_ibi = NaN(nbins, numel(T));
meanspdf_dsp = NaN(nbins, numel(T));
meanspdf_reo = NaN(nbins, numel(T));
meanspdf_fwd = NaN(nbins, numel(T));

meanscdf_ibi = NaN(nbins, numel(T));
meanscdf_dsp = NaN(nbins, numel(T));
meanscdf_reo = NaN(nbins, numel(T));
meanscdf_fwd = NaN(nbins, numel(T));

pdfY_ibi = NaN(nbins, numel(T));
pdfY_dsp = NaN(nbins, numel(T));
pdfY_reo = NaN(nbins, numel(T));
pdfY_fwd = NaN(nbins, numel(T));

errYibi = cell(numel(T), 1);
errYdsp = cell(numel(T), 1);
errYreo = cell(numel(T), 1);
errYfwd = cell(numel(T), 1);

cdfY_ibi = NaN(nbins, numel(T));
cdfY_dsp = NaN(nbins, numel(T));
cdfY_reo = NaN(nbins, numel(T));
cdfY_fwd = NaN(nbins, numel(T));

% --- Processing
for idT = 1:numel(T)
        
    fprintf(['Computing scaled traj. means distributions for T = ', num2str(T(idT)), '°C ']); tic
    
    % Get data
    ibi = data.interboutintervals{idT};
    dsp = data.displacements{idT};
    trn = data.dtheta{idT};
    
    % Get turn events
    isturn = abs(trn) > theta_threshold;
    reo = trn;
    reo(~isturn) = NaN;
    reo = abs(reo);
    fwd = trn;
    fwd(isturn) = NaN;
    fwd = abs(fwd);
        
    % Get trajectory means
    trajmean_ibi = mean(ibi, 1, 'omitnan');
    trajmean_dsp = mean(dsp, 1, 'omitnan');
    trajmean_reo = mean(reo, 1, 'omitnan');
    trajmean_fwd = mean(fwd, 1, 'omitnan');
    
    % Get temperature means
    tempmean_ibi = mean(ibi, 'all', 'omitnan');
    tempmean_dsp = mean(dsp, 'all', 'omitnan');
    tempmean_reo = mean(reo, 'all', 'omitnan');
    tempmean_fwd = mean(fwd, 'all', 'omitnan');
    
    % Get rescaled trajectory means (Y)
    Y_ibi = trajmean_ibi./tempmean_ibi;
    Y_dsp = trajmean_dsp./tempmean_dsp;
    Y_reo = trajmean_reo./tempmean_reo;
    Y_fwd = trajmean_fwd./tempmean_fwd;
    
    % Get pdf of real trajectory means
    switch method
        case 'hist'
            [meanspdf_ibi(:, idT), meansbin_ibi] = computePDF(binsi, trajmean_ibi, 'mode', 'edges', 'method', method);
            [meanspdf_dsp(:, idT), meansbin_dsp] = computePDF(binsd, trajmean_dsp, 'mode', 'edges', 'method', method);
            [meanspdf_reo(:, idT), meansbin_reo] = computePDF(binsr, trajmean_reo, 'mode', 'edges', 'method', method);
            [meanspdf_fwd(:, idT), meansbin_fwd] = computePDF(binsf, trajmean_fwd, 'mode', 'edges', 'method', method);
        case 'kde'
            [meanspdf_ibi(:, idT), meansbin_ibi] = computePDF(binsi, trajmean_ibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
            [meanspdf_dsp(:, idT), meansbin_dsp] = computePDF(binsd, trajmean_dsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
            [meanspdf_reo(:, idT), meansbin_reo] = computePDF(binsr, trajmean_reo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
            [meanspdf_fwd(:, idT), meansbin_fwd] = computePDF(binsf, trajmean_fwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
    end
    
    % Get pdf and cdf of rescaled trajectory means (Y)
    switch method
        case 'hist'
            [pdfY_ibi(:, idT), binsyibi] = computePDF(ebinsyibi, Y_ibi, 'mode', 'edges', 'method', method);
            [pdfY_dsp(:, idT), binsydsp] = computePDF(ebinsydsp, Y_dsp, 'mode', 'edges', 'method', method);
            [pdfY_reo(:, idT), binsyreo] = computePDF(ebinsyreo, Y_reo, 'mode', 'edges', 'method', method);
            [pdfY_fwd(:, idT), binsyfwd] = computePDF(ebinsyfwd, Y_fwd, 'mode', 'edges', 'method', method);
            
            cdfY_ibi(:, idT) = computeCDF(ebinsyibi, Y_ibi, 'mode', 'edges', 'method', method);
            cdfY_dsp(:, idT) = computeCDF(ebinsydsp, Y_dsp, 'mode', 'edges', 'method', method);
            cdfY_reo(:, idT) = computeCDF(ebinsyreo, Y_reo, 'mode', 'edges', 'method', method);
            cdfY_fwd(:, idT) = computeCDF(ebinsyfwd, Y_fwd, 'mode', 'edges', 'method', method);
        case 'kde'
            [pdfY_ibi(:, idT), binsyibi] = computePDF(ebinsyibi, Y_ibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
            [pdfY_dsp(:, idT), binsydsp] = computePDF(ebinsydsp, Y_dsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
            [pdfY_reo(:, idT), binsyreo] = computePDF(ebinsyreo, Y_reo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
            [pdfY_fwd(:, idT), binsyfwd] = computePDF(ebinsyfwd, Y_fwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
            
            cdfY_ibi(:, idT) = computeCDF(ebinsyibi, Y_ibi, 'mode', 'edges', 'method', method, 'param', kdeopt(1));
            cdfY_dsp(:, idT) = computeCDF(ebinsydsp, Y_dsp, 'mode', 'edges', 'method', method, 'param', kdeopt(2));
            cdfY_reo(:, idT) = computeCDF(ebinsyreo, Y_reo, 'mode', 'edges', 'method', method, 'param', kdeopt(3));
            cdfY_fwd(:, idT) = computeCDF(ebinsyfwd, Y_fwd, 'mode', 'edges', 'method', method, 'param', kdeopt(4));
    end
    
    % Get error on pdf for display
    fun = @(y) computePDF(ebinsyibi, y, 'mode', 'edges', 'method', 'kde', 'param', kdeopt(1));
    errYibi{idT} = bootci(nboots, {fun, Y_ibi}, 'Options', statset('UseParallel', usepar));

    fun = @(y) computePDF(ebinsydsp, y, 'mode', 'edges', 'method', 'kde', 'param', kdeopt(2));
    errYdsp{idT} = bootci(nboots, {fun, Y_dsp}, 'Options', statset('UseParallel', usepar));
    
    fun = @(y) computePDF(ebinsyreo, y, 'mode', 'edges', 'method', 'kde', 'param', kdeopt(3));
    errYreo{idT} = bootci(nboots, {fun, Y_reo}, 'Options', statset('UseParallel', usepar));
    
    fun = @(y) computePDF(ebinsyfwd, y, 'mode', 'edges', 'method', 'kde', 'param', kdeopt(4));
    errYfwd{idT} = bootci(nboots, {fun, Y_fwd}, 'Options', statset('UseParallel', usepar));
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% --- Get mean rescaled CDF
mcdfYibi = mean(cdfY_ibi, 2);
mcdfYdsp = mean(cdfY_dsp, 2);
mcdfYreo = mean(cdfY_reo, 2);
mcdfYfwd = mean(cdfY_fwd, 2);

% --- Display
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);

% * PDF of real trajectory means
figure; ax = gca; hold on
ax.ColorOrder = colors;
plot(meansbin_ibi, meanspdf_ibi);
legend(ax, labels);
xlabel(ax, '<\delta{t}>_{traj.} [s]');
ylabel(ax, 'pdf');
axis(ax, 'square');

figure; ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(meansbin_dsp, meanspdf_dsp);
legend(ax, labels);
xlabel(ax, '<d>_{traj.} [mm]');
ylabel(ax, 'pdf');
axis(ax, 'square');

figure; ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(meansbin_reo, meanspdf_reo);
legend(ax, labels);
xlabel(ax, '<\delta\theta_t>_{traj.} [deg]');
ylabel(ax, 'pdf');

figure; ax = gca; hold(ax, 'on');
ax.ColorOrder = colors;
plot(meansbin_fwd, meanspdf_fwd);
legend(ax, labels);
xlabel('<\delta\theta_f>_{traj.} [deg]');
ylabel('pdf');

% * PDF of rescaled trajectory means (Y)
% Prepare figures
figure('Name', 'Yibipdf'); hold on
axyibi = gca;
xlabel(axyibi, '<\delta{t}>_{traj.}/<\delta{t}>_T');
ylabel(axyibi, 'pdf');
axis(axyibi, 'square');

figure('Name', 'Ydsppdf'); hold on
axydsp = gca;
xlabel(axydsp, '<d>_{traj.}/<d>_T');
ylabel(axydsp, 'pdf');
axis(axydsp, 'square');

figure('Name', 'Yreopdf'); hold on
axyreo = gca;
xlabel(axyreo, '<\delta\theta_{t}>_{traj.}/<\delta\theta_{t}>_T');
ylabel(axyreo, 'pdf');
axis(axyreo, 'square');

figure('Name', 'Yfwdpdf'); hold on
axyfwd = gca;
xlabel(axyfwd, '<\delta\theta_{f}>_{traj.}/<\delta\theta_{f}>_T');
ylabel(axyfwd, 'pdf');
axis(axyfwd, 'square');

lineopt = struct;
lineopt.MarkerSize = 8;
lineopt.LineWidth = 1.25;
shadopt = struct;
shadopt.FaceAlpha = 0.275;

% Plot with shaded error bars
pibi = cell(numel(T), 1);
pdsp = cell(numel(T), 1);
preo = cell(numel(T), 1);
pfwd = cell(numel(T), 1);
for idT = 1:numel(T)
    
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    
    pibi{idT} = bierrorshaded(binsyibi, pdfY_ibi(:, idT), ...
        errYibi{idT}(1, :), errYibi{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axyibi);
    
    pdsp{idT} = bierrorshaded(binsydsp, pdfY_dsp(:, idT), ...
        errYdsp{idT}(1, :), errYdsp{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axydsp);
    
    preo{idT} = bierrorshaded(binsyreo, pdfY_reo(:, idT), ...
        errYreo{idT}(1, :), errYreo{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axyreo);
    
    pfwd{idT} = bierrorshaded(binsyfwd, pdfY_fwd(:, idT), ...
        errYfwd{idT}(1, :), errYfwd{idT}(2, :), ...
        'line', lineopt, 'patch', shadopt, 'ax', axyfwd);
    
end
% Add legends
legend(axyibi, [pibi{:}]);
legend(axydsp, [pdsp{:}]);
legend(axyreo, [preo{:}]);
legend(axyfwd, [pfwd{:}]);

% * Plot CDF of rescaled trajectory means (Y)
labels{end + 1} = 'Mean';
figure('Name', 'Yibicdf'); hold on
axyci = gca;
axyci.ColorOrder = colors;
plot(binsyibi, cdfY_ibi);
plot(axyci, binsyibi, mcdfYibi, '--k');
legend(axyci, labels);
xlabel(axyci, '<\delta{t}>_{traj.}/<\delta{t}>_T');
ylabel(axyci, 'cdf');
axis(axyibi, 'square');

figure('Name', 'Ydspcdf'); hold on
axycd = gca;
axycd.ColorOrder = colors;
plot(binsydsp, cdfY_dsp);
plot(axycd, binsydsp, mcdfYdsp, '--k');
legend(axycd, labels);
xlabel(axycd, '<d>_{traj.}/<d>_T');
ylabel(axycd, 'cdf');
axis(axycd, 'square');

figure('Name', 'Yreocdf'); hold on
axycr = gca;
axycr.ColorOrder = colors;
plot(binsyreo, cdfY_reo);
plot(axycr, binsyreo, mcdfYreo, '--k');
legend(axycr, labels);
xlabel(axycr, '<\delta\theta_{t}>_{traj.}/<\delta\theta_{t}>_T');
ylabel(axycr, 'cdf');
axis(axycr, 'square');

figure('Name', 'Yfwdcdf'); hold on
axycf = gca;
axycf.ColorOrder = colors;
plot(binsyfwd, cdfY_fwd);
plot(axycf, binsyfwd, mcdfYfwd, '--k');
legend(axycf, labels);
xlabel(axycf, '<\delta\theta_{f}>_{traj.}/<\delta\theta_{f}>_T');
ylabel(axycf, 'cdf');
axis(axycf, 'square');

% --- Save
fprintf('Saving...');

METADATA = '(pdf) n bins x m temperatures, (cdf) n temperature x 1 cell, with meansbin_xxx';
save(fn1_out, 'METADATA', ...
    'meansbin_ibi', 'meansbin_dsp', 'meansbin_reo', 'meansbin_fwd', ...
    'meanscdf_ibi', 'meanscdf_dsp', 'meanscdf_reo', 'meanscdf_fwd', ...
    'meanspdf_ibi', 'meanspdf_dsp', 'meanspdf_reo', 'meanspdf_fwd');

save(fn2_out, 'binsyibi', 'mcdfYibi', 'binsydsp', 'mcdfYdsp', ...
    'binsyreo', 'mcdfYreo', 'binsyfwd', 'mcdfYfwd');
fprintf('\tDone.\n');