% Distributions of positions, interbout intervals, displacements and turn
% angles. PDF are averaged across experiments, error bars are the SEM. Also
% plots boxplots of medians, mean and variance.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Destination file, leave empty to not save
% prefix_pdf = [pwd '/Data/Matfiles/meanpdf_'];
% prefix_stats = [pwd '/Data/Matfiles/statistics_'];
prefix_pdf = '';
prefix_stats = '';

% * Temperatures
T = [18, 22, 26, 30, 33];
% T = 22;

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
% filt.bout_freq = 0.1;   % minimum average bout frequency
filt.n_bouts = 10;      % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Displacement fix
theta_threshold = 10;    % deg
displ_factor = 1.6;

% * Statistical tests
statest = 'kruskalwallis';

% * Bins
method = 'kde';         % Method for PDF
mode = 'centers';         % Mode for bins
bws = [0.1, 0.1, 0.1, 0.5, 0.5, 0.5]; % Bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 250;

e.x = [90*pixsize, 1170*pixsize];       % bins edges for x (mm)
e.ibi = [0, 5];                         % bins for interbout interval (s)
e.disp = [0, 6];                        % bins for displacement (mm)
e.turn = [-180, 180];                   % bins for turn angle (°, wrapped to 180)
e.v = [0, 10];                           % bins for speecity (mm/s)

% * Figures names
fignames = {'MXpositions', 'MInterboutIntervals', 'MDisplacements', 'MTurnAngles', 'Mspeed'};
colors = rainbow(numel(T));

% --- Prepare bin vectors
switch mode
    case 'edges'
        bx = linspace(e.x(1), e.x(2), nbins+1)';
        bi = linspace(e.ibi(1), e.ibi(2), nbins+1)';
        bd = linspace(e.disp(1), e.disp(2), nbins+1)';
        bt = linspace(e.turn(1), e.turn(2), nbins+1)';
        bv = linspace(e.v(1), e.v(2), nbins+1)';
    case 'centers'
        bx = linspace(e.x(1), e.x(2), nbins)';
        bi = linspace(e.ibi(1), e.ibi(2), nbins)';
        bd = linspace(e.disp(1), e.disp(2), nbins)';
        bt = linspace(e.turn(1), e.turn(2), nbins)';
        bv = linspace(e.v(1), e.v(2), nbins)';
end

% --- Prepare figures
figs = cell(5, 1);
for idx_fig = 1:5
    figs{idx_fig} = figure; hold on
end

% --- Prepare arrays
allx = cell(numel(T), 1);
alli = cell(numel(T), 1);
alld = cell(numel(T), 1);
allt = cell(numel(T), 1);
allv = cell(numel(T), 1);
Imedians = cell(numel(T), 1);
Imeans = cell(numel(T), 1);
Istds = cell(numel(T), 1);
Dmedians = cell(numel(T), 1);
Dmeans = cell(numel(T), 1);
Dstds = cell(numel(T), 1);
Tmedians = cell(numel(T), 1);
Tmeans = cell(numel(T), 1);
Tstds = cell(numel(T), 1);
Vmedians = cell(numel(T), 1);
Vmeans = cell(numel(T), 1);
Vstds = cell(numel(T), 1);

% --- Loop over each temperatures and compute features PDF
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing distributions for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Preparation
    switch mode
        case 'edges'
            pdfx = NaN(numel(bx)-1, size(ftrack, 1));
            pdfi = NaN(numel(bi)-1, size(ftrack, 1));
            pdfd = NaN(numel(bd)-1, size(ftrack, 1));
            pdft = NaN(numel(bt)-1, size(ftrack, 1));
            pdfv = NaN(numel(bv)-1, size(ftrack, 1));
        case 'centers'
            pdfx = NaN(numel(bx), size(ftrack, 1));
            pdfi = NaN(numel(bi), size(ftrack, 1));
            pdfd = NaN(numel(bd), size(ftrack, 1));
            pdft = NaN(numel(bt), size(ftrack, 1));
            pdfv = NaN(numel(bv), size(ftrack, 1));
    end
    
    % Preparation
    allx{idT} = cell(size(ftrack, 1), 1);
    alli{idT} = cell(size(ftrack, 1), 1);
    alld{idT} = cell(size(ftrack, 1), 1);
    allt{idT} = cell(size(ftrack, 1), 1);
    allv{idT} = cell(size(ftrack, 1), 1);
    median_ibi = NaN(size(ftrack, 1), 1);
    mean_ibi = NaN(size(ftrack, 1), 1);
    std_ibi = NaN(size(ftrack, 1), 1);
    median_displ = NaN(size(ftrack, 1), 1);
    mean_displ = NaN(size(ftrack, 1), 1);
    std_displ = NaN(size(ftrack, 1), 1);
    median_turn = NaN(size(ftrack, 1), 1);
    mean_turn = NaN(size(ftrack, 1), 1);
    std_turn = NaN(size(ftrack, 1), 1);
    median_spee = NaN(size(ftrack, 1), 1);
    mean_spee = NaN(size(ftrack, 1), 1);
    std_spee = NaN(size(ftrack, 1), 1);
    
    for idexp = 1:size(ftrack, 1)
        
        [position, intboutint, displ, turnangle, ~, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt);
        
        isturn = abs(turnangle) > theta_threshold;
        displ(isturn) = displ(isturn)./displ_factor;
        
        spee = displ.*pixsize./(intboutint./framerate);
                
        % - PDF
        switch method
            case 'hist'
                [pdfx(:, idexp), cx] = computePDF(bx, position.*pixsize, 'method', method, 'mode', mode);
                [pdfi(:, idexp), ci] = computePDF(bi, intboutint./framerate, 'method', method, 'mode', mode);
                [pdfd(:, idexp), cd] = computePDF(bd, displ.*pixsize, 'method', method, 'mode', mode);
                [pdft(:, idexp), ct] = computePDF(bt, turnangle, 'method', method, 'mode', mode);
                [pdfv(:, idexp), cv] = computePDF(bv, spee, 'method', method, 'mode', mode);
            case 'kde'
                [pdfx(:, idexp), cx] = computePDF(bx, position.*pixsize, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(1)});
                [pdfi(:, idexp), ci] = computePDF(bi, intboutint./framerate, 'method', method, 'mode', mode, 'param', kdeopt(2));
                [pdfd(:, idexp), cd] = computePDF(bd, displ.*pixsize, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(3), 'Support', 'positive'});
                [pdft(:, idexp), ct] = computePDF(bt, turnangle, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(4)});
                [pdfv(:, idexp), cv] = computePDF(bv, spee, 'method', method, 'mode', mode, 'param', {'BandWidth', bws(5), 'Support', 'positive'});
        end

        % - Statistics
        [mean_ibi(idexp), median_ibi(idexp), std_ibi(idexp)] = getStats(intboutint./framerate);
        [mean_displ(idexp), median_displ(idexp), std_displ(idexp)] = getStats(displ.*pixsize);
        [mean_turn(idexp), median_turn(idexp), std_turn(idexp)] = getStats(turnangle);
        [mean_spee(idexp), median_spee(idexp), std_spee(idexp)] = getStats(spee);
        
        % - Store
        allx{idT}{idexp} = position.*pixsize;
        alli{idT}{idexp} = intboutint./framerate;
        alld{idT}{idexp} = displ.*pixsize;
        allt{idT}{idexp} = turnangle;
        allv{idT}{idexp} = spee;
        
        fprintf('.');
    end
    
    % Compute mean pdf
    mpdfx = mean(pdfx, 2);
    epdfx = std(pdfx, [], 2)/sqrt(size(ftrack, 1));
    mpdfi = mean(pdfi, 2);
    epdfi = std(pdfi, [], 2)/sqrt(size(ftrack, 1));
    mpdfd = mean(pdfd, 2);
    epdfd = std(pdfd, [], 2)/sqrt(size(ftrack, 1));
    mpdft = mean(pdft, 2);
    epdft = std(pdft, [], 2)/sqrt(size(ftrack, 1));
    mpdfv = mean(pdfv, 2);
    epdfv = std(pdfv, [], 2)/sqrt(size(ftrack, 1));
    
    % Fix errors to show in log scale
    epdft(mpdft - epdft <= 0) = NaN;
    epdft = fillmissing(epdft, 'constant', min(epdft));
    mpdft(mpdft <= 0) = NaN;
    mpdft = fillmissing(mpdft, 'previous');
    mpdft = fillmissing(mpdft, 'next');
    
    % Get statistics
    mx = trapz(cx, cx.*mpdfx);
    mi = trapz(ci, ci.*mpdfi);
    md = trapz(cd, cd.*mpdfd);
    mt = trapz(ct, ct.*mpdft);
    mv = trapz(cv, cv.*mpdfv);
    
    % --- Store per-temperature statistics
    Imedians{idT} = median_ibi;
    Imeans{idT} = mean_ibi;
    Istds{idT} = std_ibi;
    Dmedians{idT} = median_displ;
    Dmeans{idT} = mean_displ;
    Dstds{idT} = std_displ;
    Tmedians{idT} = median_turn;
    Tmeans{idT} = mean_turn;
    Tstds{idT} = std_turn;
    Vmedians{idT} = median_spee;
    Vmeans{idT} = mean_spee;
    Vstds{idT} = std_spee;
    
    fprintf('\t Done (%2.2fs).\n', toc);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 4;
    lineopt.LineWidth = 2;
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    
    set(0, 'CurrentFigure', figs{1})
    erb{1}{idT} = errorshaded(cx, mpdfx, epdfx, 'line', lineopt, 'patch', shadopt);
    xlabel('x [mm]');
    ylabel('<pdf>_{batch}');
    ax = gca;
    plot([mx mx], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{2});
    erb{2}{idT} = errorshaded(ci, mpdfi, epdfi, 'line', lineopt, 'patch', shadopt);
    xlabel('\delta{t} [s]');
    ylabel('<pdf>_{batch}');
    ax = gca;
    ax.XLim = [0 e.ibi(2)];
    ax.YLim = [0 1.9];
    plot([mi mi], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{3});
    erb{3}{idT} = errorshaded(cd, mpdfd, epdfd, 'line', lineopt, 'patch', shadopt);
    xlabel('d [mm]');
    ylabel('<pdf>_{batch}');
    ax = gca;
    ax.XLim = [0 e.disp(2)];
    ax.YLim = [0 1];
    plot([md md], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    set(0, 'CurrentFigure', figs{4});
    erb{4}{idT} = errorshaded(ct, mpdft, epdft, 'line', lineopt, 'patch', shadopt);
    xlabel('\delta\theta [deg]');
    ylabel('<pdf>_{batch}');    
    ax = gca; ax.YScale = 'log';
    ax.XLim = [e.turn(1) e.turn(2)];
    ax.YLim = [1e-4 1e-1];
    plot([mt, mt], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .4);
    
    set(0, 'CurrentFigure', figs{5});
    erb{5}{idT} = errorshaded(cv, mpdfv, epdfv, 'line', lineopt, 'patch', shadopt);
    xlabel('Mean speed [mm/s]');
    ylabel('<pdf>_{batch}');
    ax = gca;
    plot([mv, mv], ax.YLim, 'Color', colors(idT, :), 'LineStyle', '--', 'LineWidth', .8);
    
    % --- Save file
    if ~isempty(prefix_pdf)
        fname = [prefix_pdf 'T' num2str(T(idT)) '.mat'];
        save(fname, 'mpdfx', 'mpdfi', 'mpdfd', 'mpdft', 'mpdfv', 'cx', 'ci', 'cd', 'ct', 'cv');
    end 
    if ~isempty(prefix_stats)
        fname = [prefix_stats 'T' num2str(T(idT)) '.mat'];
        save(fname, 'median_ibi', 'mean_ibi', 'std_ibi', ...
            'median_displ', 'mean_displ', 'std_displ', ...
            'median_turn', 'mean_turn', 'std_turn', ...
            'median_spee', 'mean_spee', 'std_spee');
    end
end

fkm = plot(NaN, NaN, 'k--', 'DisplayName', 'Mean', 'LineWidth', .4);

% Set figures names & legends
for idx_fig = 1:numel(figs)
    set(0, 'CurrentFigure', figs{idx_fig});
    legend([erb{idx_fig}{:} fkm]);
    figs{idx_fig}.Name = fignames{idx_fig};
    axis square
end

set(0, 'CurrentFigure', figs{4});
ax = gca;
ptc = patch([-theta_threshold, -theta_threshold, theta_threshold, theta_threshold], ...
    [ax.YLim(1), ax.YLim(2), ax.YLim(2), ax.YLim(1)], [0.2, 0.2, 0.2]);
ptc.EdgeColor = 'none';
ptc.FaceAlpha = 0.25;
ptc.DisplayName = 'Forward';

% --- Display boxplots of statistics
maxsize = max(cellfun(@numel, Imeans));
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);

pointopt = struct;
pointopt.Color = colors;
pointopt.jitter = 0;
boxopt = struct;
boxopt.BoxColors = colors;

% * Mean interbout interval
% - Prepare data
Imeans = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Imeans, 'UniformOutput', false);
g = cell(size(Imeans));
for idx = 1:size(Imeans, 1)
    g{idx} = idx.*ones(numel(Imeans{idx}), 1);
end
g = cat(1, g{:});

boxopt.Labels = labels;

X = cat(1, Imeans{:});

% - Display
f = figure; hold on
f.Name = 'MeaIBI';
beautifulbox(X, g, pointopt, boxopt);
title('Mean interbout interval');
ylabel('<\delta{t}>_{batch} [s]');
axis([-Inf Inf 0.5 2.5]);

% - Add significance
pairs = {[2, 3]};
addStars(Imeans, 'pairs', pairs, 'test', statest);
axis square

% * Median interbout interval
% - Prepare data
Imedians = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Imedians, 'UniformOutput', false);
X = cat(1, Imedians{:});

% - Display
f = figure; hold on
f.Name = 'MedIBI';
beautifulbox(X, g, pointopt, boxopt);
title('Median interbout interval');
ylabel('Median(\delta{t})_{batch} [s]');
axis([-Inf Inf 0.5 1.5]);

% - Add significance
pairs = {[2, 3]};
addStars(Imedians, 'pairs', pairs, 'test', statest);
axis square

% * Std interbout interval
% - Prepare data
Istds = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Istds, 'UniformOutput', false);
g = cell(size(Istds));
for idx = 1:size(Istds, 1)
    g{idx} = idx.*ones(numel(Istds{idx}), 1);
end
g = cat(1, g{:});

boxopt.Labels = labels;

X = cat(1, Istds{:});

% - Display
f = figure; hold on
f.Name = 'MeaIBI';
beautifulbox(X, g, pointopt, boxopt);
title('Std interbout interval');
ylabel('std(\delta{t})_{batch} (s)');
axis([-Inf Inf 0 10]);

% - Add significance
pairs = {[1, 2], [2, 3], [3, 4], [4, 5]};
addStars(Istds, 'pairs', pairs, 'test', statest);
axis square

% * Mean displacement
% - Prepare data
Dmeans = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Dmeans, 'UniformOutput', false);
X = cat(1, Dmeans{:});

% - Display
f = figure; hold on
f.Name = 'MeaDISP';
beautifulbox(X, g, pointopt, boxopt);
title('Mean displacement');
ylabel('<d>_{batch} [mm]');
axis([-Inf Inf 0.5 2]);

% - Add significance
pairs = {[2, 3]};
addStars(Dmeans, 'pairs', pairs, 'test', statest);
axis square

% * Median displacement
% - Prepare data
Dmedians = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Dmedians, 'UniformOutput', false);
X = cat(1, Dmedians{:});

% - Display
f = figure; hold on
f.Name = 'MedDISP';
beautifulbox(X, g, pointopt, boxopt);
title('Median displacement');
ylabel('Median(d)_{batch} [mm]');
axis([-Inf Inf 0.5 2]);

% - Add significance
pairs = {[2, 3]};
addStars(Dmedians, 'pairs', pairs, 'test', statest);
axis square

% * Std displacement
% - Prepare data
Dstds = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Dstds, 'UniformOutput', false);
X = cat(1, Dstds{:});

% - Display
f = figure; hold on
f.Name = 'MedDISP';
beautifulbox(X, g, pointopt, boxopt);
title('Std displacement');
ylabel('std(d)_{batch} [mm]');
axis([-Inf Inf 0.5 1.25]);

% - Add significance
pairs = {[1, 2], [2, 3], [3, 4], [4, 5]};
addStars(Dstds, 'pairs', pairs, 'test', statest);
axis square

% * Turn angle width
% - Prepare data
Tstds = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Tstds, 'UniformOutput', false);
X = cat(1, Tstds{:}).^2;

% - Display
f = figure; hold on
f.Name = 'STURN';
beautifulbox(X./1000, g, pointopt, boxopt);
title('Turn angle width');
ylabel('1000 \times <\delta{\theta}^2>_{batch} [deg^2]');
axis([-Inf Inf 0 2.5]);

% - Add significance
pairs = {[1, 2]; [2, 3]; [3, 4]; [4, 5]};
addStars(Tstds, 'pairs', pairs, 'test', statest);
axis square

% * Mean speed
% - Prepare data
Vmeans = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Vmeans, 'UniformOutput', false);
X = cat(1, Vmeans{:});

% - Display
f = figure; hold on
f.Name = 'MeaSPEED';
beautifulbox(X, g, pointopt, boxopt);
title('Mean speed');
ylabel('<v>_{batch} [mm/s]');
axis([-Inf Inf 0 3]);

% - Add significance
pairs = {[2, 3]};
addStars(Vmeans, 'pairs', pairs, 'test', statest);
axis square

% * Median speed
% - Prepare data
Vmedians = cellfun(@(x) cat(1, x, NaN(maxsize - numel(x), 1)), Vmedians, 'UniformOutput', false);
X = cat(1, Vmedians{:});

% - Display
f = figure; hold on
f.Name = 'MedSPEED';
beautifulbox(X, g, pointopt, boxopt);
title('Median speed');
ylabel('Median(v)_{batch} [mm/s]');
axis([-Inf Inf 0 3]);

% - Add significance
pairs = {[2, 3]};
addStars(Vmedians, 'pairs', pairs, 'test', statest);
axis square