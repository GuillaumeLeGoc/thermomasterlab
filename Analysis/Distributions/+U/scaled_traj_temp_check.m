% Compare (a) distributions scaled by the temperature mean and (b)
% distributions scaled by the trajectories means.

close all
clear
clc

% --- Parameters
% * % * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum number of bouts per seconds
filt.minx = 0;
filt.maxx = Inf;

pixsize = 0.09;         % pixel size in mm
theta_threshold = 10;   % threshold for turns/forwards
displ_factor = 1.6;     % scaling factor for displacements of turns

% * Bins
method = 'hist';         % Method for PDF ('kde' or 'hist')
mode = 'centers';       % Mode for bins
bws = [0.2, 0.2, 0.2];  % Bandwidths for KDE (ibi, dsp, reo)
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 100;
lim_ibi = [0, 8];
lim_dsp = [0, 8];
lim_reo = [0, 4];

% --- Preparations
% * Bin vectors
switch mode
    case 'edges'
        binsi = linspace(lim_ibi(1), lim_ibi(2), nbins + 1)';
        binsd = linspace(lim_dsp(1), lim_dsp(2), nbins + 1)';
        binsr = linspace(lim_reo(1), lim_reo(2), nbins + 1)';
        cbinsi = edges2centers(binsi);
        cbinsd = edges2centers(binsd);
        cbinsr = edges2centers(binsr);
    case 'centers'
        cbinsi = linspace(lim_ibi(1), lim_ibi(2), nbins)';
        cbinsd = linspace(lim_ibi(1), lim_dsp(2), nbins)';
        cbinsr = linspace(lim_ibi(1), lim_reo(2), nbins)';
end

% * Figures
colors = rainbow(numel(T));
fibi_intra = figure; aibi_intra = axes(fibi_intra); hold(aibi_intra, 'on');
fibi_inter = figure; aibi_inter = axes(fibi_inter); hold(aibi_inter, 'on');
fdsp_intra = figure; adsp_intra = axes(fdsp_intra); hold(adsp_intra, 'on');
fdsp_inter = figure; adsp_inter = axes(fdsp_inter); hold(adsp_inter, 'on');
freo_intra = figure; areo_intra = axes(freo_intra); hold(areo_intra, 'on');
freo_inter = figure; areo_inter = axes(freo_inter); hold(areo_inter, 'on');

% * Arrays
pdf_ibi = NaN(nbins, numel(T));
pdf_dsp = NaN(nbins, numel(T));
pdf_reo = NaN(nbins, numel(T));
pdf_ibi_s = NaN(nbins, numel(T));
pdf_dsp_s = NaN(nbins, numel(T));
pdf_reo_s = NaN(nbins, numel(T));
cdf_ibi = NaN(nbins, numel(T));
cdf_dsp = NaN(nbins, numel(T));
cdf_reo = NaN(nbins, numel(T));
cdf_ibi_s = NaN(nbins, numel(T));
cdf_dsp_s = NaN(nbins, numel(T));
cdf_reo_s = NaN(nbins, numel(T));

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing scaled distributions for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Pool parameters from all experiments
    allibi = cell(size(ftrack, 1), 1);
    allibi_s = cell(size(ftrack, 1), 1);
    alldsp = cell(size(ftrack, 1), 1);
    alldsp_s = cell(size(ftrack, 1), 1);
    allreo = cell(size(ftrack, 1), 1);
    allreo_s = cell(size(ftrack, 1), 1);
    for idexp = 1:size(ftrack, 1)
        
        % Get parameters
        [~, ibi, dsp, reo, ~, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        ibi = ibi./framerate;                   % convert to s
        ibi_s = ibi./nanmean(ibi);
        ibi = ibi(:);
        ibi_s = ibi_s(:);
        ibi(isnan(ibi)) = [];
        ibi_s(isnan(ibi_s)) = [];
        allibi{idexp} = ibi;
        allibi_s{idexp} = ibi_s;
        
        dsp = dsp.*pixsize;                     % convert to mm
        reo = abs(reo);
        isturn = reo > theta_threshold;         % detect turns
        dsp(isturn) = dsp(isturn)./displ_factor;% rescale displacements of turns
        dsp_s = dsp./nanmean(dsp);
        dsp = dsp(:);
        dsp_s = dsp_s(:);
        dsp(isnan(dsp)) = [];
        dsp_s(isnan(dsp_s)) = [];
        alldsp{idexp} = dsp;
        alldsp_s{idexp} = dsp_s;
        
        reo(~isturn) = NaN;
        reo_s = reo./nanmean(reo);
        reo = reo(:);
        reo_s = reo_s(:);
        reo(isnan(reo)) = [];
        reo_s(isnan(reo_s)) = [];
        reo(reo == 0) = eps;    % fix for kde
        reo_s(reo_s == 0) = eps;
        allreo{idexp} = reo;
        allreo_s{idexp} = reo_s;
        
        fprintf('.');
    end
    
    % Pool data
    allibi = cat(1, allibi{:});
    alldsp = cat(1, alldsp{:});
    allreo = cat(1, allreo{:});
    allibi_s = cat(1, allibi_s{:});
    alldsp_s = cat(1, alldsp_s{:});
    allreo_s = cat(1, allreo_s{:});
    
    % Scale by mean over temperature
    allibi = allibi./mean(allibi);
    alldsp = alldsp./mean(alldsp);
    allreo = allreo./mean(allreo);
    
    % Get distributions
    switch method
        case 'hist'
            pdf_ibi(:, idT) = computePDF(cbinsi, allibi, 'method', method);
            pdf_dsp(:, idT) = computePDF(cbinsd, alldsp, 'method', method);
            pdf_reo(:, idT) = computePDF(cbinsr, allreo, 'method', method);
            
            pdf_ibi_s(:, idT) = computePDF(cbinsi, allibi_s, 'method', method);
            pdf_dsp_s(:, idT) = computePDF(cbinsd, alldsp_s, 'method', method);
            pdf_reo_s(:, idT) = computePDF(cbinsr, allreo_s, 'method', method);
            
            cdf_ibi(:, idT) = computeCDF(cbinsi, allibi, 'method', method);
            cdf_dsp(:, idT) = computeCDF(cbinsd, alldsp, 'method', method);
            cdf_reo(:, idT) = computeCDF(cbinsr, allreo, 'method', method);
            
            cdf_ibi_s(:, idT) = computeCDF(cbinsi, allibi_s, 'method', method);
            cdf_dsp_s(:, idT) = computeCDF(cbinsd, alldsp_s, 'method', method);
            cdf_reo_s(:, idT) = computeCDF(cbinsr, allreo_s, 'method', method);
        case 'kde'
            pdf_ibi(:, idT) = computePDF(cbinsi, allibi, 'method', method, 'param', kdeopt(1));
            pdf_dsp(:, idT) = computePDF(cbinsd, alldsp, 'method', method, 'param', kdeopt(2));
            pdf_reo(:, idT) = computePDF(cbinsr, allreo, 'method', method, 'param', kdeopt(3));
            
            pdf_ibi_s(:, idT) = computePDF(cbinsi, allibi_s, 'method', method, 'param', kdeopt(1));
            pdf_dsp_s(:, idT) = computePDF(cbinsd, alldsp_s, 'method', method, 'param', kdeopt(2));
            pdf_reo_s(:, idT) = computePDF(cbinsr, allreo_s, 'method', method, 'param', kdeopt(3));
            
            cdf_ibi(:, idT) = computeCDF(cbinsi, allibi, 'method', method, 'param', kdeopt(1));
            cdf_dsp(:, idT) = computeCDF(cbinsd, alldsp, 'method', method, 'param', kdeopt(2));
            cdf_reo(:, idT) = computeCDF(cbinsr, allreo, 'method', method, 'param', kdeopt(3));
            
            cdf_ibi_s(:, idT) = computeCDF(cbinsi, allibi_s, 'method', method, 'param', kdeopt(1));
            cdf_dsp_s(:, idT) = computeCDF(cbinsd, alldsp_s, 'method', method, 'param', kdeopt(2));
            cdf_reo_s(:, idT) = computeCDF(cbinsr, allreo_s, 'method', method, 'param', kdeopt(3));
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % --- Display
    figure;
    
    subplot(221); hold on
    plot(cbinsi, pdf_ibi(:, idT), 'Color', colors(idT, :), 'DisplayName', '/<X>_T');
    plot(cbinsi, pdf_ibi_s(:, idT), '--', 'Color', colors(idT, :), 'DisplayName', '/<X>_{traj.}');
    xlabel('\delta{t}/<\delta{t}>');
    ylabel('pdf');
    legend;
    
    subplot(222); hold on
    plot(cbinsd, pdf_dsp(:, idT), 'Color', colors(idT, :), 'DisplayName', '/<X>_T');
    plot(cbinsd, pdf_dsp_s(:, idT), '--', 'Color', colors(idT, :), 'DisplayName', '/<X>_{traj.}');
    xlabel('d/<d>');
    ylabel('pdf');
    legend;
    
    subplot(223); hold on
    plot(cbinsr, pdf_reo(:, idT), 'Color', colors(idT, :), 'DisplayName', '/<X>_T');
    plot(cbinsr, pdf_reo_s(:, idT), '--', 'Color', colors(idT, :), 'DisplayName', '/<X>_{traj.}');
    xlabel('\delta\theta/<\delta\theta>');
    ylabel('pdf');
    ax = gca;
    ax.YScale = 'log';
    legend('Location', 'southwest');
    
end