% Check wether we are in steady-state.

close all
clear
% clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 5;    % sequence length for each fish in seconds
filt.bout_freq = .1;    % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% * Displacement fix
theta_threshold = 10;    % deg
displ_factor = 1.6;

% * Bins
method = 'hist';      % Method for PDF
mode = 'edges';       % Mode for bins
nbins = 60;

e.x = [100*pixsize, 1100*pixsize]; % bins centers for x (mm)
e.ibi = [0, 10];                    % bins for interbout interval (s)
e.disp = [0, 15];                   % bins for displacement (mm)
e.turn = [-180, 180];              % bins for turn angle (°, wrapped to 180)

bx = linspace(e.x(1), e.x(2), nbins)';
bi = linspace(e.ibi(1), e.ibi(2), nbins)';
bd = linspace(e.disp(1), e.disp(2), nbins)';
bt = linspace(e.turn(1), e.turn(2), 120)';

%  * Figures
colors = [0, 0, 0; 0.33, 0.33, 0.33; 0.75, 0.75, 0.75];

% * Initialization
Xp = cell(numel(T), 3);
Xpr = cell(numel(T), 3);
Ib = cell(numel(T), 3);
Ibr = cell(numel(T), 3);
Di = cell(numel(T), 3);
Dir = cell(numel(T), 3);
Tu = cell(numel(T), 3);
Tur = cell(numel(T), 3);

% --- Loop over each temperatures
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Checking steady-state for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    % Get features from each experiment
    xpool = cell(size(ftrack, 1), 3);
    xpoolr = cell(size(ftrack, 1), 3);
    ipool = cell(size(ftrack, 1), 3);
    ipoolr = cell(size(ftrack, 1), 3);
    dpool = cell(size(ftrack, 1), 3);
    dpoolr = cell(size(ftrack, 1), 3);
    tpool = cell(size(ftrack, 1), 3);
    tpoolr = cell(size(ftrack, 1), 3);
    
    for idexp = 1:size(ftrack, 1)
        
        [x, i, d, t, ~, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        % Displacement fix
        isturn = abs(t) > theta_threshold;
        d(isturn) = d(isturn)./displ_factor;
        
        nframes = size(x, 1);
        
        % Shuffle for control
        xr = shuffleArray(x, 1);
        ir = shuffleArray(i, 1);
        dr = shuffleArray(d, 1);
        tr = shuffleArray(t, 1);
        
        % Split data into three temporal parts
        start = 0;
        for idtier = 1:3
            
            % Get indices of given tier
            tierx = (start + 1):min(floor(idtier*nframes/3), nframes);
            tier = (start + 1):min(floor(idtier*(nframes - 1)/3), nframes - 1);
            
            % Limit to those indices
            xtier = x(tierx, :);
            xtierr = xr(tierx, :);
            itier = i(tier, :);
            itierr = ir(tier, :);
            dtier = d(tier, :);
            dtierr = dr(tier, :);
            ttier = t(tier, :);
            ttierr = tr(tier, :);
            
            % Remove nans
            xtier = xtier(:);
            xtier(isnan(xtier)) = [];
            xtierr = xtierr(:);
            xtierr(isnan(xtierr)) = [];
            itier = itier(:);
            itier(isnan(itier)) = [];
            itierr = itierr(:);
            itierr(isnan(itierr)) = [];
            dtier = dtier(:);
            dtier(isnan(dtier)) = [];
            dtierr = dtierr(:);
            dtierr(isnan(dtierr)) = [];
            ttier = ttier(:);
            ttier(isnan(ttier)) = [];
            ttierr = ttierr(:);
            ttierr(isnan(ttierr)) = [];
            
            % Store
%             xpool{idexp, idtier} = xtier.*pixsize;
%             xpoolr{idexp, idtier} = xtierr.*pixsize;
%             ipool{idexp, idtier} = itier./framerate;
%             ipoolr{idexp, idtier} = itierr./framerate;
%             dpool{idexp, idtier} = dtier.*pixsize;
%             dpoolr{idexp, idtier} = dtierr.*pixsize;
%             tpool{idexp, idtier} = ttier;
%             tpoolr{idexp, idtier} = ttierr;
            
            xpool{idexp, idtier} = mean(xtier.*pixsize);
            xpoolr{idexp, idtier} = mean(xtierr.*pixsize);
            ipool{idexp, idtier} = mean(itier./framerate);
            ipoolr{idexp, idtier} = mean(itierr./framerate);
            dpool{idexp, idtier} = mean(dtier.*pixsize);
            dpoolr{idexp, idtier} = mean(dtierr.*pixsize);
            tpool{idexp, idtier} = mean(abs(ttier));
            tpoolr{idexp, idtier} = mean(abs(ttierr));
            
            start = floor(idtier*nframes/3);
        end
            
        fprintf('.');
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % Create figures
%     figx = figure; axx = gca; hold on
%     figi = figure; axi = gca; hold on
%     figd = figure; axd = gca; hold on
%     figt = figure; axt = gca; hold on
    
    % Pool all experiments & get pdf
    for idtier = 1:size(xpool, 2)
        
        % Pool
        Xp{idT, idtier} = cat(1, xpool{:, idtier});
        Xpr{idT, idtier} = cat(1, xpoolr{:, idtier});
        Ib{idT, idtier} = cat(1, ipool{:, idtier});
        Ibr{idT, idtier} = cat(1, ipoolr{:, idtier});
        Di{idT, idtier} = cat(1, dpool{:, idtier});
        Dir{idT, idtier} = cat(1, dpoolr{:, idtier});
        Tu{idT, idtier} = cat(1, tpool{:, idtier});
        Tur{idT, idtier} = cat(1, tpoolr{:, idtier});
        
        % Compute pdf
        [pdfx, cx] = computePDF(bx, Xp{idT, idtier}, 'mode', mode, 'method', method);
        [pdfi, ci] = computePDF(bi, Ib{idT, idtier}, 'mode', mode, 'method', method);
        [pdfd, cd] = computePDF(bd, Di{idT, idtier}, 'mode', mode, 'method', method);
        [pdft, ct] = computePDF(bt, Tu{idT, idtier}, 'mode', mode, 'method', method);
        
%         % Display
%         p = plot(axx, cx, pdfx);
%         p.Color = colors(idtier, :);
%         p.DisplayName = [num2str(idtier) '° third'];
%         xlabel(axx, 'X position [mm]');
%         ylabel(axx, 'pdf');
%         title(axx, ['T = ' num2str(T(idT)) '°C']);
%         
%         q = plot(axi, ci, pdfi);
%         q.Color = colors(idtier, :);
%         q.DisplayName = [num2str(idtier) '° third'];
%         xlabel(axi, '\delta{t} [s]');
%         ylabel(axi, 'pdf');
%         title(axi, ['T = ' num2str(T(idT)) '°C']);
%         
%         r = plot(axd, cd, pdfd);
%         r.Color = colors(idtier, :);
%         r.DisplayName = [num2str(idtier) '° third'];
%         xlabel(axd, 'd [mm]');
%         ylabel(axd, 'pdf');
%         title(axd, ['T = ' num2str(T(idT)) '°C']);
%         
%         s = plot(axt, ct, pdft);
%         s.Color = colors(idtier, :);
%         s.DisplayName = [num2str(idtier) '° third'];
%         xlabel(axt, '\delta\theta [deg]');
%         ylabel(axt, 'pdf');
%         title(axt, ['T = ' num2str(T(idT)) '°C']);
    end
    
    % Cosmetic
%     axis(axx, 'square');
%     legend(axx);
%     axis(axi, 'square');
%     legend(axi);
%     axis(axd, 'square');
%     legend(axd);
%     axis(axt, 'square');
%     legend(axt);
    
end

% Tests
pointopt = struct;
pointopt.Color = colors;
pointopt.jitter = 0;
boxopt = struct;
boxopt.BoxColors = colors;

for idT = 1:numel(T)
    
    It = Ib(idT, :);
    Dt = Di(idT, :);
    Tt = Tu(idT, :);
    g = cell(numel(It), 1);
    for idx = 1:numel(It)
        g{idx} = idx.*ones(numel(It{idx}), 1);
    end
    g = cat(1, g{:});
    
    Ipool = cat(1, It{:});
    Dpool = cat(1, Dt{:});
    Tpool = cat(1, Tt{:});
    
    % --- Display
    figure;
    subplot(2, 2, 1);
    boxchart(g, Ipool, 'Notch', 'on');
    ax = gca;
    ax.XTick = 1:3;
    ax.XTickLabel = {'Third 1', 'Third 2', 'Third 3'};
    ylabel('<\delta{t}> (s)')
    addStars(It, 'test', 'ks');
    
    subplot(2, 2, 2);
    boxchart(g, Dpool, 'Notch', 'on');
    ax = gca;
    ax.XTick = 1:3;
    ax.XTickLabel = {'Third 1', 'Third 2', 'Third 3'};
    ylabel('<d> (mm)');
    addStars(Dt, 'test', 'ks');
    
    subplot(2, 2, 3);
    boxchart(g, Tpool, 'Notch', 'on');
    ax = gca;
    ax.XTick = 1:3;
    ax.XTickLabel = {'Third 1', 'Third 2', 'Third 3'};
    ylabel('<|\delta\theta|> (deg)');
    addStars(Tt, 'test', 'ks');
    
    sgtitle(['T = ' num2str(T(idT)) '°C']);
    
end

% Pool temperatures
allI = cell(3, 1);
allD = cell(3, 1);
allT = cell(3, 1);

for idtier = 1:3
    
    allI{idtier} = cat(1, Ib{:, idtier});
    allD{idtier} = cat(1, Di{:, idtier});
    allT{idtier} = cat(1, Tu{:, idtier});
    
end

allg = cell(numel(allI), 1);
for idx = 1:numel(allI)
    allg{idx} = idx.*ones(numel(allI{idx}), 1);
end
allg = cat(1, allg{:});
pallI = cat(1, allI{:});
pallD = cat(1, allD{:});
pallT = cat(1, allT{:});

figure;
subplot(2, 2, 1);
boxchart(allg, pallI, 'Notch', 'on');
ax = gca;
ax.XTick = 1:3;
ax.XTickLabel = {'Third 1', 'Third 2', 'Third 3'};
ylabel('<\delta{t}> (s)');
addStars(allI, 'test', 'ks');
pi = multitest(allI, 'ks');
title(['all p > ' num2str(min(pi(:)))]);

subplot(2, 2, 2);
boxchart(allg, pallD, 'Notch', 'on');
ax = gca;
ax.XTick = 1:3;
ax.XTickLabel = {'Third 1', 'Third 2', 'Third 3'};
ylabel('<d> (mm)');
addStars(allD, 'test', 'ks');
pd = multitest(allD, 'ks');
title(['all p > ' num2str(min(pd(:)))]);

subplot(2, 2, 3);
boxchart(allg, pallT, 'Notch', 'on');
ax = gca;
ax.XTick = 1:3;
ax.XTickLabel = {'Third 1', 'Third 2', 'Third 3'};
ylabel('<|\delta\theta|> (deg)');
addStars(allT, 'test', 'ks');
pt = multitest(allT, 'ks');
title(['all p > ' num2str(min(pt(:)))]);