% Create sequence from different length and look at the mean & variance to
% check if it's TCL compatible. Compare with real sequences.

% close all
clear

% --- Files
seqfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];

% --- Temperature
T = [18, 22, 26, 30, 33];

% --- Parameters
sizes = 16:4:256;  % size of shuffled trajectories
n = 1e4;                             % number of trajectories
pixsize = 0.09;
framerate = 25;

% --- Get data
D = load(seqfile);

% --- Processing
figure; hold on
colors = rainbow(numel(T));
for idT = 1:numel(T)
    
    % - Get data
    ibi = D.interboutintervals{idT};
    dsp = D.displacements{idT};
    dte = D.dtheta{idT};
    
    ibi = ibi(:); ibi(isnan(ibi)) = [];
    dsp = dsp(:); dsp(isnan(dsp)) = [];
    dte = dte(:); dte(isnan(dte)) = [];
    
    % - Generate shuffled trajectories
    varibi = NaN(numel(sizes), 1);
    vardsp = NaN(numel(sizes), 1);
    vardte = NaN(numel(sizes), 1);
    for s = 1:numel(sizes)
        
        sz = sizes(s);
        
        mibi = NaN(n, 1);
        mdsp = NaN(n, 1);
        mdte = NaN(n, 1);
        for ids = 1:n
            
            rdid = randi(numel(ibi), [sz, 1]);
            subibi = ibi(rdid);
            mibi(ids) = mean(subibi);
            
            rdid = randi(numel(dsp), [sz, 1]);
            subdsp = dsp(rdid);
            mdsp(ids) = mean(subdsp);
            
            rdid = randi(numel(dte), [sz, 1]);
            subdte = dte(rdid);
            mdte(ids) = mean(subdte);
            
        
        end
        varibi(s) = var(mibi);
%         vardsp(s) = var(mdsp);
%         vardte(s) = var(mdte);
    end
    
%     figure; hold on
    
%     plot(sizes, vardsp, 'DisplayName', 'd');
%     plot(sizes, vardte, 'DisplayName', '\delta\theta');
    slope = flipud(sizes(end).*varibi(end)./sizes);
    plot(sizes, slope, 'Color', [0.2 0.2 0.2 0.5]);
    p{idT} = plot(sizes, varibi, 'DisplayName', ['T=' num2str(T(idT)) '°C'], 'Color', colors(idT, :));
    xlabel('Sample size n');
    ylabel(['Var(<\delta{t}>)_' num2str(n)]);
end
ax = gca;
ax.XScale = 'log';
ax.YScale = 'log';
a = sizes(1)*100;
p{end+1} = plot([NaN NaN], [NaN NaN], 'Color', [0.2 0.2 0.2 0.5], 'DisplayName', 'slope 1/n');
legend([p{:}]);