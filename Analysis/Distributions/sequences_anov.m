% Get trajectories and perform ANOVA test to show that each trajectories
% are specific.

% close all
clear

% --- Files
seqfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];

% --- Temperatures
T = [18, 22, 26, 30, 33];

% --- Parameters
ibi_cutoff = 20;

% --- Get data
D = load(seqfile);

% --- Processing
for idT = 1:numel(T)
    
    % Build sequences matrix
    ibi = D.interboutintervals{idT};
    ibi(ibi > ibi_cutoff) = NaN;
    
    dsp = D.displacements{idT};
    dsp(isnan(dsp)) = [];
    dsp = dsp(:);
    dte = D.dtheta{idT};
    dte(isnan(dte)) = [];
    dte = dte(:);
    
    M = [ibi./framerate, dsp.*pixsize, abs(dte)];
    
    corr(M)
end

a = D.interboutintervals{idT}./framerate;
b = D.displacements{idT}.*pixsize;
am = nanmean(a)'; bm = nanmean(b)';
O = [am, bm];