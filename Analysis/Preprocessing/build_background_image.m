img_dir = '/home/ljp/Science/Projects/ThermoMaster/Data/Samples_for_RC';

tic

mimg = buildBackgroundImage(img_dir);
imwrite(uint8(mimg), '/home/ljp/Science/Projects/ThermoMaster/Data/backgroundmatlab.pgm');

toc

function mean_image = buildBackgroundImage(img_dir)

warning('off', 'MATLAB:imagesci:readpnm:extraData');

imgs = dir([img_dir filesep '*pgm']);

mean_image = double(imread([imgs(1).folder filesep imgs(1).name]));
for idx = 2:numel(imgs)
    
    mean_image = mean_image + double(imread([imgs(idx).folder filesep imgs(idx).name]));
    
end

mean_image = mean_image./numel(imgs);
end