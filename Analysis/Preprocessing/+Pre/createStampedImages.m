function [] = createStampedImages(run_path)
% This function creates a set of image based on images found in run_path
% containing the temperatures and the timestamp in metadata.mat.

warning('off', 'MATLAB:imagesci:readpnm:extraData');

% Check input
% -----------
if ~strcmp(run_path(end), filesep)
    run_path = [run_path filesep];
end

% Options
% -------
extension = 'pgm';

% Prepare directories
% -------------------
img_dir = [run_path 'Images' filesep];
stp_dir = [run_path 'Stamps' filesep];

if ~exist(stp_dir, 'dir')
    mkdir(stp_dir);
end

% List images
% -----------
image_list = dir([img_dir '*.' extension]);
n_images = length(image_list);
input_name = @(n) [img_dir image_list(n).name];
output_name = @(n) [stp_dir image_list(n).name(1:end-3) 'tif'];

if n_images == 0
    error('No images found.');
end

% Load temperatures data and prepare their position in output image
% -----------------------------------------------------------------
img_ref = imread(input_name(1));
md = load([run_path 'metadata.mat']);
T_left = md.T_left;
T_right = md.T_right;
T_integer = unique(int8([T_left T_right]));
%n_T = numel(T_integer);
%cmap = jet(n_T);
time = md.time;
txt_position = [1 1 ; round(size(img_ref, 2)/2) 1 ; (size(img_ref, 2) - 82) 1];

% Create an empty image with temperatures and time stamps
% -------------------------------------------------------
for idx = 1:n_images
    
    if mod(idx, 100) == 0
        fprintf('Image #%i...\n', idx);
    end
    
    caption = zeros(33, size(img_ref, 2));
    T_l = int8(T_left(idx));
    T_r = int8(T_right(idx));
    if T_l < mean(T_integer)
        left_color = 'blue';
    else
        left_color = 'red';
    end
    
    if T_r < mean(T_integer)
        right_color = 'blue';
    else
        right_color = 'red';
    end

    %box_color = {cmap(T_integer == T_r, :), cmap(T_integer == T_l, :), [0 0 0]};
    box_color = {right_color, 'black', left_color};
    T_left_txt = [num2str(T_left(idx)) '°C'];
    T_right_txt = [num2str(T_right(idx)) '°C'];
    time_txt = [num2str(time(idx)) 's'];
    text_str = {T_right_txt ; time_txt ; T_left_txt};
    img_caption = insertText(caption, txt_position, text_str,'FontSize',18,'BoxColor',...
    box_color,'BoxOpacity',0.4,'TextColor','white');
    img = double(imread(input_name(idx)));
    img = img./max(max(img));
    img_txt = cat(1, cat(3, img, img, img), img_caption);
    imwrite(img_txt, output_name(idx))
end

warning('on', 'MATLAB:imagesci:readpnm:extraData');