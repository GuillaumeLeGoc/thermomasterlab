function [] = moveImages(run_path)
% ThermoMaster - This function moves all pgm files from run_path to
% run_path/Images


% Check input
% -----------
if ~strcmp(run_path(end), filesep)
    run_path = [run_path filesep];
end

% Parameters
% ----------
output_dir = [run_path 'Images' filesep];
extension = 'pgm';

if ~exist(output_dir, 'dir')
    mkdir(output_dir);
end

% List images
% -----------
image_list = dir([run_path '*.' extension]);
n_images = length(image_list);

input_name = @(n) [run_path image_list(n).name];
output_name = @(n) [output_dir image_list(n).name];
    
% Move all images
% ---------------
for idx_img  = 1:n_images
    oldname = input_name(idx_img);
    newname = output_name(idx_img);
    java.io.File(oldname).renameTo(java.io.File(newname));
end

end