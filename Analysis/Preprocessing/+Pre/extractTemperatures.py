# find the last line, change to a file you have
import time
import os
import re
start = time.time()

directory = "/home/ljp/Science/Projects/ThermoMaster/Data/2019-03-12/Run 05/Images/"
files = os.listdir(directory)
timestamp = []
templeft = []
tempright = []

# Pattern string : #Timestamp:163643384822064;TempLeft:29.96;TempRight:30.04
for f in files:

    with open( (directory + f), 'rb') as fid:
        l = list(fid)[-1].decode()
        r = re.search("#Timestamp:(.*);TempLeft", l)
        timestamp.append(r.group(1))
        r = re.search("TempLeft:(.*);TempRight", l)
        templeft.append(r.group(1))
        r = re.search("TempRight:(.*)", l)
        tempright.append(r.group(1))

stop = time.time()

print(stop-start)