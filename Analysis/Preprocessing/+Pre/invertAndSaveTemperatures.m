function [] = invertAndSaveTemperatures(run_path)
% This functions is the combination of 'readTemperatures' and
% 'invertImages'.

warning('off', 'MATLAB:imagesci:readpnm:extraData');

% Check input
% -----------
if ~strcmp(run_path(end), filesep)
    run_path = [run_path filesep];
end

% Parameters
% ----------
extension = 'pgm';
metadata_path = [run_path 'temperature_data.mat'];
output_dir = [run_path 'Inverted' filesep];

if ~exist(output_dir, 'dir')
    mkdir(output_dir);
end

% Find images in run_path
% -----------------------
image_list = dir([run_path 'Images' filesep '*.' extension]);
n_images = length(image_list);
input_name = @(n) [run_path 'Images' filesep image_list(n).name];
output_name = @(n) [output_dir image_list(n).name(1:end-3) 'tif'];

if n_images == 0
    error('No images found.');
end

% Initialisation
% --------------
timestamp = NaN(n_images, 1);
T_left = NaN(n_images, 1);
T_right = NaN(n_images, 1);

% Read images one by one
% ----------------------
for idx = 1:n_images
    
    if mod(idx, 100) == 0
        fprintf('Image #%i...\n', idx);
    end
    
    % Get metadata
    % ------------
    fid = fopen(input_name(idx), 'r');        % open file
    offset = 1;                             % initialise pointer offset from the end of file
    fseek(fid, -offset, 'eof');             % place pointer at the end of file
    last_char = fgetl(fid);                 % get the last character
    
    while ~strcmp(last_char, newline)       % read characters one by one until a newline if found
        offset = offset + 1;
        fseek(fid, -offset, 'eof');
        last_char = fread(fid, 1, '*char'); 
    end
    
    fseek(fid, -offset+1, 'eof');           % place pointer to the begining of the last line
    
    last_line = fgetl(fid);                 % read the last line in full
    
    fclose(fid);
    
    twopo_loc = strfind(last_line, ':') + 1;    % split last line in three
    comma_loc = strfind(last_line, ';') - 1;
    
    timestamp(idx) = str2double(last_line(twopo_loc(1):comma_loc(1)));
    T_left(idx) = str2double(last_line(twopo_loc(2):comma_loc(2)));
    T_right(idx) = str2double(last_line(twopo_loc(3):end));
    
    % Invert and save image
    % ---------------------
    img = imread(input_name(idx));
    img_inverted = imcomplement(img);
    imwrite(img_inverted, output_name(idx));
end

t = timestamp - timestamp(1);               % in nanoseconds
time = t.*1e-9;                             % convert to seconds

save(metadata_path, 'time', 'T_left', 'T_right');

end