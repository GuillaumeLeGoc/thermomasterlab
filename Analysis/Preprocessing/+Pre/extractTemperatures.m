function [] = extractTemperatures(run_path)
% Pre.extractTemperatures. This function reads pgm files from ThermoMaster
% setup and returns the time vector, the temperature at the left and right
% points of the pool.

warning('off', 'MATLAB:imagesci:readpnm:extraData');

% Check input
% -----------
if ~strcmp(run_path(end), filesep)
    run_path = [run_path filesep];
end

% Parameters
% ----------
extension = 'pgm';

% Find images in run_path
% -----------------------
image_list = dir([run_path 'Images' filesep '*.' extension]);
n_images = length(image_list);
make_name = @(n) [run_path 'Images' filesep image_list(n).name];

if n_images == 0
    error('No images found.');
end

% Initialisation
% --------------
timestamp = NaN(n_images, 1);
T_left = NaN(n_images, 1);
T_right = NaN(n_images, 1);

% Read images one by one
% ----------------------
for i = 1:n_images
    
    statusInfo(i, n_images, 50);
    
    switch computer
        
        case 'GLNXA64'
            fname = make_name(i);
            s = ['tail -n 1 "' fname '"'];      %  linux command to read the end of file without loading it
            [~, last_line] = unix(s);
            
        otherwise
            
            fid = fopen(make_name(i), 'r');     % open file
            offset = 1;                         % initialise pointer offset from the end of file
            fseek(fid, -offset, 'eof');         % place pointer at the end of file
            last_char = fgetl(fid);             % get the last character
            
            while ~strcmp(last_char, newline)   % read characters one by one until a newline if found
                offset = offset + 1;
                fseek(fid, -offset, 'eof');
                last_char = fread(fid, 1, '*char');
            end
            
            fseek(fid, -offset+1, 'eof');       % place pointer to the begining of the last line
            
            last_line = fgetl(fid);             % read the last line in full
            
            fclose(fid);
            
    end
    
    
    
    twopo_loc = strfind(last_line, ':') + 1;    % split last line in three
    comma_loc = strfind(last_line, ';') - 1;
    
    timestamp(i) = str2double(last_line(twopo_loc(1):comma_loc(1)));
    T_left(i) = str2double(last_line(twopo_loc(2):comma_loc(2)));
    T_right(i) = str2double(last_line(twopo_loc(3):end));
end
fprintf('\n');

t = timestamp - timestamp(1);           % in nanoseconds
t = t.*1e-9;                            % convert to seconds

% Save metadata
if ~exist([run_path 'stamps.mat'], 'file')
    save([run_path 'stamps.mat'], 't', 'T_right', 'T_left');
else
    copyfile([run_path 'stamps.mat'], [run_path 'stamps.bak']);
    save([run_path 'stamps.mat'], 't', 'T_right', 'T_left');
end

end