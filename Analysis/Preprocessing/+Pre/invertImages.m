function [] = invertImages(run_path)
% This function inverts images and saves them in run_path/Inverted.

warning('off', 'MATLAB:imagesci:readpnm:extraData');    % disable warnings for pgm reading

% Check input
% -----------
if ~strcmp(run_path(end), filesep)
    run_path = [run_path filesep];
end

% Parameters
% ----------
input_dir = [run_path 'Images' filesep];
output_dir = [run_path 'Inverted' filesep];
extension = 'pgm';

if ~exist(output_dir, 'dir')
    mkdir(output_dir);
end

% List images
% -----------
image_list = dir([input_dir '*.' extension]);
n_images = length(image_list);

input_name = @(n) [input_dir image_list(n).name];
output_name = @(n) [output_dir image_list(n).name(1:end-3) 'tif'];

% Image inversion
% ---------------
for idx = 1:n_images
    
    if mod(idx, 100) == 0
        fprintf('Image #%i...\n', idx);
    end
    
    img = imread(input_name(idx));
    img_inverted = imcomplement(img);
    imwrite(img_inverted, output_name(idx));
end
end