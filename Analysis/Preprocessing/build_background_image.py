import skimage
import pims
import time
import numpy as np

start = time.time()

img_dir = '/home/ljp/Science/Projects/ThermoMaster/Data/Samples_for_RC/*pgm'
seq = pims.ImageSequence(img_dir)

mimg = np.zeros(seq.frame_shape)

for img in seq:
    
    mimg = mimg + img.astype(float)

mimg = (mimg/len(seq))
skimage.io.imsave('/home/ljp/Science/Projects/ThermoMaster/Data/backgroundpython.pgm', mimg.astype('uint8', copy=False))
stop = time.time()

print(stop - start)