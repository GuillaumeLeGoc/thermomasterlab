% Move tracking directory.

cwd = strcat(pwd, filesep, 'Data', filesep);

dat = '2020-12-04';
run = 1;

input_dir = strcat(cwd, dat, filesep, "Run ", sprintf('%02i', run), filesep, ...
    "Images", filesep, "Tracking_Result");

if exist(input_dir, 'dir')
    
    output_dir = strcat(cwd, dat, filesep, "Run ", sprintf('%02i', run), filesep);
    
%     if ~exist(strcat(output_dir, "Tracking_Result"), 'dir')
        unix(strcat('cp -r "', input_dir, '" "', output_dir, '"'));
%     else
%         warning('Destination already exists, exiting.');
%     end
else 
    warning('Target directory doesn''t exist, exiting.');
end