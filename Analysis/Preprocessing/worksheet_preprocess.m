% Worksheet to use ThermoMaster functions.
clear

base = '/home/ljp/Science/Projects/ThermoMaster/Data/';

dat = '2020-12-04';
n_run = 2;

path_to_run = [base dat filesep 'Run ' sprintf('%02i', n_run) filesep];

move_image = 'y';
extract_temp = 'y';
invert_image = 'n';
create_temp_stamp = 'n';

% Move images
switch move_image
    case 'y'
        tic
        Pre.moveImages(path_to_run);
        toc
end

% Invert and/or save temperature
if strcmp(invert_image, 'y') && strcmp(extract_temp, 'y')
    tic
    Pre.invertAndSaveTemperatures(path_to_run);
    toc
else
    % Invert Images
    switch invert_image
        case 'y'
            tic
            Pre.invertImages(path_to_run);
            toc
    end
    
    % Save Temperatures
    switch extract_temp
        case 'y'
            fprintf('Extracting timestamps...\n');
            tic
            Pre.extractTemperatures(path_to_run);
            toc
    end
end

% Create temperature stamps
switch create_temp_stamp
    case 'y'
        tic
        Pre.createStampedImages(path_to_run);
        toc
end