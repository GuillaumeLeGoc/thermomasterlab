% Analyze gradient data : look at the effect of the gradient on next bout
% parameters.

clear
clc

% --- Parameters
% * Definitions
Tgradient = [18, 33];
Tbaseline = 22;
duration_gradient = 40*60;  % seconds
duration_baseline = 5*60;   % seconds
settletime = 5*60;          % time after gradient to establish it (seconds)s

Tref1 = 22;                 % sign deltaT with respect to this
Tref2 = 26;

% Thresholds
maxibi = 6;     % s
maxdsp = 5;     % mm
maxreo = 180;   % deg
turnthresh = 10;    % deg, threshold defining turn/forward

% Figures
nicered = [195, 49, 36]./255;
niceblk = [11, 23, 40]./255;

% Bins
nbinsX = 10;
nbinsdX = 10;
nbinsT = 50;
nbinsdT = 50;
bl_nbins = 20;
gr_nbins = 100;

% * Pool
pixsize = 0.08;
offset = 30;     % beginning of the pool (pixels)
xl = 0;             % mm
xr = 100;           % mm

% * Filters
filt.seq_length = 5;	% sequence length for each fish in seconds
filt.n_bouts = 5;       % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Preparation
% Get experiments list (18 left, 33 right)
[tmptracks1, tmpstamps1] = getGradientList(Tgradient(1), Tgradient(2), Tbaseline);
% reverse
[tmptracks2, tmpstamps2] = getGradientList(Tgradient(2), Tgradient(1), Tbaseline);

% Put into a cell, first element is 18-33, the second is 33-18
alltracks = {tmptracks1; tmptracks2};
allstamps = {tmpstamps1; tmpstamps2};

% Init.
bl_acurrenX = cell(numel(alltracks), 1);
bl_aprevidX = cell(numel(alltracks), 1);
bl_aprevidXs = cell(numel(alltracks), 1);
bl_acurribi = cell(numel(alltracks), 1);
bl_acurrdsp = cell(numel(alltracks), 1);
bl_acurrreo = cell(numel(alltracks), 1);
gr_acurrenT = cell(numel(alltracks), 1);
gr_aprevidT = cell(numel(alltracks), 1);
gr_aprevidTs = cell(numel(alltracks), 1);
gr_acurribi = cell(numel(alltracks), 1);
gr_acurrdsp = cell(numel(alltracks), 1);
gr_acurrreo = cell(numel(alltracks), 1);

% --- Gather data
% idorder = 1 : 18-33, idorder = 2 : 33-18
for idorder = 1:numel(alltracks)
    
    ftrack = alltracks{idorder};
    fstamp = allstamps{idorder};
    
    % Handles to convert position to temperature
    if idorder == 1
        Tl = Tgradient(1);
        Tr = Tgradient(2);
    elseif idorder == 2
        Tl = Tgradient(2);
        Tr = Tgradient(1);
    end
    
    fprintf('Processing %i-%i gradient', Tl, Tr); tic
    
    a = (Tl - Tr)./(xl - xr);
    b = (Tr*xl - Tl*xr)./(xl - xr);
    hT = @(x) a.*x + b;
    
    % Init.
    bl_pcurrenX = cell(numel(ftrack), 1);
    bl_pprevidX = cell(numel(ftrack), 1);
    bl_pprevidXs = cell(numel(ftrack), 1);
    bl_pcurribi = cell(numel(ftrack), 1);
    bl_pcurrdsp = cell(numel(ftrack), 1);
    bl_pcurrreo = cell(numel(ftrack), 1);
    gr_pcurrenT = cell(numel(ftrack), 1);
    gr_pprevidT = cell(numel(ftrack), 1);
    gr_pprevidTs = cell(numel(ftrack), 1);
    gr_pcurribi = cell(numel(ftrack), 1);
    gr_pcurrdsp = cell(numel(ftrack), 1);
    gr_pcurrreo = cell(numel(ftrack), 1);
    
    % Loop over experiments
    for idexp = 1:numel(ftrack)
        
        fprintf('.');
        
        % Read tracks and get positions
        [xpos, ibi, dsp, reo, frames, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        ntraj = size(xpos, 2);
        
        % Init.
        bl_currenX = cell(ntraj, 1);
        bl_previdX = cell(ntraj, 1);
        bl_previdXs = cell(ntraj, 1);
        bl_curribi = cell(ntraj, 1);
        bl_currdsp = cell(ntraj, 1);
        bl_currreo = cell(ntraj, 1);
        gr_currenT = cell(ntraj, 1);
        gr_previdT = cell(ntraj, 1);
        gr_previdTs = cell(ntraj, 1);
        gr_curribi = cell(ntraj, 1);
        gr_currdsp = cell(ntraj, 1);
        gr_currreo = cell(ntraj, 1);
        
        for s = 1:ntraj
            
            f = frames(:, s);
            x = xpos(:, s) - offset;
            i = ibi(:, s);
            d = dsp(:, s);
            r = reo(:, s);
            
            % Find the trajectory start and stop
            start = find(~isnan(f), 1, 'first');
            stop = find(~isnan(f), 1, 'last');
            
            % Extract values
            x(isnan(x)) = [];
            i(isnan(i)) = [];
            d(isnan(d)) = [];
            r(isnan(r)) = [];
            
            % Convert x to temperature
            X = x.*pixsize;
            T = hT(X);
            
            % Match previous felt delta T and current bout
            dT = diff(T);
            dX = diff(X);
            dT = dT(1:end - 1);
            dX = dX(1:end - 1);
            
            T = T(2:end - 1);
            X = X(2:end - 1);
            
            % Signed dT with respect to Tref
            %             dTs = -dT.*sign((T - Tref));
            dTs = dT;
            dTs(T > Tref2) = -dTs(T > Tref2);
            
            dXs = dX;
            dXs(T > Tref2) = -dXs(T > Tref2);
%             dXs(T > Tref1 & T < Tref2) = abs(dXs(T > Tref1 & T < Tref2));
            
            i = i(1:end - 1);
            d = d(2:end);
            r = r(2:end);
            
            % Convert to physical
            i = i./framerate;
            d = d.*pixsize;
            r = abs(r);
            
            % Store in baseline or in gradient
            if f(start)/framerate > duration_baseline + settletime
                % Gradient
                gr_currenT{s} = T;
                gr_previdT{s} = dT;
                gr_previdTs{s} = dTs;
                gr_curribi{s} = i;
                gr_currdsp{s} = d;
                gr_currreo{s} = r;
            elseif f(stop)/framerate < duration_baseline
                % Baseline
                bl_currenX{s} = X;
                bl_previdX{s} = dX;
                bl_previdXs{s} = dXs;
                bl_curribi{s} = i;
                bl_currdsp{s} = d;
                bl_currreo{s} = r;
            end
        end
        
        % Cleanup
        bl_currenX(cellfun(@isempty, bl_currenX)) = [];
        bl_previdX(cellfun(@isempty, bl_previdX)) = [];
        bl_previdXs(cellfun(@isempty, bl_previdXs)) = [];
        bl_curribi(cellfun(@isempty, bl_curribi)) = [];
        bl_currdsp(cellfun(@isempty, bl_currdsp)) = [];
        bl_currreo(cellfun(@isempty, bl_currreo)) = [];
        gr_currenT(cellfun(@isempty, gr_currenT)) = [];
        gr_previdT(cellfun(@isempty, gr_previdT)) = [];
        gr_previdTs(cellfun(@isempty, gr_previdTs)) = [];
        gr_curribi(cellfun(@isempty, gr_curribi)) = [];
        gr_currdsp(cellfun(@isempty, gr_currdsp)) = [];
        gr_currreo(cellfun(@isempty, gr_currreo)) = [];
        
        % Pool
        bl_pcurrenX{idexp} = cat(1, bl_currenX{:});
        bl_pprevidX{idexp} = cat(1, bl_previdX{:});
        bl_pprevidXs{idexp} = cat(1, bl_previdXs{:});
        bl_pcurribi{idexp} = cat(1, bl_curribi{:});
        bl_pcurrdsp{idexp} = cat(1, bl_currdsp{:});
        bl_pcurrreo{idexp} = cat(1, bl_currreo{:});
        gr_pcurrenT{idexp} = cat(1, gr_currenT{:});
        gr_pprevidT{idexp} = cat(1, gr_previdT{:});
        gr_pprevidTs{idexp} = cat(1, gr_previdTs{:});
        gr_pcurribi{idexp} = cat(1, gr_curribi{:});
        gr_pcurrdsp{idexp} = cat(1, gr_currdsp{:});
        gr_pcurrreo{idexp} = cat(1, gr_currreo{:});
        
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % Pool
    bl_acurrenX{idorder} = cat(1, bl_pcurrenX{:});
    bl_aprevidX{idorder} = cat(1, bl_pprevidX{:});
    bl_aprevidXs{idorder} = cat(1, bl_pprevidXs{:});
    bl_acurribi{idorder} = cat(1, bl_pcurribi{:});
    bl_acurrdsp{idorder} = cat(1, bl_pcurrdsp{:});
    bl_acurrreo{idorder} = cat(1, bl_pcurrreo{:});
    gr_acurrenT{idorder} = cat(1, gr_pcurrenT{:});
    gr_aprevidT{idorder} = cat(1, gr_pprevidT{:});
    gr_aprevidTs{idorder} = cat(1, gr_pprevidTs{:});
    gr_acurribi{idorder} = cat(1, gr_pcurribi{:});
    gr_acurrdsp{idorder} = cat(1, gr_pcurrdsp{:});
    gr_acurrreo{idorder} = cat(1, gr_pcurrreo{:});
    
end

% --- Pool all results
bl_acurrenX = cat(1, bl_acurrenX{:});
bl_aprevidX = cat(1, bl_aprevidX{:});
bl_aprevidXs = cat(1, bl_aprevidXs{:});
bl_acurribi = cat(1, bl_acurribi{:});
bl_acurrdsp = cat(1, bl_acurrdsp{:});
bl_acurrreo = cat(1, bl_acurrreo{:});
gr_acurrenT = cat(1, gr_acurrenT{:});
gr_aprevidT = cat(1, gr_aprevidT{:});
gr_aprevidTs = cat(1, gr_aprevidTs{:});
gr_acurribi = cat(1, gr_acurribi{:});
gr_acurrdsp = cat(1, gr_acurrdsp{:});
gr_acurrreo = cat(1, gr_acurrreo{:});

% Cleanup
blrmibi = bl_acurribi > maxibi;
blrmdsp = bl_acurrdsp > maxdsp;
blrmreo = bl_acurrreo > maxreo;
blrmdx = abs(bl_aprevidX) > maxdsp;
bltorm = ((blrmibi | blrmdsp) | blrmreo) | blrmdx;
bl_acurrenX(bltorm) = [];
bl_aprevidX(bltorm) = [];
bl_aprevidXs(bltorm) = [];
bl_acurribi(bltorm) = [];
bl_acurrdsp(bltorm) = [];
bl_acurrreo(bltorm) = [];
grrmibi = gr_acurribi > maxibi;
grrmdsp = gr_acurrdsp > maxdsp;
grrmreo = gr_acurrreo > maxreo;
grrmdT = abs(gr_aprevidT) > abs(a).*maxdsp;
grtorm = ((grrmibi | grrmdsp) | grrmreo) | grrmdT;
gr_acurrenT(grtorm) = [];
gr_aprevidT(grtorm) = [];
gr_aprevidTs(grtorm) = [];
gr_acurribi(grtorm) = [];
gr_acurrdsp(grtorm) = [];
gr_acurrreo(grtorm) = [];

bl_allX = [bl_acurrenX, bl_aprevidX, bl_aprevidXs];
bl_allY = [bl_acurribi, bl_acurrdsp, bl_acurrreo];
gr_allX = [gr_acurrenT, gr_aprevidT, gr_aprevidTs];
gr_allY = [gr_acurribi, gr_acurrdsp, gr_acurrreo];

% --- Analysis
% Init.
bl_X1 = bl_allX(:, 1);
bl_X2 = bl_allX(:, 2);
bl_X3 = bl_allX(:, 3);
gr_X1 = gr_allX(:, 1);
gr_X2 = gr_allX(:, 2);
gr_X3 = gr_allX(:, 3);

% Cosmetics
yn = {'\delta{t}_i (s)', 'd_i (mm)', '\delta\theta_i (deg)'};
vn = {'dt', 'd', 'dtheta'};
Plims = [0.7, 1.1 ; 1, 2.2 ; 10, 35];
Dlims = [0.75, 1 ; 0.8, 3 ; 10, 25];
limscol = [0, 1.5 ; 0, 4 ; 0, 35];

dTxlbls = '\Delta{T}_{i-1} signed (°C)';
dTxlbl = '\Delta{T}_{i-1} (°C)';

% Bins for T and delta T for surface plot
gr_binsX1 = linspace(min(gr_X1), max(gr_X1), nbinsT + 1);
gr_binsX2 = linspace(min(gr_X2), max(gr_X2), nbinsdT + 1);
bl_binsX1 = linspace(min(bl_X1), max(bl_X1), nbinsX + 1);
bl_binsX2 = linspace(min(bl_X2), max(bl_X2), nbinsdX + 1);
grcX1 = edges2centers(gr_binsX1);
grcX2 = edges2centers(gr_binsX2);
blcX1 = edges2centers(bl_binsX1);
blcX2 = edges2centers(bl_binsX2);

% For smoothing
gr_intX1 = linspace(gr_binsX1(1), gr_binsX1(end), 1000);
gr_intX2 = linspace(gr_binsX2(2), gr_binsX2(end), 1000);
[gr_XX1, gr_XX2] = meshgrid(gr_intX1, gr_intX2);
bl_intX1 = linspace(bl_binsX1(1), bl_binsX1(end), 1000);
bl_intX2 = linspace(bl_binsX2(2), bl_binsX2(end), 1000);
[bl_XX1, bl_XX2] = meshgrid(bl_intX1, bl_intX2);

% Prepare options for plots
lobl = struct;
lobl.Color = niceblk;
lobl.Marker = 'none';
lobl.LineWidth = 2;
lobl.DisplayName = 'Baseline';
shadopt = struct;
shadopt.FaceAlpha = 0.275;
logr = lobl;
logr.Color = nicered;
logr.DisplayName = 'Gradient';

for idd = 1:size(gr_allY, 2)
    
    blY = bl_allY(:, idd);
    grY = gr_allY(:, idd);
    
    % * X/T
    
    % Compute mean & std in bins equally populated
    [blmeanY, blsemY, cX] = binMeanEqualElements(bl_X1, blY, bl_nbins, [], @mean);
    [grmeanY, grsemY, cT] = binMeanEqualElements(gr_X1, grY, gr_nbins, [], @mean);
    
    % Display
    [axbl, axgr] = make2axes;
    ebl = errorshaded(cX, blmeanY, blsemY, ...
        'line', lobl, 'patch', shadopt, 'ax', axbl);
    xlim(axbl, [0, 100]);
    ylim(axbl, Plims(idd, :));
    xlabel(axbl, 'X_{i} (mm)');
    ylabel(axbl, yn{idd});
    axbl.XAxis.Color = niceblk;
    axbl.YAxis.Color = niceblk;
    
    egr = errorshaded(cT, grmeanY, grsemY, ...
        'line', logr, 'patch', shadopt, 'ax', axgr);
    xlim(axgr, [18, 33]);
    ylim(axgr, Plims(idd, :));
    xlabel(axgr, 'T_{i} (°C)');
    ylabel(axgr, yn{idd});
    axgr.XAxis.Color = nicered;
    axgr.YAxis.Color = nicered;
    axgr.XTick = [18, 22, 26, 30, 33];
    
    legend(axbl, ebl, 'Location', 'northwest');
    legend(axgr, egr, 'Location', 'northeast');
    
    % * deltaX/deltaT
    % Compute mean & std in bins equally populated
    [blmeanY, blsemY, cdX] = binMeanEqualElements(bl_X2, blY, bl_nbins, [], @mean);
    [grmeanY, grsemY, cdT] = binMeanEqualElements(gr_X2, grY, gr_nbins, [], @mean);
    
    % Display
    [axbl, axgr] = make2axes;
    ebl = errorshaded(cdX, blmeanY, blsemY, ...
        'line', lobl, 'patch', shadopt, 'ax', axbl);
    xlim(axbl, [-4, 4]);
    ylim(axbl, Dlims(idd, :));
    xlabel(axbl, '\Delta{X}_{i-1} (mm)');
    ylabel(axbl, yn{idd});
    axbl.XAxis.Color = niceblk;
    axbl.YAxis.Color = niceblk;
    
    egr = errorshaded(cdT, grmeanY, grsemY, ...
        'line', logr, 'patch', shadopt, 'ax', axgr);
    xlim(axgr, [-abs(a)*4, abs(a)*4]);
    ylim(axgr, Dlims(idd, :));
    xlabel(axgr, dTxlbl);
    ylabel(axgr, yn{idd});
    axgr.XAxis.Color = nicered;
    axgr.YAxis.Color = nicered;
    
    legend(axbl, ebl, 'Location', 'northwest');
    legend(axgr, egr, 'Location', 'northeast');
    
    % * deltaX/deltaT signed
    % Compute mean & std in bins equally populated
    [blmeanY, blsemY, cdX] = binMeanEqualElements(bl_X3, blY, bl_nbins, [], @mean);
    [grmeanY, grsemY, cdT] = binMeanEqualElements(gr_X3, grY, gr_nbins, [], @mean);
    
    % Display
    [axbl, axgr] = make2axes;
    ebl = errorshaded(cdX, blmeanY, blsemY, ...
        'line', lobl, 'patch', shadopt, 'ax', axbl);
    xlim(axbl, [-4, 4]);
    ylim(axbl, Dlims(idd, :));
    xlabel(axbl, '\Delta{X}_{i-1} signed (mm)');
    ylabel(axbl, yn{idd});
    axbl.XAxis.Color = niceblk;
    axbl.YAxis.Color = niceblk;
    
    egr = errorshaded(cdT, grmeanY, grsemY, ...
        'line', logr, 'patch', shadopt, 'ax', axgr);
    xlim(axgr, [-abs(a)*4, abs(a)*4]);
    ylim(axgr, Dlims(idd, :));
    xlabel(axgr, dTxlbls);
    ylabel(axgr, yn{idd});
    axgr.XAxis.Color = nicered;
    axgr.YAxis.Color = nicered;
    
    legend(axbl, ebl, 'Location', 'northwest');
    legend(axgr, egr, 'Location', 'northeast');
    
    %     % * Relative change
    %     blX = bl_X2./bl_X1;
    %     grX = gr_X2./gr_X1;
    %
    %     % Compute mean & std in bins
    %     [blmeanY, blsemY, blcrel] = binMeanEqualElements(blX, blY, bl_nbins, [], @mean);
    %     [grmeanY, grsemY, grcrel] = binMeanEqualElements(grX, grY, gr_nbins, [], @mean);
    %
    %     % Display
    %     figure;
    %     subplot(2, 1, 1);
    %     errorshaded(blcrel, blmeanY, blsemY, ...
    %         'line', lineopt, 'patch', shadopt);
    %     xlabel('\Delta{X}_{i-1}/X_{i}');
    %     ylabel(yn{idd});
    %     grid off
    %     title('Baseline');
    %
    %     subplot(2, 1, 2);
    %     errorshaded(grcrel, grmeanY, grsemY, ...
    %         'line', lineopt, 'patch', shadopt);
    %     xlabel('\Delta{T}_{i-1}/T_{i}');
    %     ylabel(yn{idd});
    %     grid off
    %     title('Gradient');
    
    % * Surface plot
    % Baseline
    Y2D = bin2XY(bl_X1, bl_X2, blY, bl_binsX1, bl_binsX2, @mean);
    smoothY2D = interp2(blcX1, blcX2, Y2D, bl_XX1, bl_XX2);
    
    figure;
    imagesc(bl_intX1, bl_intX2, smoothY2D);
    c = colorbar;
    title(c, yn{idd});
    caxis(limscol(idd, :));
    xlabel('X (mm)');
    ylabel('\Delta{X} (mm)');
    view(2);
    title('Baseline');
    axis square
    xlim([0, 100]);
    ylim([-4, 4]);
    grid off
    colormap(inferno)
    
    % Gradient
    Y2D = bin2XY(gr_X1, gr_X2, grY, gr_binsX1, gr_binsX2);
    smoothY2D = interp2(grcX1, grcX2, Y2D, gr_XX1, gr_XX2);
    
    figure;
    imagesc(gr_intX1, gr_intX2, smoothY2D);
    c = colorbar;
    title(c, yn{idd});
    caxis(limscol(idd, :));
    xlabel('T (°C)');
    ylabel('\Delta{T} (°C)');
    view(2);
    title('Gradient');
    axis square
    ax = gca;
    ax.XTick = [18, 22, 26, 30, 33];
    xlim([18, 33]);
    ylim([-abs(a)*4, abs(a)*4]);
    grid off
    colormap(inferno)
    
    %     % * Regression
    %     tbl = table(X1, X2, Y, 'VariableNames', {'T', 'dT', vn{idd}});
    %     mdl = fitlm(tbl);
    %     disp(mdl);
end

% pturn

fun = @(x) sum(x > turnthresh)./numel(x);

% Compute mean & std in bins equally populated
[blptrn, ~, cX] = binMeanEqualElements(bl_X1, bl_acurrreo, bl_nbins, [], fun);
[grptrn, ~, cT] = binMeanEqualElements(gr_X1, gr_acurrreo, gr_nbins, [], fun);

% Display
[axbl, axgr] = make2axes;
p = plot(axbl, cX, blptrn);
p.LineWidth = 2;
p.Color = niceblk;
p.DisplayName = 'Baseline';
xlim(axbl, [0, 100]);
ylim(axbl, [0, 1]);
xlabel(axbl, 'X_{i} (mm)');
ylabel(axbl, 'p_{turn}');
axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;

q = plot(axgr, cT, grptrn);
q.LineWidth = 2;
q.Color = nicered;
q.DisplayName = 'Gradient';
xlim(axgr, [18, 33]);
ylim(axgr, [0, 1]);
xlabel(axgr, 'T_{i} (°C)');
ylabel(axgr, 'p_{turn}');
axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;

legend(axbl, p, 'Location', 'northwest');
legend(axgr, q, 'Location', 'northeast');

% * deltaX/deltaT
% Compute mean & std in bins equally populated
[blptrn, ~, cdX] = binMeanEqualElements(bl_X2, bl_acurrreo, bl_nbins, [], fun);
[grptrn, ~, cdT] = binMeanEqualElements(gr_X2, gr_acurrreo, gr_nbins, [], fun);

% Display
[axbl, axgr] = make2axes;
p = plot(axbl, cdX, blptrn);
p.LineWidth = 2;
p.Color = niceblk;
p.DisplayName = 'Baseline';
xlim(axbl, [-4, 4]);
ylim(axbl, [0, 1]);
xlabel(axbl, '\Delta{X}_{i-1} (mm)');
ylabel(axbl, 'p_{turn}');
axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;

q = plot(axgr, cdT, grptrn);
q.LineWidth = 2;
q.Color = nicered;
q.DisplayName = 'Gradient';
xlim(axgr, [-abs(a)*4, abs(a)*4]);
ylim(axgr, [0, 1]);
xlabel(axgr, dTxlbl);
ylabel(axgr, 'p_{turn}');
axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;

legend(axbl, p, 'Location', 'northwest');
legend(axgr, q, 'Location', 'northeast');

% * deltaX/deltaT signed
% Compute mean & std in bins equally populated
[blptrn, ~, cdX] = binMeanEqualElements(bl_X3, bl_acurrreo, bl_nbins, [], fun);
[grptrn, ~, cdT] = binMeanEqualElements(gr_X3, gr_acurrreo, gr_nbins, [], fun);

% Display
[axbl, axgr] = make2axes;
p = plot(axbl, cdX, blptrn);
p.LineWidth = 2;
p.Color = niceblk;
p.DisplayName = 'Baseline';
xlim(axbl, [-4, 4]);
ylim(axbl, [0, 1]);
xlabel(axbl, '\Delta{X}_{i-1} signed (mm)');
ylabel(axbl, 'p_{turn}');
axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;

q = plot(axgr, cdT, grptrn);
q.LineWidth = 2;
q.Color = nicered;
q.DisplayName = 'Gradient';
xlim(axgr, [-abs(a)*4, abs(a)*4]);
ylim(axgr, [0, 1]);
xlabel(axgr, dTxlbls);
ylabel(axgr, 'p_{turn}');
axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;

legend(axbl, p, 'Location', 'northwest');
legend(axgr, q, 'Location', 'northeast');

% * Surface plot
% Baseline
Y2D = bin2XY(bl_X1, bl_X2, bl_acurrreo, bl_binsX1, bl_binsX2, fun);
smoothY2D = interp2(blcX1, blcX2, Y2D, bl_XX1, bl_XX2);

figure;
imagesc(bl_intX1, bl_intX2, smoothY2D);
c = colorbar;
title(c, 'p_{turn}');
caxis([0, 1]);
xlabel('X (mm)');
ylabel('\Delta{X} (mm)');
view(2);
title('Baseline');
axis square
xlim([0, 100]);
ylim([-4, 4]);
grid off
colormap(inferno)

% Gradient
Y2D = bin2XY(gr_X1, gr_X2, gr_acurrreo, gr_binsX1, gr_binsX2, fun);
smoothY2D = interp2(grcX1, grcX2, Y2D, gr_XX1, gr_XX2);

figure;
imagesc(gr_intX1, gr_intX2, smoothY2D);
c = colorbar;
title(c, 'p_{turn}');
caxis([0, 1]);
xlabel('T (°C)');
ylabel('\Delta{T} (°C)');
view(2);
title('Gradient');
axis square
ax = gca;
ax.XTick = [18, 22, 26, 30, 33];
xlim([18, 33]);
ylim([-abs(a)*4, abs(a)*4]);
grid off
colormap(inferno)