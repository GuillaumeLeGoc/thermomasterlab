% Analyze gradient experiments : presence heatmap, mean over time

% close all
clear
clc

% --- Parameters
% * Definitions
Tgradient = [18, 33];
Tbaseline = 22;
duration_gradient = 40*60;  % seconds
duration_baseline = 5*60;   % seconds
settletime = 5*60;            % time after gradient to establish it (seconds)

% * Bins
nbinpos = 50;          % number of bins
stationp = 10;         % steady-state percentage from end
startsteady = duration_baseline + duration_gradient - stationp/100*duration_gradient;
Tbins = linspace(Tgradient(1), Tgradient(2), nbinpos + 1);
xbins = linspace(0, 100, nbinpos + 1);

% * Figures
nicered = [195, 49, 36]./255;
niceblk = [11, 23, 40]./255;

% * Pool
offset = 30;
pixsize = 0.08;
xl = 0;    % mm
xr = xl + 100;           % mm

% * Filters for trajectories
filt.seq_length = 5;	% sequence length for each fish in seconds
filt.n_bouts = 5;      % minimum average bout frequency or number of bouts
filt.minx = 0;
filt.maxx = Inf;

% --- Preparation
% Get experiments list (18 left, 33 right)
[tmptracks1, tmpstamps1] = getGradientList(Tgradient(1), Tgradient(2), Tbaseline);
% reverse
[tmptracks2, tmpstamps2] = getGradientList(Tgradient(2), Tgradient(1), Tbaseline);

% Put into a cell, first element is 18-33, the second is 33-18
alltracks = {tmptracks1; tmptracks2};
allstamps = {tmpstamps1; tmpstamps2};

% Init array
abl_x = cell(numel(alltracks), 1);
agr_x = cell(numel(alltracks), 1);
bl_ppdf = cell(numel(alltracks), 1);
gr_ppdfstat = cell(numel(alltracks), 1);

% --- Gather data
% idorder = 1 : 18-33, idorder = 2 : 33-18
for idorder = 1:numel(alltracks)
    
    ftrack = alltracks{idorder};
    fstamp = allstamps{idorder};
    
    % Handles to convert position to temperature
    if idorder == 1
        Tl = Tgradient(1);
        Tr = Tgradient(2);
    elseif idorder == 2
        Tl = Tgradient(2);
        Tr = Tgradient(1);
    end
    
    a = (Tl - Tr)./(xl - xr);
    b = (Tr*xl - Tl*xr)./(xl - xr);
    hT = @(x) a.*x + b;
    
    fprintf('Processing %i-%i gradient', Tl, Tr); tic
    
    % Init arrays
    pbl_x = cell(numel(ftrack), 1);
    pgr_x = cell(numel(ftrack), 1);
    bl_pdf = NaN(nbinpos, numel(ftrack));
    gr_pdfstat = NaN(nbinpos, numel(ftrack));
    
    % Loop over experiments
    for idexp = 1:numel(ftrack)
        
        fprintf('.');
        
        % Read tracks and get positions
        [xpos, ~, ~, ~, frames, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        ntraj = size(xpos, 2);
        
        bl_sexptemp = cell(ntraj, 1);
        gr_sexptemp = cell(ntraj, 1);
        for s = 1:ntraj
            
            f = frames(:, s);
            x = xpos(:, s) - offset;
            
            % Find the trajectory start and stop
            start = find(~isnan(f), 1, 'first');
            stop = find(~isnan(f), 1, 'last');
            
            % Limit trajectory to the trajectory itself
            ffilled = f(start:stop);
            xfilled = x(start:stop);
            
            % Fill NaNs
            ffilled = fillmissing(ffilled, 'linear');
            xfilled = fillmissing(xfilled, 'previous');
            xfilled = fillmissing(xfilled, 'next');
            
            % Bound values and convert to mm
            xfilled = xfilled.*pixsize;
            xfilled(xfilled < xl) = xl;
            xfilled(xfilled > xr) = xr;
            
            % Discriminate baseline of gradient conditions
            if f(start)/framerate > duration_baseline + settletime
                % Gradient
                
                % Convert to temperature
                temp = hT(xfilled);
                
                % Get steady state times
                time = ffilled./framerate;
                steadstart = find(time >= startsteady, 1);
                gr_sexptemp{s} = temp(steadstart:end);
                
            elseif f(stop)/framerate < duration_baseline
                % Baseline
                
                bl_sexptemp{s} = xfilled;
                
            end
        end
                
        % Get steady-state pdf
        gr_sexptemp(cellfun(@isempty, gr_sexptemp)) = [];
        datastation = cat(1, gr_sexptemp{:});
        [gr_pdfstat(:, idexp), gr_cbinstat] = computePDF(Tbins, datastation, 'mode', 'edges', 'method', 'kde');
        
        % Get baseline pdf
        bl_sexptemp(cellfun(@isempty, bl_sexptemp)) = [];
        databline = cat(1, bl_sexptemp{:});
        [bl_pdf(:, idexp), bl_cbins] = computePDF(xbins, databline, 'mode', 'edges', 'method', 'kde');
        
        % Store
        pbl_x{idexp} = databline;
        pgr_x{idexp} = datastation;
        
        figure;
        plot(bl_cbins, bl_pdf(:,idexp));
        hold on
        histogram(databline, xbins, 'Normalization', 'pdf', 'EdgeColor', 'none');
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % Store
    abl_x{idorder} = cat(1, pbl_x{:});
    agr_x{idorder} = cat(1, pgr_x{:});
    bl_ppdf{idorder} = bl_pdf;
    gr_ppdfstat{idorder} = gr_pdfstat;
end

% --- Get mean pdf and sem
bl_ppdf = cat(2, bl_ppdf{:});
% bl_mpdf = mean(bl_ppdf, 2);
bl_mpdf = median(bl_ppdf, 2);
bl_epdf = std(bl_ppdf, [], 2)./sqrt(size(bl_ppdf, 2));

gr_ppdfstat = cat(2, gr_ppdfstat{:});
gr_mpdf = mean(gr_ppdfstat, 2);
gr_epdf = std(gr_ppdfstat, [], 2)./sqrt(size(gr_ppdfstat, 2));

% --- Get cdf
abl_x = cat(1, abl_x{:});
agr_x = cat(1, agr_x{:});

[blcdf, xbl, bll, blu] = ecdf(abl_x);
[grcdf, xgr, grl, gru] = ecdf(agr_x);

% --- Display
Tl = Tgradient(1);
Tr = Tgradient(2);

lineopt = struct;
lineopt.Color = niceblk;
lineopt.Marker = 'none';
% lineopt.MarkerSize = 4;
lineopt.LineWidth = 2;
lineopt.DisplayName = 'Baseline';
shadopt = struct;
shadopt.FaceAlpha = 0.275;
shadopt.DisplayName = 's.e.m.';

% PDF
figure;

% Baseline
axbl = axes; hold(axbl, 'on');
grid(axbl, 'off');

[eb, pb] = errorshaded(bl_cbins, bl_mpdf, bl_epdf, 'line', lineopt, 'patch', shadopt);
pub = plot(axbl, [xl, xr], [1/(xr - xl), 1/(xr - xl)], '--', ...
    'DisplayName', 'Uniform');
pub.Color = niceblk;
pub.LineWidth = 1.25;

convfac = (1/100)*15;

xlim(axbl, [0, 100]);
ylim(axbl, [0, 0.15*convfac]);
xlabel(axbl, 'position (mm)');
ylabel(axbl, '<pdf>_{batch} (baseline)');
axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;
axis(axbl, 'square');

% Gradient
axgr = axes; hold(axgr, 'on');
axgr.Position = axbl.Position;
axgr.XAxisLocation = 'top';
axgr.YAxisLocation = 'right';
axgr.Color = 'none';
axis(axgr, 'square');
grid(axgr, 'off');

lineopt.Color = nicered;
lineopt.DisplayName = ['Gradient, last ' num2str(stationp) '%'];

[eg, pg] = errorshaded(gr_cbinstat, gr_mpdf, gr_epdf, 'line', lineopt, 'patch', shadopt);
% pug = plot([Tl, Tr], [1/(Tr - Tl), 1/(Tr - Tl)], '--', ...
%     'DisplayName', 'Uniform gradient');
pug.Color = nicered;
pug.LineWidth = 1.25;

xlim(axgr, [18, 33]);
axgr.XTick = [18, 22, 26, 30, 33];
ylim(axgr, [0, 0.15]);
xlabel(axgr, 'temperature (°C)');
ylabel(axgr, ['<pdf>_{batch} (last ' num2str(stationp) '% of gradient)']);
axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;

legend(axbl, [eb, pub], 'Location', 'northeast');
legend(axgr, eg, 'Location', 'northwest');

% CDF
[axbl, axgr] = make2axes;

plot(axbl, [0, 100], [0, 1], 'k--', 'LineWidth', 0.75);
plot(axgr, [18, 33], [0.5, 0.5], 'k-.', 'LineWidth', 1);

p = plot(axbl, xbl, blcdf);
p.LineWidth = 3;
p.Color = niceblk;

q = plot(axgr, xgr, grcdf);
q.LineWidth = 3;
q.Color = nicered;

axbl.XAxis.Color = niceblk;
axbl.YAxis.Color = niceblk;
xlabel(axbl, 'position (mm)');
ylabel(axbl, 'cdf');
axis(axbl, 'square');

axgr.XAxis.Color = nicered;
axgr.YAxis.Color = nicered;
xlabel(axgr, 'temperature (°C)');
ylabel(axgr, 'cdf');
xlim(axgr, [18, 33]);
axgr.XTick = [18, 22, 26, 30, 33];
axis(axgr, 'square');