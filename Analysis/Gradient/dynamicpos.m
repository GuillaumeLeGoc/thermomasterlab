% Look at evolution of position/equivalent temperature during gradient.

clear
clc

% --- Parameters
% * Definitions
Tgradient = [18, 33];
Tbaseline = 22;
duration_gradient = 40*60;  % seconds
duration_baseline = 5*60;   % seconds
settletime = 5*60;            % time after gradient to establish it (seconds)s

% Bins
timebinsize = 10;           % size in second if temporal bin 

fsus = 5;           % undersampling factor

% * Pool
pixsize = 0.08;
xl = 0;             % mm
xr = xl + 100;           % mm
xoffset = 30;       % offset in pixel given ROI

% * Filters
filt.seq_length = 5;	% sequence length for each fish in seconds
filt.n_bouts = 5;      % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Preparation
% Get experiments list (18 left, 33 right)
[tmptracks1, tmpstamps1] = getGradientList(Tgradient(1), Tgradient(2), Tbaseline);
% reverse
[tmptracks2, tmpstamps2] = getGradientList(Tgradient(2), Tgradient(1), Tbaseline);

% Put into a cell, first element is 18-33, the second is 33-18
alltracks = {tmptracks1; tmptracks2};
allstamps = {tmpstamps1; tmpstamps2};

% Init.
boutalltime = cell(numel(alltracks), 1);
boutallXpos = cell(numel(alltracks), 1);
timealltime = cell(numel(alltracks), 1);
timeallXpos = cell(numel(alltracks), 1);

% --- Gather data
% idorder = 1 : 18-33, idorder = 2 : 33-18
for idorder = 1:numel(alltracks)
    
    ftrack = alltracks{idorder};
    fstamp = allstamps{idorder};
    
    % Handles to convert position to temperature
    if idorder == 1
        Tl = Tgradient(1);
        Tr = Tgradient(2);
        hX = @(x) x;
    elseif idorder == 2
        Tl = Tgradient(2);
        Tr = Tgradient(1);
        hX = @(x) xr - x;
    end
    
    fprintf('Processing %i-%i gradient', Tl, Tr); tic
        
    % Init.
    pbouttime = cell(numel(ftrack), 1);
    pboutXpos = cell(numel(ftrack), 1);
    ptimetime = cell(numel(ftrack), 1);
    ptimeXpos = cell(numel(ftrack), 1);
    
    % Loop over experiments
    for idexp = 1:numel(ftrack)
        
        fprintf('.');
        
        % Read tracks and get positions
        [xpos, ~, ~, ~, frames, ~, framerate] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        ntraj = size(xpos, 2);
        
        % Init.
        bouttime = cell(ntraj, 1);
        boutXpos = cell(ntraj, 1);
        timetime = cell(ntraj, 1);
        timeXpos = cell(ntraj, 1);
        
        for s = 1:ntraj
            
            f = frames(:, s);
            x = xpos(:, s) - xoffset;
            
            % Fill NaNs for time distributions
            ffilled = fillmissing(f, 'linear');
            xfilled = fillmissing(x, 'previous');
            xfilled = fillmissing(xfilled, 'next');
            
            % Remove NaNs for bout distributions
            f(isnan(f)) = [];
            x(isnan(x)) = [];
            
            % Convert to physical
            f = f./framerate;
            ffilled = ffilled./framerate;
            x = x.*pixsize;
            x = hX(x);
            xfilled = xfilled.*pixsize;
            xfilled = hX(xfilled);
            
            % Undersample
            ffilled = ffilled(1:fsus:end);
            xfilled = xfilled(1:fsus:end);
            
            % Store
            bouttime{s} = f;
            boutXpos{s} = x;
            timetime{s} = ffilled;
            timeXpos{s} = xfilled;
            
        end
        
        % Pool
        pbouttime{idexp} = cat(1, bouttime{:});
        pboutXpos{idexp} = cat(1, boutXpos{:});
        ptimetime{idexp} = cat(1, timetime{:});
        ptimeXpos{idexp} = cat(1, timeXpos{:});
        
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % Pool
    boutalltime{idorder} = cat(1, pbouttime{:});
    boutallXpos{idorder} = cat(1, pboutXpos{:});
    timealltime{idorder} = cat(1, ptimetime{:});
    timeallXpos{idorder} = cat(1, ptimeXpos{:});
    
end

% --- Pool all results
boutalltime = cat(1, boutalltime{:});
boutallXpos = cat(1, boutallXpos{:});
timealltime = cat(1, timealltime{:});
timeallXpos = cat(1, timeallXpos{:});

% --- Analysis
Tl = Tgradient(1);
Tr = Tgradient(2);
a = (Tl - Tr)./(xl - xr);
b = (Tr*xl - Tl*xr)./(xl - xr);
hT = @(x) a.*x + b;

totaltime = duration_gradient + duration_baseline;

% Time binning & mean
ntimebin = numel(0:timebinsize:totaltime);

[b_binnedtime, b_etimebin] = discretize(boutalltime, ntimebin);
[t_binnedtime, t_etimebin] = discretize(timealltime, ntimebin);

% Compute mean & s.d. of v2 within bins
b_mbin = accumarray(b_binnedtime, boutallXpos, [], @mean);
b_sdbin = accumarray(b_binnedtime, boutallXpos, [], @std);
b_skbin = accumarray(b_binnedtime, boutallXpos, [], @skewness);
t_mbin = accumarray(t_binnedtime, timeallXpos, [], @mean);
t_sdbin = accumarray(t_binnedtime, timeallXpos, [], @std);
t_skbin = accumarray(t_binnedtime, timeallXpos, [], @skewness);

% Count elements in each bins and get s.e.m.
b_nelmtsperbin = histcounts(boutalltime, b_etimebin);
b_sebin = b_sdbin./sqrt(b_nelmtsperbin');
t_nelmtsperbin = histcounts(timealltime, t_etimebin);
t_sebin = t_sdbin./sqrt(t_nelmtsperbin');

b_ctimebin = edges2centers(b_etimebin);
t_ctimebin = edges2centers(t_etimebin);

% --- Display
% * First moment
% - Bouts
figure('Name', 'M1Bout'); hold on
p0 = patch([duration_baseline, duration_baseline + settletime, duration_baseline + settletime, duration_baseline], ...
    [0, 0, 100, 100], [206 112 18]./255);
p0.FaceAlpha = 0.25;
p1 = patch([duration_baseline + settletime, b_ctimebin(end), b_ctimebin(end), duration_baseline + settletime], ...
    [0, 0, 100, 100], [206 18 18]./255);
p1.FaceAlpha = 0.25;

errorbar(b_ctimebin, b_mbin, b_sebin, 'k');
xlabel('time (s)');
ylabel('<x position> (bouts, mm)');
ylim([30 70]);
ax = gca;
yyaxis right
ax.YAxis(2).Color = ax.YAxis(1).Color;
ylim([30 70]);
ax.YAxis(2).TickLabels=arrayfun(@(x) num2str(hT(x)), ax.YAxis(1).TickValues, 'UniformOutput', false);
ylabel('equiv. temperature during gradient (°C)');

legend('gradient settling', 'gradient condition', 'data');

% - Time
figure('Name', 'M1Time'); hold on
p0 = patch([duration_baseline, duration_baseline + settletime, duration_baseline + settletime, duration_baseline], ...
    [0, 0, 100, 100], [206 112 18]./255);
p0.FaceAlpha = 0.25;
p1 = patch([duration_baseline + settletime, b_ctimebin(end), b_ctimebin(end), duration_baseline + settletime], ...
    [0, 0, 100, 100], [206 18 18]./255);
p1.FaceAlpha = 0.25;

errorbar(t_ctimebin, t_mbin, t_sebin, 'k');
xlabel('time (s)');
ylabel('<x position> (time, mm)');
ylim([30 70]);
ax = gca;
yyaxis right
ax.YAxis(2).Color = ax.YAxis(1).Color;
ylim([30 70]);
ax.YAxis(2).TickLabels=arrayfun(@(x) num2str(hT(x)), ax.YAxis(1).TickValues, 'UniformOutput', false);
ylabel('equiv. temperature during gradient (°C)');

legend('gradient settling', 'gradient condition', 'data');

% * Second moment
% - Bouts
figure('Name', 'M2Bout'); hold on
p0 = patch([duration_baseline, duration_baseline + settletime, duration_baseline + settletime, duration_baseline], ...
    [20, 20, 40, 40], [206 112 18]./255);
p0.FaceAlpha = 0.25;
p1 = patch([duration_baseline + settletime, b_ctimebin(end), b_ctimebin(end), duration_baseline + settletime], ...
    [20, 20, 40, 40], [206 18 18]./255);
p1.FaceAlpha = 0.25;
ylim([20, 40]);
plot(b_ctimebin, b_sdbin, 'k');
xlabel('time (s)');
ylabel('s.d.(x position) (bout, mm)');
legend('gradient settling', 'gradient condition', 'data');

% - Time
figure('Name', 'M2Time'); hold on
p0 = patch([duration_baseline, duration_baseline + settletime, duration_baseline + settletime, duration_baseline], ...
    [20, 20, 40, 40], [206 112 18]./255);
p0.FaceAlpha = 0.25;
p1 = patch([duration_baseline + settletime, b_ctimebin(end), b_ctimebin(end), duration_baseline + settletime], ...
    [20, 20, 40, 40], [206 18 18]./255);
p1.FaceAlpha = 0.25;
ylim([20, 40]);
plot(t_ctimebin, t_sdbin, 'k');
xlabel('time (s)');
ylabel('s.d.(x position) (time, mm)');
legend('gradient settling', 'gradient condition', 'data');

% * Third moment
% - Bouts
figure('Name', 'M3Bout'); hold on
p0 = patch([duration_baseline, duration_baseline + settletime, duration_baseline + settletime, duration_baseline], ...
    [-0.8, -0.8, 0.8, 0.8], [206 112 18]./255);
p0.FaceAlpha = 0.25;
p1 = patch([duration_baseline + settletime, b_ctimebin(end), b_ctimebin(end), duration_baseline + settletime], ...
    [-0.8, -0.8, 0.8, 0.8], [206 18 18]./255);
p1.FaceAlpha = 0.25;
ylim([-0.8, 0.8]);
plot(b_ctimebin, b_skbin, 'k');
xlabel('time (s)');
ylabel('skew.(x position) (bout)');
legend('gradient settling', 'gradient condition', 'data');

% - Time
figure('Name', 'M3Time'); hold on
p0 = patch([duration_baseline, duration_baseline + settletime, duration_baseline + settletime, duration_baseline], ...
    [-0.8, -0.8, 0.8, 0.8], [206 112 18]./255);
p0.FaceAlpha = 0.25;
p1 = patch([duration_baseline + settletime, b_ctimebin(end), b_ctimebin(end), duration_baseline + settletime], ...
    [-0.8, -0.8, 0.8, 0.8], [206 18 18]./255);
p1.FaceAlpha = 0.25;
ylim([-0.8, 0.8]);
plot(t_ctimebin, t_skbin, 'k');
xlabel('time (s)');
ylabel('skew.(x position) (time)');
legend('gradient settling', 'gradient condition', 'data');