% Gather bouts features from all experiments and save them for
% later use. The baseline condition before each gradient is saved along the
% gradients features.
% LEGACY

clear
close all
clc

root = '/home/ljp/Science/Projects/ThermoMaster/Data/';	% Data  directory

% Define paths and names
exp_list_name = 'Uniform_list.txt';	% List containing experiments to process
protocol_name = 'Protocol.txt';         % name of protocol file within runs folder
output_dir = [root 'Datasets' filesep];

% Parameters
% ----------
settle_time = 300;      % time to consider the gradient to be established (seconds)
L = 100;
l = 45;                 % pool size

% Bouts detection parameters
th = struct;
bout_window = 0;        % will use default values

% Read experiments list
% ---------------------
exp = readtable([root exp_list_name]);
n_exps = numel([exp.RunNumber{:}]) - numel(strfind([exp.RunNumber{:}], ',')); % number of experiments

% Loop over all experiments
% -------------------------------------------------------------------------

fprintf('Pooling data from %i experiments ... \n', n_exps);
tic;
c = 0;
for idx_dat = 1:numel(exp.Date)
    
    dat = char(exp.Date(idx_dat));                         % read date
    runs = eval(['[' exp.RunNumber{idx_dat} ']']);         % read runs for that day
    baselines = eval(['[' exp.Baseline{idx_dat} ']']);     % read baselines
    
    % Specify manually
    %     dat = '2018-06-28';
    %     runs = 1;
    
    for idx_run = 1:numel(runs)
        
        c = c + 1;
        
        statusInfo(c, n_exps);
        
        run = runs(idx_run);
        baseline = baselines(idx_run);
        
        % Read data
        % ----------------------------------------------------------------------
        % Make paths
        % ----------
        path_to_run = [root dat filesep 'Run ' sprintf('%02i', run) filesep];
        path_to_md = [path_to_run 'stamps.mat'];
        path_to_tr = [path_to_run 'tracks.mat'];
        path_to_pr = [path_to_run protocol_name];
        path_to_ri = [path_to_run 'ROI.txt'];
        
        % Load
        % ----
        [P.durations, P.temperatures] = Protocols.parse(path_to_pr);    % protocol
        M = load(path_to_md);                   % time and temperatures stamps
        T = load(path_to_tr, 'body');           % tracking
        T = T.body;
        
        % Get extra info
        % --------------
        n_conditions = numel(P.durations);      % number of conditions
        n_fish = size(T.x, 2);                  % number of fish
        framerate = 1/mean(diff(M.t));          % image acquisition frequency
        if exist(path_to_ri, 'file')
            ROI = load(path_to_ri);             % pixel size
            pix_size = mean([L/(ROI(2) - ROI(1)), l/(ROI(4) - ROI(3))]);
        else
            warning('ROI file not found, using 0.08 mm / pixel.');
            pix_size = .08;
        end
        
        % Convert times in frames
        % -----------------------
        timings_t = [0 ; cumsum(P.durations)];  % time of start and stop of conditions
        timings_f = floor(timings_t*framerate);
        settle_f = round(settle_time*framerate);
        durations_f = round(P.durations*framerate);
        
        % Extract bout features during each conditions
        % -----------------------------------------------------------------
        blm = false;        % baseline marker
        for idx_cond = 1:n_conditions
            
            % Define onset and offset of the condition
            start = timings_f(idx_cond) + settle_f;
            stop = min(start + durations_f(idx_cond), size(T.x, 1));    % deals with boundaries
            
            X = T.x(start:stop, :);
            Y = T.y(start:stop, :);
            
            % Get bouts events
            B = Bouts.find(X, Y, framerate, th, bout_window);
            
            % Get temperatures
            left_temperature = P.temperatures(idx_cond, 1);
            right_temperature = P.temperatures(idx_cond, 2);
            
            % ----------
            % Baseline |
            % ----------
            if (left_temperature == right_temperature) && (left_temperature == baseline)
                
                Time_bl = M.t(start:stop);
                
                xPosition_bl = cell(n_fish, 1);
                xPosition_full_bl = cell(n_fish, 1);
                yPosition_bl = cell(n_fish, 1);
                yPosition_full_bl = cell(n_fish, 1);
                Interbout_interval_bl = cell(n_fish, 1);
                Displacement_bl = cell(n_fish, 1);
                Swim_angle_bl = cell(n_fish, 1);
                Turn_angle_bl = cell(n_fish, 1);
                
                % Loop over fish
                for fish = 1:n_fish
                    
                    % Compute features
                    x_positions = Bouts.limit(X(:, fish), B(fish));   % x positions of bouts (pix)
                    y_positions = Bouts.limit(Y(:, fish), B(fish));
                    
                    % --- Interbouts intervals
                    ibi = diff(B{fish});     % time between two bouts (frames)
                    
                    % --- Displacement
                    x_pos_only = x_positions;
                    x_pos_only(isnan(x_pos_only)) = [];
                    y_pos_only = y_positions;
                    y_pos_only(isnan(y_pos_only)) = [];
                    displacement = sqrt(diff(x_pos_only).^2 + diff(y_pos_only).^2); % displacement in one bout (pix)
                    
                    % --- Angles
                    coord = [x_pos_only, y_pos_only];
                    dcoord = diff(coord, 1, 1);
                    z = dcoord(:, 1) + 1i.*dcoord(:, 2);
                    angles = rad2deg(angle(z));
                    dangles = NaN(size(angles, 1) - 1, 1);
                    for idco = 1:size(angles, 1) - 1
                        source = angles(idco);
                        target = angles(idco + 1);
                        delta = target - source;
                        % Take minimal angle
                        if delta > 180
                            delta = delta - 360;
                        elseif delta < 180
                            delta = delta + 360;
                        end
                        dangles(idco) = wrapTo180(delta);
                    end
                    
                    % Convert to real units
                    xPosition_bl{fish} = x_positions.*pix_size;     % (mm)
                    yPosition_bl{fish} = y_positions.*pix_size;     % (mm)
                    xPosition_full_bl{fish} = X(:, fish).*pix_size; % (mm)
                    yPosition_full_bl{fish} = Y(:, fish).*pix_size; % (mm)
                    Interbout_interval_bl{fish} = ibi./framerate;	% (s)
                    Displacement_bl{fish} = displacement.*pix_size; % (mm)
                    Swim_angle_bl{fish} = angles;                   % (deg)
                    Turn_angle_bl{fish} = dangles;                  % (deg)
                end
                
                blm = true;     % baseline condition marker
                
                % -------------
                % Stimulation |
                % -------------
            elseif left_temperature ~= baseline || right_temperature ~= baseline
                
                Time_gr = M.t(start:stop);
                
                xPosition_gr = cell(n_fish, 1);
                xPosition_full_gr = cell(n_fish, 1);
                yPosition_gr = cell(n_fish, 1);
                yPosition_full_gr = cell(n_fish, 1);
                Interbout_interval_gr = cell(n_fish, 1);
                Displacement_gr = cell(n_fish, 1);
                Swim_angle_gr = cell(n_fish, 1);
                Turn_angle_gr = cell(n_fish, 1);
                
                for fish = 1:n_fish
                    
                    % Compute features
                    x_positions = Bouts.limit(X(:, fish), B(fish));
                    y_positions = Bouts.limit(Y(:, fish), B(fish));
                    
                    % --- Interbout intervals
                    ibi = diff(B{fish});
                    
                    % --- Displacements
                    x_pos_only = x_positions;
                    x_pos_only(isnan(x_pos_only)) = [];
                    y_pos_only = y_positions;
                    y_pos_only(isnan(y_pos_only)) = [];
                    displacement = sqrt(diff(x_pos_only).^2 + diff(y_pos_only).^2);
                    
                    % --- Angles
                    coord = [x_pos_only, y_pos_only];
                    dcoord = diff(coord, 1, 1);
                    z = dcoord(:, 1) + 1i.*dcoord(:, 2);
                    angles = rad2deg(angle(z));
                    dangles = NaN(size(angles, 1) - 1, 1);
                    for idco = 1:size(angles, 1) - 1
                        source = angles(idco);
                        target = angles(idco + 1);
                        delta = target - source;
                        % Take minimal angle
                        if delta > 180
                            delta = delta - 360;
                        elseif delta < 180
                            delta = delta + 360;
                        end
                        dangles(idco) = wrapTo180(delta);
                    end
                    
                    % Convert to real units
                    if exist(path_to_ri, 'file')
                        ROI = load(path_to_ri);
                        pix_size = mean([L/(ROI(2) - ROI(1)), l/(ROI(4) - ROI(3))]);
                    else
                        warning('ROI file not found, using 0.08 mm / pixel.');
                        pix_size = .08;
                    end
                    
                    xPosition_gr{fish} = x_positions.*pix_size;
                    xPosition_full_gr{fish} = X(:, fish).*pix_size;
                    yPosition_gr{fish} = y_positions.*pix_size;
                    yPosition_full_gr{fish} = Y(:, fish).*pix_size;
                    Interbout_interval_gr{fish} = ibi./framerate;
                    Displacement_gr{fish} = displacement.*pix_size;
                    Swim_angle_gr{fish} = angles;
                    Turn_angle_gr{fish} = dangles;
                end
                
                Experiment.Tleft = left_temperature;
                Experiment.Tright = right_temperature;
                Experiment.Tbaseline = baseline;
                Experiment.Date = dat;
                Experiment.RunNumber = run;
                
                % Save features
                if blm % check baseline - gradient consistency
                    
                    if left_temperature == right_temperature
                        filename_gr = [output_dir 'uniform_' num2str(left_temperature) '_' ...
                            num2str(right_temperature) '_' num2str(yyyymmdd(datetime(dat))) ...
                            '_' num2str(run) '.mat'];
                    else
                        filename_gr = [output_dir 'gradient_' num2str(left_temperature) '_' ...
                            num2str(right_temperature) '_' num2str(yyyymmdd(datetime(dat))) ...
                            '_' num2str(run) '.mat'];
                    end
                    
                    save(filename_gr, 'Experiment', 'Time_gr', 'Time_bl', ...
                        'xPosition_gr', 'xPosition_full_gr', 'yPosition_gr', 'yPosition_full_gr', 'Interbout_interval_gr', 'Displacement_gr', 'Turn_angle_gr', 'Swim_angle_gr', ...
                        'xPosition_bl', 'xPosition_full_bl', 'yPosition_bl', 'yPosition_full_bl', 'Interbout_interval_bl', 'Displacement_bl', 'Turn_angle_bl', 'Swim_angle_bl');
                else
                    error('Everything is fucked up.');
                end
                
                blm = false;
            end
        end
    end
end

fprintf('\nDone (%2.2fs)\n', toc);