function [bouts_position, sd] = find_angle(x, y, angles, framerate, th, bout_window)
% Bouts.find_angle. This function finds bouts from x-y coordinates and
% heading angles.
%
% INPUTS :
% ------
% x : n_times x 1 array of x positions.
% y : n_times x 1 array of y positions.
% t : n_times x 1 array of heading angles in degrees.
% framerate : sample rate (in Hz).
% th : structure with fields :
%       - noise : filter on speed, multiplies the variance of the 90%
%                 lowest values,
%       - max_if_distance : maximum distance between two frames (in pixels),
%       - max_distance : maximum distance of a bout event (in pixels),
%       - min_distance : minimum distance of a bout event (in pixels),
%       - mpd : interbout interval (in seconds).
%       - all : boolean, if true, do not filter bouts.
%
% bout_window : duration of a bout (in seconds). A default value is set if
% input is 0 or no input.
%
% OUTPUTS :
% -------
% bouts_positions : n_bouts x 1 array, containing indices of each bout
% event in frames.

% Disable warning messages
warning('off', 'signal:findpeaks:largeMinPeakHeight');
warning('off', 'MATLAB:polyfit:PolyNotUnique');

% Default values
% --------------
if ~isfield(th, 'max_if_distance')
    th.max_if_distance = 50;    % remove tracking errors (pix)
end
if ~isfield(th, 'min_if_distance')
    th.min_if_distance = 0.05;     % remove small displacement (pix)
end
if ~isfield(th, 'max_if_angle')
    th.max_if_angle = 120;       % max delta heading between to frames (deg)
end
if ~isfield(th, 'mph')
    th.mph = 1;               % minimum peak height
end
if ~isfield(th, 'mpd')
    th.mpd = 0.3;               % min time between two bouts events (sec)
end
if ~isfield(th, 'max_distance')
    th.max_distance = 150;      % max distance during one bout (pix)
end
if ~isfield(th, 'min_distance')
    th.min_distance = 2;        % min distance during one bout (pix)
end
if ~isfield(th, 'min_angle')
    th.min_angle = 0;          % min angular displacement during one bout (deg)
end
if ~isfield(th, 'max_angle')
    th.max_angle = 120;         % max angular displacement during one bout (deg)
end
if ~isfield(th, 'all')
    th.all = false;             % dont filter bouts
end
if ~exist('bout_window', 'var')
    bout_window = 0.3;         % time window around a bout
elseif exist('bout_window', 'var') && bout_window == 0
    bout_window = 0.3;
end

n_times = size(x, 1);
win_frames = round(bout_window*framerate/2);

% Prepare data
% ------------
% Compute differentials
dx = diff(x);
dy = diff(y);
dtheta = diff(angles);
dtheta = rad2deg(atan2(sin(dtheta), cos(dtheta)));

% Filters
s = sqrt(dx.^2 + dy.^2);
dx(s > th.max_if_distance) = 0;
dy(s > th.max_if_distance) = 0;
dtheta(s > th.max_if_distance) = 0;
dx(s < th.min_if_distance) = 0;
dy(s < th.min_if_distance) = 0;
dtheta(s < th.min_if_distance) = 0;

% Compute variances
vardxy = nanvar((dx + dy)./2);
vardth = nanvar(dtheta);

% Compute significant displacement metric
sd = (dtheta.^2/vardth).*( (dx.^2 + dy.^2)./vardxy);

% Find bouts
% ----------
[~, locs] = findpeaks(sd, 'MinPeakHeight', th.mph, ...
    'MinPeakDistance', th.mpd*framerate);

if isempty(locs)
    bouts_position = [];
    return
end

% Validate bouts
% --------------
bouts_init = zeros(n_times, 1);
for idx_pks = 1:numel(locs)
    
    % Region around the bout
    ti = max(locs(idx_pks) - win_frames, 1);                % initial
    tf = min(locs(idx_pks) + win_frames, size(x, 1));       % final
    
    % Compute displacement
    d = sqrt( (x(tf) - x(ti)).^2 + (y(tf) - y(ti))^2 );
    % Compute angular displacement
    deltaa = angles(tf) - angles(ti);
    da = rad2deg(atan2(sin(deltaa), cos(deltaa)));
    
    % Validate the bout on distance criterion
    if d > th.min_distance && d < th.max_distance
        bouts_init(locs(idx_pks)) = 1;
    elseif abs(da) > th.min_angle && abs(da) < th.max_angle
        bouts_init(locs(idx_pks)) = 1;
    end
end

bouts_init = find(bouts_init);
bouts_init = bouts_init - floor(2*win_frames/3);
bouts_init(bouts_init <= 0) = [];
bouts_position = bouts_init;