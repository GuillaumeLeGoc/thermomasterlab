function [bouts_position, s] = find(x, y, framerate, th, bout_window)
% Bouts.find. This function takes a set of x-y coordinates over time and
% detects the bouts events, finding peaks in the squared speed signal.
% Peaks are filtered by a minimum peak height, minimum distance between
% peaks, maximum distance between two frames and minimum distance travelled
% during the bout event.
%
% INPUTS :
% ------
% x : n_times x n_fish array of x positions.
% y : n_times x n_fish array of y positions.
% framerate : sample rate (in Hz).
% th : structure with fields :
%       - noise : filter on speed, multiplies the variance of the 90%
%                 lowest values,
%       - max_if_distance : maximum distance between two frames (in pixels),
%       - max_distance : maximum distance of a bout event (in pixels),
%       - min_distance : minimum distance of a bout event (in pixels),
%       - time : interbout interval (in seconds).
%       - (optional) all : boolean, if true, do not filter bouts.
%
% bout_window : duration of a bout (in seconds). A default value is set if
% input is 0 or no input.
%
% OUTPUTS :
% -------
% bouts_positions : n_fish x 1  cell, containing indices of each bout event
% in frames for each fish.

% Disable warning message when no peaks are found
warning('off', 'signal:findpeaks:largeMinPeakHeight');

% Default values
% --------------
if ~isfield(th, 'max_if_distance')
    th.max_if_distance = 30;    % remove tracking errors
end
if ~isfield(th, 'min_if_distance')
    th.min_if_distance = 1;     % remove small displacement
end
if ~isfield(th, 'max_distance')
    th.max_distance = 200;      % max distance during one bout
end
if ~isfield(th, 'min_distance')
    th.min_distance = 3;        % min distance during one bout
end
if ~isfield(th, 'time')
    th.time = 0.4;              % min time between two bouts events
end
if ~isfield(th, 'mph')
    th.mph = 1;                 % threshold for speed peak height (normalized)
end
if ~isfield(th, 'all')
    th.all = false;
end
if ~exist('bout_window', 'var')
    bout_window = 0.5;          % time window around a bout
elseif exist('bout_window', 'var') && bout_window == 0
    bout_window = 0.5;
end

% Initialise and loop over fish
% -----------------------------
n_times = size(x, 1);
n_fish = size(x, 2);
win_frames = round(bout_window*framerate/2);
bouts_position = cell(n_fish, 1);

for fish = 1:n_fish
    
    % Prepare data
    % ------------
    x_f = x(:, fish);
    y_f = y(:, fish);
    
    % Compute x and y speed components
    dx = diff(x_f);
    dy = diff(y_f);

    % Compute squared displacement
    s = dx.^2 + dy.^2;
    
    % Filter too high displacement and too small between two frames
    s(s > th.max_if_distance.^2) = 0;
    s(s < th.min_if_distance.^2) = 0;
    
    % Normalize by variance
    vardxdy = var((dx + dy)/2, 'omitnan');
    s = s./(2*vardxdy);
    
    % Find bouts
    % ----------
    if ~th.all
        [~, locs] = findpeaks(s, 'MinPeakDistance', round(th.time*framerate), ...
            'MinPeakHeight', th.mph);
    else
        bouts_position{fish} = 1:numel(s);
        continue;
    end
    
    % Filter with total distance during one bout
    % ------------------------------------------
    bouts_init = zeros(n_times, 1);
    
    for idx_pks = 1:numel(locs)
        
        % Region around the bout
        ti = max(locs(idx_pks) - win_frames, 1);                % initial
        tf = min(locs(idx_pks) + win_frames, size(x_f, 1));     % final
        
        % Compute displacement during this bout
        d = sqrt( (x_f(tf) - x_f(ti)).^2 + (y_f(tf) - y_f(ti))^2 );
        
        % Validate the bout on distance criterion
        if d > th.min_distance && d < th.max_distance
            bouts_init(locs(idx_pks)) = 1;
        end
    end
    
    bouts_init = find(bouts_init);
    bouts_init = bouts_init - floor(1*win_frames/3);
    bouts_init(bouts_init <= 0) = [];
    bouts_position{fish} = bouts_init;
end

end