function X_b = limit(X, B)
% Bouts.limit. Replace positions in X where no bout event occured by NaN.
%
% INPUTS :
% ------
% X : n_times x n_fish array containing fish' positions over time.
% B : n_fish x 1 cell, containing time steps where a bout occured for each
% fish.
%
% OUTPUT :
% ------
% X_b : X positions limited to time steps where a bout occured.

if ~iscell(B)
    B = {B};
end

X_b = NaN(size(X));
n_fish = size(X, 2);

for fish = 1:n_fish
    X_b(B{fish}, fish) = X(B{fish}, fish);
end

end