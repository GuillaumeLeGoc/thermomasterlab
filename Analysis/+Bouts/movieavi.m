% Display a movie with fish locations and shows when there's bouts.
% With AVI file

clear
close all
clc

warning('off', 'images:imshow:magnificationMustBeFitForDockedFigure');

pix_size = .08;

% Files
fimages = '/home/ljp/Science/Projects/ThermoMaster/Data/CopperControls/spontaneous_cuivre/21-06-30/E3_copper/run_01/movie/fc2_save_2021-06-30-170429-0000.avi';
ftracks = '/home/ljp/Science/Projects/ThermoMaster/Data/CopperControls/spontaneous_cuivre/21-06-30/E3_copper/run_01/movie/Tracking_Result_fc2_save_2021-06-30-170429-0000/tracking.txt';

vid = VideoReader(fimages);
tracks = readtable(ftracks);

framerate = vid.FrameRate;

n_seq = max(unique(tracks.id));  % number of trajectories

% Find bouts
th = struct;
bout_window = 0;
x_b = NaN(size(tracks.xBody, 1), 1);
y_b = NaN(size(tracks.xBody, 1), 1);
t_b = NaN(size(tracks.xBody, 1), 1);

for s = 1:n_seq
    x = tracks.xBody(tracks.id == s);
    y = tracks.yBody(tracks.id == s);
%     t = tracks.tBody(tracks.id == s);
    if numel(x) < framerate/2
        continue
    end
%     b = Bouts.find_angle(x, y, t, framerate, th, bout_window);
%     B{1} = b;
    B = Bouts.find(x, y, framerate, th, bout_window);
    x_b(tracks.id == s) = Bouts.limit(x, B);
    y_b(tracks.id == s) = Bouts.limit(y, B);
end

% Loop show images
figure;
usr_msg = '';
tic
for imgidx = 1:n_images
    
    if mod(imgidx, 1) == 0
        fprintf(repmat(sprintf('\b'), 1, numel(usr_msg)));  % erase previous message
        usr_msg = [num2str(imgidx) '/' num2str(n_images) '   [' num2str(imgidx/toc) ' FPS]'];
        fprintf(usr_msg);
    end
    
    img = imread(images(imgidx));
    hold off
    imshow(img);
    hold on
    scatter(tracks.xBody(tracks.imageNumber==imgidx-1, :), tracks.yBody(tracks.imageNumber==imgidx-1, :), '+r');
    scatter(x_b(tracks.imageNumber==imgidx-1, :), y_b(tracks.imageNumber==imgidx-1, :), '+g');
    drawnow limitrate
    
end