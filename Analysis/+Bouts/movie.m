% Display a movie with fish locations and shows when there's bouts.
clear
close all
clc

warning('off', 'images:imshow:magnificationMustBeFitForDockedFigure');
warning('off', 'MATLAB:imagesci:readpnm:extraData');

% Experiment directory
cwd = '/home/ljp/Science/Projects/ThermoMaster/Data/2019-03-13/Run 03/';

% Make files names
images = dir([cwd 'Images' '/' '*pgm']);
n_images = numel(images);
images = @(n) [cwd 'Images' '/' 'Frame_' sprintf('%06i', n) '.pgm'];

path_to_ri = [cwd 'ROI.txt'];
if exist(path_to_ri, 'file')
    ROI = load(path_to_ri);             % pixel size
    pix_size = mean([100/(ROI(2) - ROI(1)), 45/(ROI(4) - ROI(3))]);
else
    warning('ROI file not found, using 0.09 mm / pixel.');
    pix_size = .09;
end

files.tracks = [cwd 'Tracking_Result' filesep 'tracking.txt'];
tracks = readtable(files.tracks);

files.stamps = [cwd 'stamps.mat'];
stamps = load(files.stamps, 't');
framerate = 1./mean(diff(stamps.t));

n_seq = max(unique(tracks.id));  % number of trajectories

% Find bouts
th = struct;
bout_window = 0;
x_b = NaN(size(tracks.xBody, 1), 1);
y_b = NaN(size(tracks.xBody, 1), 1);
t_b = NaN(size(tracks.xBody, 1), 1);

for s = 1:n_seq
    x = tracks.xBody(tracks.id == s);
    y = tracks.yBody(tracks.id == s);
%     t = tracks.tBody(tracks.id == s);
    if numel(x) < framerate/2
        continue
    end
%     b = Bouts.find_angle(x, y, t, framerate, th, bout_window);
%     B{1} = b;
    B = Bouts.find(x, y, framerate, th, bout_window);
    x_b(tracks.id == s) = Bouts.limit(x, B);
    y_b(tracks.id == s) = Bouts.limit(y, B);
end

% Loop show images
figure;
usr_msg = '';
tic
for imgidx = 1:n_images
    
    if mod(imgidx, 1) == 0
        fprintf(repmat(sprintf('\b'), 1, numel(usr_msg)));  % erase previous message
        usr_msg = [num2str(imgidx) '/' num2str(n_images) '   [' num2str(imgidx/toc) ' FPS]'];
        fprintf(usr_msg);
    end
    
    img = imread(images(imgidx));
    hold off
    imshow(img);
    hold on
    scatter(tracks.xBody(tracks.imageNumber==imgidx-1, :), tracks.yBody(tracks.imageNumber==imgidx-1, :), '+r');
    scatter(x_b(tracks.imageNumber==imgidx-1, :), y_b(tracks.imageNumber==imgidx-1, :), '+g');
    drawnow limitrate
    
end