function [] = displayMovie(dat, run, varargin)

% --- Check input
p = inputParser;
p.addRequired('dat', @(x) ischar(x)||isstring(x)||isdatetime(x));
p.addRequired('run', @isnumeric);
p.addOptional('fmt', 'pgm', @(x) ischar(x)||isstring(x));
p.parse(dat, run, varargin{:});
dat = p.Results.dat;
run = p.Results.run;
fmt = p.Results.fmt;

% --- Disable warnings
warning('off', 'images:imshow:magnificationMustBeFitForDockedFigure');
warning('off', 'MATLAB:imagesci:readpnm:extraData');

% --- Build paths
cwd = [pwd, filesep, 'Data' filesep];
path_tracks = fullfile(cwd, dat, ['Run ' sprintf('%02i', run)], 'Tracking_Result', 'tracking.txt');
path_images = ['/usr/local/DATA/Data_Behaviour', filesep, dat, filesep, ['Run ' sprintf('%02i', run)], filesep, 'Images', filesep, '*.', fmt];
path_stamps = fullfile(cwd, dat, ['Run ' sprintf('%02i', run)], 'stamps.mat');

% --- Get data
imgs = dir(path_images);
ntimes = size(imgs, 1);
loadimg = @(idx) imread(fullfile(imgs(idx).folder, imgs(idx).name));
tracks = readtable(path_tracks);
stamps = load(path_stamps, 't');
framerate = 1./mean(diff(stamps.t));

% --- Find bouts
seqs = unique(tracks.id);  % different objects
th = struct;
bout_window = 0;
x_b = NaN(size(tracks.xBody, 1), 1);
y_b = NaN(size(tracks.xBody, 1), 1);

for s = seqs'
    x = tracks.xBody(tracks.id == s);
    y = tracks.yBody(tracks.id == s);
    if numel(x) < framerate/2
        continue;
    end
    
    B = Bouts.find(x, y, framerate, th, bout_window);
    x_b(tracks.id == s) = Bouts.limit(x, B);
    y_b(tracks.id == s) = Bouts.limit(y, B);
end

t = 1;

% --- Create first stack
img = loadimg(1);

% --- Handles
mktxt = @(s) [num2str(s, '%i'), '/', num2str(ntimes, '%i')];

% --- Create figure
fig = figure('Visible', 'off'); hold on
h = imshow(img);
sn = scatter(tracks.xBody(tracks.imageNumber==t-1, :), tracks.yBody(tracks.imageNumber==t-1, :), '+r');
sy = scatter(x_b(tracks.imageNumber==t-1, :), y_b(tracks.imageNumber==t-1, :), '+g');
ax = gca;

% --- Create t slider
ht = uicontrol('Style', 'slider', ...
    'Min', 1, 'Max', ntimes, ...
    'SliderStep', [1/(ntimes-1) 1/(ntimes-1)], ...
    'Value', t, ...
    'Position', [260 40 260 20]);

% --- Create slider info
hf = uicontrol('Style', 'text', ...
    'String', mktxt(0), ...
    'Position', [260, 10, 260, 20]);

% --- Create play/pause button
hp = uicontrol('Style', 'togglebutton', ...
    'Position', [530 40 20 20], ...
    'String', '|>');

% --- Create FPS box
hb = uicontrol('Style', 'edit', ...
    'Position', [560 40 50 20]);

% --- Link buttons
ht.Callback = @updateT;
ht.ButtonDownFcn = {@stopSlide, hp};
hp.Callback = {@playPause, ht, hb};

hlt1 = addlistener(ht, 'ContinuousValueChange', @updateT);
setappdata(ht, 'sliderListener', hlt1);

fig.Visible = 'on';

% -------------------------------------------------------------------------

    function updateT(slider_handle, ~)
        % Updates the t image to be displayed.
        t = floor(slider_handle.Value);
        img = loadimg(t);
        set(h, 'Cdata', img);
        sn.XData = tracks.xBody(tracks.imageNumber==t-1, :);
        sn.YData = tracks.yBody(tracks.imageNumber==t-1, :);
        
        sy.XData = x_b(tracks.imageNumber==t-1, :);
        sy.YData = y_b(tracks.imageNumber==t-1, :);
        
        drawnow;
        
        % Update text
        hf.String = mktxt(t);
    end

    function playPause(toggle_button, ~, sliderhandle, edithandle)
        sliderhandle.Enable = 'inactive';
        while get(toggle_button, 'Value')
            current_value = sliderhandle.Value;
            if current_value == ntimes
                current_value = 0;
            end
            sliderhandle.Value = current_value + 1;
            notify(sliderhandle, 'ContinuousValueChange');
            pause(1/str2double(edithandle.String));
        end
        sliderhandle.Enable = 'on';
    end

    function stopSlide(~, ~, toggle_button)
        toggle_button.Value = 0;
    end
end