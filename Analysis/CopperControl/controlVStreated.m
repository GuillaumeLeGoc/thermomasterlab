% Takes two FastTrack sets of file, find bouts and show they are not statistically
% different (hopefully) + plot both MSD

clear
clc

% --- Definitions
setcontrol = {'/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_01/Tracking_Results/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_02/Tracking_Result_fc2_save_2021-06-30-153707-0000/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_02/Tracking_Result_fc2_save_2021-06-30-153707-0001/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_03/Tracking_Result_fc2_save_2021-06-30-162230-0000/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_03/Tracking_Result_fc2_save_2021-06-30-162230-0001/tracking.txt'};

settreated = {'/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_01/Tracking_Result_fc2_save_2021-06-30-170429-0000/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_01/Tracking_Result_fc2_save_2021-06-30-170429-0001/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_02/Tracking_Result_fc2_save_2021-06-30-174524-0000/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_02/Tracking_Result_fc2_save_2021-06-30-174524-0001/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_03/Tracking_Result_fc2_save_2021-06-30-183213-0000/tracking.txt', ...
    '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_03/Tracking_Result_fc2_save_2021-06-30-183213-0001/tracking.txt'};

% setcontrol = {'/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_02/Tracking_Result_fc2_save_2021-06-30-153707-0000/tracking.txt', ...
%      '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/E3/run_02/Tracking_Result_fc2_save_2021-06-30-153707-0001/tracking.txt'};
% settreated = {'/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_02/Tracking_Result_fc2_save_2021-06-30-174524-0000/tracking.txt', ...
%     '/home/guillaume/Science/Projects/ThermoMaster/Data/CopperControl/2021-06-30/Copper/run_02/Tracking_Result_fc2_save_2021-06-30-174524-0001/tracking.txt'};
    
framerate = 25;     % hertz
pixsize = 0.08;     % mm

% Bout detection parameters
th = struct;
th.mph = 1;
th.min_distance = 1;
th.max_distance = 150;
th.time = 0.2;
bout_window = 0.25;

maxi = Inf;
maxd = Inf;

% Filter trajectories
filt.seq_length = 25;       % minimal sequence length for each fish in seconds
filt.n_bouts = 10;          % minimal number of bouts
filt.minx = 0;
filt.maxx = Inf;

% Time window for MSD
ntimes = 10;    % in seconds

% ---  Control
% * Initialization
ibiC = cell(numel(setcontrol), 1);
dspC = cell(numel(setcontrol), 1);
reoC = cell(numel(setcontrol), 1);
seqC = cell(numel(setcontrol), 1);

boutIC = cell(numel(setcontrol), 1);
boutDC = cell(numel(setcontrol), 1);
boutRC = cell(numel(setcontrol), 1);

% --- Processing
fprintf('Processing control'); tic
for idf = 1:numel(setcontrol)
    
    % Get kinematics parameters
    [x, i, d, r, f, af, y] = Tracks.getFeaturesNT(setcontrol{idf}, framerate, filt, false, th, bout_window);
    
    % i : ibi, d : displacement, t : reorientation, f : frames
    
    nseq = size(x, 2); % number of different trajectories
    
    % Get mean values for each trajectory
    mi = NaN(nseq, 1);
    md = NaN(nseq, 1);
    mr = NaN(nseq, 1);
    xy_time = cell(nseq, 1);
    for s = 1:nseq
        
        ti = i(:, s)./framerate;
        td = d(:, s).*pixsize;
        tr = r(:, s);
        
        ti(ti > maxi) = NaN;
        td(td > maxd) = NaN;
        
        mi(s) = mean(ti, 'omitnan');
        md(s) = mean(td, 'omitnan');
        mr(s) = mean(abs(tr), 'omitnan');
        
        % Build time and positions vectors to compute MSD later
        t = f(:, s)./framerate;    % time
        t(isnan(t)) = [];
        xpos = x(:, s).*pixsize;
        xpos(isnan(xpos)) = [];     % x position
        ypos = y(:, s).*pixsize;
        ypos(isnan(ypos)) = [];     % y position
        
        xy_time{s} = [t, xpos, ypos];
        
    end
    
    % Find bouts artifacts
    torm = d > maxd/pixsize | i > maxi.*framerate;
    
    % Store bout values
    i(torm) = NaN;
    i = i(:);
    i(isnan(i)) = [];
    boutIC{idf} = i./framerate;
    
    d(torm) = NaN;
    d = d(:);
    d(isnan(d)) = [];
    boutDC{idf} = d.*pixsize;
    
    r(torm) = NaN;
    r = r(:);
    r(isnan(r)) = [];
    boutRC{idf} = r;
    
    % Store mean values
    ibiC{idf} = mi;
    dspC{idf} = md;
    reoC{idf} = mr;
    seqC{idf} = xy_time;
    
%     figure;
%     subplot(2, 2, 1);
%     histogram(boutIC{idf}, 'Normalization', 'pdf', 'EdgeColor', 'none');
%     
%     subplot(2, 2, 2);
%     histogram(boutDC{idf}, 'Normalization', 'pdf', 'EdgeColor', 'none');
%     
%     subplot(2, 2, 3);
%     histogram(boutRC{idf}, 'Normalization', 'pdf', 'EdgeColor', 'none');
    
    fprintf('.');
    
end

fprintf('\tDone (%2.2fs).\n', toc);

% Pool sequences for MSD
seqC = cat(1, seqC{:});

% Compute MSD with msdanalyzer package
fprintf('Computing control MSD...'); tic

msd_time = msdanalyzer(2, 'mm', 's');
msd_time = msd_time.addAll(seqC);
msd_time = msd_time.computeMSD;

% Get mean & sem
results_time = msd_time.getMeanMSD;
results_time(:, 1) = results_time(:, 1);
[~, stop] = min(abs(results_time(:, 1) - ntimes));
delaysC = results_time(1:stop, 1);
mmsdC = results_time(1:stop, 2);
std_msdC = results_time(1:stop, 3);
emsdC = std_msdC./sqrt(results_time(1:stop, 4));

fprintf('\tDone (%2.2fs).\n', toc);

% --- Treated
% * Initialization
ibiT = cell(numel(settreated), 1);
dspT = cell(numel(settreated), 1);
reoT = cell(numel(settreated), 1);
seqT = cell(numel(settreated), 1);

boutIT = cell(numel(settreated), 1);
boutDT = cell(numel(settreated), 1);
boutRT = cell(numel(settreated), 1);

% * Processing
fprintf('Processing treated'); tic
for idf = 1:numel(settreated)
    
    % Get kinematics parameters
    [x, i, d, r, f, af, y] = Tracks.getFeaturesNT(settreated{idf}, framerate, filt, false, th, bout_window);
    
    % i : ibi, d : displacement, t : reorientation, f : frames
    
    nseq = size(x, 2); % number of different trajectories
    
    % Get mean values for each trajectory
    mi = NaN(nseq, 1);
    md = NaN(nseq, 1);
    mr = NaN(nseq, 1);
    xy_time = cell(nseq, 1);
    for s = 1:nseq
        
        ti = i(:, s)./framerate;
        td = d(:, s).*pixsize;
        tr = r(:, s);
        
        ti(ti > maxi) = NaN;
        td(td > maxd) = NaN;
        
        mi(s) = mean(ti, 'omitnan');
        md(s) = mean(td, 'omitnan');
        mr(s) = mean(abs(tr), 'omitnan');
        
        % Build time and positions vectors to compute MSD later
        t = f(:, s)./framerate;    % time
        t(isnan(t)) = [];
        xpos = x(:, s).*pixsize;
        xpos(isnan(xpos)) = [];     % x position
        ypos = y(:, s).*pixsize;
        ypos(isnan(ypos)) = [];     % y position
        
        xy_time{s} = [t, xpos, ypos];
        
    end
    
    % Find bouts artifacts
    torm = d > maxd/pixsize & i > maxi.*framerate;
    
    % Store bout values
    i(torm) = NaN;
    i = i(:);
    i(isnan(i)) = [];
    boutIT{idf} = i./framerate;
    
    d(torm) = NaN;
    d = d(:);
    d(isnan(d)) = [];
    boutDT{idf} = d.*pixsize;
    
    r(torm) = NaN;
    r = r(:);
    r(isnan(r)) = [];
    boutRT{idf} = r;
    
    % Store mean values
    ibiT{idf} = mi;
    dspT{idf} = md;
    reoT{idf} = mr;
    seqT{idf} = xy_time;
    
%     figure;
%     subplot(2, 2, 1);
%     histogram(boutIT{idf}, 'Normalization', 'pdf', 'EdgeColor', 'none');
%     
%     subplot(2, 2, 2);
%     histogram(boutDT{idf}, 'Normalization', 'pdf', 'EdgeColor', 'none');
%     
%     subplot(2, 2, 3);
%     histogram(boutRT{idf}, 'Normalization', 'pdf', 'EdgeColor', 'none');
    
    fprintf('.');
    
end

fprintf('\tDone (%2.2fs).\n', toc);

% Pool sequences for MSD
seqT = cat(1, seqT{:});

% Compute MSD with msdanalyzer package
fprintf('Computing treated MSD...'); tic

msd_time = msdanalyzer(2, 'mm', 's');
msd_time = msd_time.addAll(seqT);
msd_time = msd_time.computeMSD;

% Get mean & sem
results_time = msd_time.getMeanMSD;
results_time(:, 1) = results_time(:, 1);
[~, stop] = min(abs(results_time(:, 1) - ntimes));
delaysT = results_time(1:stop, 1);
mmsdT = results_time(1:stop, 2);
std_msdT = results_time(1:stop, 3);
emsdT = std_msdT./sqrt(results_time(1:stop, 4));

fprintf('\tDone (%2.2fs).\n', toc);

% --- Gather data

% Recover experiments that are split in two files
rboutIC = {cat(1, boutIC{1}); cat(1, boutIC{[2, 3]}); cat(1, boutIC{[4, 5]})};
rboutDC = {cat(1, boutDC{1}); cat(1, boutDC{[2, 3]}); cat(1, boutDC{[4, 5]})};
rboutRC = {cat(1, boutRC{1}); cat(1, boutRC{[2, 3]}); cat(1, boutRC{[4, 5]})};

rtrajIC = {cat(1, ibiC{1}); cat(1, ibiC{[2, 3]}); cat(1, ibiC{[4, 5]})};
rtrajDC = {cat(1, dspC{1}); cat(1, dspC{[2, 3]}); cat(1, dspC{[4, 5]})};
rtrajRC = {cat(1, reoC{1}); cat(1, reoC{[2, 3]}); cat(1, reoC{[4, 5]})};

rboutIT = {cat(1, boutIT{[1, 2]}); cat(1, boutIT{[3, 4]}); cat(1, boutIT{[5, 6]})};
rboutDT = {cat(1, boutDT{[1, 2]}); cat(1, boutDT{[3, 4]}); cat(1, boutDT{[5, 6]})};
rboutRT = {cat(1, boutRT{[1, 2]}); cat(1, boutRT{[3, 4]}); cat(1, boutRT{[5, 6]})};

rtrajIT = {cat(1, ibiT{[1, 2]}); cat(1, ibiT{[2, 3]}); cat(1, ibiT{[5, 6]})};
rtrajDT = {cat(1, dspT{[1, 2]}); cat(1, dspT{[2, 3]}); cat(1, dspT{[5, 6]})};
rtrajRT = {cat(1, reoT{[1, 2]}); cat(1, reoT{[2, 3]}); cat(1, reoT{[5, 6]})};

% rtrajIC = ibiC;
% rtrajDC = dspC;
% rtrajRC = reoC;
% rtrajIT = ibiT;
% rtrajDT = dspT;
% rtrajRT = reoT;

mboutIC = cellfun(@mean, rboutIC);
mboutDC = cellfun(@mean, rboutDC);
mboutRC = cellfun(@(x) mean(abs(x)), rboutRC);
mboutIT = cellfun(@mean, rboutIT);
mboutDT = cellfun(@mean, rboutDT);
mboutRT = cellfun(@(x) mean(abs(x)), rboutRT);

% mboutIC = cellfun(@median, rboutIC);
% mboutDC = cellfun(@median, rboutDC);
% mboutRC = cellfun(@(x) mean(abs(x)), rboutRC);
% mboutIT = cellfun(@median, rboutIT);
% mboutDT = cellfun(@median, rboutDT);
% mboutRT = cellfun(@(x) median(abs(x)), rboutRT);

% Pool
tallI = {cat(1, rtrajIC{:}); cat(1, rtrajIT{:})};
tallD = {cat(1, rtrajDC{:}); cat(1, rtrajDT{:})};
tallR = {cat(1, rtrajRC{:}); cat(1, rtrajRT{:})};

ballI = {cat(1, rboutIC{:}); cat(1, rboutIT{:})};
ballD = {cat(1, rboutDC{:}); cat(1, rboutDT{:})};
ballR = {abs(cat(1, rboutRC{:})); abs(cat(1, rboutRT{:}))};

ntrajs = cellfun(@numel, tallI);

% Group for traj.
tgp1 = ones(numel(tallI{1}), 1);    % grouping variables
tgp2 = 2.*ones(numel(tallI{2}), 1);
tgp = [tgp1; tgp2];
tpallI = cat(1, tallI{:});
tpallD = cat(1, tallD{:});
tpallR = cat(1, tallR{:});

% Group for bouts
bgp1 = ones(numel(ballI{1}), 1);
bgp2 = 2.*ones(numel(ballI{2}), 1);
bgp = [bgp1; bgp2];
bpallI = cat(1, ballI{:});
bpallD = cat(1, ballD{:});
bpallR = cat(1, ballR{:});

% Shuffle
rpallI = shuffleArray(tpallI);
rpallD = shuffleArray(tpallD);
rpallR = shuffleArray(tpallR);
rallI = {rpallI(1:ntrajs(1)); rpallI(ntrajs(1) + 1 : end)};
rallD = {rpallD(1:ntrajs(1)); rpallD(ntrajs(1) + 1 : end)};
rallR = {rpallR(1:ntrajs(1)); rpallR(ntrajs(1) + 1 : end)};

% --- Display

% * Box plots traj.
figure;

% Interbout intervals
subplot(2, 2, 1);
boxchart(tgp, tpallI);
ylabel('<\delta{t}>_{traj.} (s)');

% Displacements
subplot(2, 2, 2);
boxchart(tgp, tpallD);
ylabel('<d> (mm)');

% Turn angles
subplot(2, 2, 3);
boxchart(tgp, tpallR);
ylabel('<|\delta\theta|> (deg)');

% * Box plots bouts
figure;

% Interbout intervals
subplot(2, 2, 1);
boxchart(bgp, bpallI);
ylabel('\delta{t} (s)');

% Displacements
subplot(2, 2, 2);
boxchart(bgp, bpallD);
ylabel('d (mm)');

% Turn angles
subplot(2, 2, 3);
boxchart(bgp, bpallR);
ylabel('|\delta\theta| (deg)');

% * Per batch
g = [ones(3, 1); 2*ones(3, 1)];

figure;
subplot(2, 2, 1); hold on
scatter(g, [mboutIC; mboutIT], 'k', 'filled');
for i = 1:3
    plot([1, 2], [mboutIC(i), mboutIT(i)], 'Color', [0.2, 0.2, 0.2]);
end
xlim([0.75, 2.25]);
ylim([0, 1]);
ax = gca;
ylabel('<\delta{t}> (s)');
ax.XTick = [1, 2];
ax.XTickLabel = {'Control', 'Treated'};
ax.YGrid = 'off';

subplot(2, 2, 2); hold on
scatter(g, [mboutDC; mboutDT], 'k', 'filled');
for i = 1:3
    plot([1, 2], [mboutDC(i), mboutDT(i)], 'Color', [0.2, 0.2, 0.2]);
end
xlim([0.75, 2.25]);
ylim([0, 1.6]);
ax = gca;
ylabel('<d> (mm)');
ax.XTick = [1, 2];
ax.XTickLabel = {'Control', 'Treated'};
ax.YGrid = 'off';

subplot(2, 2, 3); hold on
scatter(g, [mboutRC; mboutRT], 'k', 'filled');
for i = 1:3
    plot([1, 2], [mboutRC(i), mboutRT(i)], 'Color', [0.2, 0.2, 0.2]);
end
xlim([0.75, 2.25]);
ylim([0, 15]);
ax = gca;
ylabel('<|\delta\theta|> (deg)');
ax.XTick = [1, 2];
ax.XTickLabel = {'Control', 'Treated'};
ax.YGrid = 'off';

% * MSD
colors = lines(2);
lineopt = struct;
lineopt.Color = colors(1, :);
lineopt.LineStyle = '-';
shadopt = struct;
shadopt.FaceAlpha = 0.275;

figure;
eC = errorshaded(delaysC, mmsdC, emsdC, 'line', lineopt, 'patch', shadopt);

lineopt.Color = colors(2, :);
eT = errorshaded(delaysT, mmsdT, emsdT, 'line', lineopt, 'patch', shadopt);

% Cosmetics for MSD plots
xlabel('delay (s)');
ylabel('<d^2> (mm^2)');
legend([eC, eT], 'Control', 'Treated', 'Location', 'northwest');