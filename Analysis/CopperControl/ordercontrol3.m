% Check if the order of tested temperatures has an impact.

clear
clc

% --- Parameters
% * Definitions
explist = [pwd filesep 'Data' filesep 'Uniform_list.txt'];

refTs = [18, 22, 26, 30, 33];
pixsize = 0.09;

ngroups = 2;     % group of deltaT

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
% filt.bout_freq = 0.1;  % minimum average bout frequency
filt.n_bouts = 10;      % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Handles
cwd = strcat(pwd, filesep, 'Data', filesep);
make_trackname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');
make_stampname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');

% * Figures
colors = rainbow(numel(refTs));

% * Read experiment list
exps = readtable(explist, 'DatetimeType', 'text');
allT = exps.Temperature;
allT = cellfun(@(x) eval(['[' x ']']), allT, 'UniformOutput', false);
allruns = exps.RunNumber;
allruns = cellfun(@(x) eval(['[' x ']']), allruns, 'UniformOutput', false);
nexp = numel(allT);

% --- Init.
allprevT = cell(numel(refTs), 1);
diffTs = cell(numel(refTs), 1);
allcurrI = cell(numel(refTs), 1);
allcurrD = cell(numel(refTs), 1);
allcurrR = cell(numel(refTs), 1);

% --- Gather data
for idT = 1:numel(refTs)
    
    refT = refTs(idT);
    
    fprintf('Checking impact of testing order at T = %i', refT); tic
    
    prevT = NaN(nexp, 1);
    currI = NaN(nexp, 1);
    currD = NaN(nexp, 1);
    currR = NaN(nexp, 1);
    
    for ide = 1:nexp
        
        T = allT{ide};
        runs = allruns{ide};
        
        refidrun = find(T == refT);
        
        if isempty(refidrun) || refidrun == 1
            continue;
        end
        
        ftrack = make_trackname(exps.Date{ide}, runs(refidrun));
        fstamp = make_stampname(exps.Date{ide}, runs(refidrun));
        
        [~, i, d, t, ~, ~, f] = Tracks.getFeatures(ftrack, fstamp, filt);
        
        mi = mean(i)./f;
        md = mean(d).*pixsize;
        mr = mean(abs(t));
        
        % Get consecutive temperature differences
        prevT(ide) = T(refidrun - 1);
        currI(ide) = mi;
        currD(ide) = md;
        currR(ide) = mr;
        
        fprintf('.');
        
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % Cleanup
    prevT(isnan(prevT)) = [];
    currI(isnan(currI)) = [];
    currD(isnan(currD)) = [];
    currR(isnan(currR)) = [];
    
    % Store
    allprevT{idT} = prevT;
    allcurrI{idT} = currI;
    allcurrD{idT} = currD;
    allcurrR{idT} = currR;
    
    % Temperature difference
    diffTs{idT} = refTs(idT) - allprevT{idT};
end

% --- Processing

% Average values per temperature
mIT = cellfun(@mean, allcurrI);
mDT = cellfun(@mean, allcurrD);
mRT = cellfun(@mean, allcurrR);

% Convert to cell
cmIT = arrayfun(@(x) x, mIT, 'UniformOutput', false);
cmDT = arrayfun(@(x) x, mDT, 'UniformOutput', false);
cmRT = arrayfun(@(x) x, mRT, 'UniformOutput', false);

% Normalize all values
ncmIT = cellfun(@(x, y) (x - y)./y, allcurrI, cmIT, 'UniformOutput', false);
ncmDT = cellfun(@(x, y) (x - y)./y, allcurrD, cmDT, 'UniformOutput', false);
ncmRT = cellfun(@(x, y) (x - y)./y, allcurrR, cmRT, 'UniformOutput', false);

% Possible differences
possdiff = unique(cat(1, diffTs{:}));
ndiffs = numel(unique(cat(1, diffTs{:})));

% Pool
pdiffT = cat(1, diffTs{:});
pcurrI = cat(1, ncmIT{:});
pcurrD = cat(1, ncmDT{:});
pcurrR = cat(1, ncmRT{:});

% Group data
% Groups with same number of elements
% edges = quantile(pdiffT, linspace(0, 1, ngroups + 1));
% edges = [-15, -7.5, -3 + eps, 3, 8];
edges = [-Inf, 0, Inf];
groups = discretize(pdiffT, edges);

% --- Display
figure;
subplot(2, 2, 1); axi = gca; hold(axi, 'on');
subplot(2, 2, 2); axd = gca; hold(axd, 'on');
subplot(2, 2, 3); axr = gca; hold(axr, 'on');

% Make colors
cols = cell(numel(pdiffT), 1);
for id = 1:numel(allprevT) 
    cols{id} =  repmat(colors(id, :), size(allprevT{id}));
end
cols = cat(1, cols{:});

boxchart(axi, groups, pcurrI, 'BoxFaceColor', 'k');
scatter(axi, groups, pcurrI, [], cols, 'filled');
boxchart(axd, groups, pcurrD, 'BoxFaceColor', 'k');
scatter(axd, groups, pcurrD, [], cols, 'filled');
boxchart(axr, groups, pcurrR, 'BoxFaceColor', 'k');
scatter(axr, groups, pcurrR, [], cols, 'filled');

% Convert to cell to add stars
cI = cell(ngroups, 1);
cD = cell(ngroups, 1);
cR = cell(ngroups, 1);
xtl = cell(ngroups, 1);
for ig = 1:ngroups
    
    cI{ig} = pcurrI(groups == ig);
    cD{ig} = pcurrD(groups == ig);
    cR{ig} = pcurrR(groups == ig);
    
    xtl{ig} = ['[' num2str(edges(ig)) ',' num2str(edges(ig + 1)) '['];
    
end

axes(axi)
addStars(cI, 'test', 'ks');
axes(axd)
addStars(cD, 'test', 'ks');
axes(axr)
addStars(cR, 'test', 'ks');

xlabel(axi, '\Delta{T} (°C)');
xlabel(axd, '\Delta{T} (°C)');
xlabel(axr, '\Delta{T} (°C)');
ylabel(axi, '(<\delta{t}> - <<\delta{t}>>_T)/<<\delta{t}>>_T');
ylabel(axd, '(<d> - <<d>>_T)/<<d>>_T');
ylabel(axr, '(<|\delta\theta|> - <<|\delta\theta|>>_T)/<<|\delta\theta|>>_T');

axi.XTick = 1:ngroups;
axi.XTickLabel = xtl;
axi.YGrid = 'off';
axd.XTick = 1:ngroups;
axd.XTickLabel = xtl;
axd.YGrid = 'off';
axr.XTick = 1:ngroups;
axr.XTickLabel = xtl;
axr.YGrid = 'off';