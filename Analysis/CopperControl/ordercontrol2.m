% Check if the order of tested temperatures has an impact.

clear
clc

% --- Parameters
% * Definitions
explist = [pwd filesep 'Data' filesep 'Uniform_list.txt'];

refTs = [18, 22, 26, 30, 33];
pixsize = 0.09;

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
% filt.bout_freq = 0.1;  % minimum average bout frequency
filt.n_bouts = 10;      % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Handles
cwd = strcat(pwd, filesep, 'Data', filesep);
make_trackname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');
make_stampname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');

% * Figures
colors = rainbow(numel(refTs));
figure;
subplot(2, 2, 1); axi = gca; hold(axi, 'on'); axi.ColorOrder = colors;
subplot(2, 2, 2); axd = gca; hold(axd, 'on'); axd.ColorOrder = colors;
subplot(2, 2, 3); axr = gca; hold(axr, 'on'); axr.ColorOrder = colors;
si = cell(numel(refTs), 1);
sd = cell(numel(refTs), 1);
sr = cell(numel(refTs), 1);

% * Read experiment list
exps = readtable(explist, 'DatetimeType', 'text');
allT = exps.Temperature;
allT = cellfun(@(x) eval(['[' x ']']), allT, 'UniformOutput', false);
allruns = exps.RunNumber;
allruns = cellfun(@(x) eval(['[' x ']']), allruns, 'UniformOutput', false);
nexp = numel(allT);

% --- Init.

% --- Processing
for idT = 1:numel(refTs)
    
    refT = refTs(idT);
    
    fprintf('Checking impact of testing order at T = %i', refT); tic
    
    prevT = NaN(nexp, 1);
    currI = NaN(nexp, 1);
    currD = NaN(nexp, 1);
    currR = NaN(nexp, 1);
    
    for ide = 1:nexp
        
        T = allT{ide};
        runs = allruns{ide};
        
        refidrun = find(T == refT);
        
        if isempty(refidrun) || refidrun == 1
            continue;
        end
        
        ftrack = make_trackname(exps.Date{ide}, runs(refidrun));
        fstamp = make_stampname(exps.Date{ide}, runs(refidrun));
        
        [~, i, d, t, ~, ~, f] = Tracks.getFeatures(ftrack, fstamp, filt);
        
        mi = mean(i)./f;
        md = mean(d).*pixsize;
        mr = mean(abs(t));
        
        % Get consecutive temperature differences
        prevT(ide) = T(refidrun - 1);
        currI(ide) = mi;
        currD(ide) = md;
        currR(ide) = mr;
        
        fprintf('.');
        
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    % Cleanup
    prevT(isnan(prevT)) = [];
    currI(isnan(currI)) = [];
    currD(isnan(currD)) = [];
    currR(isnan(currR)) = [];
    
    % Average
    uT = unique(prevT);
    npT = numel(uT);
    mcI = NaN(npT, 1);
    mcD = NaN(npT, 1);
    mcR = NaN(npT, 1);
    for iT = 1:npT
        
        mcI(iT) = mean(currI(prevT == uT(iT)));
        mcD(iT) = mean(currD(prevT == uT(iT)));
        mcR(iT) = mean(currR(prevT == uT(iT)));
        
    end
    
    % --- Display
    dn = [num2str(refT) '°C'];
    si{idT} = scatter(axi, prevT, currI, [], colors(idT, :), 'filled', 'DisplayName', dn);
    p = plot(axi, uT, mcI, 'Color', colors(idT, :));
    p.Color(4) = 0.66;
    sd{idT} = scatter(axd, prevT, currD, [], colors(idT, :), 'filled', 'DisplayName', dn);
    p = plot(axd, uT, mcD, 'Color', colors(idT, :));
    p.Color(4) = 0.66;
    sr{idT} = scatter(axr, prevT, currR, [], colors(idT, :), 'filled', 'DisplayName', dn);
    p = plot(axr, uT, mcR, 'Color', colors(idT, :));
    p.Color(4) = 0.66;
end

% Cosmetic
xlabel(axi, 'previous temperature (°C)');
xlabel(axd, 'previous temperature (°C)');
xlabel(axr, 'previous temperature (°C)');
ylabel(axi, '<\delta{t}> (s)');
ylabel(axd, '<d> (mm)');
ylabel(axr, '<|\delta\theta|> (deg)');
xlim(axi, [17.5, 33.5]);
xlim(axd, [17.5, 33.5]);
xlim(axr, [17.5, 33.5]);
ylim(axi, [0, 2.5]);
ylim(axd, [0, 3]);
ylim(axr, [0, 40]);
axi.XTick = [18, 22, 26, 30, 33];
axd.XTick = [18, 22, 26, 30, 33];
axr.XTick = [18, 22, 26, 30, 33];
axi.YGrid = 'off';
axd.YGrid = 'off';
axr.YGrid = 'off';
li = legend(axi, [si{:}]);
title(li, 'Current T (°C)');