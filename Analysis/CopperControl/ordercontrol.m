% Check if the order of tested temperatures has an impact.

clear
clc

% --- Parameters
% * Definitions
explist = [pwd filesep 'Data' filesep 'Uniform_list.txt'];

pixsize = 0.09;

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
% filt.bout_freq = 0.1;   % minimum average bout frequency
filt.n_bouts = 10;      % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Handles
cwd = strcat(pwd, filesep, 'Data', filesep);
make_trackname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');
make_stampname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');

% * Read experiment list
exps = readtable(explist, 'DatetimeType', 'text');
allT = exps.Temperature;
allT = cellfun(@(x) eval(['[' x ']']), allT, 'UniformOutput', false);
allruns = exps.RunNumber;
allruns = cellfun(@(x) eval(['[' x ']']), allruns, 'UniformOutput', false);
nexp = numel(allT);

% --- Init.
diffT = cell(nexp, 1);
diffI = cell(nexp, 1);
diffD = cell(nexp, 1);
diffR = cell(nexp, 1);

% --- Processing
fprintf('Checking impact of testing order'); tic
for ide = 1:nexp
    
    T = allT{ide};
    runs = allruns{ide};
    nrun = numel(runs);
    
    mi = NaN(nrun, 1);
    md = NaN(nrun, 1);
    mr = NaN(nrun, 1);
    
    for idr = 1:nrun
        
        ftrack = make_trackname(exps.Date{ide}, runs(idr));
        fstamp = make_stampname(exps.Date{ide}, runs(idr));
    
        [~, i, d, t, ~, ~, f] = Tracks.getFeatures(ftrack, fstamp, filt);
    
        mi(idr) = mean(i)./f;
        md(idr) = mean(d).*pixsize;
        mr(idr) = mean(abs(t));
        
    end
    
    % Get consecutive temperature differences
    diffT{ide} = diff(T);
    diffI{ide} = diff(mi);
    diffD{ide} = diff(md);
    diffR{ide} = diff(mr);
    
    fprintf('.');
        
end

fprintf('\tDone (%2.2fs).\n', toc);

% Cleanup
diffT(cellfun(@isempty, diffT)) = [];
diffI(cellfun(@isempty, diffI)) = [];
diffD(cellfun(@isempty, diffD)) = [];
diffR(cellfun(@isempty, diffR)) = [];

% Substract mean
diffIM = cellfun(@(x) x - mean(x), diffI, 'UniformOutput', false);
diffDM = cellfun(@(x) x - mean(x), diffD, 'UniformOutput', false);
diffRM = cellfun(@(x) x - mean(x), diffR, 'UniformOutput', false);

diffTM = diffT;
diffTM(cellfun(@(x) isequal(numel(x), 1), diffT)) = [];
diffIM(cellfun(@(x) isequal(x, 0), diffIM)) = [];
diffDM(cellfun(@(x) isequal(x, 0), diffDM)) = [];
diffRM(cellfun(@(x) isequal(x, 0), diffRM)) = [];

% --- Display
figure;
subplot(2, 2, 1); axi = gca; hold(axi, 'on');
subplot(2, 2, 2); axd = gca; hold(axd, 'on');
subplot(2, 2, 3); axr = gca; hold(axr, 'on');

for ide = 1:numel(diffTM)
    
    scatter(axi, diffTM{ide}, diffIM{ide}, 'k', 'filled');
    scatter(axd, diffTM{ide}, diffDM{ide}, 'k', 'filled');
    scatter(axr, diffTM{ide}, diffRM{ide}, 'k', 'filled');
    
end
    
    
% --- Cosmetic
xlabel(axi, 'T_j - T_i (°C)');
ylabel(axi, '<\delta{t}>_j - <\delta{t}>_i (s)');
xlabel(axd, 'T_j - T_i (°C)');
ylabel(axd, '<d>_j - <d>_i (mm)');
xlabel(axr, 'T_j - T_i (°C)');
ylabel(axr, '<|\delta\theta|>_j - <|\delta\theta|>_i (deg)');