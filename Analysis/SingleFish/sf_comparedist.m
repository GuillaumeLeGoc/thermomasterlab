% Distributions of parameters of single fish & comparison with 26

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_allsequences_T' num2str(T) '.mat'];
fn_pk = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_featuresmatrix_pk_T' num2str(T) '.mat'];
fn_p = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_featuresmatrix_p_T' num2str(T) '.mat'];
fn_all26 = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep 'allsequences.mat'];
fn_all26_pk = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix_pk' '.mat'];
fn_all26_p = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'featuresmatrix_p' '.mat'];

% * Bins
method = 'kde';         % Method for PDF
mode = 'centers';         % Mode for bins
bws = [0.1, 0.1, 0.5, 0.05, 0.05, 0.05]; % Bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 250;
nbinst = 100;

cibi = linspace(0, 3, nbins);		% bins for interbout interval (s)
cdisp = linspace(0, 6, nbins);      % bins for displacement (mm)
cturn = linspace(-180, 180, nbins); % bins for turn angle (°, wrapped to 180)
ckf = linspace(0, 1.5, nbinst);		% bins for kflip (s^-1)
cpt = linspace(0, 1, nbinst);		% bins for pturn (bout^-1)
cpf = linspace(0, 1, nbinst);       % bins for pflip (bout^-1)

% * Load data
data = load(fn_dataset);
nfish = numel(data.xpos);
kfpt = load(fn_pk);
pfpt = load(fn_p);
all26data = load(fn_all26);
all26kfpt = load(fn_all26_pk);
all26pfpt = load(fn_all26_p);

% --- Prepare figures
colors = [cm10(nfish), repmat(0.35, nfish, 1)];
figure; hold on; axibi = gca;
figure; hold on; axdsp = gca;
figure; hold on; axreo = gca;
figure; hold on; axkfp = gca;
figure; hold on; axptr = gca;
figure; hold on; axpfp = gca;

% --- Processing (single fish)

for idF = 1:nfish
    
    % Get data
    intboutint = data.interboutintervals{idF}(:);
    turnangle = data.dtheta{idF}(:);
    displacement = data.displacements{idF}(:);
    
    intboutint(isnan(intboutint)) = [];
    turnangle(isnan(turnangle)) = [];
    displacement(isnan(displacement)) = [];
    
    kf = kfpt.feat_mat_pooled{idF}(:, 5);
    pt = kfpt.feat_mat_pooled{idF}(:, 4);
    pf = pfpt.feat_mat_pooled{idF}(:, 5);
    
    % Get pdf
    tmpibi = computePDF(cibi, intboutint, 'mode', mode, 'method', method, 'param', kdeopt(1));
    tmpdsp = computePDF(cdisp, displacement, 'mode', mode, 'method', method, 'param', kdeopt(2));
    tmptag = computePDF(cturn, turnangle, 'mode', mode, 'method', method, 'param', {'Bandwidth', bws(3)});
    tmpkfp = computePDF(ckf, kf, 'mode', mode, 'method', method, 'param', kdeopt(4));
    tmpptr = computePDF(cpt, pt, 'mode', mode, 'method', method, 'param', kdeopt(5));
    tmppfp = computePDF(cpf, pf, 'mode', mode, 'method', method, 'param', kdeopt(6));
    
    p = plot(axibi, cibi, tmpibi);
    p.Color = colors(idF, :);
    p.DisplayName = ['Fish ' num2str(idF)];
    p = plot(axdsp, cdisp, tmpdsp);
    p.Color = colors(idF, :);
    p.DisplayName = ['Fish ' num2str(idF)];
    p = plot(axreo, cturn, tmptag);
    p.Color = colors(idF, :);
    p.DisplayName = ['Fish ' num2str(idF)];
    p = plot(axkfp, ckf, tmpkfp);
    p.Color = colors(idF, :);
    p.DisplayName = ['Fish ' num2str(idF)];
    p = plot(axptr, cpt, tmpptr);
    p.Color = colors(idF, :);
    p.DisplayName = ['Fish ' num2str(idF)];
    p = plot(axpfp, cpf, tmppfp);
    p.Color = colors(idF, :);
    p.DisplayName = ['Fish ' num2str(idF)];
end

% Pooled
% Get data
intboutint = cellfun(@(x) x(:), data.interboutintervals, 'UniformOutput', false);
intboutint = cat(1, intboutint{:});
turnangle = cellfun(@(x) x(:), data.dtheta, 'UniformOutput', false);
turnangle = cat(1, turnangle{:});
displacement = cellfun(@(x) x(:), data.displacements, 'UniformOutput', false);
displacement = cat(1, displacement{:});

intboutint(isnan(intboutint)) = [];
turnangle(isnan(turnangle)) = [];
displacement(isnan(displacement)) = [];

kf = kfpt.feat_mat_pooled;
kf = cellfun(@(x) x(:, 5), kf, 'UniformOutput', false);
kf = cat(1, kf{:});
pt = kfpt.feat_mat_pooled;
pt = cellfun(@(x) x(:, 4), pt, 'UniformOutput', false);
pt = cat(1, pt{:});
pf = pfpt.feat_mat_pooled;
pf = cellfun(@(x) x(:, 5), pf, 'UniformOutput', false);
pf = cat(1, pf{:});

% Get pdf
pdfibi = computePDF(cibi, intboutint, 'mode', mode, 'method', method, 'param', kdeopt(1));
pdfdsp = computePDF(cdisp, displacement, 'mode', mode, 'method', method, 'param', kdeopt(2));
pdftag = computePDF(cturn, turnangle, 'mode', mode, 'method', method, 'param', {'Bandwidth', bws(3)});
pdfkfp = computePDF(ckf, kf, 'mode', mode, 'method', method, 'param', kdeopt(4));
pdfptr = computePDF(cpt, pt, 'mode', mode, 'method', method, 'param', kdeopt(5));
pdfpfp = computePDF(cpf, pf, 'mode', mode, 'method', method, 'param', kdeopt(6));

% --- Processing (all 26)
% Get data
intboutint_all26 = all26data.interboutintervals{3}(:);
turnangle_all26 = all26data.dtheta{3}(:);
displacement_all26 = all26data.displacements{3}(:);

intboutint_all26(isnan(intboutint_all26)) = [];
turnangle_all26(isnan(turnangle_all26)) = [];
displacement_all26(isnan(displacement_all26)) = [];

kf_all26 = all26kfpt.feat_mat_pooled{3}(:, 5);
pt_all26 = all26kfpt.feat_mat_pooled{3}(:, 4);
pf_all26 = all26pfpt.feat_mat_pooled{3}(:, 5);

% Get pdf
apdfibi = computePDF(cibi, intboutint_all26, 'mode', mode, 'method', method, 'param', kdeopt(1));
apdfdsp = computePDF(cdisp, displacement_all26, 'mode', mode, 'method', method, 'param', kdeopt(2));
apdftag = computePDF(cturn, turnangle_all26, 'mode', mode, 'method', method, 'param', {'Bandwidth', bws(3)});
apdfkfp = computePDF(ckf, kf_all26, 'mode', mode, 'method', method, 'param', kdeopt(4));
apdfptr = computePDF(cpt, pt_all26, 'mode', mode, 'method', method, 'param', kdeopt(5));
apdfpfp = computePDF(cpf, pf_all26, 'mode', mode, 'method', method, 'param', kdeopt(6));

% --- Display
plot(axibi, cibi, pdfibi, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
plot(axibi, cibi, apdfibi, 'r', 'LineWidth', 2, 'DisplayName', 'All 26°C');
xlabel(axibi, '\delta{t} (s)');
ylabel(axibi, 'pdf');
legend(axibi);

plot(axdsp, cdisp, pdfdsp, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
plot(axdsp, cdisp, apdfdsp, 'r', 'LineWidth', 2, 'DisplayName', 'All 26°C');
xlabel(axdsp, 'd (mm)');
ylabel(axdsp, 'pdf');
legend(axdsp);

plot(axreo, cturn, pdftag, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
plot(axreo, cturn, apdftag, 'r', 'LineWidth', 2, 'DisplayName', 'All 26°C');
legend(axreo);
axreo.YScale = 'log';
xlabel(axreo, '\delta\theta (deg)');
ylabel(axreo, 'pdf');
ylim(axreo, [1e-4, 1e-1]);

plot(axkfp, ckf, pdfkfp, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
plot(axkfp, ckf, apdfkfp, 'r', 'LineWidth', 2, 'DisplayName', 'All 26°C');
legend(axkfp);
xlabel(axkfp, 'k_{flip}');
ylabel(axkfp, 'pdf');

plot(axptr, cpt, pdfptr, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
plot(axptr, cpt, apdfptr, 'r', 'LineWidth', 2, 'DisplayName', 'All 26°C');
legend(axptr);
xlabel(axptr, 'p_{turn}');
ylabel(axptr, 'pdf');

plot(axpfp, cpf, pdfpfp, 'k', 'LineWidth', 2, 'DisplayName', 'Pooled');
plot(axpfp, cpf, apdfpfp, 'r', 'LineWidth', 2, 'DisplayName', 'All 26°C');
legend(axpfp);
xlabel(axpfp, 'p_{flip}');
ylabel(axpfp, 'pdf');