clear

D = 1;
tau = 100;
dt = 1;
nexp = 1000;
ntime = 1500;
delays = linspace(1, ntime, 1500);

k = 1/tau;
x0 = randn([1, nexp])*sqrt(D*tau);

ousignals = generateOU(nexp, ntime, dt, k, D, 0, x0);

ous2 = (ousignals - x0).^2;

varperexpOld = NaN(numel(delays), nexp);
varperexpNew = NaN(numel(delays), nexp);
for idt = 1:numel(delays)
    
    % Select delay
    delt = round(delays(idt)./dt);
    
    % Moving time windows
%     varout = movvar(ousignals, delt, 0, 1);
%     varperexpOld(idt, :) = mean(varout, 1);
    
    varout = var(ousignals(1:delt, :), [], 1);
    varperexpOld(idt, :) = varout;
    
%     meaout = movmean(ous2, delt, 1);
% 	varperexpNew(idt, :) = mean(meaout, 1);
    X = ousignals(1:delt, :);
    X = (X - X(1, :)).^2;
    varperexpNew(idt, :) = mean(X, 1);
    
end

% Mean
mvarouOld = mean(varperexpOld, 2);
mvarouNew = mean(varperexpNew, 2);

% Variance across exp
varacro = var(ousignals, [], 2);

T = delays;
theo = D*tau*(1 - tau./(2*T).*(1- exp(-2*T./tau)));
theo4 = D*tau*(1 - 2*tau./(T).*(1- exp(-T./(2*tau))));

intvar = cumsum(varacro)./delays';

figure; hold on
plot(0:dt:ntime-1, varacro, 'DisplayName', 'var(X(t))_{fish}');
plot(delays, mvarouOld, 'DisplayName', 'var(X(t))_{t=0:\Delta{t}}');
plot(delays, mvarouNew, 'DisplayName', '<(X(t)-X(0))^2>_{t=0:\Delta{t}}');
plot(T, theo, 'DisplayName', 'A(\Delta{t})');
plot(T, theo4, 'DisplayName', 'A2(\Delta{t})');
plot([0, ntime], [D*tau, D*tau], 'k--', 'DisplayName', 'D\tau');
ax = gca;
plot([tau tau], ax.YLim, 'k--', 'DisplayName', '\tau');
xlabel('\Delta{t} (s)');
ylabel('[X^2]');
legend('Location', 'southeast');