% Export data in matfile, version for single fish experiment

clear

% * Temperatures
T = 26;
pixsize = 0.08;

% * Files
explist_file = [pwd filesep 'Data' filesep 'Uniform1fish_list.txt'];
outfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'sf_allsequences_T' num2str(T) '.mat'];

% * Filters
filt.seq_length = 25;      % min sequence length for each fish in seconds
% filt.bout_freq = .1;     % minimum average bout frequency
filt.n_bouts = 5;          % min number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Displacement fix
theta_threshold = 10;    % deg
displ_factor = 1.4;

% Build file names
[ftrack, fstamp, nfish] = getExperimentList(T, [], explist_file);

% * Init. arrays
xpos = cell(size(ftrack, 1), 1);
ypos = cell(size(ftrack, 1), 1);
interboutintervals = cell(size(ftrack, 1), 1);
displacements = cell(size(ftrack, 1), 1);
dtheta = cell(size(ftrack, 1), 1);
bouttime = cell(size(ftrack, 1), 1);
framerates = cell(size(ftrack, 1), 1);

% Get features from each experiment
fprintf('Gathering data '); tic

for idexp = 1:size(ftrack, 1)
        
    [exp_xpo, exp_ibi, exp_dsp, exp_reo, exp_fra, ~, framerate, exp_ypo] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
    
    nseq = size(exp_ibi, 2);
    
    % Loop over sequence
    x = cell(nseq, 1);
    y = cell(nseq, 1);
    i = cell(nseq, 1);
    d = cell(nseq, 1);
    r = cell(nseq, 1);
    t = cell(nseq, 1);
    toclean = false(nseq, 1);
    for s = 1:nseq
        
        % Position
        trajx = exp_xpo(:, s);
        trajx(isnan(trajx)) = [];
        trajx = trajx.*pixsize;
        x{s} = trajx;
        
        trajy = exp_ypo(:, s);
        trajy(isnan(trajy)) = [];
        trajy = trajy.*pixsize;
        y{s} = trajy;
        
        % Interbout interval
        trajibi = exp_ibi(:, s);
        trajibi(isnan(trajibi)) = [];
        trajibi = trajibi./framerate;
        i{s} = trajibi;
        
        % Turn angle
        trajreo = exp_reo(:, s);
        trajreo(isnan(trajreo)) = [];
        r{s} = trajreo;
        isturn = abs(trajreo) > theta_threshold;
        trireo = trajreo;
        trireo(~isturn) = 0;
        trireo(trireo < 0) = -1;
        trireo(trireo > 0) = +1;
        % filter out if not all three bout types are present
        if numel(unique(trireo)) < 3
            toclean(s) = true;
            continue;
        end
        
        % Displacement
        trajdsp = exp_dsp(:, s);
        trajdsp(isnan(trajdsp)) = [];
        trajdsp = trajdsp.*pixsize;
        trajdsp(isturn) = trajdsp(isturn)./displ_factor;
        d{s} = trajdsp;
        
        % Time vector of bouts
        trajt = exp_fra(:, s);
        trajt(isnan(trajt)) = [];
        trajt = trajt./framerate;
        t{s} = trajt;
        
    end
    
    % Cleanup
    x(toclean) = [];
    y(toclean) = [];
    i(toclean) = [];
    d(toclean) = [];
    r(toclean) = [];
    t(toclean) = [];
    
    % Match number of bouts by filling with NaNs
    x = fixCellSize(x);
    y = fixCellSize(y);
    i = fixCellSize(i);
    d = fixCellSize(d);
    r = fixCellSize(r);
    t = fixCellSize(t);
    
    % Concatenate & store
    xpos{idexp} = cat(2, x{:});
    ypos{idexp} = cat(2, y{:});
    interboutintervals{idexp} = cat(2, i{:});
    displacements{idexp} = cat(2, d{:});
    dtheta{idexp} = cat(2, r{:});
    bouttime{idexp} = cat(2, t{:});
    framerates{idexp} = framerate;
    
    fprintf('%i.', idexp);
end

fprintf('\tDone (%2.2fs.)\n', toc);

META = 'nfish x 1 cell. nbout x nsequences arrays. Trailing NaNs to match sizes. x, y, dsp in mm, time in secs. Turn displacements fixed.';
save(outfile, 'xpos', 'ypos', ...
    'interboutintervals', 'displacements', 'dtheta', ...
    'bouttime', 'framerates', 'META');
