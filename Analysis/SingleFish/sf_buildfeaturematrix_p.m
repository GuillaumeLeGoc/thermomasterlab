% Build feature matrix and save it. With rates for flip and proba for turns

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_allsequences_T' num2str(T) '.mat'];
fn_out = [pwd filesep 'Data' filesep 'Matfiles' filesep 'sf_featuresmatrix_p_T' num2str(T) '.mat'];

% * Settings
theta_threshold = 10;    % deg
ncorr = 10;
pflipmethod = 'nplus1';    % method to extract pflip, 'acf' or 'nplus1'

% * Load data
data = load(fn_dataset);
nfish = numel(data.xpos);

% --- Processing
feat_mat_pooled = cell(nfish, 1);
ttraj_pooled = cell(nfish, 1);
trajduration_pooled = cell(nfish, 1);
for idF = 1:nfish
	
    fprintf('Creating sequences for fish %i...', idF); tic
    
    % Get data
    bouttime = data.bouttime{idF};
    intboutint = data.interboutintervals{idF};
    turnangle = data.dtheta{idF};
    displacement = data.displacements{idF};
    
    nseq = size(intboutint, 2);
    
    allfeat = NaN(nseq, 5);
    toclean = false(nseq, 1);
    ttraj = NaN(nseq, 1);
    trajduration = NaN(nseq, 1);
    for s = 1:nseq
        
        % - Trajectory time point
        timevec = bouttime(:, s);
        timevec(isnan(timevec)) = [];
        ttraj(s) = timevec(1);
        
        % - Interbout interval
        ibi = intboutint(:, s);
        ibi(isnan(ibi)) = [];
        
        
        trajduration(s) = sum(ibi);
        
        % - Turn angles
        dtheta = turnangle(:, s);
        dtheta(isnan(dtheta)) = [];
        isturn = abs(dtheta) > theta_threshold;
        
        % pturn
        nturns = sum(abs(dtheta) > theta_threshold);
        nbouts = numel(dtheta);
        pturn = nturns/nbouts;
        
        % pflip
        dthetabin = dtheta;
        dthetabin(dtheta < -theta_threshold) = -1;
        dthetabin(dtheta > theta_threshold) = 1;
        dthetabin(abs(dtheta) < theta_threshold) = 0;
        switch pflipmethod
            case 'acf'
                dthetabin(dthetabin == 0) = [];
                [xco, lag] = xcorr(dthetabin, ncorr, 'biased');
                selected_inds = find(lag == 0):find(lag == 0) + ncorr;
                xco = xco(selected_inds)./max(xco(selected_inds));
                x = 1:ncorr;
                g = fittype(@(pflip, x) (pturn^2).*(1 - 2*pflip).^x);
                fitt = fit(x', xco(2:end), g, 'StartPoint', 0.25);
            case 'nplus1'
                % Create lagged vectors
                dthetai = dthetabin(1:end  - 1);
                dthetan = dthetabin(2:end);
                binedges = [-1, 0, 1, 1 + eps];
                bindti = discretize(dthetai, binedges);
                meandtn = accumarray(bindti, dthetan, [], @mean, NaN);
                if any(isnan(meandtn)) || numel(meandtn) < 3
                    toclean(s) = true;
                    continue;
                end
                g = @(pflip, x) pturn*(1-2*pflip).*x;
                fitt = fit((-1:1)', meandtn, g, 'StartPoint', 0.25, ...
                    'Lower', 0, 'Upper', 1);
                if fitt.pflip == 0 || fitt.pflip == 1
                    toclean(s) = true;  % ignore failed fit
                    continue;
                end
        end
        
        % mean turn
        dtheta(~isturn) = [];
        dtheta = abs(dtheta);
        sigturn = mean(dtheta);
        
        % displacement
        displ = displacement(:, s);
        displ(isnan(displ)) = [];
        
        % Fill array
        allfeat(s, 1) = sigturn;
        allfeat(s, 2) = mean(ibi);
        allfeat(s, 3) = mean(displ);
        allfeat(s, 4) = pturn;
        allfeat(s, 5) = fitt.pflip;
        
    end
    
    % Cleanup
    allfeat(toclean, :) = [];
    ttraj(toclean) = [];
    trajduration(toclean) = [];
    
    ttraj_pooled{idF} = ttraj;
    feat_mat_pooled{idF} = allfeat;
    trajduration_pooled{idF} = trajduration;
    
    fprintf('\t Done (%2.2fs)\n', toc);
    
end

% --- Save file
if ~isempty(fn_out)
    fprintf('Saving...'); tic
    META = 'nsequences x 5 features. dtheta(turn) - ibi - d - pturn - kflip';
    save(fn_out, 'META', 'feat_mat_pooled', 'ttraj_pooled', 'trajduration_pooled');
    fprintf('\t Done (%2.2fs)\n', toc);
else
    warning('Matrix not saved, specify a nonempty file name.');
end