% Display several trajectories from different temperatures.

% close all
clear
clc

% --- Parameters
T = 26;

fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_allsequences_T' num2str(T) '.mat'];

ntrials = 1;

% * Trajectory
nbouts = 30;

% * Pool
len = [0, 100];
wid = [0, 45];

% * Figures
scolor0 = [0, 0, 0];
scolor1 = [0.5, 0.5, 0.5];
alpha = 0.5;
xybox = [len(1), wid(1), len(2), wid(2)];
cx = 5/len(2);
cy = 5/wid(2);  % curvature of pool angles
basemarkersize = 18;    % 1s = this area of marker size

% * Load data
data = load(fn_dataset);
X = data.xpos;
Y = data.ypos;
I = data.interboutintervals;
B = data.bouttime;
nfish = size(X, 1);

% --- Processing
for idF = 13%1:nfish
    
    % * Prepare figure
    fig = figure; hold on
    ax = gca;
    axis equal
    xlim(ax, len);
    ylim(ax, wid);
    grid off
    box off
    axis off
    m = rectangle('Position', xybox, 'Curvature', [cx, cy]);
    m.EdgeColor = [0.2, 0.2, 0.2];
    
    % Get data
    x = X{idF};
    y = Y{idF};
    i = I{idF};
    b = B{idF};
    ntraj = size(x, 2);
    
    % Pick random trajectory in first or second half
    idtraj0 = 59;%randi(round(ntraj/2), 1);
    idtraj1 = 17;%randi([round(ntraj/2) + 1, ntraj], 1);
    
    % Select corresponding data (first half)
    xx = x(:, idtraj0);
    xx(isnan(xx)) = [];
    yy = y(:, idtraj0);
    yy(isnan(yy)) = [];
    ii = i(:, idtraj0);
    ii(isnan(ii)) = [];
    ii = [ii; 1];
    
    color = scolor0;
    p = plot(ax, xx, yy);
    p.Color = [0.5, 0.5, 0.5, 0.5];
    p.LineWidth = 1;
    s = scatter(ax, xx, yy, ii*basemarkersize, color, 'filled');
    plot(ax, xx(1), yy(1), '.k');   % start point
    
    % Select corresponding data (second half)
    xx = x(:, idtraj1);
    xx(isnan(xx)) = [];
    yy = y(:, idtraj1);
    yy(isnan(yy)) = [];
    ii = i(:, idtraj1);
    ii(isnan(ii)) = [];
    ii = [ii; 1];
    
    color = scolor1;
    p = plot(ax, xx, yy);
    p.Color = [0.5, 0.5, 0.5, 0.5];
    p.LineWidth = 1;
    s = scatter(ax, xx, yy, ii*basemarkersize, color, 'filled');
    plot(ax, xx(1), yy(1), '.k');   % start point
    
    % add legend
    scatter(3, 5, basemarkersize/2, 'filled', 'MarkerFaceColor', 'k');
    scatter(5, 5, basemarkersize, 'filled', 'MarkerFaceColor', 'k');
    scatter(7, 5, basemarkersize*2, 'filled', 'MarkerFaceColor', 'k');
    
    text(2, 3, '.5');
    text(4, 3, '1');
    text(6, 3, '2s');
    
    title(['Fish ' num2str(idF)]);
end