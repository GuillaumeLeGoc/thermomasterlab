% Plot distance travelled during forward bouts and turn bouts

% close all
clear
clc

% --- Parameters
explist_file = [pwd filesep 'Data' filesep 'Uniform1fish_list.txt'];

% * Definitions
pixsize = 0.08;
theta_threshold = 10;    % deg

% * Temperatures
T = 26;

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
filt.bout_freq = .1;   % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% Build file names
[ftrack, fstamp] = getExperimentList(T, [], explist_file);

nfish = numel(ftrack);

% --- Prepare arrays
dfwd = cell(nfish, 1);
dtrn = cell(nfish, 1);

% --- Loop over each experiments
for idx_exp = 1:size(ftrack, 1)
    
    [~, ~, displ, turnangle] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt);
    
    displ_fwd = displ.*pixsize;
    displ_trn = displ.*pixsize;
    displ_fwd(abs(turnangle) > theta_threshold) = [];
    displ_trn(abs(turnangle) < theta_threshold) = [];
    
    dfwd{idx_exp} = displ_fwd;
    dtrn{idx_exp} = displ_trn;
    
    fprintf('.');
end

dfwd = cat(1, dfwd{:});
dtrn = cat(1, dtrn{:});

% --- Average
mdispl_fwd = mean(dfwd);
edispl_fwd = std(dfwd)./sqrt(numel(dfwd));
mdispl_trn = mean(dtrn);
edispl_trn = std(dtrn)./sqrt(numel(dtrn));
mratio = mdispl_trn./mdispl_fwd;
eratio = mratio.*sqrt( (edispl_fwd./mdispl_fwd).^2 + (edispl_trn./mdispl_trn).^2);

fprintf('d(turn)/d(forward) = %1.4f +/- %0.4f\n', mratio, eratio);