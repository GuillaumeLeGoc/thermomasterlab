% Parameters mean from each single fish

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_allsequences_T' num2str(T) '.mat'];
fn_all26 = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep 'allsequences.mat'];

% * Bins
method = 'kde';         % Method for PDF
mode = 'centers';       % Mode for bins
bws = [0.1, 0.1, 0.5, 0.05, 0.05]; % Bandwidths for KDE
kdeopt = @(n) {'BandWidth', bws(n), 'Support', 'positive'};
nbins = 250;
nbinst = 100;

cibi = linspace(0, 3, nbins);                         % bins for interbout interval (s)
cdisp = linspace(0, 6, nbins);                        % bins for displacement (mm)
cturn = linspace(-180, 180, nbins);                   % bins for turn angle (°, wrapped to 180)

% * Load data
data = load(fn_dataset);
nfish = numel(data.xpos);
data_all26 = load(fn_all26);

% --- Processing (single fish)
MeanIBI = NaN(nfish, 1);
MeanDSP = NaN(nfish, 1);
MediIBI = NaN(nfish, 1);
MediDSP = NaN(nfish, 1);
for idF = 1:nfish
    
    % Get data
    intboutint = data.interboutintervals{idF}(:);
    turnangle = data.dtheta{idF}(:);
    displacement = data.displacements{idF}(:);
    
    intboutint(isnan(intboutint)) = [];
    turnangle(isnan(turnangle)) = [];
    displacement(isnan(displacement)) = [];
    
    MeanIBI(idF) = mean(intboutint);
    MediIBI(idF) = median(intboutint);
    MeanDSP(idF) = mean(displacement);
    MediDSP(idF) = median(displacement);
    
end

meanibi_all26 = mean(data_all26.interboutintervals{3}, 'all', 'omitnan');
meandsp_all26 = mean(data_all26.displacements{3}, 'all', 'omitnan');

figure; hold on
plot(1:nfish, MeanIBI, 'k');
plot(1:nfish, MediIBI, 'r');
plot(1:nfish, repmat(meanibi_all26, [nfish, 1]), '--k');
xlabel('Fish #');
ylabel('\delta{t} (s)');
legend('Mean', 'Median', 'Mean all 26°C');

figure; hold on
plot(1:nfish, MeanDSP, 'k');
plot(1:nfish, MediDSP, 'r');
plot(1:nfish, repmat(meandsp_all26, [nfish, 1]), '--k');
xlabel('Fish #');
ylabel('d (mm)');
legend('Mean', 'Median', 'Mean all 26°C');