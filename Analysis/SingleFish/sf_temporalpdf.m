% Check for steady state in single fish experiments.

% close all
clear
clc

% --- Parameters
% * Temperature
T = 26;

% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_allsequences_T' num2str(T) '.mat'];

% * Settings
ntiers = 6;
maxtime = 3600*2;   % experiment duration in seconds

% * Bins
nbins = 50;
mode = 'edges';
method = 'kde';
bi = linspace(0, 5, nbins + 1);
bd = linspace(0, 5, nbins + 1);

% * Load data
data = load(fn_dataset);
nfish = numel(data.xpos);

colors = cm10(nfish);

% * Init
ipool = cell(nfish, ntiers);
dpool = cell(nfish, ntiers);

% --- Processing
for idF = 1:nfish
        
    % Get data
    b = data.bouttime{idF};
    i = data.interboutintervals{idF};
    d = data.displacements{idF};
    
    % Full time vector
    b(1, :) = [];
    timvec = linspace(0, maxtime, ntiers + 1);
    
    % Split data into n temporal parts
    for idtier = 1:ntiers
        
        % Find bouts in this range
        start = timvec(idtier);
        stop = timvec(idtier + 1);
        
        I = find(b > start & b <= stop);
        
        % Limit to those indices
        itier = i(I);
        dtier = d(I);
        
        % Store
        ipool{idF, idtier} = itier;
        dpool{idF, idtier} = dtier;

    end
    
    % Create figures
    figi = figure; axi = gca; hold on
    figd = figure; axd = gca; hold on
    
    % Pool all experiments & get pdf
    for idtier = 1:size(ipool, 2)
        
        if isempty(ipool{idF, idtier})
            continue;
        end
        
        % Compute pdf
        [pdfi, ci] = computePDF(bi, ipool{idF, idtier}, 'mode', mode, 'method', method);
        [pdfd, cd] = computePDF(bd, dpool{idF, idtier}, 'mode', mode, 'method', method);
        
        % Display
        q = plot(axi, ci, pdfi);
        q.Color = colors(idtier, :);
        q.DisplayName = [num2str(idtier) 'th part'];
        xlabel(axi, '\delta{t} [s]');
        ylabel(axi, 'pdf');
        legend(axi);
        
        r = plot(axd, cd, pdfd);
        r.Color = colors(idtier, :);
        r.DisplayName = [num2str(idtier) 'th part'];
        xlabel(axd, 'd [mm]');
        ylabel(axd, 'pdf');
        legend(axd);
        
    end
        
end