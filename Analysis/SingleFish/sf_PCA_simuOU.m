% Attempt to fit manually a OU process to the actual data (one fish).

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'sf_featuresmatrix_pk_T' num2str(T) '.mat'];
fileallfish = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk.mat'];

% * Settings
dtres = 1;
timevec = 0:dtres:7200-1;
delays = 1:10:7200-1;
ncorrtime = 7200-1;
ncorr = round(ncorrtime./dtres);

% * Load file
ftmp = load(filefeatmat);
featmat = ftmp.feat_mat_pooled;
nfish = numel(featmat);
nfeat = size(featmat{1}, 2);
ttraj = ftmp.ttraj_pooled;
ftmp = load(fileallfish);

all26_coefPC1 = ftmp.pc1intrap;
all26_coefPC2 = ftmp.pc2intrap;
sfac26 = ftmp.sfac;
all26_varprojPC1 = var(ftmp.pc1proj{3});
all26_varprojPC2 = var(ftmp.pc2proj{3});

% * Simulation settings
nit = 20;   % total number of runs
rangetau1 = linspace(2000, 3000, 1000);          % 1/k, k = restoring force term
rangetau2 = linspace(1900, 2100, 1000);
rangeD1 = linspace(6.5e-4, 6.6e-4, 50);         % diffusion coef
rangeD2 = linspace(2.0e-4, 2.2e-4, 50);
ntime = timevec(end);
nexp = 500;
mu = 0;
x0 = 0;
dt = dtres;

% Init. simu
bacf1 = NaN(ncorr + 1, nit);
bacf2 = NaN(ncorr + 1, nit);
bvar1 = NaN(numel(delays), nit);
bvar2 = NaN(numel(delays), nit);
btau1 = NaN(nit, 1);
btau2 = NaN(nit, 1);
bD1 = NaN(nit, 1);
bD2 = NaN(nit, 1);
rtau1 = NaN(nit, 1);
rtau2 = NaN(nit, 1);
rD1 = NaN(nit, 1);
rD2 = NaN(nit, 1);

% --- PCA for each fish in data
% Init.
pc1proj = cell(nfish, 1);
pc2proj = cell(nfish, 1);

% * Projection on PC
for idF = 1:nfish
    
    samplefeatmat = featmat{idF};
    
    sfm = samplefeatmat./sfac26;
    
    % Projection on all base
    projonpc1 = sfm*all26_coefPC1;
    projonpc2 = sfm*all26_coefPC2;
    
    % Store
    pc1proj{idF} = projonpc1;
    pc2proj{idF} = projonpc2;
end

% Resample proj on PC1
pc1projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc1proj, 'UniformOutput', false);
pc1projres = cat(1, pc1projres{:});
pc2projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc2proj, 'UniformOutput', false);
pc2projres = cat(1, pc2projres{:});

% Select data
pcavec1 = pc1projres;
pcavec2 = pc2projres;
pcavec12 = [pcavec1', pcavec2'];

% * ACF
[apfa, laga] = corOU(pcavec12, ncorr);
apf1 = apfa(:, 1:nfish);
apf2 = apfa(:, (nfish + 1):end);
mlpf1 = laga;
mlpf2 = laga;

% Mean
mapf1 = mean(apf1, 2);
eapf1 = std(apf1, [], 2)./sqrt(nfish);
mapf2 = mean(apf2, 2);
eapf2 = std(apf2, [], 2)./sqrt(nfish);

% * PCA variance over time
vpfa = varOU(delays, pcavec12, dtres);
vpf1 = vpfa(:, 1:nfish);
vpf2 = vpfa(:, (nfish + 1):end);
mvpf1 = mean(vpf1, 2);
evpf1 = std(vpf1, [], 2)./sqrt(nfish);
mvpf2 = mean(vpf2, 2);
evpf2 = std(vpf2, [], 2)./sqrt(nfish);

% --- Simulation of OU process
for it = 1:nit
    
    fprintf('Simulations, iteration %i...\n', it);
    
    fprintf('Fitting ACF to find best tau...'); tic;
    
    % - Tau estimation
    sweepmcorrou1 = NaN(ncorr + 1, numel(rangetau1));
    sweepmcorrou2 = NaN(ncorr + 1, numel(rangetau1));
    sweeplcorrou1 = NaN(ncorr + 1, numel(rangetau1));
    sweeplcorrou2 = NaN(ncorr + 1, numel(rangetau1));
    
    for idtau = 1:numel(rangetau1)
        
        k1 = 1./rangetau1(idtau);
        k2 = 1./rangetau2(idtau);
        
        ousignals1 = generateOU(nexp, ntime, dt, k1, 1, mu, x0);
        ousignals2 = generateOU(nexp, ntime, dt, k2, 1, mu, x0);
        
        ous12 = [ousignals1, ousignals2];
        [apfa, laga] = corOU(ous12, ncorr);
        ape1 = apfa(:, 1:nexp);
        ape2 = apfa(:, (nexp + 1):end);
        
        % Mean
        sweeplcorrou1(:, idtau) = laga;
        sweepmcorrou1(:, idtau) = mean(ape1, 2);
        sweeplcorrou2(:, idtau) = laga;
        sweepmcorrou2(:, idtau) = mean(ape2, 2);
    end
    
    % * Residual sum of square
    rssacf1 = sum( (sweepmcorrou1 - repmat(mapf1, 1, numel(rangetau1))).^2);
    [rtau1(it), rargminacf1] = min(rssacf1);
    bacf1(:, it) = sweepmcorrou1(:, rargminacf1);
    besttau1 = rangetau1(rargminacf1);
    btau1(it) = besttau1;
    
    rssacf2 = sum( (sweepmcorrou2 - repmat(mapf2, 1, numel(rangetau2))).^2);
    [rtau2(it), rargminacf2] = min(rssacf2);
    bacf2(:, it) = sweepmcorrou2(:, rargminacf2);
    besttau2 = rangetau2(rargminacf2);
    btau2(it) = besttau2;
    
    fprintf('\tDone(%2.2fs).\n', toc);
    
    % --- Apply selected tau to fit var(t)
    fprintf('Sweeping for best D...'); tic;
    sweepmvarou1 = NaN(numel(delays), numel(rangeD1));
    sweepmvarou2 = NaN(numel(delays), numel(rangeD2));
    
    parfor idD = 1:numel(rangeD1)
        
        k1 = 1./besttau1;
        k2 = 1./besttau2;
        
        d1 = rangeD1(idD);
        d2 = rangeD2(idD);
        
        ousignals1 = generateOU(nexp, ntime, dt, k1, d1, mu, x0);
        ousignals2 = generateOU(nexp, ntime, dt, k2, d2, mu, x0);
        
        ous12 = [ousignals1, ousignals2];
        vpea = varOU(delays, ous12, dt);
        vpe1 = vpea(:, 1:nexp);
        vpe2 = vpea(:, (nexp + 1):end);
        
        % Mean
        sweepmvarou1(:, idD) = mean(vpe1, 2);
        sweepmvarou2(:, idD) = mean(vpe2, 2);
    end
    
    % * Residual sum of square
    rssvar1 = sum( (sweepmvarou1 - repmat(mvpf1, 1, numel(rangeD1))).^2);
    [rD1(it), rargminvar1] = min(rssvar1);
    bvar1(:, it) = sweepmvarou1(:, rargminvar1);
    bestD1 = rangeD1(rargminvar1);
    bD1(it) = bestD1;
    
    rssvar2 = sum( (sweepmvarou2 - repmat(mvpf2, 1, numel(rangeD1))).^2);
    [rD2(it), rargminvar2] = min(rssvar2);
    bvar2(:, it) = sweepmvarou2(:, rargminvar2);
    bestD2 = rangeD2(rargminvar2);
    bD2(it) = bestD2;
    
    % % * Analytical solution
    % T = delays;
    % theo1 = bestD1*besttau1*(1 - 2*besttau1./T.*(1- exp(-T./(2*besttau1))));
    % theo2 = bestD2*besttau2*(1 - 2*besttau2./T.*(1- exp(-T./(2*besttau2))));
    
    fprintf('\tDone(%2.2fs).\n', toc);
    
end

% Generate OU extended signals with best parameters
% fprintf('Generate extended signal...'); tic;
% bestou1 = generateOU(nexp, extdelays(end), dt, 1/besttau1, bestD1, mu, x0);
% bestou2 = generateOU(nexp, extdelays(end), dt, 1/besttau2, bestD2, mu, x0);
%
% mextvar1 = mean(var(bestou1, [], 1));
% mextvar2 = mean(var(bestou2, [], 1));
%
% fprintf('\tDone (%2.2fs).\n', toc);

% --- Get mean +/- sem of best signals and tau & D
facf1 = mean(bacf1, 2);
eacf1 = std(bacf1, [], 2)./sqrt(nit);
facf2 = mean(bacf2, 2);
eacf2 = std(bacf2, [], 2)./sqrt(nit);
fvar1 = mean(bvar1, 2);
evar1 = std(bvar1, [], 2)./sqrt(nit);
fvar2 = mean(bvar2, 2);
evar2 = std(bvar2, [], 2)./sqrt(nit);
ftau1 = mean(btau1);
etau1 = std(btau1)./sqrt(nit);
ftau2 = mean(btau2);
etau2 = std(btau2)./sqrt(nit);
fD1   = mean(bD1);
eD1   = std(bD1)./sqrt(nit);
fD2   = mean(bD2);
eD2   = std(bD2)./sqrt(nit);

% --- Display
% * ACF PC1
figure; hold on;

% - Data
lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean ACF from data';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf1, mapf1, eapf1, 'line', lopt, 'patch', sopt);

% - Simulation
pf = plot(laga, facf1, 'r');
pf.DisplayName = ['OU simu, \tau=' num2str(ftau1, '%4.0f') '\pm' num2str(etau1, '%2.0f') 's'];

% - Cosmetic
xlabel('Delays (s)');
ylabel('ACF(Proj. on PC1)');
legend([pm, pf]);

% * ACF PC2
figure; hold on;

% - Data
lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean ACF from data';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf2, mapf2, eapf2, 'line', lopt, 'patch', sopt);

% - Simulation
pf = plot(laga, facf2, 'r');
pf.DisplayName = ['OU simu, \tau=' num2str(ftau2, '%4.0f') '\pm' num2str(etau2, '%2.0f') 's'];

xlabel('Delays (s)');
ylabel('ACF(Proj. on PC2)');
legend([pm, pf]);

% * Variance on PC1
figure; hold on;

% - Data
lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Moving variance from data';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(delays, mvpf1, evpf1, 'line', lopt, 'patch', sopt);
% pa = plot([delays(1), delays(end)], [all26_varprojPC1, all26_varprojPC1]);
% pa.LineStyle = '--';
% pa.Color = [0, 0, 0];
% pa.DisplayName = 'Multi-fish';
ylim([0, 0.8]);

plot([ftau1, ftau1], [0, 0.8], 'k--', 'DisplayName', '\tau');

% - Simulation
pf = plot(delays, fvar1, 'r');
pf.DisplayName = ['OU simu, D=' num2str(fD1), '\pm' num2str(eD1)];

% % - Analytical
% pt = plot(T, theo1);
% pt.DisplayName = 'Analytical';

% - Cosmetics
xlabel('\Delta{t} [s]');
ylabel('<var(proj on PC1)_{\Delta{t}}>_{fish}');
legend([pm, pf], 'Location', 'southeast');

% * Variance on PC2
figure; hold on;

% - Data
lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Moving variance from data';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(delays, mvpf2, evpf2, 'line', lopt, 'patch', sopt);
% pa = plot([delays(1), delays(end)], [all26_varprojPC2, all26_varprojPC2]);
% pa.LineStyle = '--';
% pa.Color = [0, 0, 0];
% pa.DisplayName = 'Multi-fish';
ylim([0, 0.25]);
plot([ftau2, ftau2], [0, 0.25], 'k--', 'DisplayName', '\tau');

% - Simulation
pf = plot(delays, fvar2, 'r');
pf.DisplayName = ['OU simu, D=' num2str(fD2) '\pm' num2str(eD2)];

% % - Analytical
% pt = plot(T, theo2);
% pt.DisplayName = 'Analytical';

xlabel('\Delta{t} [s]');
ylabel('<var(proj on PC2)_{\Delta{t}}>_{fish}');
legend([pm, pf], 'Location', 'southeast');