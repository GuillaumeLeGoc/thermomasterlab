% PCA for single fish experiments : check the intra versus inter individual
% variability.
% Features matrix order :
% 1 - turn amplitude
% 2 - ibi
% 3 - d
% 4 - pturn
% 5 - pflip

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'sf_featuresmatrix_pk_T' num2str(T) '.mat'];
fileallfish = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk.mat'];

% * Settings
projbins = linspace(-5, 10, 75);
timevec = 0:1:7200-1;
delays = 1:10:7200-1;
ncorrtime = 7200-1;

% * Load file
ftmp = load(filefeatmat);
featmat = ftmp.feat_mat_pooled;
nfish = numel(featmat);
ntrajs = cellfun(@(x) size(x, 1), featmat);
nfeat = size(featmat{1}, 2);
ttraj = ftmp.ttraj_pooled;
trajduration = ftmp.trajduration_pooled;

ftmp = load(fileallfish);
all26_coefPC1 = ftmp.pc1intrap;
all26_coefPC2 = ftmp.pc2intrap;
all26_projPC1 = ftmp.pc1proj{3};
all26_projPC2 = ftmp.pc2proj{3};
sfac26 = ftmp.sfac;

% * Figures
fishtoshow = 13;%1:nfish;
colors = cm20(nfish);

% --- PCA for each fish
% Init.
coeffpc1 = NaN(nfeat, nfish);
coeffpc2 = NaN(nfeat, nfish);
explained = NaN(nfeat, nfish);
pc1proj = cell(nfish, 1);
pc2proj = cell(nfish, 1);
pdf_pc1proj = NaN(size(projbins, 2), nfish);
pdf_pc2proj = NaN(size(projbins, 2), nfish);
pc1pc2grp = cell(nfish, 1);
dname = cell(nfish + 1, 1);

% * PCA
for idF = 1:nfish
    
    samplefeatmat = featmat{idF};
    
    sfm = samplefeatmat./std(samplefeatmat);
    
    [coeff, ~, ~, ~, explained(:, idF)] = pca(sfm);
    
    if coeff(2, 1) > 0
        coeff(:, 1) = -coeff(:, 1);
    end
    
    coeffpc1(:, idF) = coeff(:, 1);
    coeffpc2(:, idF) = coeff(:, 2);
    
    dname{idF} = ['Fish' num2str(idF)];
    
    % Projection on all 26 base
    sfmproj = samplefeatmat./sfac26;
    projonpc1 = sfmproj*all26_coefPC1;
    projonpc2 = sfmproj*all26_coefPC2;
    
    % PDF of projections on PC1 & 2
    pdf_pc1proj(:, idF) = computePDF(projbins, projonpc1, 'method', 'kde', 'param', {'Bandwidth', 0.5});
    pdf_pc2proj(:, idF) = computePDF(projbins, projonpc2, 'method', 'kde', 'param', {'Bandwidth', 0.5});
    
    pc1proj{idF} = projonpc1;
    pc2proj{idF} = projonpc2;
    pc1pc2grp{idF} = idF.*ones(numel(projonpc1), 1);    % grouping variable
    
end

% Projection from all fish at 26°C pdf
all26_pdfprojpc1 = computePDF(projbins, all26_projPC1, 'method', 'kde', 'param', {'Bandwidth', 0.5});
all26_pdfprojpc2 = computePDF(projbins, all26_projPC2, 'method', 'kde', 'param', {'Bandwidth', 0.5});

% * Align traces
dtres = mean(diff(timevec));
pc1projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc1proj, 'UniformOutput', false);
pc1projres = cat(1, pc1projres{:});
pc2projres = cellfun(@(x, y) interp1(x - x(1), y, timevec, 'linear', y(end)), ttraj, pc2proj, 'UniformOutput', false);
pc2projres = cat(1, pc2projres{:});

% try with nan instead of interp
% pc1projres = NaN(nfish, numel(timevec));
% pc2projres = NaN(nfish, numel(timevec));
% for idf = 1:nfish
%     
% 	ids = round(ttraj{idf}./dtres) + 1; 
%     pc1projres(idf, ids) = pc1proj{idf};
%     pc2projres(idf, ids) = pc2proj{idf};
%     
% end

% * PCA proj. variance inter & intra
% - All var
% varot1 = var(pc1projres, [], 1);
% varot2 = var(pc2projres, [], 1);
mvarall1 = var(cat(1, pc1proj{:}));
evarall1 = bootci(1000, @var, cat(1, pc1proj{:}));
mvarall2 = var(cat(1, pc2proj{:}));
evarall2 = bootci(1000, @var, cat(1, pc2proj{:}));

% - Intra
varof1 = var(pc1projres, [], 2);
varof2 = var(pc2projres, [], 2);
mvof1 = mean(varof1);
svof1 = std(varof1)./sqrt(size(varof1, 1));
evof1 = [mvof1 - svof1; mvof1 + svof1];
mvof2 = mean(varof2);
svof2 = std(varof2)./sqrt(size(varof2, 1));
evof2 = [mvof2 - svof2; mvof2 + svof2];

% % * Moving variance
% varperfish1 = NaN(numel(delays), nfish);
% varperfish2 = NaN(numel(delays), nfish);
% % varperfish_shuf = NaN(numel(delays), nfish);
% semvarperfish1 = NaN(numel(delays), nfish);
% semvarperfish2 = NaN(numel(delays), nfish);
% % semvarperfish_shuf = NaN(numel(delays), nfish);
% for idF = 1:nfish
%     
%     % Select data
%     pcavec1 = pc1projres(idF, :) - mean(pc1projres(idF, :));
%     pcavec2 = pc2projres(idF, :) - mean(pc2projres(idF, :));
%     
% %     pcavec_shuf = shuffleArray(pcavec); % shuffle (control)
%     
%     % Init.
%     varperdt1 = NaN(numel(delays), 1);
%     varperdt2 = NaN(numel(delays), 1);
% %     varperdt_shuf = NaN(numel(delays), 1);
%     semvarperdt1 = NaN(numel(delays), 1);
%     semvarperdt2 = NaN(numel(delays), 1);
% %     semvarperdt_shuf = NaN(numel(delays), 1);
%     
%     % Processing
%     for idt = 1:numel(delays)
%     
%         % Select delay
%         dt = round(delays(idt)./dtres);
%         
%         % Non-overlapping time windows
% %         % Make sure the vector is divisible
% %         pctoproc = pcavec;
% %         pctoproc_shuf = pcavec_shuf;
% %         remain = mod(numel(pctoproc), dt);
% %         if remain
% %             pctoproc(end:end+(dt-remain)) = NaN;
% %             pctoproc_shuf(end:end+(dt-remain)) = NaN;
% %         end
% %         
% %         % Split by time windows
% %         pcavecdt = reshape(pctoproc, dt, []);
% %         pcavecdt_shuf = reshape(pctoproc_shuf, dt, []);
% %         
% %         % Get variance & mean variance
% %         varout = nanvar(pcavecdt, [], 1);
% %         varout_shuf = nanvar(pcavecdt_shuf, [], 1);
% %         nwindows = numel(pctoproc);
% 
%         % Moving time windows
%         varout = movvar(pcavec1, dt);
% %         varout_shuf = movvar(pcavec_shuf, dt);
%         nwindows = numel(pcavec1)./dt;  % number of indep. windows
%         varperdt1(idt) = mean(varout);
%         semvarperdt1(idt) = std(varout)./sqrt(nwindows);
% %         varperdt_shuf(idt) = mean(varout_shuf);
% %         semvarperdt_shuf(idt) = std(varout_shuf)./sqrt(nwindows);
% 
%         varout = movvar(pcavec2, dt);
%         nwindows = numel(pcavec2, dt);
%         varperdt2(idt) = mean(varout);
%         semvarperdt2(idt) = std(varout)./sqrt(nwindows);
%     end
%     
%     % Store
%     varperfish1(:, idF) = varperdt1;
%     semvarperfish1(:, idF) = semvarperdt1;
%     varperfish2(:, idF) = varperdt2;
%     semvarperfish2(:, idF) = semvarperdt2;
%     
% %     varperfish_shuf(:, idF) = varperdt_shuf;
% %     semvarperfish_shuf(:, idF) = semvarperdt_shuf;
% end

% Mean
% mvpf1 = mean(varperfish1, 2);
% evpf1 = std(varperfish1, [], 2)./sqrt(size(varperfish1, 2));
% mvpf2 = mean(varperfish2, 2);
% evpf2 = std(varperfish2, [], 2)./sqrt(size(varperfish2, 2));

% - ACF
ncorr = round(ncorrtime./dtres);
acfperfish1 = NaN(ncorr + 1, nfish);
acfperfish2 = NaN(ncorr + 1, nfish);
lagperfish1 = NaN(ncorr + 1, nfish);
lagperfish2 = NaN(ncorr + 1, nfish);
% acfperfish_shuf = NaN(ncorr + 1, nfish);

for idF = 1:nfish
    
    pcavec1 = pc1projres(idF, :);
    pcavec1 = pcavec1 - mean(pcavec1, 'omitnan'); % center
%     pcavec_shuf = shuffleArray(pcavec1); % shuffle (control)
    pcavec2 = pc2projres(idF, :);
    pcavec2 = pcavec2 - mean(pcavec2, 'omitnan');
    
    [xco, lag] = xcorr_withnan(pcavec1', ncorr);
%     [xco, lag] = xcorr(pcavec1, ncorr, 'normalized');
%     xco_shuf = xcorr(pcavec_shuf, ncorr, 'normalized');
    selected_inds = find(lag == 0):find(lag == 0) + ncorr;
    acfperfish1(:, idF) = xco(selected_inds);
    lagperfish1(:, idF) = lag(selected_inds).*dtres;
%     acfperfish_shuf(:, idF) = xco_shuf(selected_inds);

    [xco, lag] = xcorr_withnan(pcavec2', ncorr);
%     [xco, lag] = xcorr(pcavec2, ncorr, 'normalized');
    selected_inds = find(lag == 0):find(lag == 0) + ncorr;
    acfperfish2(:, idF) = xco(selected_inds);
    lagperfish2(:, idF) = lag(selected_inds).*dtres;
end

% Mean
mlpf1 = mean(lagperfish1, 2);
mapf1 = mean(acfperfish1, 2);
eapf1 = std(acfperfish1, [], 2)./sqrt(size(acfperfish1, 2));
mlpf2 = mean(lagperfish2, 2);
mapf2 = mean(acfperfish2, 2);
eapf2 = std(acfperfish2, [], 2)./sqrt(size(acfperfish2, 2));

% --- Display

% - Cloud projections
f1 = figure('Name', '2DPCSpaceProj');
tbl = table(cat(1, pc1proj{:}), cat(1, pc2proj{:}), cat(1, pc1pc2grp{:}), 'VariableNames', {'PC1 proj.', 'PC2 proj.', 'Fish'});
scatterhistogram(f1, tbl, 'PC1 proj.', 'PC2 proj.', 'GroupVariable', 'Fish', ...
    'HistogramDisplayStyle', 'smooth', 'LineStyle', '-', 'LineWidth', 2, ...
    'Color', colors, 'MarkerAlpha', 0.25);
legend;
xlim([-3, 6]);
ylim([1.8660, 10.0849]);

% - Projection over time
figure('Name', 'All1DProjPC1'); 
axpot1 = gca; hold on; axpot1.ColorOrder = colors;
figure('Name', 'All1DProjPC2');
axpot2 = gca; hold on; axpot2.ColorOrder = colors;
q = cell(numel(fishtoshow), 1);
r = cell(numel(fishtoshow), 1);
for idF = fishtoshow
    
    % 2D
    figure('Name', ['2DProjFish' num2str(idF)]); hold on
    scatter(pc2proj{idF}, pc1proj{idF}, [], ttraj{idF}, 'filled');
    
    x = pc1proj{idF}';
    y = pc2proj{idF}';
    z = zeros(size(x));
    t = ttraj{idF}';
%     surface([x; x], [y; y], [z; z], [t; t],...
%             'facecol','interp',...
%             'edgecol','interp',...
%             'linew',2);
	c = colorbar;
    title(c, 'Time (s)');
    xlabel('Projection on PC1');
    ylabel('Projection on PC2');
    title(['Fish ' num2str(idF)]);
    
    % 1D, all, PC1
    x = ttraj{idF};
    xm = x;
    xi = x;
    xf = x;
    d = trajduration{idF};
    y =  pc1proj{idF};% - pc1proj{idF}(1);
    xxx = cell(numel(x), 1);
    yyy = cell(numel(y), 1);
    for s = 1:numel(x)
        xx = [x(s) x(s) + d(s)];
        xm(s) = x(s) + d(s)/2;
        xf(s) = x(s) + d(s);
        yy = [y(s) y(s)];
        px = plot(axpot1, xx, yy);
        px.Color = colors(idF, :);
        
        xxx{s} = [xi(s), xm(s), xf(s)];
        yyy{s} = [y(s), y(s), y(s)];
    end
    
    xxx = [xxx{:}];
    yyy = [yyy{:}];
    
    p = plot(axpot1, xm, y, '.');
    p.Color = colors(idF, :);
    q{idF} = plot(axpot1, xxx, yyy, '-');
    q{idF}.Color = [colors(idF, :), 0.25];
    q{idF}.DisplayName = ['Fish ' num2str(idF)];
    
    % 1D, all, PC2
    x = ttraj{idF};
    xm = x;
    xi = x;
    xf = x;
    d = trajduration{idF};
    y =  pc2proj{idF};% - pc1proj{idF}(1);
    xxx = cell(numel(x), 1);
    yyy = cell(numel(y), 1);
    for s = 1:numel(x)
        xx = [x(s) x(s) + d(s)];
        xm(s) = x(s) + d(s)/2;
        xf(s) = x(s) + d(s);
        yy = [y(s) y(s)];
        px = plot(axpot2, xx, yy);
        px.Color = colors(idF, :);
        
        xxx{s} = [xi(s), xm(s), xf(s)];
        yyy{s} = [y(s), y(s), y(s)];
    end
    
    xxx = [xxx{:}];
    yyy = [yyy{:}];
    
    p = plot(axpot2, xm, y, '.');
    p.Color = colors(idF, :);
    r{idF} = plot(axpot2, xxx, yyy, '-');
    r{idF}.Color = [colors(idF, :), 0.25];
    r{idF}.DisplayName = ['Fish ' num2str(idF)];
end

xlabel(axpot1, 'Time (s)');
ylabel(axpot1, 'Projection on PC1');
legend(axpot1, [q{:}], 'Location', 'southeast');
xlabel(axpot2, 'Time (s)');
ylabel(axpot2, 'Projection on PC2');
legend(axpot2, [r{:}], 'Location', 'southeast');

% - Per fish PC1 coefficients
figure('Name', 'PerFishPC1Coeff'); hold on
axmpc1 = gca; axmpc1.ColorOrder = colors;
coeffpc1full = coeffpc1;
dname{end} = 'All 26°C';
axmpc1.ColorOrder(nfish + 1, :) = [0, 0, 0];
bar(axmpc1, coeffpc1full, 'EdgeColor', 'none');
plot(axmpc1, 1:nfeat, all26_coefPC1, 'k');
% axis(axmpc1, 'square');
axmpc1.XTick = 1:nfeat;
axmpc1.XTickLabel = {'\delta\theta_t', '\delta{t}', 'd', 'p_{turn}', 'k_{flip}'};
ylabel(axmpc1, 'PC1 coefficient');
axmpc1.YLim = [-1, 1];
legend(axmpc1, dname, 'Location', 'southeast');

% - Per fish PC2 coefficients
figure('Name', 'PerFishPC2Coeff'); hold on
axmpc2 = gca; axmpc2.ColorOrder = colors;
coeffpc1full = coeffpc2;
dname{end} = 'All 26°C';
axmpc2.ColorOrder(nfish + 1, :) = [0, 0, 0];
bar(axmpc2, coeffpc1full, 'EdgeColor', 'none');
plot(axmpc2, 1:nfeat, all26_coefPC2, 'k');
% axis(axmpc2, 'square');
axmpc2.XTick = 1:nfeat;
axmpc2.XTickLabel = {'\delta\theta_t', '\delta{t}', 'd', 'p_{turn}', 'k_{flip}'};
ylabel(axmpc2, 'PC2 coefficient');
axmpc2.YLim = [-1, 1];
legend(axmpc2, dname, 'Location', 'southeast');

% - Explained variance
figure('Name', 'ExplVar'); hold on
ax = gca;
ax.ColorOrder = colors;
bar(explained, 'EdgeColor', 'none');
ax.XTick = 1:5;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
leg = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
legend(leg, 'AutoUpdate', 'off');
% axis square
ax.XGrid = 'off';
ylabel(ax, 'Explained variance (%)');

% - PDF of projections on PC1
figure('Name', 'ProjPC1pdf'); hold on
ylim([-0.05, 0.9]);
p = cell(nfish);
for idF = 1:nfish
    
    p{idF} = plot(projbins, pdf_pc1proj(:, idF));
    p{idF}.Color = colors(idF, :);
    p{idF}.DisplayName = ['Fish' num2str(idF) ', ' num2str(ntrajs(idF)) ' traj.'];
    
end

pp = plot(projbins, all26_pdfprojpc1, 'k', 'DisplayName', 'All 26°C');

axis square
xlabel('Projection on PC1');
ylabel('pdf');
legend([p{:}, pp], 'Location', 'northwest');

% - PDF of projections on PC2
figure('Name', 'ProjPC2pdf'); hold on
ylim([-0.05, 0.9]);
p = cell(nfish);
for idF = 1:nfish
    
    p{idF} = plot(projbins, pdf_pc2proj(:, idF));
    p{idF}.Color = colors(idF, :);
    p{idF}.DisplayName = ['Fish' num2str(idF) ', ' num2str(ntrajs(idF)) ' traj.'];
    
end

pp = plot(projbins, all26_pdfprojpc2, 'k', 'DisplayName', 'All 26°C');

axis square
xlabel('Projection on PC2');
ylabel('pdf');
legend([p{:}, pp], 'Location', 'northwest');

% % - Variance on PC1 per fish over time
% figure('Name', 'VarPerFishTimePC1'); hold on
% axvpft = gca;
% % figure('Name', 'VarPerFishTimeSHUF'); hold on
% % axvpfts = gca;
% plts = cell(nfish, 1);
% pltss = cell(nfish, 1);
% for idF = 1:nfish
%     
%     lineopt = struct;
%     lineopt.DisplayName = ['Fish ' num2str(idF)];
%     lineopt.Color = colors(idF, :);
%     lineopt.LineAlpha = 0.25;
%     lineopt.Marker = 'none';
%     lineopt.LineWidth = 1;
%     shadopt = struct;
%     shadopt.FaceAlpha = 0.1;
%     
%     y = varperfish1(:, idF);
%     ey = semvarperfish1(:, idF);
%     
%     plts{idF} = errorshaded(delays, y, ey, 'line', lineopt, 'patch', shadopt, 'ax', axvpft);
%     
% %     y = varperfish_shuf(:, idF);
% %     ey = semvarperfish_shuf(:, idF);
%     
% %     pltss{idF} = errorshaded(delays, y, ey, 'line', lineopt, 'patch', shadopt, 'ax', axvpfts);
% end
% 
% lopt = struct;
% lopt.Color = [0.2, 0.2, 0.2];
% lopt.DisplayName = 'Mean';
% lopt.Marker = 'none';
% lopt.LineWidth = 2;
% sopt = struct;
% sopt.FaceAlpha = 0.275;
% pm = errorshaded(delays, mvpf1, evpf1, 'line', lopt, 'patch', sopt, 'ax', axvpft);
% 
% pp = plot(axvpft, [0, delays(end)], [var(all26_projPC1), var(all26_projPC1)], 'k--', 'DisplayName', 'All 26°C');
% 
% xlabel(axvpft, '\Delta{t} [s]');
% ylabel(axvpft, '<var(PC1 proj.)_{\Delta{t}}>');
% legend(axvpft, [plts{:}, pm, pp], 'Location', 'northwest');
% title(axvpft, 'Intra-individual variability');
% 
% % xlabel(axvpfts, '\Delta{t} [s]');
% % ylabel(axvpfts, '<var(pca proj.)_{\Delta{t}}> (shuffled)');
% % legend(axvpfts, [pltss{:}], 'Location', 'northwest');
% 
% % - Variance on PC2 per fish over time
% figure('Name', 'VarPerFishTimePC2'); hold on
% axvpft = gca;
% plts = cell(nfish, 1);
% for idF = 1:nfish
%     
%     lineopt = struct;
%     lineopt.DisplayName = ['Fish ' num2str(idF)];
%     lineopt.Color = colors(idF, :);
%     lineopt.LineAlpha = 0.25;
%     lineopt.Marker = 'none';
%     lineopt.LineWidth = 1;
%     shadopt = struct;
%     shadopt.FaceAlpha = 0.1;
%     
%     y = varperfish2(:, idF);
%     ey = semvarperfish2(:, idF);
%     
%     plts{idF} = errorshaded(delays, y, ey, 'line', lineopt, 'patch', shadopt, 'ax', axvpft);
%     
% end
% 
% lopt = struct;
% lopt.Color = [0.2, 0.2, 0.2];
% lopt.DisplayName = 'Mean';
% lopt.Marker = 'none';
% lopt.LineWidth = 2;
% sopt = struct;
% sopt.FaceAlpha = 0.275;
% pm = errorshaded(delays, mvpf2, evpf2, 'line', lopt, 'patch', sopt, 'ax', axvpft);
% 
% pp = plot(axvpft, [0, delays(end)], [var(all26_projPC2), var(all26_projPC2)], 'k--', 'DisplayName', 'All 26°C');
% 
% xlabel(axvpft, '\Delta{t} [s]');
% ylabel(axvpft, '<var(PC2 proj.)_{\Delta{t}}>');
% legend(axvpft, [plts{:}, pm, pp], 'Location', 'northwest');
% title(axvpft, 'Intra-individual variability');

% - ACF on PC1 per fish
figure('Name', 'ACFPC1PerFish'); hold on
ax = gca; ax.ColorOrder = colors;

px = plot(lagperfish1, acfperfish1);
px = arrayfun(@(s) setfield(s, 'Color', [s.Color, 0.5]), px);

lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf1, mapf1, eapf1, 'line', lopt, 'patch', sopt);

legs = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
legs{end + 1} = 'Mean';
legend([px; pm], legs);
xlabel('Delays (s)');
ylabel('ACF(Proj. on PC1)');

% % Shuffle
% figure('Name', 'ACFvarPerFishSHUF'); hold on
% ax = gca; ax.ColorOrder = colors;
% plot(lagperfish, acfperfish_shuf);
% legs = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
% legend(legs);
% xlabel('Delays (s)');
% ylabel('ACF(Proj. on PC1 (centered, shuffled)');

% - ACF on PC2 per fish
figure('Name', 'ACFPC2PerFish'); hold on
ax = gca; ax.ColorOrder = colors;

px = plot(lagperfish2, acfperfish2);
px = arrayfun(@(s) setfield(s, 'Color', [s.Color, 0.5]), px);

lopt = struct;
lopt.Color = [0.1, 0.1, 0.1];
lopt.DisplayName = 'Mean';
lopt.Marker = 'none';
lopt.LineWidth = 2;
sopt = struct;
sopt.FaceAlpha = 0.275;
pm = errorshaded(mlpf2, mapf2, eapf2, 'line', lopt, 'patch', sopt);

legs = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);
legs{end + 1} = 'Mean';
legend([px; pm], legs);
xlabel('Delays (s)');
ylabel('ACF(Proj. on PC2)');

% - Variances across fish & time (inter & intra)
% figure('Name', 'VarTime'); hold on
% plot(timevec, varot1);
% plot(timevec, varot2);
% legend('PC1', 'PC2');
% xlabel('Time (s)');
% ylabel('Var(popC1)_{fish}');

figure('Name', 'VarInterIntra'); hold on; ax = gca;
X = [mvof1, mvarall1; mvof2, mvarall2];
E = cat(3, [evof1'; evof2'], [evarall1'; evarall2']);
cols = lines(7);
ax.ColorOrder = [cols(4, :); cols(5, :)];
b = bar(X, 'EdgeColor', 'none');

[ngroups, nbars] = size(X);
for idb = 1:nbars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, E(:, :, idb), 'Color', [0.01, 0.01, 0.01]);
end

ax.XTick = 1:2;
ax.XTickLabel = {'PC1', 'PC2'};
ylabel(ax, 'Variance');
legend(ax, 'Intra', 'Pooled');