% Plot residence time in left or right state distributions & PSD

% close all
clear
% clc

% --- Parameters
% * Temperatures
T = 26;

% * File names
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'sf_allsequences_T' num2str(T) '.mat'];

% * Fit parameters
theta_threshold = 10;   % deg

% * Bins
nbins = 250;
freq = logspace(log10(5e-2), log10(1), nbins)';
ncorr = 500;            % frames

% * Get data
data = load(fn_dataset);
nfish = size(data.dtheta, 1);

% --- Prepare figures
colors = cm10(nfish);
figs = cell(2, 1);
figs{1} = figure; hold on
figs{1}.Name = 'SFTelegraphACFPooled';
figs{2} = figure; hold on
figs{2}.Name = 'SFTelegraphPSDPooled';
plt_acf = cell(numel(T), 1);
plt_psd = cell(numel(T), 1);

% --- Processing
nu_acf = NaN(nfish, 1);
nu_psd = NaN(nfish, 1);
ci_psd = NaN(nfish, 2);
ci_acf = NaN(nfish, 2);
kflips = NaN(nfish, 1);
ci_kflips = NaN(nfish, 2);
for idF = 1:nfish
        
    fprintf(['Computing ACF of RTS for fish  ', num2str(idF)]); tic
    
    % Get data
    turnangle = data.dtheta{idF};
    time = data.bouttime{idF};
    framerate = data.framerates{idF};
    nseq = size(turnangle, 2);
    
    mdtheta = mean(turnangle, 'all', 'omitnan');    % fish bias
    mibi = mean(data.interboutintervals{idF}, 'all', 'omitnan');
    istrn = abs(turnangle) > theta_threshold;
    pturn = sum(istrn, 'all')./sum(~isnan(turnangle), 'all');
    
    pooled_pxx = NaN(nseq, numel(freq));
    pooled_acf = NaN(nseq, ncorr + 1);
    pooled_plg = NaN(nseq, ncorr + 1);
    dthetai = cell(nseq, 1);
    dthetan = cell(nseq, 1);
    toclean = false(nseq, 1);
    for seq = 1:nseq
        
        % Bouts
        dtheta = turnangle(:, seq);
        dtheta(isnan(dtheta)) = [];
        dtheta = dtheta - mdtheta;
        
        % Binarize turn angles
        dthetacopy = dtheta;
        dtheta(dthetacopy > theta_threshold) = 1;
        dtheta(dthetacopy < -theta_threshold) = -1;
        dtheta(abs(dthetacopy) < theta_threshold) = NaN;
        
        % Create the telegraph signal
        t = time(:, seq);
        t(isnan(t)) = [];
        tvec = t(1:end - 1) - t(1);
        fvec = round(tvec*framerate);
        fr = fvec(1):fvec(end);
        dthetatime = NaN(size(fr));
        dthetatime(fvec + 1) = dtheta;
        dthetatime = fillmissing(dthetatime, 'previous');
        dthetatime = fillmissing(dthetatime, 'next');
        
        if numel(unique(dthetatime)) < 2
            toclean(seq) = true;
            continue
        end
        
        % Create lagged vectors for pflip
        dtheta(isnan(dtheta)) = 0;
        dthetai{seq} = dtheta(1:end  - 1);
        dthetan{seq} = dtheta(2:end);
        
        % Get ACF
        [xco, lag] = xcorr(dthetatime, ncorr, 'normalized');
        selected_inds = find(lag == 0):find(lag == 0) + ncorr;
        pooled_acf(seq, :) = xco(selected_inds);
        pooled_plg(seq, :) = lag(selected_inds)./framerate;
        
        % Get PSD
        pooled_pxx(seq, :) = periodogram(dthetatime, hamming(length(dthetatime)), freq, framerate);
    end
	
    pooled_plg(toclean, :) = [];
    pooled_acf(toclean, :) = [];
    pooled_pxx(toclean, :) = [];
    
    % --- kflip
    % - ACF
    macf = mean(pooled_acf, 'omitnan');
    mplg = mean(pooled_plg, 'omitnan');
    eacf = std(pooled_acf, [], 1, 'omitnan')./sqrt(size(pooled_acf, 1));
    
    % - PSD
    mpxx = mean(pooled_pxx, 1, 'omitnan');
    epxx = std(pooled_pxx, [], 1, 'omitnan')./sqrt(size(pooled_pxx, 1));
    
    % * Fit
    % - ACF
    expfit = @(nu, x) exp(-2*x.*nu);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', 0, 'Upper', Inf, 'StartPoint', 1);
    mdl = fittype(expfit, 'options', fo);
    ft_acf = fit(mplg', macf', mdl);
    
    % - PXX
    lorenfit = @(nu, I, x) (I*4*nu)./(4*nu^2 + (2*pi*x).^2);
    fo = fitoptions('Method', 'NonlinearLeastSquares', 'Lower', [0 0], 'Upper', [10 100], 'StartPoint', [0.25 1]);
    mdl = fittype(lorenfit, 'options', fo);
    ft_psd = fit(freq, mpxx', mdl);
    
	nu_acf(idF) = ft_acf.nu;
    nu_psd(idF) = ft_psd.nu;
    dummy = confint(ft_psd);
    ci_psd(idF, :) = dummy(:, 1)';
    dummy = confint(ft_acf);
    ci_acf(idF, :) = dummy(:, 1)';
    
    % --- pflip
    % Pool sequences
    dthetai = cat(1, dthetai{:});
    dthetan = cat(1, dthetan{:});
    
    % Binning
    binedges = [-1, 0, 1, 1 + eps];
    bindti = discretize(dthetai, binedges);                 % regroup in bins
    meandtn = accumarray(bindti, dthetan, [], @mean, NaN);  % average over bins
    stddtn = accumarray(bindti, dthetan, [], @std, NaN);    % std
    % Get sem
    nelmtsperbin = histcounts(dthetai, binedges);
    semdtn = stddtn./sqrt(nelmtsperbin');
    
    % Fit
    yfit = @(pflip, x) pturn.*(1-2*pflip).*x;
    ft = fit((-1:1)', meandtn, yfit, 'StartPoint', 0.25);
    kflips(idF) = ft.pflip./mibi;
    dummy = confint(ft);
    ci_kflips(idF, :) = dummy(:, 1)'./mibi;
    
    % - Display
    lineopt = struct;
    lineopt.Color = colors(idF, :);
    lineopt.LineStyle = 'none';
    lineopt.Marker = '.';
    lineopt.MarkerSize = 8;
    lineopt.DisplayName = ['Fish ' num2str(idF)];
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    set(0, 'CurrentFigure', figs{1});
    plt_acf{idF} = errorshaded(mplg(1:4:end), macf(1:4:end), eacf(1:4:end), 'line', lineopt, 'patch', shadopt);
    pfit = plot(mplg, ft_acf(mplg));
    pfit.Color = colors(idF, :);
    
    set(0, 'CurrentFigure', figs{2});
    plt_psd{idF} = plot(freq, 10*log10(mpxx));
    plt_psd{idF}.DisplayName = ['Fish ' num2str(idF)];
    plt_psd{idF}.Color = colors(idF, :);
    plt_psd{idF}.LineStyle = 'none';
    plt_psd{idF}.Marker = '.';
    plt_psd{idF}.MarkerSize = 8;
    qfit = plot(freq, 10*log10(ft_psd(freq)));
    qfit.Color = colors(idF, :);
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% - Cosmetic
set(0, 'CurrentFigure', figs{1});
fkfit = plot([NaN, NaN], [NaN, NaN], 'k');
fkfit.DisplayName = 'Fit';
legend([plt_acf{:}, fkfit]);
xlabel('Delays (s)');
ylabel('ACF');
xlim([0, 20.25]);
axis square

set(0, 'CurrentFigure', figs{2});
fkfit = plot([NaN, NaN], [NaN, NaN], 'k');
fkfit.DisplayName = 'Fit';
legend([plt_psd{:}, fkfit], 'Location', 'southwest');
ax = gca;
ax.XScale = 'log';
axis square
xlabel('Frequency (Hz)');
ylabel('Power (dB/Hz)');

% - Plot extracted nu values
f = figure; hold on
f.Name = 'kFlipFromPooledPSD';
col = [0.8, 0, 0];
coltransp = 0.9;
pp = plot(1:nfish, kflips);
pp.Color = [col, coltransp];
pp.DisplayName = 'p_{flip}/<\delta{t}>';
neg = nu_psd - ci_psd(:, 1);
pos = nu_psd - ci_psd(:, 2);
e = errorbar(1:nfish, nu_psd, neg, pos);
e.DisplayName = 'k_{flip} from PSD';
e.Marker = 'diamond';
e.Color = [0.5 0.5 0.5];
xlabel('Fish #');
ylab = ylabel('k_{flip} (s^{-1})');
title('Telegraph time constant');
neg = nu_acf - ci_acf(:, 1);
pos = nu_acf - ci_acf(:, 2);
p = errorbar(1:nfish, nu_acf, neg, pos);
p.Marker = 'o';
p.Color = 'k';
p.DisplayName = 'k_{flip} from ACF';
legend([e, p, pp], 'Location', 'southeast');
axis square