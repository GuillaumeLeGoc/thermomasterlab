function [position, itboutval, displ, turnangle, frames, allframes, framerate, ypos] = getFeatures(file_tracks, file_stamp, filt, pooled, thresholds, bout_window)
% [position, itboutval, displ, turnangle, frames, allframes, framerate, ypos] = TRACKS.GETFEATURES(file_tracks, file_stamp, filt, pooled, thresholds)
% TRACKS.GETFEATURES gather bouts feratures found in tracking file
% FILES_TRACKS, using framerate found in FILES_STAMPS and parameters
% specified by THRESHOLDS for bout detection. FILT is used to ignore
% trajectories that doesn't meet condition. POOLED specifies if
% TRACKS.GETFEATURES should return pooled features from all trajectories or
% separate each sequences in different columns.
%
% INPUTS :
% ------
% file_tracks : string giving the path to the tracking file.
% file_stamps : string giving the path to the stamps file.
% filt : stucture with two elements,
%   - filt.seq_length : minimum sequence length in seconds.
%   - filt.bout_freq : mimimum number of bouts per seconds during the
%   sequence
%   - filt.n_bouts : if specified, take sequence with only at least this
%   amount of bouts
%   - filt.minx : remove if fish has a x position < minx
%   - filt.maxx : remove if fish has a x position > maxx
% thresholds (optional) : structure for thresholds of bouts detection. See
% BOUTS.FIND for more infos.
% pooled (optional, default is true) : boolean, if false, each sequences
% are placed on a different columns. Filling is made with NaNs.
%
% RETURNS :
% -------
% position : 1D vector containing all position of bouts.
% itboutval : 1D vector containing all interbout intervals.
% displ : 1D vector containing all displacement made during all bouts.
% turnangle : 1D vector containing all turn angles between two bouts.
% frames : 1D vector containing frame number where bouts occured.
% [NOTE : if pooled is false, all 5 previous outputs are 2D matrix,
% n_frames x n_sequences]
% framerate : mean framerate during the experiment.

% --- Default values
if ~exist('pooled', 'var')
    pooled = true;
elseif isempty(pooled)
    pooled = true;
end
if ~exist('thresholds', 'var')
    thresholds = struct;
end
if isfield(filt, 'n_bouts')
    n_bouts = @(x, y) filt.n_bouts;
elseif isfield(filt, 'bout_freq')
    n_bouts = @(fr, N) filt.bout_freq*N/fr;
end
if ~exist('bout_window', 'var')
    bout_window = 0;
end

threshold_theta = deg2rad(120);

% --- Check input
if iscell(file_tracks)
    file_tracks = file_tracks{:};
end
if iscell(file_stamp)
    file_stamp = file_stamp{:};
end

% --- Load data
T = readtable(file_tracks);
M = load(file_stamp, 't');

framerate = 1/mean(diff(M.t));
n_ids = max(T.id);
n_frames = max(T.imageNumber);

% --- Preparation
position = NaN(n_frames + 1, 0);
itboutval = NaN(n_frames, 0);
displ = NaN(n_frames, 0);
turnangle = NaN(n_frames, 0);
frames = NaN(n_frames + 1, 0);
allframes = NaN(n_frames + 1, 0);
ypos = NaN(n_frames + 1, 0);

for id = 0:n_ids
    
    % Read data
    X = T.xBody(T.id == id);
    Y = T.yBody(T.id == id);
    A = T.tBody(T.id == id);
    F = T.imageNumber(T.id == id);
    
    % Find discontinuities in the sequence
    df = diff(F);
    frame_disc = [0 ; find(df>1) ; numel(F)];
    n_subseq = numel(frame_disc);
    
    % Loop on subsequences
    for idx_subseq = 1:n_subseq - 1
        
        start = frame_disc(idx_subseq) + 1;
        stop = frame_disc(idx_subseq + 1);
        x = X(start:stop);
        y = Y(start:stop);
        a = A(start:stop);
        f = F(start:stop);
        
        % Compute angles to detect tracking errors
        da = diff(a);
        da = atan2(sin(da), cos(da));
        idxtocorr = find(abs(da) > threshold_theta);
%         a(idxtocorr(1:2:end) + 1) = wrapTo2Pi(a(idxtocorr(1:2:end) + 1) + pi);
        angle_disc = [0 ; idxtocorr - 1; numel(a)];
%         angle_disc = [0 ; numel(a)];
        n_subsubseq = numel(angle_disc);
        
        for idx_subsubseq = 1:n_subsubseq - 1
            
            start2 = angle_disc(idx_subsubseq) + 1;
            stop2 = angle_disc(idx_subsubseq + 1);
            xx = x(start2:stop2);
            yy = y(start2:stop2);
            aa = a(start2:stop2);
            ff = f(start2:stop2);

            % Filters
            if any(xx < filt.minx) || any(xx > filt.maxx)
                continue;
            end
            
            if numel(xx) < filt.seq_length*framerate
                continue;
            end
            
            % Find bouts
%             b = Bouts.find_angle(xx, yy, aa, framerate, thresholds);
%             B{1} = b;
            B = Bouts.find(xx, yy, framerate, thresholds, bout_window);
            
            % Filter
            if numel(B{:}) < n_bouts(framerate, numel(xx))
                continue;
            end
            
            % * frames
            ff = Bouts.limit(ff, B);
            ff(isnan(ff)) = [];
            
            % * x position
            x_b = Bouts.limit(xx, B);
            x_b(isnan(x_b)) = [];
            p = NaN(n_frames + 1, 1);
            p(ff + 1) = x_b;
            position(:, end + 1) = p;
            
            % * Interbout intervals
            i = NaN(n_frames, 1);
            i(ff(2:end) + 1) = diff(B{1});
            itboutval(:, end + 1) = i;
            
            % * Displacements
            y_b = Bouts.limit(yy, B);
            y_b(isnan(y_b)) = [];
            dx = diff(x_b);
            dy = diff(y_b);
            d = NaN(n_frames, 1);
            d(ff(2:end) + 1) = sqrt(dx.^2 + dy.^2);
            displ(:, end + 1) = d;
            
            % * Turn angle
            angles = Bouts.limit(aa, B);
            angles(isnan(angles)) = [];
            dangles = diff(angles);
            t = NaN(n_frames, 1);
            t(ff(2:end) + 1) = rad2deg(atan2(sin(dangles), cos(dangles)));
            turnangle(:, end + 1) = t;
            
            % * Frames
            fr = NaN(n_frames + 1, 1);
            fr(ff + 1) = ff;
            frames(:, end + 1) = fr;
            
            % * Non-bouts-limited frames
            af = NaN(n_frames + 1, 1);
            af(F(start2:stop2) + 1) = F(start2:stop2);
            allframes(:, end + 1) = af;
            
            % * x position
            y_b = Bouts.limit(yy, B);
            y_b(isnan(y_b)) = [];
            py = NaN(n_frames + 1, 1);
            py(ff + 1) = y_b;
            ypos(:, end + 1) = py;
        end
    end
end

if pooled
    position = position(:); position(isnan(position)) = [];
    itboutval = itboutval(:); itboutval(isnan(itboutval)) = [];
    displ = displ(:); displ(isnan(displ)) = [];
    turnangle = turnangle(:); turnangle(isnan(turnangle)) = [];
    frames = frames(:); frames(isnan(frames)) = [];
end
end