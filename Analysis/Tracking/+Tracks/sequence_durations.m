% Plot distribution of sequences durations.

% close all
clear
clc

% --- Parameters
% * Definitions
T = [18, 22, 26, 30, 33];
nbins = 100;
figure; hold on
colors = rainbow(numel(T));

bins = linspace(0, 120, nbins);

% --- Loop over experiments
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    % Build file names
    [ftrack, fstamp] = getExperimentList(temperature);
    
    alldurations = cell(size(ftrack, 1), 1);
    
    for idx_run = 1:size(ftrack, 1)
                
        % --- Read data
        M = load(fstamp{idx_run});
        D = readtable(ftrack{idx_run});
        framerate = mean(1./diff(M.t));
        
        % --- Pool data from each sequences
        n_seq = max(unique(D.id));  % number of trajectories
        
        % Initialize output arrays
        pool_durations = cell(1, n_seq + 1);
        for s = 0:n_seq
            pool_durations{s + 1} = sum(D.id == s)/framerate;          
        end
        
        alldurations{idx_run} = [pool_durations{:}];
        
    end
    
    alldurations = [alldurations{:}];
    
    pdf = computePDF(bins, alldurations);
    
    ax = subplot(3, 2, idT);
    plot(ax, bins, pdf, 'Color', colors(idT, :));
    ax.FontSize = 12;
    xlabel(ax, 'Seq. duration [s]');
    axis(ax, [0, max(bins), 0, 0.3])
    leg = sprintf('%i sequences, %i > 30s', numel(alldurations), sum(alldurations >= 25));
    legend(leg);
end