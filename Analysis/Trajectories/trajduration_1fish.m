% Look at trajectories durations

% close all
clear
clc

% --- Parameters
% * Temperatures
T = 26;

% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences1fishT' num2str(T) '.mat'];

% * Bins
nbins = 200;
method = 'kde'; % Method for PDF ('kde' or 'hist')
mode = 'edges'; % Mode for bins
bw = 4;       % Bandwidths for KDE (ibi, dsp, reo, fwd)
bindurlim = [0, 600];
binboulim = [0, 300];

% --- Preparations
% * Bin vectors
switch mode
    case 'edges'
        binsi = linspace(bindurlim(1), bindurlim(2), nbins + 1)';
        binsb = linspace(binboulim(1), binboulim(2), nbins + 1)';
    case 'centers'
        binsi = linspace(bindurlim(1), bindurlim(2), nbins)';
        binsi = centers2edges(binsi);
        binsb = linspace(binboulim(1), binboulim(2), nbins)';
        binsb = centers2edges(binsb);
end

% * Load data
data = load(fn_dataset, 'interboutintervals');

nfish = numel(data.interboutintervals);

% * Figures
colors = lines(nfish);

% * Arrays
trajdur = cell(numel(T), 1);
pdfdur = NaN(nbins, numel(T));
bindur = NaN(nbins, numel(T));
numbouts = cell(numel(T), 1);
pdfbou = NaN(nbins, numel(T));

% --- Processing
for idF = 1:nfish
        
    fprintf(['Getting traj. duration for fish ', num2str(idF)]); tic
    
    % Get data
    ibi = data.interboutintervals{idF};
    
    % Get durations
    trajdur{idF} = nansum(ibi);
    numbouts{idF} = sum(~isnan(ibi));
    
    % Get pdf
    switch method
        case 'kde'
            [pdfdur(:, idF), cbindur] = computePDF(binsi, trajdur{idF}, 'mode', 'edges', 'method', method, 'param', {'Bandwidth', bw});
            [pdfbou(:, idF), cbinbou] = computePDF(binsb, numbouts{idF}, 'mode', 'edges', 'method', method, 'param', {'Bandwidth', bw});
        case 'hist'
            [pdfdur(:, idF), cbindur] = computePDF(binsi, trajdur{idF}, 'mode', 'edges', 'method', method);
            [pdfdur(:, idF), cbinbou] = computePDF(binsb, numbouts{idF}, 'mode', 'edges', 'method', method);
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% Get mean & std
mdur = cellfun(@mean, trajdur);
sdur = cellfun(@std, trajdur);
medur = cellfun(@median, trajdur);
mbou = cellfun(@mean, numbouts);
sbou = cellfun(@std, numbouts);
mebou = cellfun(@median, numbouts);

% Get pooled mean & std
durpool = cat(2, trajdur{:});
mdurpool = mean(durpool);
sdurpool = std(durpool);
medurpool = median(durpool);
boupool = cat(2, numbouts{:});
mboupool = mean(boupool);
sboupool = std(boupool);
meboupool = median(boupool);

% --- Display
labels = arrayfun(@(x) ['Fish ' num2str(x)], 1:nfish, 'UniformOutput', false);

% * PDF of trajectory durations
figure; ax = gca; hold on
ax.ColorOrder = colors;
plot(cbindur, pdfdur);
legend(ax, labels);
xlabel(ax, 'Trajectory duration (s)');
ylabel(ax, 'pdf');
axis(ax, 'square');
title('Single fish batches');

% * PDF of number of bouts per trajectory
figure; ax = gca; hold on
ax.ColorOrder = colors;
plot(cbinbou, pdfbou);
legend(ax, labels);
xlabel('Number of bouts per traj.');
ylabel('pdf');
axis(ax, 'square');
title('Single fish batches');

% * Mean +/- s.d.
figure; ax = gca; hold on
errorbar(1:nfish, mdur, sdur, 'k');
plot(1:nfish, medur, 'Marker', 'o', 'Color', [0.8, 0.2, 0.2]);
xlabel('Fish #');
ylabel('Trajectory duration');
legend('Mean \pm s.d.', 'Median');
ax.XTick = 1:nfish;
ylim([0 250]);
title('Single fish batches');

% * Mean +/- s.d. number of bouts per traj.
figure; ax = gca; hold on
errorbar(1:nfish, mbou, sbou, 'k');
plot(1:nfish, mebou, 'Marker', 'o', 'Color', [0.8, 0.2, 0.2]);
xlabel('Fish #');
ylabel('Number of bouts per traj.');
legend('Mean \pm s.d.', 'Median');
ax.XTick = 1:nfish;
ylim([0 180]);
title('Single fish batches');