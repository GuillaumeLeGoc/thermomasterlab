% Get number of trajecories and bouts given temperature with used filters.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;
framerate = 25;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Filters
filt.seq_length = 25;    % sequence length for each fish in seconds
% filt.bout_freq = .1;    % minimum average bout frequency
filt.n_bouts = 10;
filt.minx = 0;
filt.maxx = Inf;

% --- Loop over each temperatures and compute features PDF
ntraj = NaN(numel(T), 1);
nbout = NaN(numel(T), 1);
ndura = NaN(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Counting trajectories for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    nt = 0;
    nb = 0;
    nd = 0;
    for idexp = 1:size(ftrack, 1)
        
        [~, I] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        nt = nt + size(I, 2);
        nb = nb + sum(~isnan(I), 'all');
        nd = nd + sum(I, 'all', 'omitnan')./framerate;
        
        fprintf('.');
    end
    
    ntraj(idT) = nt;
    nbout(idT) = nb;
    ndura(idT) = nd./nt;
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
end