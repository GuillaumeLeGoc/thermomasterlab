% Select fish with a trajectory long enough to plot.

% close all
clear
clc

% --- Parameters
T = [18, 26, 33];

pixsize = 0.09;

ntries = 20;    % Number of figs
nframes = 3000; % Minimum number of frames
idexclu = [];   % Trajectory to ignore
nocc = 10;      % Plot 1 point each nocc time steps
colors = rainbow(numel(T));

X = cell(numel(T), 1);
Y = cell(numel(T), 1);
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf('Finding good trajectories to display for T = %i°C...\n', temperature);
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    X{idT} = cell(0);
    Y{idT} = cell(0);
    counter = 0;
    for idexp = 1:size(ftrack, 1)
        
        % Load data
        tracks = readtable(ftrack{idexp});
        
        % Get trajectories ID
        trajid = unique(tracks.id);
        
        % Find trajectories of interest
        for s = trajid'
            
            if numel(tracks.imageNumber(tracks.id == s)) >= nframes
                X{idT}{end + 1} = tracks.xBody(tracks.id == s).*pixsize;
                Y{idT}{end + 1} = tracks.yBody(tracks.id == s).*pixsize;
            end
        end
    end
end

for geni = 1:ntries
    
    f = figure; ax = axes(f); hold(ax, 'on');
    axis equal
    ax.XLim = [0, 100];
    ax.YLim = [0, 45];
    ax.XTick = [];
    ax.YTick = [];
    ax.Box = 'on';
    ax.GridLineStyle = 'none';
    
    id = randi([1, numel(X{1})]);
    nimage = numel(X{1}{id});
    for idT = 1:numel(T)
        
        nimg2 = cellfun(@numel, X{idT});
        [~, id2] = min(abs(nimg2 - nimage));
        
        p = plot(X{idT}{id2}(1:nocc:end), Y{idT}{id2}(1:nocc:end));
        p.Color = colors(idT, :);
    end
end