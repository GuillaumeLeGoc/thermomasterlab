% Look at trajectories durations

% close all
clear
clc

% --- Parameters
% * Files
fn_dataset = [pwd, filesep, 'Data', filesep, 'Matfiles', filesep, 'allsequences.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Bins
nbins = 200;
method = 'kde'; % Method for PDF ('kde' or 'hist')
mode = 'edges'; % Mode for bins
bw = 4;       % Bandwidths for KDE (ibi, dsp, reo, fwd)
bindurlim = [0, 600];
binboulim = [0, 300];

% --- Preparations
% * Bin vectors
switch mode
    case 'edges'
        binsi = linspace(bindurlim(1), bindurlim(2), nbins + 1)';
        binsb = linspace(binboulim(1), binboulim(2), nbins + 1)';
    case 'centers'
        binsi = linspace(bindurlim(1), bindurlim(2), nbins)';
        binsi = centers2edges(binsi);
        binsb = linspace(binboulim(1), binboulim(2), nbins)';
        binsb = centers2edges(binsb);
end

% * Load data
data = load(fn_dataset, 'interboutintervals');

% * Figures
colors = rainbow(numel(T));

% * Arrays
trajdur = cell(numel(T), 1);
pdfdur = NaN(nbins, numel(T));
numbouts = cell(numel(T), 1);
pdfbou = NaN(nbins, numel(T));

% --- Processing
for idT = 1:numel(T)
        
    fprintf(['Getting traj. duration for T = ', num2str(T(idT)), '°C ']); tic
    
    % Get data
    ibi = data.interboutintervals{idT};
    
    % Get durations
    trajdur{idT} = nansum(ibi);
    numbouts{idT} = sum(~isnan(ibi));
    
    % Get pdf
    switch method
        case 'kde'
            [pdfdur(:, idT), cbindur] = computePDF(binsi, trajdur{idT}, 'mode', 'edges', 'method', method, 'param', {'Bandwidth', bw});
            [pdfbou(:, idT), cbinbou] = computePDF(binsb, numbouts{idT}, 'mode', 'edges', 'method', method, 'param', {'Bandwidth', bw});
        case 'hist'
            [pdfdur(:, idT), cbindur] = computePDF(binsi, trajdur{idT}, 'mode', 'edges', 'method', method);
            [pdfdur(:, idT), cbinbou] = computePDF(binsb, numbouts{idT}, 'mode', 'edges', 'method', method);
    end
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% Get mean & std
mdur = cellfun(@mean, trajdur);
sdur = cellfun(@std, trajdur);
medur = cellfun(@median, trajdur);
mbou = cellfun(@mean, numbouts);
sbou = cellfun(@std, numbouts);
mebou = cellfun(@median, numbouts);

% Get pooled mean & std
durpool = cat(2, trajdur{:});
mdurpool = mean(durpool);
sdurpool = std(durpool);
medurpool = median(durpool);
boupool = cat(2, numbouts{:});
mboupool = mean(boupool);
sboupool = std(boupool);
meboupool = median(boupool);

% --- Display
labels = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);

% * PDF of trajectory durations
figure; ax = gca; hold on
ax.ColorOrder = colors;
plot(cbindur, pdfdur);
legend(ax, labels);
xlabel(ax, 'Trajectory duration (s)');
ylabel(ax, 'pdf');
axis(ax, 'square');
title('10-fish batches');

% * PDF of number of bouts per trajectory
figure; ax = gca; hold on
ax.ColorOrder = colors;
plot(cbinbou, pdfbou);
legend(ax, labels);
xlabel('Number of bouts per traj.');
ylabel('pdf');
axis(ax, 'square');
title('10-fish batches');

% * Mean +/- s.d. traj. durations
figure; hold on
errorbar(T, mdur, sdur, 'k');
plot(T, medur, 'Marker', 'o', 'Color', [0.8, 0.2, 0.2]);
xlabel('Temperature (°C)');
ylabel('Trajectory duration');
legend('Mean \pm s.d.', 'Median');
ylim([0 250]);
title('10-fish batches');

% * Mean +/- s.d. number of bouts per traj.
figure; hold on
errorbar(T, mbou, sbou, 'k');
plot(T, mebou, 'Marker', 'o', 'Color', [0.8, 0.2, 0.2]);
xlabel('Temperature (°C)');
ylabel('Number of bouts per traj.');
legend('Mean \pm s.d.', 'Median');
ylim([0 180]);
title('10-fish batches');