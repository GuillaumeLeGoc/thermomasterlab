% Plot trajectories in the fish pool

file = '/home/guillaume/Science/Projects/ThermoMaster/Data/2019-07-24/Run 01/Tracking_Result/tracking.txt';

n_trajs = 100;

T = readtable(file);

f = figure; hold on
f.Name = 'ExampleTrajectory';
for id = 1:n_trajs
    
    x = T.xBody(T.id == id - 1);
    y = T.yBody(T.id == id - 1);
    p = plot(x(1:10:end), y(1:10:end));
    p.LineWidth = 1.25; 
    p.LineStyle = '-';
    p.Marker = 'none';
    p.MarkerSize = 1.25;
    
end

ax = gca;
axis([0 1200 0 600])
axis equal