% Test bout detection parameters

clear
close all
clc

filename = '/home/guillaume/Science/Projects/ThermoMaster/Data/2020-12-17/Run 02/Tracking_Result/tracking.txt';
metaname = '/home/guillaume/Science/Projects/ThermoMaster/Data/2020-12-17/Run 02/stamps.mat';
pixsize = 0.09;
th = struct;
fishid = 148;

T = readtable(filename);
M = load(metaname);

framerate = 1/mean(diff(M.t));

x = T.xBody(T.id == fishid);
y = T.yBody(T.id == fishid);
t = T.tBody(T.id == fishid);
frames = T.imageNumber(T.id == fishid);

[B, sd] = Bouts.find_angle(x, y, t, framerate, th);

images = NaN(max(T.imageNumber), 1);
images(frames + 1) = frames;
xf = NaN(max(T.imageNumber), 1);
xf(frames + 1) = x;
yf = NaN(max(T.imageNumber), 1);
yf(frames + 1) = y;
tf = NaN(max(T.imageNumber), 1);
tf(frames + 1) = t;
sf = NaN(max(T.imageNumber), 1);
sf(frames(2:end) + 1) = sd;
bf = B + frames(1);

figure;
plot(x, flip(y)); title('Trajectory');
figure;
plot(xf); title('X');
hold on; plot(bf, xf(bf), 'ro')
figure;
plot(yf); title('Y');
hold on; plot(bf, yf(bf), 'ro')
figure;
plot(tf); title('\theta');
hold on; plot(bf, tf(bf), 'ro');
figure;
plot(sf); title('Speed');
hold on; plot(bf, sf(bf), 'ro');

b{1} = B;
angles = Bouts.limit(t, b);
angles(isnan(angles)) = [];
dangles = diff(angles);
dtheta = rad2deg(atan2(sin(dangles), cos(dangles)));

a = t;
tocorr = true;
i = 0;
while sum(tocorr)
    i = i + 1;
    dang = diff(a);
    dang = atan2(sin(dang), cos(dang));
    tocorr = [false; abs(dang) > deg2rad(140)];
    a = fillmissing(a, 'previous', 'EndValues', 'nearest', 'MissingLocations', tocorr);
    if i > numel(t)
        break
    end
end
% Get heading change
angles = Bouts.limit(a, b);
angles(isnan(angles)) = [];
dangles = diff(angles);
dtheta2 = rad2deg(atan2(sin(dangles), cos(dangles)));
figure; histogram(dtheta);
hold on
histogram(dtheta2);