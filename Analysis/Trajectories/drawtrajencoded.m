% Display nice trajecory of fish, encoding turn anglesand displacement

% close all
clear
clc

% --- Parameters
T = 26;

% * Tracks files
trfiles = [pwd, '/Data/Uniform1fish_list.txt'];

% * Trajectory
nbouts = 100;

% * Pool
len = [0, 100];
wid = [0, 45];
pixsize = 0.09;         % mm

% * Filters
filt.seq_length = 25;	% min. sequence length for each fish in seconds
filt.n_bouts = nbouts;  % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

theta_tresh = 10;

% * Figures
cmap = div_bkr(20);
xybox = [len(1), wid(1), len(2), wid(2)];
cx = 5/len(2);
cy = 5/wid(2);  % curvature of pool angles
bms = 16;    % 1mm = this area of marker size

% * Get experiment list
[ftrack, fstamp, nfish] = getExperimentList(T, [], trfiles);

% --- Processing
for idexp = 1:size(ftrack, 1)
    
    % - Get data
    [x, i, ~, t, ~, ~, ~, y] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
    
    if isempty(x)
        continue;
    end
    
    % Pick random trajectory
    idtraj = randi(size(x, 2), 1);
    
    % Select corresponding data
    xx = x(:, idtraj);
    xx(isnan(xx)) = [];
    xx = xx(1:nbouts-1).*pixsize;
    yy = y(:, idtraj);
    yy(isnan(yy)) = [];
    yy = yy(1:nbouts-1).*pixsize;
    ii = i(:, idtraj);
    ii(isnan(ii)) = [];
    ii = ii(1:nbouts-1).*pixsize;
    dtheta = t(:, idtraj);
    dtheta(isnan(dtheta)) = [];
    dtheta = dtheta(1:nbouts-1);
%     dtheta(abs(dtheta) < theta_tresh) = 0;
    
    % - Display
    fig = figure; hold on
    ax = gca;
    axis equal
    xlim(ax, len);
    ylim(ax, wid);
    grid off
    box off
    axis off
    m = rectangle('Position', xybox, 'Curvature', [cx, cy]);
    m.EdgeColor = [0.2, 0.2, 0.2];
    p = plot(xx, yy);
    p.Color = [0.5, 0.5, 0.5, 0.5];
    p.LineWidth = 1;
    s = scatter(xx, yy, ii*bms, -dtheta, 'filled');
    colormap(div_bkr);  % blue = right, red = left
    c = colorbar;
    title(c, '\delta\theta');
    caxis([-30, 30]);
    c.Ticks = [-30, 0, 30];
    c.TickLabels = {'-30°', '0°', '+30°'};
    c.FontSize = 10;
    
    % add legend
    scatter(5, 5, 0.5*bms, 'filled', 'MarkerFaceColor', 'k');
    str = '.5';
    text(2, 3, str);
    scatter(8, 5, 1*bms, 'filled', 'MarkerFaceColor', 'k');
    str = '1';
    text(6, 3, str);
    scatter(11, 5, 2*bms, 'filled', 'MarkerFaceColor', 'k');
    str = '2 s';
    text(9, 3, str);
end