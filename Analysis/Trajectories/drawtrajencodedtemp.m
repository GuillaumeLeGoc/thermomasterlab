% Display several trajectories from different temperatures.

% close all
clear
clc

% --- Parameters
% T = [18, 26, 33];
T = 30;
allT = [18, 22, 26, 30, 33];

ntrials = 20;

% * Trajectory
nbouts = 60;

% * Pool
len = [0, 100];
wid = [0, 45];
pixsize = 0.09;         % mm

% * Filters
filt.seq_length = 25;	% sequence length for each fish in seconds
filt.n_bouts = nbouts;  % minimum number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Figures
colors = rainbow(numel(allT));
xybox = [len(1), wid(1), len(2), wid(2)];
cx = 5/len(2);
cy = 5/wid(2);  % curvature of pool angles
basemarkersize = 26;    % 1s = this area of marker size

% --- Processing
for id = 1:ntrials
    
    % * Prepare figure
    fig = figure; hold on
    ax = gca;
    axis equal
    xlim(ax, len);
    ylim(ax, wid);
    grid off
    box off
    axis off
    m = rectangle('Position', xybox, 'Curvature', [cx, cy]);
    m.EdgeColor = [0.2, 0.2, 0.2];
    
    for idT = 1:numel(T)
        
        % * Get experiment list
        [ftrack, fstamp, nfish] = getExperimentList(T(idT));
        
        % Randomly pick an experiment
        idexp = randi(size(ftrack, 1), 1);
        
        % - Get data
        [x, i, ~, ~, ~, ~, framerate, y] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        % Pick random trajectory
        idtraj = randi(size(x, 2), 1);
        
        % Select corresponding data
        xx = x(:, idtraj);
        xx(isnan(xx)) = [];
        xx = xx(1:nbouts-1).*pixsize;
        yy = y(:, idtraj);
        yy(isnan(yy)) = [];
        yy = yy(1:nbouts-1).*pixsize;
        ii = i(:, idtraj);
        ii(isnan(ii)) = [];
        ii = ii(1:nbouts-1)./framerate;
        
        color = colors(T(idT) == allT, :);
        p = plot(ax, xx, yy);
        p.Color = [0.5, 0.5, 0.5, 0.5];
        p.LineWidth = 1;
        s = scatter(ax, xx, yy, ii*basemarkersize, color, 'filled');
        plot(ax, xx(1), yy(1), '.k');   % start point
        
        % add legend
        scatter(5, 5, basemarkersize, 'filled', 'MarkerFaceColor', 'k');
        str = '1 s';
        tt = text(2, 3, str);
                
    end
end