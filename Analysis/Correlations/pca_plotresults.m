% Plot pc1 coeff from intra temperature and intertemperature

% close all
clear
clc

% --- Parameters

% * Files
fintrapool = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pca_intrapooled.mat'];
fintermean = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pca_intermean.mat'];
finterpool = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pca_interpooled.mat'];

% * Figures
intracolor = [10/255 10/255 10/255];
intercolor = [150/255 150/255 150/255];

% --- Load data
matintra = load(fintrapool);
% matinter = load(finterpool);
matinter = load(fintermean);

% --- Processing
% - Plot explained variance
figure; hold on
groups = [matintra.explained, matinter.mexplained];
barcol = [intracolor; intercolor];
nbars = size(groups, 2);
ngroups = size(groups, 1);
b = bar(groups);
for idb = 1:nbars
    % Colors
    b(idb).FaceColor = 'flat';
    b(idb).CData = barcol(idb, :);
end
ax = gca;
ax.XTick = 1:5;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel(ax, 'Explained variance [%]');
legend('Intra pooled', 'Inter pooled');
axis square

% - Plot PC1 coeff
figure; hold on

p0 = plot(matintra.pc1coeff);
p0.Color = intracolor;
p0.DisplayName = 'Intra pooled';
p0.Marker = 'o';
manualerrbar(1:5, matintra.errpc1', 'Color', p0.Color);

p1 = plot(matinter.mcoeffpc1);
p1.Color = intercolor;
p1.DisplayName = 'Inter pooled';
p1.Marker = 'diamond';
manualerrbar(1:5, matinter.errpc1, 'Color', p1.Color);

ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.XLim = [.5, 5.5];
ax.YLim = [-.8, .8];
ylabel('coeff(PC1)');
axis square

legend(ax, [p0, p1], 'Location', 'southeast');

% - Plot PC2 coeff
figure; hold on

p0 = plot(matintra.pc2coeff);
p0.Color = intracolor;
p0.DisplayName = 'Intra pooled';
p0.Marker = 'o';
manualerrbar(1:5, matintra.errpc2', 'Color', p0.Color);

p1 = plot(matinter.mcoeffpc2);
p1.Color = intercolor;
p1.DisplayName = 'Inter pooled';
p1.Marker = 'diamond';
manualerrbar(1:5, matinter.errpc2, 'Color', p1.Color);

ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.XLim = [.5, 5.5];
ax.YLim = [-.8, .8];
ylabel('coeff(PC2)');
axis square

legend(ax, [p0, p1], 'Location', 'southeast');