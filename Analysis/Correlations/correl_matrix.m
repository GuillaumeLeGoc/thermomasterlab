% Build features matrix and get correlation matrix.

% close all
clear
clc

% --- Parameters
% * Definitions
pixsize = 0.09;

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
theta_threshold = 6;    % deg
scale_turn_disp = 1.6;  % turn displacement scaling factor (mm)
ncorr = 20;

% * Control
% 1 : IBI
% 2 : pturn
% 3 : pflip
% 4 : sigmaturn
% 5 : d
idx_to_shuffle = [1, 2, 3, 4, 5];

% * Figures
colors = cm10(10);

% * Filters
filt.seq_length = 30;	% sequence length for each fish in seconds
filt.bout_freq = .1;   % minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing correlation matrix for ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(temperature);
    
    X = NaN(0, 5);
    for idx_exp = 1:size(ftrack, 1)
        
        % Get sequences
        [~, intboutint, displacement, turnangle, ~, ~, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        nseq = size(intboutint, 2);
        
        for s = 1:nseq
             
            % - Interbout interval
            ibi = intboutint(:, s)./framerate;
            ibi(isnan(ibi)) = [];
            
            if numel(ibi) < ncorr
                continue;
            end
            
            X(end + 1, 1) = median(ibi);
            
            % - Turn angles
            dtheta = turnangle(:, s);
            dtheta(isnan(dtheta)) = [];
            
            % pturn
            nturns = sum(abs(dtheta) > theta_threshold);
            nbouts = numel(dtheta);
            pturn = nturns/nbouts;
            X(end, 2) = pturn;
            
            % pflip
            dthetabin = dtheta;
            dthetabin(dtheta < -theta_threshold) = -1;
            dthetabin(dtheta > theta_threshold) = 1;
            dthetabin(abs(dtheta) < theta_threshold) = 0;
            if numel(unique(dthetabin)) == 1
                continue;
            end
            [xco, lag] = xcorr(dthetabin, ncorr, 'biased');
            selected_inds = find(lag == 0):find(lag == 0) + ncorr;
            xco = xco(selected_inds)./max(xco(selected_inds));
            x = 1:ncorr;
            g = fittype(@(pflip, x) (pturn^2).*(1 - 2*pflip).^x);
            fitt = fit(x', xco(2:end), g, 'StartPoint', 0.2);
            X(end, 3) = fitt.pflip;
            
            % varturn
            dtheta(abs(dtheta) < theta_threshold) = [];
            sigturn = std(dtheta);
            X(end, 4) = sigturn;
            
            % displacement
            isturn = abs(dtheta) > theta_threshold;
            displ = displacement(:, s).*pixsize;
            displ(isnan(displ)) = [];
            displ(isturn) = displ(isturn)./scale_turn_disp;
            X(end, 5) = median(displ);
            
        end
        
        fprintf('.');
    end
    
    % - Get correlations matrix
    R = corrcoef(X);
    for idx = idx_to_shuffle
        X(:, idx) = X(randperm(length(X(:, idx)))', idx);
    end
    Rshuffled = corrcoef(X);
    
    % - Get coefficients
    ibi_ptrn(idT) = R(1, 2);
    ibi_pflp(idT) = R(1, 3);
    ibi_strn(idT) = R(1, 4);
    ibi_disp(idT) = R(1, 5);
    ptrn_pflp(idT) = R(2, 3);
    ptrn_strn(idT) = R(2, 4);
    ptrn_disp(idT) = R(2, 5);
    pflp_strn(idT) = R(3, 4);
    pflp_disp(idT) = R(3, 5);
    strn_disp(idT) = R(4, 5);
    
    % - Get coefficients (shuffled)
    ibi_ptrn_s(idT) = Rshuffled(1, 2);
    ibi_pflp_s(idT) = Rshuffled(1, 3);
    ibi_strn_s(idT) = Rshuffled(1, 4);
    ibi_disp_s(idT) = Rshuffled(1, 5);
    ptrn_pflp_s(idT) = Rshuffled(2, 3);
    ptrn_strn_s(idT) = Rshuffled(2, 4);
    ptrn_disp_s(idT) = Rshuffled(2, 5);
    pflp_strn_s(idT) = Rshuffled(3, 4);
    pflp_disp_s(idT) = Rshuffled(3, 5);
    strn_disp_s(idT) = Rshuffled(4, 5);
    
    fprintf('\tDone (%2.2fs).\n', toc);
    
    f = figure;
    f.Name = ['CorrMatT' num2str(temperature)];
    imagesc(R);
    colormap(div_bkr);
    colorbar;
    ax = gca;
    ax.XTick = [1, 2, 3, 4, 5];
    ax.YTick = [1, 2, 3, 4, 5];
    ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
    ax.YTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
    ax.XTickLabelRotation = 45;
    ax.YTickLabelRotation = 45;
    title(['T = ' num2str(temperature) '°C']);
    axis square
    grid off
    caxis([-.5 .5])
end

f = figure; hold on
f.Name = 'CorrelCoeff';
ax = gca;
ax.ColorOrder = colors;
plot(T, ibi_ptrn, 'o-', 'DisplayName', 'IBI-p_{turn}');
plot(T, ibi_pflp, '+-', 'DisplayName', 'IBI-p_{flip}');
plot(T, ibi_strn, '*-', 'DisplayName', 'IBI-\sigma_{turn}');
plot(T, ibi_disp, 'h-', 'DisplayName', 'IBI-d');
plot(T, ptrn_pflp, 'x-', 'DisplayName', 'p_{turn}-p_{flip}');
plot(T, ptrn_strn, 's-', 'DisplayName', 'p_{turn}-\sigma_{turn}');
plot(T, ptrn_disp, 'd-', 'DisplayName', 'p_{turn}-d');
plot(T, pflp_strn, '^-', 'DisplayName', 'p_{flip}-\sigma_{turn}');
plot(T, pflp_disp, '>-', 'DisplayName', 'p_{flip}-d');
plot(T, strn_disp, 'p-', 'DisplayName', '\sigma_{turn}-d');
l = legend;
l.Location = 'west';
axis([10, 33, -.6 .6]);
xlabel('Temperature [°C]');
ylabel('Correlation coeff.');
ax.XTick = [18, 22, 26, 30, 33];

f = figure; hold on
f.Name = 'CorrelCoeff_shuffled';
ax = gca;
ax.ColorOrder = colors;
plot(T, ibi_ptrn_s, 'o-', 'DisplayName', 'IBI-p_{turn}');
plot(T, ibi_pflp_s, '+-', 'DisplayName', 'IBI-p_{flip}');
plot(T, ibi_strn_s, '*-', 'DisplayName', 'IBI-\sigma_{turn}');
plot(T, ibi_disp_s, 'h-', 'DisplayName', 'IBI-d');
plot(T, ptrn_pflp_s, 'x-', 'DisplayName', 'p_{turn}-p_{flip}');
plot(T, ptrn_strn_s, 's-', 'DisplayName', 'p_{turn}-\sigma_{turn}');
plot(T, ptrn_disp_s, 'd-', 'DisplayName', 'p_{turn}-d');
plot(T, pflp_strn_s, '^-', 'DisplayName', 'p_{flip}-\sigma_{turn}');
plot(T, pflp_disp_s, '>-', 'DisplayName', 'p_{flip}-d');
plot(T, strn_disp_s, 'p-', 'DisplayName', '\sigma_{turn}-d');
l = legend;
l.Location = 'west';
axis([10, 33, -.6 .6]);
xlabel('Temperature [°C]');
ylabel('Correlation coeff.');
ax.XTick = [18, 22, 26, 30, 33];
title('Shuffled');