% PCA on features matrix, with proba for turn and rates for flip.
% Features matrix order :
% 1 - turn amplitude
% 2 - ibi
% 3 - d
% 4 - pturn
% 5 - kflip

% close all
clear
clc

% --- Parameters
% * Feature matrix location
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix_pk_200ms.mat'];

% * Outfile for T = 26°C
fnout1 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCA26_pk_200ms.mat'];
fnout2 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAInter_pk_200ms.mat'];
fnout3 = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk_200ms.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
nboots = 1000;
ns = 2;         % number of sigma for confidence interval
nbins = 200;    % for projections pdf
binspc1 = linspace(-5, 5, nbins + 1);
binspc2 = linspace(-5, 5, nbins + 1);
bw = 0.1;

% * Figures
colors = rainbow(numel(T));
intermeancol = [0, 0, 0];

% --- Load file
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
nfeat = size(featmat{1}, 2);

% --- Inter-temperature PCA (mean matrix)

fprintf('Inter-temperature PCA...'); tic

% - Compute bootstrapped mean of parameters
btfeatmat = cell(numel(T), 1);
for idT = 1:numel(T)
    
    % Get matrix
    X = featmat{idT};
    
    % Bootstrap the mean
    btfeatmat{idT} = bootstrp(nboots, @mean, X);
   
end

% - PCA for each set of means
coeffspc1 = NaN(nfeat, nboots);
coeffspc2 = NaN(nfeat, nboots);
explaineds = NaN(nfeat, nboots);
for idb = 1:nboots
    
    % Create matrix
    samplefeatmat = NaN(numel(T), nfeat);
    for idT = 1:numel(T)
        samplefeatmat(idT, :) = btfeatmat{idT}(idb, :);
    end
    samplefeatmat = samplefeatmat./std(samplefeatmat);
    
    % PCA
    [coef, ~, ~, ~, expl] = pca(samplefeatmat);
    
    if numel(expl) == 4
        expl(end + 1) = 0;
    end
    
    % Store
    coeffspc1(:, idb) = coef(:, 1);
    coeffspc2(:, idb) = coef(:, 2);
    explaineds(:, idb) = expl;
end

% - Get confidence intervals
intermean_mcpc1 = mean(coeffspc1, 2);
intermean_mcpc2 = mean(coeffspc2, 2);
intermean_mexpl = mean(explaineds, 2);
intermean_errpc1 = NaN(nfeat, 2);
intermean_errpc2 = NaN(nfeat, 2);
intermean_errexpl = NaN(nfeat, 2);
for idf = 1:nfeat
    sg = std(coeffspc1(idf, :), [], 2);
    intermean_errpc1(idf, :) = [intermean_mcpc1(idf) - ns*sg, intermean_mcpc1(idf) + ns*sg];
    sg = std(coeffspc2(idf, :), [], 2);
    intermean_errpc2(idf, :) = [intermean_mcpc2(idf) - ns*sg, intermean_mcpc2(idf) + ns*sg];
    sg = std(explaineds(idf, :), [], 2);
    intermean_errexpl(idf, :) = [intermean_mexpl(idf) - ns*sg, intermean_mexpl(idf) + ns*sg];
end

fprintf('\tDone (%2.2fs).\n', toc);

% - Display
% Variance explained
figure('Name', 'InterMean_VarExplained'); hold on
ax = gca;
ax.ColorOrder = intermeancol;
b = bar(intermean_mexpl);
b.EdgeColor = 'none';
manualerrbar(1:4, intermean_errexpl, 'k');
ax = gca;
ax.XTick = 1:nfeat;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ax.XGrid = 'off';
ylabel('Explained variance [%]');
title('Explained var. (Mean matrix)');
axis square
axis([0.5, 5.5, 0, 100]);

% --- Intra-temperature PCA

fprintf('PCA by temperature...'); tic

% - PCA
pc1intrabt = NaN(nfeat, numel(T));
ci_pc1intrabt = NaN(2, nfeat, numel(T));
pc2intrabt = NaN(nfeat, numel(T));
ci_pc2intrabt = NaN(2, nfeat, numel(T));
explained = NaN(nfeat, numel(T));
ci_explained = NaN(2, nfeat, numel(T));

for idT = 1:numel(T)
    
    samplefeatmat = featmat{idT};
    
    sfm = samplefeatmat./std(samplefeatmat);
    
    [coeff, ~, ~, ~, explained(:, idT)] = pca(sfm);
        
    if coeff(2, 1) > 0
        coeff(:, 1) = -coeff(:, 1);
    end
    
    pc1intrabt(:, idT) = coeff(:, 1);
    pc2intrabt(:, idT) = coeff(:, 2);
    
    tmp = bootci(nboots, @pca, sfm);
    ci_pc1intrabt(:, :, idT) = tmp(:, :, 1);
    ci_pc2intrabt(:, :, idT) = tmp(:, :, 2);
    ci_explained(:, :, idT) = bootci(nboots, @(x) getNthOutput(@pca, 5, x), sfm);
end

fprintf('\tDone(%2.2fs).\n', toc);

% --- Pooled matrix, standardized by temperature

fprintf('PCA on pooled matrix...'); tic

featmat_std = cellfun(@(x) (x - mean(x))./std(x), featmat, 'UniformOutput', false);
featmatp_std = cat(1, featmat_std{:});
fm = cat(1, featmat{:});
sfac = std(fm);

% - PCA
[coeffp, ~, latp, ~, explp] = pca(featmatp_std);

pc1intrap = coeffp(:, 1);
pc2intrap = coeffp(:, 2);

% - Get projections
pc1proj = cell(numel(T), 1);
pc2proj = cell(numel(T), 1);
pdfpc1proj = NaN(nbins, numel(T));
pdfpc2proj = NaN(nbins, numel(T));
pc1pc2projgrp = cell(numel(T), 1);
for idT = 1:numel(T)
    
    proj = (featmat{idT}./sfac)*coeffp;
    
    pc1proj{idT} = proj(:, 1);
    pc2proj{idT} = proj(:, 2);
    
    [pdfpc1proj(:, idT), cbinspc1] = computePDF(binspc1, pc1proj{idT}, 'mode', 'edges', 'method', 'kde', 'param', {'Bandwidth', bw});
    [pdfpc2proj(:, idT), cbinspc2] = computePDF(binspc2, pc2proj{idT}, 'mode', 'edges', 'method', 'kde', 'param', {'Bandwidth', bw});
    
    pc1pc2projgrp{idT} = idT.*ones(numel(pc1proj{idT}), 1);
end

fprintf('\tDone (%2.2fs).\n', toc);

% --- Display

% - Explained variance (by temperature)
figure('Name', 'VarExlained_byTemp'); hold on
ax = gca;
ax.ColorOrder = rainbow(numel(T));
b = bar(explained, 'EdgeColor', 'none');
plot(1:nfeat, explp, 'k');
ax.XTick = 1:5;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
leg = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
leg{end + 1} = 'All temp. pooled, standardized';
legend(leg, 'AutoUpdate', 'off');
axis square
ax.XGrid = 'off';
ylabel(ax, 'Explained variance [%]');

% - Eigenvalues (pooled)
figure('Name', 'Eigenvalues'); hold on
ax = gca;
bar(latp, 'EdgeColor', 'none', 'FaceColor', 'k');
plot([0, 6], [1, 1], 'r');
ax.XTick = 1:5;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel(ax, 'Eigenvalues');
grid off

% - PC1 coefficients (by temperature)
mixPC1 = [pc1intrabt, intermean_mcpc1];

f0 = figure('Name', 'PC1Coeff_byTemp'); hold on
axpc1 = gca;
axpc1.ColorOrder = colors;
bar(axpc1, pc1intrabt, 'EdgeColor', 'none');
plot(axpc1, 1:nfeat, pc1intrap, 'k');
p = plot(axpc1, 1:nfeat, intermean_mcpc1);
p.LineStyle = 'none';
p.Marker = 's';
p.MarkerEdgeColor = 'none';
p.MarkerFaceColor = intermeancol;
leg = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
leg{end + 1} = 'All temp. pooled, standardized';
leg{end + 1} = 'Inter-temperature';
legend(axpc1, leg, 'Location', 'southeast', 'AutoUpdate', 'off');
axpc1.XTick = 1:nfeat;
axpc1.XTickLabel = {'\delta\theta_t', '\delta{t}', 'd', 'p_{turn}', 'k_{flip}'};
axpc1.YLim = [-1, 1];
ylabel(axpc1, 'PC1 coefficient');
grid off
axis square
axpc1.YTick = [-1, 0, 1];
ylim([-1, 1])
xlim([0.5 5.5])

% - PC2 coefficients (by temperature)
mixPC2 = [pc2intrabt, intermean_mcpc2];

f1 = figure('Name', 'PC2Coeff_byTemp'); hold on
axpc2 = gca;
axpc2.ColorOrder = colors;
bar(axpc2, pc2intrabt, 'EdgeColor', 'none');
plot(axpc2, 1:nfeat, pc2intrap, 'k');
p = plot(axpc2, 1:nfeat, intermean_mcpc2);
p.LineStyle = 'none';
p.Marker = 's';
p.MarkerEdgeColor = 'none';
p.MarkerFaceColor = intermeancol;
leg = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
leg{end + 1} = 'All temp. pooled, standardized';
leg{end + 1} = 'Inter-temperature';
legend(axpc2, leg, 'Location', 'southeast', 'AutoUpdate', 'off');
axpc2.XTick = 1:nfeat;
axpc2.XTickLabel = {'\delta\theta_t', '\delta{t}', 'd', 'p_{turn}', 'k_{flip}'};
axpc2.YLim = [-1, 1];
ylabel(axpc2, 'PC2 coefficient');
grid off
axis square
axpc2.YTick = [-1, 0, 1];
ylim([-1, 1])
xlim([0.5 5.5])

% - Scatter plot in 2D PC space
cols = {'#214478', '#3d944c', '#d9b424', '#ff7f2a', '#ff2800'};
tbl = table(cat(1, pc1proj{:}), cat(1, pc2proj{:}), cat(1, pc1pc2projgrp{:}), ...
    'VariableNames', {'PC1 proj.', 'PC2 proj.', 'T'});
figure('Name', 'PC1PC2ProjPooled');
scatterhistogram(tbl, 'PC1 proj.', 'PC2 proj.', 'GroupVariable', 'T', ...
    'HistogramDisplayStyle', 'smooth', 'LineStyle', '-', ...
    'Color', cols, 'MarkerAlpha', 0.25);
xlim([-3,6]);

% --- Save
if ~isempty(fnout1)
    coeffpc1_26 = pc1intrabt(:, T == 26);
    coeffpc2_26 = pc2intrabt(:, T == 26);
    pc1proj_26 = pc1proj{T == 26};
    pc2proj_26 = pc2proj{T == 26};
    save(fnout1, 'coeffpc1_26', 'coeffpc2_26', 'pc1proj_26', 'pc2proj_26');
end
if ~isempty(fnout2)
    coeffpc1_intertemp = intermean_mcpc1;
    coeffpc2_intertemp = intermean_mcpc2;
    save(fnout2, 'coeffpc1_intertemp', 'coeffpc2_intertemp');
end
if ~isempty(fnout3)
    save(fnout3, 'pc1intrap', 'pc2intrap', 'pc1proj', 'pc2proj', 'sfac');
end