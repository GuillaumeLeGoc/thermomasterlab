% Look at PCA intratemperature, pooled version. Sequences are pooled
% together, but the mean is substracted in each temperature submatrix.
% Projection of the centered data.

% close all
clear
clc

% --- Parameters
% * Feature matrix location
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix'];

% * Save
filename = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pca_intrapooled.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
nbins = 200;
nboots = 1000;          % number of bootstrap samples

% * Figures
colors = rainbow(numel(T));

% * Control
% 1 : IBI
% 2 : pturn
% 3 : pflip
% 4 : sigmaturn
% 5 : d
idx_to_shuffle = [1, 2, 3, 4, 5];

% --- Load file
feat_mat_pooled = load(filefeatmat);
feat_mat_pooled = feat_mat_pooled.feat_mat_pooled;

% --- Processing
fprintf('Intratemperature, pooled PC analysis...'); tic;

% Standardize
feat_mat_pooled_std = cellfun(@(x) (x - mean(x))./std(x), feat_mat_pooled, 'UniformOutput', false);

% Pool
allfeat_std = cat(1, feat_mat_pooled_std{:});
sfac = std(allfeat_std);
allfeat_std = allfeat_std./sfac;

% Shuffle
allfeat_std_s = allfeat_std;
for idx = idx_to_shuffle
    allfeat_std_s(:, idx) = allfeat_std_s(randperm(length(allfeat_std_s(:, idx)))', idx);
end

% - Correlations
R = corrcoef(allfeat_std);
R_s = corrcoef(allfeat_std_s);

% - PCA
[coeff, ~, ~, ~, explained] = pca(allfeat_std);
[coeff_s, ~, ~, ~, explained_s] = pca(allfeat_std_s);

% - Bootstrapping to get confidence intervals
ci_coeff = bootci(nboots, @pca, allfeat_std);
ci_explained = bootci(nboots, @(x) getNthOutput(@pca, 5, x), allfeat_std);
ci_coeff_s = bootci(nboots, @pca, allfeat_std_s);
ci_explained_s = bootci(nboots, @(x) getNthOutput(@pca, 5, x), allfeat_std_s);

errpc1 = ci_coeff(:, :, 1);
errpc2 = ci_coeff(:, :, 2);
errpc1_s = ci_coeff_s(:, :, 1);
errpc2_s = ci_coeff_s(:, :, 2);

% - Get projections and PDF
bins1 = linspace(-5, 5, nbins);
bins2 = linspace(-5, 5, nbins);
pc1pdf = NaN(nbins, numel(T));
pc2pdf = NaN(nbins, numel(T));
for idT = 1:numel(T)
    X = feat_mat_pooled_std{idT}./sfac;
    pc = X*coeff;
    [pc1pdf(:, idT), cbins1] = computePDF(bins1, pc(:, 1), 'method', 'kde', 'mode', 'centers');
    [pc2pdf(:, idT), cbins2] = computePDF(bins2, pc(:, 2), 'method', 'kde', 'mode', 'centers');
end

% - Save
pc1coeff = coeff(:, 1);
pc2coeff = coeff(:, 2);
save(filename, 'pc1coeff', 'pc2coeff', 'errpc1', 'errpc2', 'explained');

fprintf('\t Done (%2.2fs).\n', toc);

% --- Display

% -- Real
% Correlations
figure('Name', 'IntraPooled_Correlations');
imagesc(R);
colormap(div_bkr);
colorbar;
ax = gca;
ax.XTick = [1, 2, 3, 4, 5];
ax.YTick = [1, 2, 3, 4, 5];
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax.YTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YTickLabelRotation = 45;
title('R on pooled features matrix');
axis square
grid off
caxis([-.5 .5])

% Variance explained
figure('Name', 'IntraPooled_VarExplained'); hold on
bar(explained);
manualerrbar(1:5, ci_explained', 'k');
ax = gca;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel('Variance explained [%]');
axis square
axis([0.5, 5.5, 0, 100]);

% PC vectors coeff
figure('Name', 'IntraPooled_PC1Coeff'); hold on
p = plot(coeff(:, 1));
p.Color = 'k';
p.Marker = 'o';
manualerrbar(1:5, errpc1', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC1)');
axis square

figure('Name', 'IntraPooled_PC2Coeff'); hold on
p = plot(coeff(:, 2));
p.Marker = 'o';
p.Color = 'k';
manualerrbar(1:5, errpc2', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ylabel('coeff(PC2)');
ax.YLim = [-1, 1];
axis square

% Projection for each temperature
figure('Name', 'IntraPooled_ProjPC1PC2Centered'); hold on
for idT = 1:numel(T)
    X = feat_mat_pooled_std{idT}./sfac;
    pc = X*coeff;
    p = plot(pc(:, 1), pc(:, 2), '.');
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC1');
ylabel('PC2');
axis([-5, 5, -5, 5]);
title('Projection in PC space');
legend;

% Projections on PC1 PDF
figure('Name', 'IntraPooled_ProjPC1PDFCentered'); hold on
for idT = 1:numel(T)
    p = plot(cbins1, pc1pdf(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC1');
ylabel('PDF');
title('Projection on PC1');
legend;

% Projections on PC2 PDF
figure('Name', 'IntraPooled_ProjPC2PDFCentered'); hold on
for idT = 1:numel(T)
    p = plot(cbins2, pc2pdf(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC2');
ylabel('PDF');
title('Projection on PC2');
legend;

% -- Shuffled
% Correlations
figure('Name', 'IntraPooled_Correlations_shuffle');
imagesc(R_s);
colormap(div_bkr);
colorbar;
ax = gca;
ax.XTick = [1, 2, 3, 4, 5];
ax.YTick = [1, 2, 3, 4, 5];
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax.YTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YTickLabelRotation = 45;
title('R on pooled features matrix (shuffled)');
axis square
grid off
caxis([-.5 .5])

% Variance explained
figure('Name', 'IntraPooled_VarExplained_shuffle'); hold on
bar(explained_s);
manualerrbar(1:5, ci_explained_s', 'k');
ax = gca;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel('Variance explained [%]');
title('Explained variance (shuffled)');
axis square
axis([0.5, 5.5, 0, 100]);

% PC vectors coeff
figure('Name', 'IntraPooled_PC1Coeff_shuffle'); hold on
plot(coeff_s(:, 1));
manualerrbar(1:5, errpc1_s', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC1)');
title('PC1 vector coefficients (shuffled)');
axis square

figure('Name', 'IntraPooled_PC2Coeff_shuffle'); hold on
plot(coeff_s(:, 2));
manualerrbar(1:5, errpc2_s', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC2)');
title('PC2 vector coefficients (shuffled)');
axis square

% Projection for each temperature
figure('Name', 'IntraPooled_ProjPC1PC2Centered_shuffle'); hold on
for idT = 1:numel(T)
    X = feat_mat_pooled_std{idT}./sfac;
    pc = X*coeff_s;
    p = plot(pc(:, 1), pc(:, 2), '.');
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC1');
ylabel('PC2');
axis([-5, 5, -5, 5]);
title('Projection in PC space (shuffled)');
legend;