% Look at PCA intratemperature, pooled version. Sequences are pooled
% together, but the mean is substracted in each temperature submatrix.
% Projection of the non-centered data.

% close all
clear
clc

% --- Parameters
% * Feature matrix location
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
nboots = 1000;
nbins = 200;

% * Figures
colors = rainbow(numel(T));

% * Control
% 1 : IBI
% 2 : pturn
% 3 : pflip
% 4 : sigmaturn
% 5 : d
idx_to_shuffle = [1, 2, 3, 4, 5];

% --- Load file
feat_mat_pooled = load(filefeatmat);
feat_mat_pooled = feat_mat_pooled.feat_mat_pooled;

% --- Processing
fprintf('Intratemperature, pooled PC analysis...'); tic;

% Standardize
feat_mat_pooled_std = cellfun(@(x) (x - mean(x))./std(x), feat_mat_pooled, 'UniformOutput', false);

% Pool
allfeat_std = cat(1, feat_mat_pooled_std{:});
sfac = std(allfeat_std);
allfeat_std = allfeat_std./sfac;

% Shuffle
allfeat_std_s = allfeat_std;
for idx = idx_to_shuffle
    allfeat_std_s(:, idx) = allfeat_std_s(randperm(length(allfeat_std_s(:, idx)))', idx);
end

% - PCA
[coeff, ~, ~, ~, explained] = pca(allfeat_std);
[coeff_s, ~, ~, ~, explained_s] = pca(allfeat_std_s);

% - Bootstrapping to get confidence intervals
ci_coeff = bootci(nboots, @pca, allfeat_std);
ci_explained = bootci(nboots, @(x) getNthOutput(@pca, 5, x), allfeat_std);
ci_coeff_s = bootci(nboots, @pca, allfeat_std_s);
ci_explained_s = bootci(nboots, @(x) getNthOutput(@pca, 5, x), allfeat_std_s);

errpc1 = ci_coeff(:, :, 1);
errpc2 = ci_coeff(:, :, 2);
errpc1_s = ci_coeff_s(:, :, 1);
errpc2_s = ci_coeff_s(:, :, 2);

% - Get projections and PDF
bins1 = linspace(0, 20, nbins);
bins2 = linspace(0, 40, nbins);
pc1pdf = NaN(nbins, numel(T));
pc2pdf = NaN(nbins, numel(T));
for idT = 1:numel(T)
    X = feat_mat_pooled{idT}./sfac;
    pc = X*coeff;
    
    [pc1pdf(:, idT), cbins1] = computePDF(bins1, pc(:, 1), 'method', 'kde', 'mode', 'centers');
    [pc2pdf(:, idT), cbins2] = computePDF(bins2, pc(:, 2), 'method', 'kde', 'mode', 'centers');
end

fprintf('\t Done (%2.2fs).\n', toc);

% --- Display

% -- Real
% Variance explained
figure('Name', 'IntraPooled_VarExplained'); hold on
bar(explained);
manualerrbar(1:5, ci_explained', 'k');
ax = gca;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel('Variance explained [%]');
axis square
axis([0.5, 5.5, 0, 100]);

% PC vectors coeff
figure('Name', 'IntraPooled_PC1Coeff'); hold on
p = plot(coeff(:, 1));
p.Color = 'k';
p.Marker = 'o';
manualerrbar(1:5, errpc1', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ylabel('coeff(PC1)');
ax.YLim = [-1, 1];
axis square

figure('Name', 'IntraPooled_PC2Coeff'); hold on
plot(coeff(:, 2), 'k');
manualerrbar(1:5, errpc2', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ylabel('coeff(PC2)');
ax.YLim = [-1, 1];
axis square

% Projection for each temperature
figure('Name', 'IntraPooled_ProjPC1PC2NotCentered');
allprojpc1 = cell(numel(T), 1);
allprojpc2 = cell(numel(T), 1);
temperaturelabel = cell(numel(T));
for idT = 1:numel(T)
    X = feat_mat_pooled{idT}./sfac;
    pc = X*coeff;
    
    allprojpc1{idT} = pc(:, 1);
    allprojpc2{idT} = pc(:, 2);
    temperaturelabel{idT} = T(idT)*ones(size(pc, 1), 1);
%     p = plot(pc(:, 1), pc(:, 2), '.');
%     p.Color = colors(idT, :);
%     p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
varnames = {'Projection on PC1', 'Projection on PC2', 'Temperature'};
allprojpc1 = cat(1, allprojpc1{:});
allprojpc2 = cat(1, allprojpc2{:});
temperaturelabel = cat(1, temperaturelabel{:});
tbl = table(allprojpc1, allprojpc2, temperaturelabel, 'VariableNames', varnames);
s = scatterhistogram(tbl, 'Projection on PC1', 'Projection on PC2', ...
    'GroupVariable', 'Temperature');
s.HistogramDisplayStyle = 'smooth';
s.LineStyle = '-';
s.LineWidth = 1.5;
s.MarkerSize = 10;
s.MarkerAlpha = 0.5*ones(numel(T), 1);
s.Color = colors;
s.LegendVisible = 'on';
s.Title = 'Projection in PC space';

% Projections on PC1 PDF
figure('Name', 'IntraPooled_ProjPC1PDFNotCentered'); hold on
for idT = 1:numel(T)
    p = plot(cbins1, pc1pdf(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC1');
ylabel('pdf');
title('Projection on PC1');
legend;

% Projection on PC2 PDF
figure('Name', 'IntraPooled_ProjPC2PDFNotCentered'); hold on
for idT = 1:numel(T)
    p = plot(cbins2, pc2pdf(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC2');
ylabel('pdf');
title('Projection on PC2');
legend;

% -- Shuffled
% Variance explained
figure('Name', 'IntraPooled_VarExplained_shuffle'); hold on
bar(explained_s);
manualerrbar(1:5, ci_explained_s', 'k');
ax = gca;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel('Variance explained [%]');
title('Explained variance (shuffled)');
axis square
axis([0.5, 5.5, 0, 100]);

% PC vectors coeff
figure('Name', 'IntraPooled_PC1Coeff_shuffle'); hold on
plot(coeff_s(:, 1), 'k');
manualerrbar(1:5, errpc1_s', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC1)');
title('PC1 vector coefficients (shuffled)');
axis square

figure('Name', 'IntraPooled_PC2Coeff_shuffle'); hold on
plot(coeff_s(:, 2), 'k');
manualerrbar(1:5, errpc2_s', 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC2)');
title('PC2 vector coefficients (shuffled)');
axis square

% Projection for each temperature
figure('Name', 'IntraPooled_ProjPC1PC2NotCentered_shuffle');
allprojpc1 = cell(numel(T), 1);
allprojpc2 = cell(numel(T), 1);
temperaturelabel = cell(numel(T));
for idT = 1:numel(T)
    X = feat_mat_pooled{idT}./sfac;
    pc = X*coeff_s;
    
    allprojpc1{idT} = pc(:, 1);
    allprojpc2{idT} = pc(:, 2);
    temperaturelabel{idT} = T(idT)*ones(size(pc, 1), 1);
%     p = plot(pc(:, 1), pc(:, 2), '.');
%     p.Color = colors(idT, :);
%     p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
varnames = {'Projection on PC1', 'Projection on PC2', 'Temperature'};
allprojpc1 = cat(1, allprojpc1{:});
allprojpc2 = cat(1, allprojpc2{:});
temperaturelabel = cat(1, temperaturelabel{:});
tbl = table(allprojpc1, allprojpc2, temperaturelabel, 'VariableNames', varnames);
s = scatterhistogram(tbl, 'Projection on PC1', 'Projection on PC2', ...
    'GroupVariable', 'Temperature');
s.HistogramDisplayStyle = 'smooth';
s.LineStyle = '-';
s.LineWidth = 1.5;
s.MarkerSize = 10;
s.MarkerAlpha = 0.5*ones(numel(T), 1);
s.Color = colors;
s.LegendVisible = 'on';
s.Title = 'Projection in PC space (shuffled)';