% Compute and display autocorrelation of the three parameters --
% displacement, turn angle & interbout intervals and pturn.

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

pixsize = 0.09;
theta_threshold = 6;

% * Window
nbouts = 20;        % in bouts
nframes = 500;      % in frames

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Prepare figures
colors = rainbow(numel(T));
fig_bibi = figure('Name', 'ACFIbi_bout'); hold on; axis square;
plt_bibi = cell(numel(T), 1);
fig_tibi = figure('Name', 'ACFIbi_time'); hold on; axis square;
plt_tibi = cell(numel(T), 1);
fig_bdsp = figure('Name', 'ACFDsp_bout'); hold on; axis square;
plt_bdsp = cell(numel(T), 1);
fig_tdsp = figure('Name', 'ACFDsp_time'); hold on; axis square;
plt_tdsp = cell(numel(T), 1);
fig_bdte = figure('Name', 'ACFTag_bout'); hold on; axis square;
plt_bdte = cell(numel(T), 1);
fig_tdte = figure('Name', 'ACFTag_time'); hold on; axis square;
plt_tdte = cell(numel(T), 1);
fig_bptu = figure('Name', 'ACFPTu_bout'); hold on; axis square;
plt_bptu = cell(numel(T), 1);
fig_tptu = figure('Name', 'ACFPTu_time'); hold on; axis square;
plt_tptu = cell(numel(T), 1);

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing ACF for T = ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp] = getExperimentList(temperature);
    
    % Initialize arrays
    pacf_bibi = cell(size(ftrack, 1), 1);
    pacf_tibi = cell(size(ftrack, 1), 1);
    pacf_bdsp = cell(size(ftrack, 1), 1);
    pacf_tdsp = cell(size(ftrack, 1), 1);
    pacf_bdte = cell(size(ftrack, 1), 1);
    pacf_tdte = cell(size(ftrack, 1), 1);
    pacf_bptu = cell(size(ftrack, 1), 1);
    pacf_tptu = cell(size(ftrack, 1), 1);
    plagstime = cell(size(ftrack, 1), 1);
    
    % Loop over experiments
    for idx_exp = 1:size(ftrack, 1)
        
        % Get features
        [~, intbout, displacement, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        % Initialize 
        nseq = size(intbout, 2);
        acf_bibi = NaN(nseq, nbouts + 1);
        acf_tibi = NaN(nseq, nframes + 1);
        acf_bdsp = NaN(nseq, nbouts + 1);
        acf_tdsp = NaN(nseq, nframes + 1);
        acf_bdte = NaN(nseq, nbouts + 1);
        acf_tdte = NaN(nseq, nframes + 1);
        acf_bptu = NaN(nseq, nbouts + 1);
        acf_tptu = NaN(nseq, nframes + 1);
        lagstime = NaN(nseq, nframes + 1);
        
        % Loop over sequences
        for seq = 1:nseq
            
            % Get sequence data
            ibi = intbout(:, seq)./framerate;
            ibi(isnan(ibi)) = [];
            ibi = ibi - mean(ibi);
            dsp = displacement(:, seq).*pixsize;
            dsp(isnan(dsp)) = [];
            dsp = dsp - mean(dsp);
            dte = turnangle(:, seq);
            dte(isnan(dte)) = [];
            dte = dte - mean(dte);
            ptu = turnangle(:, seq);
            ptu(isnan(ptu)) = [];
            ptu(abs(ptu) < theta_threshold) = 0;
            ptu(ptu ~= 0) = 1;
            ptu = ptu - mean(ptu);
            
            % Filter out too short sequences or fish that turned all in the
            % same direction
            if numel(ibi) < 2*nbouts
                continue;
            elseif ~any(dte)
                continue;
            end
            
            % - Get ACF in bouts
            [xco, lag] = xcorr(ibi, nbouts, 'biased');
            selected_inds = find(lag == 0):find(lag == 0) + nbouts;
            acf_bibi(seq, :) = xco(selected_inds)./max(xco(selected_inds));  % normalization
            
            xco = xcorr(dsp, nbouts, 'biased');
            acf_bdsp(seq, :) = xco(selected_inds)./max(xco(selected_inds));  % normalization
            
            xco = xcorr(dte, nbouts, 'biased');
            acf_bdte(seq, :) = xco(selected_inds)./max(xco(selected_inds));  % normalization
            
            xco = xcorr(ptu, nbouts, 'biased');
            acf_bptu(seq, :) = xco(selected_inds)./max(xco(selected_inds));
            
            % - Get ACF in time : fill vector where fish is idle with the
            % preceeding value
            afr = allframes(:, seq) + 1;    
            afr(isnan(afr)) = [];           % full time vector
            fr = frames(:, seq) + 1;
            fr(isnan(fr)) = [];
            fr = fr - fr(1);
            fr(1) = [];                     % bout time points
            
            % Build time vector with values
            ibitime = NaN(size(afr));
            ibitime(fr) = ibi;
            ibitime = fillmissing(ibitime, 'previous');
            ibitime = fillmissing(ibitime, 'next');
            dsptime = NaN(size(afr));
            dsptime(fr) = dsp;
            dsptime = fillmissing(dsptime, 'previous');
            dsptime = fillmissing(dsptime, 'next');
            dtetime = NaN(size(afr));
            dtetime(fr) = dte;
            dtetime = fillmissing(dtetime, 'previous');
            dtetime = fillmissing(dtetime, 'next');
            ptutime = NaN(size(afr));
            ptutime(fr) = ptu;
            ptutime = fillmissing(ptutime, 'previous');
            ptutime = fillmissing(ptutime, 'next');
            
            % Compute ACF
            [xcotime, lagtime] = xcorr(ibitime, 'biased');
            selected_inds = find(lagtime == 0):find(lagtime == 0) + nframes;
            acf_tibi(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));  % norm
            
            xcotime = xcorr(dsptime, 'biased');
            acf_tdsp(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));  % norm
            
            xcotime = xcorr(dtetime, 'biased');
            acf_tdte(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));  % norm
            
            xcotime = xcorr(ptutime, 'biased');
            acf_tptu(seq, :) = xcotime(selected_inds)./max(xcotime(lagtime==0));
            
            lagstime(seq, :) = lagtime(selected_inds)./framerate;
        end
        
        % Store
        pacf_bibi{idx_exp} = acf_bibi;
        pacf_tibi{idx_exp} = acf_tibi;
        pacf_bdsp{idx_exp} = acf_bdsp;
        pacf_tdsp{idx_exp} = acf_tdsp;
        pacf_bdte{idx_exp} = acf_bdte;
        pacf_tdte{idx_exp} = acf_tdte;
        pacf_bptu{idx_exp} = acf_bptu;
        pacf_tptu{idx_exp} = acf_tptu;
        plagstime{idx_exp} = lagstime;
        
        fprintf('.');
    end
    
    % Pool all ACF
    pacf_bibi = cat(1, pacf_bibi{:});
    pacf_tibi = cat(1, pacf_tibi{:});
    pacf_bdsp = cat(1, pacf_bdsp{:});
    pacf_tdsp = cat(1, pacf_tdsp{:});
    pacf_bdte = cat(1, pacf_bdte{:});
    pacf_tdte = cat(1, pacf_tdte{:});
    pacf_bptu = cat(1, pacf_bptu{:});
    pacf_tptu = cat(1, pacf_tptu{:});
    plagstime = cat(1, plagstime{:});
    
    % Compute mean & sem across trajectories
    macf_bibi = nanmean(pacf_bibi, 1);
    eacf_bibi = nanstd(pacf_bibi, [], 1)/sqrt(size(pacf_bibi, 1));
    macf_tibi = nanmean(pacf_tibi, 1);
    eacf_tibi = nanstd(pacf_tibi, [], 1)/sqrt(size(pacf_tibi, 1));
    macf_bdsp = nanmean(pacf_bdsp, 1);
    eacf_bdsp = nanstd(pacf_bdsp, [], 1)/sqrt(size(pacf_bdsp, 1));
    macf_tdsp = nanmean(pacf_tdsp, 1);
    eacf_tdsp = nanstd(pacf_tdsp, [], 1)/sqrt(size(pacf_tdsp, 1));
    macf_bdte = nanmean(pacf_bdte, 1);
    eacf_bdte = nanstd(pacf_bdte, [], 1)/sqrt(size(pacf_bdte, 1));
    macf_tdte = nanmean(pacf_tdte, 1);
    eacf_tdte = nanstd(pacf_tdte, [], 1)/sqrt(size(pacf_tdte, 1));
    macf_bptu = nanmean(pacf_bptu, 1);
    eacf_bptu = nanstd(pacf_bptu, [], 1)/sqrt(size(pacf_bptu, 1));
    macf_tptu = nanmean(pacf_tptu, 1);
    eacf_tptu = nanstd(pacf_tptu, [], 1)/sqrt(size(pacf_tptu, 1));
    mlagstime = nanmean(plagstime);
    
    % --- Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(temperature) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 15;
    lineopt.LineStyle = '-';
    shadopt.FaceAlpha = 0.275;
    
    % Bouts
    set(0, 'CurrentFigure', fig_bibi);
    plt_bibi{idT} = errorshaded(1:nbouts, macf_bibi(2:end), eacf_bibi(2:end), 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [bout]');
    ylabel('<ACF(\delta{t})>_{traj.}');
    
    set(0, 'CurrentFigure', fig_bdsp);
    plt_bdsp{idT} = errorshaded(1:nbouts, macf_bdsp(2:end), eacf_bdsp(2:end), 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [bout]');
    ylabel('<ACF(d)>_{traj.}');
    
    set(0, 'CurrentFigure', fig_bdte);
    plt_bibi{idT} = errorshaded(1:nbouts, macf_bdte(2:end), eacf_bdte(2:end), 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [bout]');
    ylabel('<ACF(\delta\theta)>_{traj.}');
    
    set(0, 'CurrentFigure', fig_bptu);
    plt_bptu{idT} = errorshaded(1:nbouts, macf_bptu(2:end), eacf_bptu(2:end), 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [bout]');
    ylabel('<ACF(p_{turn})>_{traj.}');
    
    % Time
    lineopt.MarkerSize = 4;

    set(0, 'CurrentFigure', fig_tibi);
    plt_tibi{idT} = errorshaded(mlagstime, macf_tibi, eacf_tibi, 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [s]');
    ylabel('<ACF(\delta{t})>_{traj.}');

    set(0, 'CurrentFigure', fig_tdsp);
    plt_tibi{idT} = errorshaded(mlagstime, macf_tdsp, eacf_tdsp, 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [s]');
    ylabel('<ACF(d)>_{traj.}');
    
    set(0, 'CurrentFigure', fig_tdte);
    plt_tdte{idT} = errorshaded(mlagstime, macf_tdte, eacf_tdte, 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [s]');
    ylabel('<ACF(\delta\theta)>_{traj.}');
    
    set(0, 'CurrentFigure', fig_tptu);
    plt_tptu{idT} = errorshaded(mlagstime, macf_tptu, eacf_tptu, 'line', lineopt, 'patch', shadopt);
    xlabel('Delay [s]');
    ylabel('<ACF(p_{turn})>_{traj.}');
    
    fprintf('\t Done (%2.2fs).\n', toc);
end

% --- Display legends
legend([plt_bibi{:}]);
legend([plt_tibi{:}]);
legend([plt_bdsp{:}]);
legend([plt_tdsp{:}]);
legend([plt_bdte{:}]);
legend([plt_tdte{:}]);
legend([plt_bptu{:}]);
legend([plt_tptu{:}]);