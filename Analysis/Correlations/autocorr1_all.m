% Look at correlations with bout n + 1 for interbout interval, displacement
% turn angles and pturns.

% --- Parameters
% * Temperatures
T = [18, 22, 26, 30, 33];

pixsize = 0.09;
theta_threshold = 6;

% * Filters
filt.seq_length = 25;	% min sequence length for each fish in seconds
filt.bout_freq = .1;	% minimum average bout frequency
filt.minx = 0;
filt.maxx = Inf;

% --- Initialization
allac_ibi = cell(numel(T), 1);
allac_dsp = cell(numel(T), 1);
allac_dte = cell(numel(T), 1);
allac_ptu = cell(numel(T), 1);

% --- Processing
for idT = 1:numel(T)
    
    temperature = T(idT);
    
    fprintf(['Computing autocorrelation at bout n + 1 for ', num2str(temperature), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp] = getExperimentList(temperature);
    
    pac_ibi = cell(size(ftrack, 1), 1);
    pac_dsp = cell(size(ftrack, 1), 1);
    pac_dte = cell(size(ftrack, 1), 1);
    pac_ptu = cell(size(ftrack, 1), 1);
    
    for idexp = 1:size(ftrack, 1)
        
        [~, intbout, displacement, turnangle, frames, allframes, framerate] = Tracks.getFeatures(ftrack(idx_exp), fstamp(idx_exp), filt, false);
        
        % Initizalization
        nseq = size(intbout, 2);
        ac_ibi = NaN(nseq, 1);
        ac_dsp = NaN(nseq, 1);
        ac_dte = NaN(nseq, 1);
        ac_ptu = NaN(nseq, 1);
        
        % Loop over sequences
        for seq = 1:nseq
            
            % Get sequence data
            ibi = intbout(:, seq)./framerate;
            ibi(isnan(ibi)) = [];
            dsp = displacement(:, seq).*pixsize;
            dsp(isnan(dsp)) = [];
            dte = turnangle(:, seq);
            dte(isnan(dte)) = [];
            ptu = turnangle(:, seq);
            ptu(isnan(ptu)) = [];
            ptu(abs(ptu) < theta_threshold) = 0;
            ptu(ptu ~= 0) = 1;
            
            % Filter out too short sequences or fish that turned all in the
            % same direction
            if numel(ibi) < 20
                continue;
            elseif ~any(dte)
                continue;
            end
            
            % - Get ACF in bouts
            c = corr([ibi(1:end-1), ibi(2:end)]);
            ac_ibi(seq) = c(2, 1);
            
            c = corr([dsp(1:end-1), dsp(2:end)]);
            ac_dsp(seq) = c(2, 1);
            
            c = corr([dte(1:end-1), dte(2:end)]);
            ac_dte(seq) = c(2, 1);
            
            c = corr([ptu(1:end-1), ptu(2:end)]);
            ac_ptu(seq) = c(2, 1);
            
        end
        
        pac_ibi{idexp} = ac_ibi;
        pac_dsp{idexp} = ac_dsp;
        pac_dte{idexp} = ac_dte;
        pac_ptu{idexp} = ac_ptu;
        
        fprintf('.');
        
    end
    
    allac_ibi{idT} = cat(1, pac_ibi{:});
    allac_dsp{idT} = cat(1, pac_dsp{:});
    allac_dte{idT} = cat(1, pac_dte{:});
    allac_ptu{idT} = cat(1, pac_ptu{:});
    
    fprintf('\tDone (%2.2fs).\n', toc);
end

% --- Prepare data for boxplot
% Fill arrays to match size
allac_ibi = fixCellSize(allac_ibi, 1);
allac_dsp = fixCellSize(allac_dsp, 1);
allac_dte = fixCellSize(allac_dte, 1);
allac_ptu = fixCellSize(allac_ptu, 1);

% Make groups by temperature
g = cell(size(allac_ibi));
for idx = 1:size(allac_ibi, 1)
    g{idx} = idx.*ones(numel(allac_ibi{idx}), 1);
end
g = cat(1, g{:});
labels = arrayfun(@(x) [num2str(x) '°C'], T, 'UniformOutput', false);

% --- Display
pointopt = struct;
pointopt.Color = colors;
pointopt.jitter = 0;
boxopt = struct;
boxopt.BoxColors = colors;
boxopt.Labels = labels;

f = figure; hold on; axis square
f.Name = 'ACinterboutinterval';
X = cat(1, allac_ibi{:});
beautifulbox(X, g, pointopt, boxopt);
title('Autocorr \delta{t}');
ylabel('<\delta{t}_{n+1}\delta{t}_n>');
axis([-Inf, Inf, -1, 1]);

f = figure; hold on; axis square
f.Name = 'ACdisplacement';
X = cat(1, allac_dsp{:});
beautifulbox(X, g, pointopt, boxopt);
title('Autocorr d');
ylabel('<d_{n+1}d_n>');
axis([-Inf, Inf, -1, 1]);

f = figure; hold on; axis square
f.Name = 'ACdtheta';
X = cat(1, allac_dte{:});
beautifulbox(X, g, pointopt, boxopt);
title('Autocorr \delta\theta');
ylabel('<\delta\theta_{n+1}\delta\theta_n>');
axis([-Inf, Inf, -1, 1]);

f = figure; hold on; axis square
f.Name = 'ACpturn';
X = cat(1, allac_ptu{:});
beautifulbox(X, g, pointopt, boxopt);
title('Autocorr turn/forward');
ylabel('<bouttype_{n+1}bouttype_n>');
axis([-Inf, Inf, -1, 1]);