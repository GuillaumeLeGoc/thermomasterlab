% Plots correlation matrix for interbout intervals, displacements and turn
% angles, and compare mean by trajectory and values per bouts.

% close all
clear
clc

% --- Parameters
theta_threshold = 10;   % deg

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix_p'];
filedatabou = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];

fn_out = [pwd filesep 'Data' filesep 'Matfiles' filesep 'corrmat5param_p.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

nboots = 1000;

% * Figures
textsize = 12;

% --- Get data
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
tmp = load(filedatabou);
boutibi = tmp.interboutintervals;
boutdsp = tmp.displacements;
boutreo = tmp.dtheta;

% --- Processing
Rtraj = NaN(5, 5, 5);
Etraj = NaN(5, 5, 5);
Rbout = NaN(3, 3, 5);
Ebout = NaN(3, 3, 5);
Ctraj = NaN(10, numel(T));
ECtraj = cell(numel(T), 1);
Cbout = NaN(3, numel(T));
ECbout = cell(numel(T), 1);
for idT = 1:numel(T)
    
    ibi = boutibi{idT};
    dsp = boutdsp{idT};
    reo = boutreo{idT};
    
    % Limit to turns
    istrn = abs(reo) > theta_threshold;
    ibi(~istrn) = NaN;
    dsp(~istrn) = NaN;
    reo(~istrn) = NaN;
    
    % Gather
    ibi = ibi(:);
    ibi(isnan(ibi)) = [];
    dsp = dsp(:);
    dsp(isnan(dsp)) = [];
    reo = reo(:);
    reo(isnan(reo)) = [];
    
    % Get Pearson R
    [Rtraj(:, :, idT), ~, RL, RU] = corrcoef(featmat{idT});
    Ctraj(:, idT) = [Rtraj(1, 2, idT); Rtraj(1, 3, idT); Rtraj(1, 4, idT); Rtraj(1, 5, idT); ...
        Rtraj(2, 3, idT); Rtraj(2, 4, idT); Rtraj(2, 5, idT); ...
        Rtraj(3, 4, idT); Rtraj(3, 5, idT); ...
        Rtraj(4, 5, idT)];
    ECtraj{idT} = [RL(1, 2), RU(1, 2); RL(1, 3), RU(1, 3); RL(1, 4), RU(1, 4); RL(1, 5), RU(1, 5); ...
        RL(2, 3), RU(2, 3); RL(2, 4), RU(2, 4); RL(2, 5), RU(2, 5); ...
        RL(3, 4), RU(3, 4); RL(3, 5), RU(3, 5); ...
        RL(4, 5), RU(4, 5)];
    
    [Rbout(:, :, idT), ~, RL, RU] = corrcoef([ibi, reo, dsp]);
    Cbout(:, idT) = [Rbout(1, 2, idT); Rbout(1, 3, idT); Rbout(2, 3, idT)];
    ECbout{idT} = [RL(1, 2), RU(1, 2) ; RL(1, 3), RU(1, 3) ; RL(2, 3), RU(2, 3)];
    
    % Display R matrix for each temperature
    figure('Name', ['Rtraj_T' num2str(T(idT))]);
    imagesc(Rtraj(:, :, idT));
    caxis([-.75 .75]);
    colormap(div_bkr);
    c = colorbar;
    title(c, 'R_{traj.}');
    c.Ticks = [-0.5, 0, 0.5];
    axis square
    grid off
    ax = gca;
    ax.XTick = 1:5;
    ax.YTick = 1:5;
    ax.XTickLabel = {'<\delta\theta_{t}>_{traj}', '<\delta{t}>_{traj}', '<d>_{traj}', 'p_{t}', 'p_{f}'}';
    ax.XTickLabelRotation = 45;
    ax.YTickLabel = {'<\delta\theta_{t}>_{traj}', '<\delta{t}>_{traj}', '<d>_{traj}', 'p_{t}', 'p_{f}'}';
    ax.YTickLabelRotation = 45;
end

% Average over temperature
mRtraj = mean(Rtraj, 3);
mRbout = mean(Rbout, 3);

% Get temperature averaged features matrix
meanTfeatmat = cellfun(@mean, featmat, 'UniformOutput', false);
meanTfeatmat = cat(1, meanTfeatmat{:});

% Get correlation matrix
[Rtemp, ~, RL, RU] = corrcoef(meanTfeatmat);
lolz = bootci(nboots, @corrcoef, meanTfeatmat);
Ctemp = [Rtemp(1, 2); Rtemp(1, 3); Rtemp(1, 4); Rtemp(1, 5); ...
        Rtemp(2, 3); Rtemp(2, 4); Rtemp(2, 5); ...
        Rtemp(3, 4); Rtemp(3, 5); ...
        Rtemp(4, 5)];
ECtemp = [RL(1, 2), RU(1, 2); RL(1, 3), RU(1, 3); RL(1, 4), RU(1, 4); RL(1, 5), RU(1, 5); ...
        RL(2, 3), RU(2, 3); RL(2, 4), RU(2, 4); RL(2, 5), RU(2, 5); ...
        RL(3, 4), RU(3, 4); RL(3, 5), RU(3, 5); ...
        RL(4, 5), RU(4, 5)];

% --- Display
% * Temperature
figure('Name', 'Rtemp_mean');
imagesc(Rtemp);
caxis([-1 1]);
colormap(div_bkr);
c = colorbar;
title(c, 'R_T');
c.Ticks = [-1, 0, 1];
axis square
grid off
ax = gca;
ax.XTick = 1:5;
ax.YTick = 1:5;
ax.XTickLabel = {'<\delta\theta_{t}>_{T}', '<\delta{t}>_{T}', '<d>_T', '<p_{t}>_T', '<p_{f}>_T'}';
ax.XTickLabelRotation = 45;
ax.YTickLabel = {'<\delta\theta_{t}>_{T}', '<\delta{t}>_{T}', '<d>_T', '<p_{t}>_T', '<p_{f}>_T'}';
ax.YTickLabelRotation = 45;

% Create fake text to get size of text box
faketxt = text(1, 1, '0.00', 'Visible', 'off', 'FontSize', textsize);
xoffset = faketxt.Extent(3)/2;

[rows, cols] = size(Rtemp);
for i = 1:rows
      for j = 1:cols
          x = j - xoffset + 0.05;
          y = i + 0.05;
          text(x, y, sprintf('%0.2f', Rtemp(i, j)), ...
              'FontSize', textsize, ...
              'Color', [.75 .75 .75]);
      end
end

% * Trajectories
% - Mean matrix
figure('Name', 'Rtraj_mean');
imagesc(mRtraj);
caxis([-.75 .75]);
colormap(div_bkr);
c = colorbar;
title(c, '<R_{traj.}>_T');
c.Ticks = [-0.5, 0, 0.5];
axis square
grid off
ax = gca;
ax.XTick = 1:5;
ax.YTick = 1:5;
ax.XTickLabel = {'<\delta\theta_{t}>_{traj}', '<\delta{t}>_{traj}', '<d>_{traj}', 'p_{t}', 'p_{f}'}';
ax.XTickLabelRotation = 45;
ax.YTickLabel = {'<\delta\theta_{t}>_{traj}', '<\delta{t}>_{traj}', '<d>_{traj}', 'p_{t}', 'p_{f}'}';
ax.YTickLabelRotation = 45;

% Create fake text to get size of text box
faketxt = text(1, 1, '0.00', 'Visible', 'off', 'FontSize', textsize);
xoffset = faketxt.Extent(3)/2;

[rows, cols] = size(mRtraj);
for i = 1:rows
      for j = 1:cols
          x = j - xoffset + 0.05;
          y = i + 0.05;
          text(x, y, sprintf('%0.2f', mRtraj(i, j)), ...
              'FontSize', textsize, ...
              'Color', [.75 .75 .75]);
      end
end

% - Bar plot
leg = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
figure('Name', 'Coeff_traj');
ax = gca;
ax.ColorOrder = rainbow(numel(T)); hold on
b = bar(Ctraj, 'LineStyle', 'none');
legend(leg, 'Location', 'southeast', 'AutoUpdate', 'off');
ax.XTick = 1:10;
ax.XTickLabel = {'\delta\theta_{t}-\delta{t}', ...
    '\delta\theta_{t}-d', ...
    '\delta\theta_{t}-p_{t}', ...
    '\delta\theta_{t}-p_{f}', ...
    '\delta{t}-d', ...
    '\delta{t}-p_{t}', ...
    '\delta{t}-p_{f}', ...
    'd-p_{t}', ...
    'd-p_{f}', ...
    'p_{t}-p_{f}'};
ax.XTickLabelRotation = 45;
ax.YTick = [-0.75, 0, 0.75];
ylabel('R_{traj.}');
ylim([-0.75, 0.75]);

% Add error bars
[ngroups, nbars] = size(Ctraj);
for idb = 1:nbars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, ECtraj{idb}, 'Color', [0.01, 0.01, 0.01]);
end

% Add values of mean matrix
yyaxis right
p = plot(1:10, Ctemp, 'o-', 'Color', [0.45, 0.45, 0.45]);
manualerrbar(1:10, ECtemp, '-', 'Color', [0.45, 0.45, 0.45]);
ax.YColor = [0.45 0.45 0.45];
ylabel(ax, 'R_{temp.}');

ax.Position = [ax.Position(1), ax.Position(2), ax.Position(3), ax.Position(4)*0.75];

% * Bouts
% - Correl matrix
figure('Name', 'Rbouts_mean');
imagesc(mRbout);
caxis([-.5 .5]);
colormap(div_bkr);
c = colorbar;
title(c, '<R_{bouts}_T>');
c.Ticks = [-0.5, 0, 0.5];
axis square
grid off
ax = gca;
ax.XTick = 1:3;
ax.YTick = 1:3;
ax.XTickLabel = {'\delta\theta_{t,bouts}', '\delta{t}_{bouts}', 'd_{bouts}'}';
ax.XTickLabelRotation = 45;
ax.YTickLabel = {'\delta\theta_{t,bouts}', '\delta{t}_{bouts}', 'd_{bouts}'}';
ax.YTickLabelRotation = 45;

% Create fake text to get size of text box
faketxt = text(1, 1, '0.00', 'Visible', 'off', 'FontSize', textsize);
xoffset = faketxt.Extent(3)/2;

[rows, cols] = size(mRbout);
for i = 1:rows
      for j = 1:cols
          x = j - xoffset + 0.05;
          y = i + 0.05;
          text(x, y, sprintf('%0.2f', mRbout(i, j)), ...
              'FontSize', textsize, ...
              'Color', [.75 .75 .75]);
      end
end

% - Bar plot
figure('Name', 'Coeff_bout');
ax = gca;
ax.ColorOrder = rainbow(numel(T)); hold on
b = bar(Cbout, 'LineStyle', 'none');
legend(leg, 'Location', 'southeast', 'AutoUpdate', 'off');
ax.XTick = 1:3;
ax.XTickLabel = {'\delta\theta_{t}-\delta{t}', ...
    '\delta\theta_{t}-d', ...
    '\delta{t}-d'};
ax.XTickLabelRotation = 45;
ylabel('R_{bouts}');
ylim([-0.5, 0.5]);
ax.YTick = [-.5, 0, 0.5];

% Add error bars
[ngroups, nbars] = size(Cbout);
for idb = 1:nbars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, ECbout{idb}, 'Color', [0.01, 0.01, 0.01]);
end

% --- Save
if ~isempty(fn_out)
    META = 'mRtraj : temperature average, Rtraj nfeat x nfeat x nTemp';
    DESC = '1. turnamp 2. ibi 3. d 4. pturn 5. pflip';
    save(fn_out, 'mRtraj', 'Rtraj', 'META');
end