% Display cloud plot in 3D of pt, kf and d and ibi as dot size

% close all
clear
clc

% --- Parameters
theta_threshold = 10;   % deg

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix_pk'];
filepca = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk.mat'];
filemeans = [pwd filesep 'Data' filesep 'Matfiles' filesep 'MeanOverTemperatures.mat'];
fileptkf = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ptkfmeanT.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Figure
colors = {'#214478', '#3d944c', '#d9b424', '#ff7f2a', '#ff2800'};
basemarksize = 64;
figure; hold on

% --- Get data
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
pcacoef = load(filepca);
pcavec1 = pcacoef.pc1intrap;
pcavec2 = pcacoef.pc2intrap;
meansT = load(filemeans);
meansK = load(fileptkf);

s = cell(numel(T), 1);
pooledX = cell(numel(T), 1);
pooledY = cell(numel(T), 1);
pooledZ = cell(numel(T), 1);
for idT = 1:numel(T)
    
    FM = featmat{idT};
    
    I = FM(:, 2);   % ibi
    X = FM(:, 3);   % dsp
    Y = FM(:, 4);   % pt
    Z = FM(:, 5);   % kf

    % Fit
    m = 'a*x + b*y';
    ft = fit([X, Y], Z, m, 'StartPoint', [0, 0]);
    
    s{idT} = scatter3(X, Y, Z, (basemarksize*I).^0.5);
%     s = scatter3(X, Y, Z, basemarksize);
    s{idT}.Marker = 'o';
    s{idT}.MarkerFaceColor = colors{idT};
    s{idT}.MarkerEdgeColor = 'none';
%     s{idT}.CData = colors(idT, :);
    s{idT}.MarkerFaceAlpha = 0.5;
    s{idT}.DisplayName = [num2str(T(idT)) '°C'];
    
    pooledX{idT} = X;
    pooledY{idT} = Y;
    pooledZ{idT} = Z;
    
    mi = meansT.mibi(idT);
    mx = meansT.mdsp(idT);
    my = meansK.mpturn(idT);
    mz = meansK.mkflip(idT);
    
    ss = scatter3(mx, my, mz, 10*(basemarksize*mi).^0.5);
    ss.Marker = 'o';
    ss.MarkerFaceColor = colors{idT};
    ss.MarkerEdgeColor = 'k';
    ss.MarkerFaceAlpha = 1;
    ss.LineWidth = 2;

    % Plot fit
    % x0 = mean(cat(1, pooledX{:}));
    % y0 = mean(cat(1, pooledY{:}));
    % z0 = mean(cat(1, pooledZ{:}));
    % x1 = [x0 - 4*pcavec1(3), x0 + 3*pcavec1(3)];
    % y1 = [y0 - 4*pcavec1(4), y0 + 3*pcavec1(4)];
    % z1 = [z0 - 4*pcavec1(5), z0 + 3*pcavec1(5)];

%     x1 = [x0 - 4*pcavec1(3), x0 + 3*pcavec1(3)];
%     y1 = [y0 - 4*pcavec1(4), y0 + 3*pcavec1(4)];
%     z1 = [z0 - 4*pcavec1(5), z0 + 3*pcavec1(5)];
    
%     plot3(x1, y1, z1, 'k--', 'DisplayName', 'PC1', 'LineWidth', 2);
    plot3(sort(X), sort(Y), ft.a.*sort(X)+ ft.b.*sort(Y), 'k--');
end

% Labels
xlabel('d (mm)');
ylabel('p_t');
zlabel('k_f (s^{-1})');

% Caption
r = scatter3(2.5, 1, 0.25, basemarksize^0.5, [0, 0, 0]);
r.MarkerEdgeColor = 'none';
r.MarkerFaceColor = 'flat';
t = scatter3(2.75, 1, 0.25, (2*basemarksize)^0.5, [0, 0, 0]);
t.MarkerEdgeColor = 'none';
t.MarkerFaceColor = 'flat';
u = scatter3(3, 1, 0.25, (3*basemarksize)^0.5, [0, 0, 0]);
u.MarkerEdgeColor = 'none';
u.MarkerFaceColor = 'flat';
u.DisplayName = '\delta{t}, 1,2,3s';
legend([s{:}, u]);

% PC space
% x0 = mean(cat(1, pooledX{:}));
% y0 = mean(cat(1, pooledY{:}));
% z0 = mean(cat(1, pooledZ{:}));
% x1 = [x0 - 4*pcavec1(3), x0 + 3*pcavec1(3)];
% y1 = [y0 - 4*pcavec1(4), y0 + 3*pcavec1(4)];
% z1 = [z0 - 4*pcavec1(5), z0 + 3*pcavec1(5)];

% plot3(x1, y1, z1, 'k--', 'DisplayName', 'PC1', 'LineWidth', 2);

% Limits
xlim([0, 3]);
ylim([0, 1]);
zlim([0, 1.0]);

ax = gca;
ax.View = [40, 25];
ax.ZTick = [0, 0.5, 1];
ax.YTick = [0, 0.5, 1];
ax.XTick = [0, 1, 2, 3];
f = gcf;
f.InnerPosition = [1, 1, 638, 517];