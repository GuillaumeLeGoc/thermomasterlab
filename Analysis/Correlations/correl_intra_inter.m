% Compute correlations matrix of the inter pooled and intra pooled matrices
% and plot coefficients to compare them.

% close all
clear
clc

% --- Parameters
% * Feature matrix location
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
nboots = 1000;

% * Figures
colors = rainbow(numel(T));

% * Control
% 1 : IBI
% 2 : pturn
% 3 : pflip
% 4 : sigmaturn
% 5 : d
idx_to_shuffle = [1, 2, 3, 4, 5];

% --- Load file
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
nfeat = size(featmat{1}, 2);

% --- Processing
% - Build the two matrices
% Inter
intermat = cat(1, featmat{:});
% intermat_std = intermat;
% intermat = cellfun(@mean, featmat, 'UniformOutput', false);
% intermat = cat(1, intermat{:});
intermat_std = intermat;

% Intra
% intramat_std = cellfun(@(x) (x - mean(x))./std(x), featmat, 'UniformOutput', false);
intramat_std = cellfun(@(x) x./mean(x), featmat, 'UniformOutput', false);
intramat_std = cat(1, intramat_std{:});

% - Compute corr matrix & bootstrap confidence interval
Rinter = corrcoef(intermat_std);
Rintra = corrcoef(intramat_std);

ciRinter = bootci(nboots, @corrcoef, intermat_std);
lociRinter = squeeze(ciRinter(1, :, :));
upciRinter = squeeze(ciRinter(2, :, :));
ciRintra = bootci(nboots, @corrcoef, intramat_std);
lociRintra = squeeze(ciRintra(1, :, :));
upciRintra = squeeze(ciRintra(2, :, :));

% - Linearize
Rinter_l = triu(Rinter, 1);
Rinter_l(Rinter_l == 0) = [];
loRinter_l = triu(lociRinter, 1);
loRinter_l(loRinter_l == 0) = [];
yneg = Rinter_l - loRinter_l;
upRinter_l = triu(upciRinter, 1);
upRinter_l(upRinter_l == 0) = [];
ypos = upRinter_l - Rinter_l;

Rintra_l = triu(Rintra, 1);
Rintra_l(Rintra_l == 0) = [];
loRintra_l = triu(lociRintra, 1);
loRintra_l(loRintra_l == 0) = [];
xneg = Rintra_l - loRintra_l;
upRintra_l = triu(upciRintra, 1);
upRintra_l(upRintra_l == 0) = [];
xpos = upRintra_l - Rintra_l;

% - Regression
rho = corr([Rintra_l', Rinter_l']);
rho = rho(2, 1);
r = corrcoef([Rintra_l', Rinter_l']);
r = r(2, 1);
p = fit(Rintra_l', Rinter_l', 'a.*x', 'StartPoint', 1);
texte = sprintf('\\rho=%1.2f\nR=%1.2f', rho, r);

% --- Display
% - Intra correlations
fig = figure; fig.Name = 'IntraPooled_Correlations';
imagesc(Rintra);
colormap(div_bkr);
caxis([-.75, .75]);
c = colorbar;
title(c, 'R');
ax = gca;
ax.XTick = [1, 2, 3, 4, 5];
ax.YTick = [1, 2, 3, 4, 5];
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax.YTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YTickLabelRotation = 45;
grid(ax, 'off');
axis(ax, 'square');
title('R on intra matrix');

% - Inter correlations
fig = figure; fig.Name = 'InterPooled_Correlations';
imagesc(Rinter);
colormap(div_bkr);
caxis([-.75, .75]);
c = colorbar;
title(c, 'R');
ax = gca;
ax.XTick = [1, 2, 3, 4, 5];
ax.YTick = [1, 2, 3, 4, 5];
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax.YTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YTickLabelRotation = 45;
grid(ax, 'off');
axis(ax, 'square');
title('R on inter matrix');

% - Coeffiecients regression
fig = figure; ax = axes(fig); hold(ax, 'on');
fig.Name = 'R_InterIntra';
e = errorbar(ax, Rintra_l, Rinter_l, yneg, ypos, xneg, xpos);
e.Color = 'k';
e.LineStyle = 'none';
e.CapSize = 0;
e.Marker = '.';
e.MarkerSize = 10;
e.MarkerFaceColor = 'r';
e.MarkerEdgeColor = 'r';
e.LineWidth = 1.25;
plot(linspace(-.75, .75, 100), p(linspace(-.75, .75, 100)), '--k', 'LineWidth', 1);
xlabel(ax, 'R_{intra}');
ylabel(ax, 'R_{inter}');
axis(ax, [-.75, .75, -.75, .75]);
ax.XTick = [-0.5, 0.5];
ax.YTick = [-0.5, 0.5];
grid(ax, 'off');
axis(ax, 'square');
l = legend;
l.Location = 'southeast';
l.Visible = 'off';
t = text(l.Position(1), l.Position(2), texte, 'Units', 'normalized');
t.FontSize = 15;
t.FontName = ax.FontName;