% Display cloud plot in 3D of ibi, kf and d.

% close all
clear
clc

% --- Parameters
theta_threshold = 10;   % deg

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix_pk'];
filepca = [pwd filesep 'Data' filesep 'Matfiles' filesep 'PCAIntrapooled_pk.mat'];
filemeans = [pwd filesep 'Data' filesep 'Matfiles' filesep 'MeanOverTemperatures.mat'];
fileptkf = [pwd filesep 'Data' filesep 'Matfiles' filesep 'ptkfmeanT.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Figure
colors = {'#214478', '#3d944c', '#d9b424', '#ff7f2a', '#ff2800'};
basemarksize = 12;
figure; hold on

% --- Get data
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
pcacoef = load(filepca);
pcavec1 = pcacoef.pc1intrap;
pcavec2 = pcacoef.pc2intrap;
meansT = load(filemeans);
meansK = load(fileptkf);

s = cell(numel(T), 1);
pooledX = cell(numel(T), 1);
pooledY = cell(numel(T), 1);
pooledZ = cell(numel(T), 1);
for idT = 1:numel(T)
    
    FM = featmat{idT};
    
    X = FM(:, 3);   % ibi
    Y = FM(:, 2);   % dsp
    Z = FM(:, 5);   % kf
    
    s{idT} = scatter3(X, Y, Z, basemarksize);
    s{idT}.Marker = 'o';
    s{idT}.MarkerFaceColor = colors{idT};
    s{idT}.MarkerEdgeColor = 'none';
%     s{idT}.CData = colors(idT, :);
    s{idT}.MarkerFaceAlpha = 0.5;
    s{idT}.DisplayName = [num2str(T(idT)) '°C'];
    
    pooledX{idT} = X;
    pooledY{idT} = Y;
    pooledZ{idT} = Z;
    
    my = meansT.mibi(idT);
    mx = meansT.mdsp(idT);
    mz = meansK.mkflip(idT);
    
    ss = scatter3(mx, my, mz, 6*basemarksize);
    ss.Marker = 'o';
    ss.MarkerFaceColor = colors{idT};
    ss.MarkerEdgeColor = 'k';
    ss.MarkerFaceAlpha = 1;
    ss.LineWidth = 2;
end

% Labels
ylabel('\delta{t} (s)');
xlabel('d (mm)');
zlabel('k_f (s^{-1})');

% Limits
xlim([0, 3]);
ylim([0.4, 2]);
zlim([0, 1.0]);

ax = gca;
ax.View = [40, 25];
ax.ZTick = [0, 0.5, 1];
ax.YTick = [0, 0.5, 1];
ax.XTick = [0, 1, 2, 3];
f = gcf;
f.InnerPosition = [1, 1, 638, 517];