% Get covariance matrix of interbout interval, displacement and turn angles
% distribution.

% close all
clear

% --- Files
seqfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];
meanseq = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix'];

% --- Temperatures
T = [18, 22, 26, 30, 33];

% --- Parameters
framerate = 25;
pixsize = 0.09;

% --- Get data
D = load(seqfile);

% --- Processing
for idT = 1:numel(T)
    
    % Build distributions matrix
    ibi = D.interboutintervals{idT};
    ibi(isnan(ibi)) = [];
    ibi = ibi(:);
    dsp = D.displacements{idT};
    dsp(isnan(dsp)) = [];
    dsp = dsp(:);
    dte = D.dtheta{idT};
    dte(isnan(dte)) = [];
    dte = dte(:);
    
    M = [ibi./framerate, dsp.*pixsize, abs(dte)];
    
    corr(M)
end

a = D.interboutintervals{idT}./framerate;
b = D.displacements{idT}.*pixsize;
am = nanmean(a)'; bm = nanmean(b)';
O = [am, bm];