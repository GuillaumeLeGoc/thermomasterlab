% PCA on mean features matrix. Takes the temperature averaged of mean 
% features.

% close all
clear
clc

% --- Parameters
% * Feature matrix location
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix'];

% * Save file
filename = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pca_intermean.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
nboots = 1000;

% * Figures
colors = rainbow(numel(T));

% * Control
% 1 : IBI
% 2 : pturn
% 3 : pflip
% 4 : sigmaturn
% 5 : d
idx_to_shuffle = [1, 2, 3, 4, 5];

% --- Load file
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
nfeat = size(featmat{1}, 2);

% --- Processing
fprintf('Intertemperature, mean matrix PC analysis...'); tic;

% - Process matrix with bootstrapping
btfeatmat = cell(numel(T), 1);
for idT = 1:numel(T)
    
    % Get matrix
    X = featmat{idT};
    
    % Bootstrap the mean
    btfeatmat{idT} = bootstrp(nboots, @mean, X);
end

% PCA for each set of means
coeffspc1 = NaN(nfeat, nboots);
coeffspc1_s = NaN(nfeat, nboots);
coeffspc2 = NaN(nfeat, nboots);
coeffspc2_s = NaN(nfeat, nboots);
explaineds = NaN(nfeat, nboots);
explaineds_s = NaN(nfeat, nboots);
for idb = 1:nboots
    
    % Create matrix
    samplefeatmat = NaN(numel(T), nfeat);
    for idT = 1:numel(T)
        samplefeatmat(idT, :) = btfeatmat{idT}(idb, :);
    end
    samplefeatmat = samplefeatmat./std(samplefeatmat);
    
    % Shuffle
    samplefeatmat_s = samplefeatmat;
    for idx = idx_to_shuffle
        samplefeatmat_s(:, idx) = samplefeatmat_s(randperm(length(samplefeatmat_s(:, idx)))', idx);
    end 
    
    % PCA
    [coef, ~, ~, ~, expl] = pca(samplefeatmat);
    [coef_s, ~, ~, ~, expl_s] = pca(samplefeatmat_s);
    
    if numel(expl) == 4
        expl(end + 1) = 0;
    end
    if numel(expl_s) == 4
        expl_s(end + 1) = 0;
    end
    
    % Store
    coeffspc1(:, idb) = coef(:, 1);
    coeffspc1_s(:, idb) = coef_s(:, 1);
    coeffspc2(:, idb) = coef(:, 2);
    coeffspc2_s(:, idb) = coef_s(:, 2);
    explaineds(:, idb) = expl;
    explaineds_s(:, idb) = expl_s;
end

% Get 95% confidence intervals
mcoeffpc1 = mean(coeffspc1, 2);
mcoeffpc1_s = mean(coeffspc1_s, 2);
mcoeffpc2 = mean(coeffspc2, 2);
mcoeffpc2_s = mean(coeffspc2_s, 2);
mexplained = mean(explaineds, 2);
mexplained_s = mean(explaineds_s, 2);
errpc1 = NaN(nfeat, 2);
errpc1_s = NaN(nfeat, 2);
errpc2 = NaN(nfeat, 2);
errpc2_s = NaN(nfeat, 2);
errexplained = NaN(nfeat, 2);
errexplained_s = NaN(nfeat, 2);
for idf = 1:nfeat
    ft = fitdist(coeffspc1(idf, :)', 'normal');
    errpc1(idf, :) = [mcoeffpc1(idf) - 2*ft.sigma, mcoeffpc1(idf) + 2*ft.sigma];
    ft = fitdist(coeffspc1_s(idf, :)', 'normal');
    errpc1_s(idf, :) = [mcoeffpc1_s(idf) - 2*ft.sigma, mcoeffpc1_s(idf) + 2*ft.sigma];
    ft = fitdist(coeffspc2(idf, :)', 'normal');
    errpc2(idf, :) = [mcoeffpc2(idf) - 2*ft.sigma, mcoeffpc2(idf) + 2*ft.sigma];
    ft = fitdist(coeffspc2_s(idf, :)', 'normal');
    errpc2_s(idf, :) = [mcoeffpc2_s(idf) - 2*ft.sigma, mcoeffpc2_s(idf) + 2*ft.sigma];
    ft = fitdist(explaineds(idf, :)', 'normal');
    errexplained(idf, :) = [mexplained(idf) - 2*ft.sigma, mexplained(idf) + 2*ft.sigma];
    ft = fitdist(explaineds_s(idf, :)', 'normal');
    errexplained_s(idf, :) = [mexplained_s(idf) - 2*ft.sigma, mexplained_s(idf) + 2*ft.sigma];
end

save(filename, 'mexplained', 'mcoeffpc1', 'mcoeffpc2', 'errexplained', 'errpc1', 'errpc2');

fprintf('\t Done (%2.2fs).\n', toc);

% --- Display
% - Real

% Variance explained
figure('Name', 'InterMean_VarExplained'); hold on
bar(mexplained);
manualerrbar(1:4, errexplained, 'k');
ax = gca;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel('Variance explained [%]');
title('Explained var. (Mean matrix)');
axis square
axis([0.5, 5.5, 0, 100]);

% PC vectors coeff
figure('Name', 'InterMean_PC1Coeff'); hold on
p = plot(mcoeffpc1);
p.Color = 'k';
p.Marker = 'diamond';
manualerrbar(1:5, errpc1, 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
axis(ax, [-Inf, Inf, -1, 1]);
ylabel('coeff(PC1)');
title('PC1 vector coefficients (Mean matrix)');
axis square

figure('Name', 'InterMean_PC2Coeff'); hold on
plot(mcoeffpc2, 'k');
manualerrbar(1:5, errpc2, 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC_2)');
title('PC2 vector coefficients (Mean matrix)');
axis square

% Projection
figure('Name', 'InterMean_ProjPC1PC2_NotCentered'); hold on
featmat_std = cat(1, featmat{:});
mfac = mean(featmat_std);
sfac = std(featmat_std);
for idT = 1:numel(T)
    X = (featmat{idT} - mfac)./sfac;
    pc = X*[mcoeffpc1, mcoeffpc2];
    p = plot(pc(:, 1), pc(:, 2), '.');
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC1');
ylabel('PC2');
axis([-5, 5, -5, 5]);
title('Projection in PC space');
legend('Location', 'southeast');

% - Shuffled
% Variance explained
figure('Name', 'InterMean_VarExplained_shuffle'); hold on
bar(mexplained_s);
manualerrbar(1:4, errexplained_s, 'k');
ax = gca;
ax.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel('Variance explained [%]');
title('Explained var. (Mean matrix, shuffled)');
axis square
axis([0.5, 5.5, 0, 100]);

% PC vectors coeff
figure('Name', 'InterMean_PC1Coeff_shuffle'); hold on
plot(mcoeffpc1_s, 'k');
manualerrbar(1:5, errpc1_s, 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ylabel('coeff(PC_1)');
title('PC1 vector coefficients (Mean matrix, shuffled)');
ax.YLim = [-1, 1];
axis square

figure('Name', 'InterMean_PC2Coeff_shuffle'); hold on
plot(mcoeffpc2_s, 'k');
manualerrbar(1:5, errpc2_s, 'k');
ax = gca;
ax.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'};
ax.XTickLabelRotation = 45;
ax.YLim = [-1, 1];
ylabel('coeff(PC_2)');
title('PC2 vector coefficients (Mean matrix, shuffled)');
axis square

% Projection
figure('Name', 'InterMean_ProjPC1PC2_NotCentered_shuffle'); hold on
featmat_std = cat(1, featmat{:});
mfac = mean(featmat_std);
sfac = std(featmat_std);
for idT = 1:numel(T)
    X = (featmat{idT} - mfac)./sfac;
    pc = X*[mcoeffpc1_s, mcoeffpc2_s];
    p = plot(pc(:, 1), pc(:, 2), '.');
    p.Color = colors(idT, :);
    p.DisplayName = ['T = ' num2str(T(idT)) '°C'];
end
axis square
xlabel('PC1');
ylabel('PC2');
axis([-5, 5, -5, 5]);
title('Projection in PC space');
legend('Location', 'southeast');