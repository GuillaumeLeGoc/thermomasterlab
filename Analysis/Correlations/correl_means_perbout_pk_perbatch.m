% Plots correlation matrix for interbout intervals, displacements and turn
% angles, and compare mean by trajectory and values per bouts. PER BATCH

% close all
clear
clc

% --- Parameters
theta_threshold = 10;   % deg

% * Files
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix_pk_perbatch.mat'];
fileRtemp = [pwd filesep 'Data' filesep 'Matfiles' filesep 'RCoeffTemp.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Get data
featmat = load(filefeatmat);
featmat = featmat.feat_mat_pooled;
Ctemp = load(fileRtemp);
Ctemp = Ctemp.Ctemp;
npairs = numel(Ctemp);

% * Figures
fcolors = @(n) cm10(n);
tcolors = rainbow(numel(T));

% --- Init
mCtraj = NaN(npairs, numel(T));
eCtraj = NaN(npairs, 2, numel(T));

% --- Processing
for idT = 1:numel(T)
    
    nbatch = numel(featmat{idT});
    Ctraj = NaN(10, nbatch);
    mRtraj = NaN(5, 5, nbatch);
    ECtraj = cell(nbatch, 1);
    
    for idexp = 1:nbatch
        
        % Get Pearson R
        Rtraj = corrcoef(featmat{idT}{idexp});
        
        Ctraj(:, idexp) = [Rtraj(1, 2); Rtraj(1, 3); Rtraj(1, 4); Rtraj(1, 5); ...
            Rtraj(2, 3); Rtraj(2, 4); Rtraj(2, 5); ...
            Rtraj(3, 4); Rtraj(3, 5); ...
            Rtraj(4, 5)];
        
        mRtraj(:, :, idexp) = Rtraj;
        
    end
    
    % * Get mean
    mRtraj = mean(mRtraj, 3);
    mCtraj(:, idT) = mean(Ctraj, 2);
    E = std(Ctraj, [], 2)./sqrt(size(Ctraj, 2));
    eCtraj(:, :, idT) = [mCtraj(:, idT) - E, mCtraj(:, idT) + E];
    
    % * Display for temperature
    figure('Name', ['Rtraj_T' num2str(T(idT))]);
    imagesc(mRtraj);
    caxis([-.75 .75]);
    colormap(div_bkr);
    c = colorbar;
    title(c, 'R_{traj.}');
    c.Ticks = [-0.5, 0, 0.5];
    axis square
    grid off
    ax = gca;
    ax.XTick = 1:5;
    ax.YTick = 1:5;
    ax.XTickLabel = {'<\delta\theta_{t}>_{traj}', '<\delta{t}>_{traj}', '<d>_{traj}', 'p_{t}', 'k_{f}'}';
    ax.XTickLabelRotation = 45;
    ax.YTickLabel = {'<\delta\theta_{t}>_{traj}', '<\delta{t}>_{traj}', '<d>_{traj}', 'p_{t}', 'k_{f}'}';
    ax.YTickLabelRotation = 45;
    
    leg = arrayfun(@(x) ['Batch ' num2str(x)], 1:nbatch, 'UniformOutput', false);
    figure('Name', 'Coeff_traj');
    ax = gca;
    ax.ColorOrder = fcolors(nbatch); hold on
    bar(Ctraj, 'EdgeColor', 'none', 'LineStyle', 'none');
    legend(leg, 'Location', 'southeast', 'AutoUpdate', 'off');
    ax.XTick = 1:npairs;
    ax.XTickLabel = {'\delta\theta_{t}-\delta{t}', ...
    '\delta\theta_{t}-d', ...
    '\delta\theta_{t}-p_{t}', ...
    '\delta\theta_{t}-k_{f}', ...
    '\delta{t}-d', ...
    '\delta{t}-p_{t}', ...
    '\delta{t}-k_{f}', ...
    'd-p_{t}', ...
    'd-k_{f}', ...
    'p_{t}-k_{f}'};
    ax.XTickLabelRotation = 45;
    ax.YTick = [-0.75, 0, 0.75];
    ylabel('R_{traj.}');
    ylim([-0.75, 0.75]);
    title(['T = ' num2str(T(idT)) '°C']);    
end

% * Display mean +/- sem
figure; ax = gca; hold(ax, 'on');
mixC = mCtraj;
ax.ColorOrder = tcolors;
b = bar(ax, mixC, 'EdgeColor', 'none');
leg = arrayfun(@(x) ['T=' num2str(x) '°C'], T, 'UniformOutput', false);
legend(ax, leg, 'Location', 'southeast', 'AutoUpdate', 'off');
ax.XTick = 1:npairs;
ax.XTickLabel = {'\delta\theta_{t}-\delta{t}', ...
    '\delta\theta_{t}-d', ...
    '\delta\theta_{t}-p_{t}', ...
    '\delta\theta_{t}-k_{f}', ...
    '\delta{t}-d', ...
    '\delta{t}-p_{t}', ...
    '\delta{t}-k_{f}', ...
    'd-p_{t}', ...
    'd-k_{f}', ...
    'p_{t}-k_{f}'};
ax.YLim = [-.75, .75];
ylabel(ax, '<R_{traj.}>_{batch}');

% Add error bars
[ngroups, nbars] = size(mixC);
for idb = 1:nbars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, eCtraj(:, :, idb), 'Color', [0.01, 0.01, 0.01]);
end