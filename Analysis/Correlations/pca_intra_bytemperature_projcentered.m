% Build features matrix and look at PCA. One per temperature.
% Projection of centered data.

% close all
clear
clc

% --- Parameters
% * Feature matrix location
filefeatmat = [pwd filesep 'Data' filesep 'Matfiles' filesep 'featuresmatrix'];

% * Save
filename = [pwd filesep 'Data' filesep 'Matfiles' filesep 'pca_intrabytemp.mat'];

% * Temperatures
T = [18, 22, 26, 30, 33];

% * Settings
nboots = 1000;
nbins = 200;

% * Figures
colors = rainbow(numel(T));

% * Control
% 1 : IBI
% 2 : pturn
% 3 : pflip
% 4 : sigmaturn
% 5 : d
nfeatures = 5;
idx_to_shuffle = 1:nfeatures;

% --- Load file
feat_mat_pooled = load(filefeatmat);
feat_mat_pooled = feat_mat_pooled.feat_mat_pooled;

% --- PCA
fprintf('Intratemperature, by temperature PC analysis...'); tic;

% Standardize data
X_std = cellfun(@(x) (x - mean(x))./std(x), feat_mat_pooled, 'UniformOutput', false);

% Shuffle
Xshuffle_std = X_std;
for idT = 1:numel(T)
    xshuff = Xshuffle_std{idT};
    for idx = idx_to_shuffle
        xshuff(:, idx) = xshuff(randperm(length(xshuff(:, idx)))', idx);
    end
    Xshuffle_std{idT} = xshuff;
end

% Correlations & PCA
corrmat = cell(numel(T), 1);
corrmat_s = cell(numel(T), 1);
explained = NaN(nfeatures, numel(T));
errexplained = cell(numel(T), 1);
explained_s = NaN(nfeatures, numel(T));
errexplained_s = cell(numel(T), 1);
coeffpc1 = NaN(nfeatures, numel(T));
errpc1 = cell(numel(T), 1);
coeffpc1_s = NaN(nfeatures, numel(T));
errpc1_s = cell(numel(T), 1);
coeffpc2 = NaN(nfeatures, numel(T));
errpc2 = cell(numel(T), 1);
coeffpc2_s = NaN(nfeatures, numel(T));
errpc2_s = cell(numel(T), 1);
for idT = 1:numel(T)
    
    X = X_std{idT};
    X_s = Xshuffle_std{idT};
    
    % R
    corrmat{idT} = corrcoef(X);
    corrmat_s{idT} = corrcoef(X_s);
    
    % PCA
    [coeffs, ~, ~, ~, explained(:, idT)] = pca(X);
    [coeffs_s, ~, ~, ~, explained_s(:, idT)] = pca(X_s);
    if T(idT) == 22
        coeffs(:, 1) = -coeffs(:, 1);
    end
    coeffpc1(:, idT) = coeffs(:, 1);
    coeffpc1_s(:, idT) = coeffs_s(:, 1);
    coeffpc2(:, idT) = coeffs(:, 2);
    coeffpc2_s(:, idT) = coeffs_s(:, 2);
        
    % Bootstrap
    ci_coeff = bootci(nboots, @pca, X);
    ci_explained = bootci(nboots, @(x) getNthOutput(@pca, 5, x), X);
    ci_coeff_s = bootci(nboots, @pca, X_s);
    ci_explained_s = bootci(nboots, @(x) getNthOutput(@pca, 5, x), X_s);
    
    errexplained{idT} = ci_explained;
    errpc1{idT} = ci_coeff(:, :, 1);
    errpc2{idT} = ci_coeff(:, :, 2);
    errexplained_s{idT} = ci_explained_s;
    errpc1_s{idT} = ci_coeff_s(:, :, 1);
    errpc2_s{idT} = ci_coeff_s(:, :, 2);
    
end

mcoeffpc1 = mean(coeffpc1, 2);
mcoeffpc1_s = mean(coeffpc1_s, 2);
mcoeffpc2 = mean(coeffpc2, 2);
mcoeffpc2_s = mean(coeffpc2_s, 2);

% Projections
projmpc = cell(numel(T), 1);
projmpc_s = cell(numel(T), 1);
bins = linspace(-5, 5, nbins);
pdfprojpc1 = NaN(nbins, numel(T));
pdfprojpc1_s = NaN(nbins, numel(T));
pdfprojpc2 = NaN(nbins, numel(T));
pdfprojpc2_s = NaN(nbins, numel(T));
for idT = 1:numel(T) 
    
    % Project original data on pc space
    X = X_std{idT};
    projmpc{idT} = X*[mcoeffpc1, mcoeffpc2];
    X_s = Xshuffle_std{idT};
    projmpc_s{idT} = X_s*[mcoeffpc1_s, mcoeffpc2_s];
        
    % Compute projections PDF
    pdfprojpc1(:, idT) = computePDF(bins, projmpc{idT}(:, 1), 'method', 'kde', 'mode', 'centers');
    pdfprojpc1_s(:, idT) = computePDF(bins, projmpc_s{idT}(:, 1), 'method', 'kde', 'mode', 'centers');
    pdfprojpc2(:, idT) = computePDF(bins, projmpc{idT}(:, 2), 'method', 'kde', 'mode', 'centers');
    pdfprojpc2_s(:, idT) = computePDF(bins, projmpc_s{idT}(:, 2), 'method', 'kde', 'mode', 'centers');
    
end

% - Save
META = 'features x temperatures';
save(filename, 'coeffpc1', 'coeffpc2', 'errpc1', 'errpc2', 'explained', 'META');

fprintf('\tDone (%2.2fs).\n', toc);

% --- Display (real)
% * Variance explained
nbars = size(explained, 2);
ngroups = size(explained, 1);
f0 = figure('Name', 'IntraByTemp_VarExplained'); ax0 = axes(f0); hold(ax0, 'on');
b = bar(ax0, explained);
ax0.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel(ax0, 'Variance explained [%]');
leg = split(num2str(T));
leg = cellfun(@(x) [x '°C'], leg, 'UniformOutput', false);
legend(ax0, leg);
axis square
axis(ax0, [0.5, 5.5, 0, 60]);
for idb = 1:nbars
    % Colors
    b(idb).FaceColor = 'flat';
    b(idb).CData = repmat(colors(idb, :), [size(explained, 2), 1]);
    
    % Error bars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, errexplained{idb}(:, 1:ngroups)', 'Color', [0.01, 0.01, 0.01]);
    b(idb).DisplayName = [num2str(T(idb)) '°C'];
end

legend(ax0, b);

% * PC1 coefficients
f1 = figure('Name', 'IntraByTemp_PC1Coeff'); ax1 = axes(f1); hold(ax1, 'on');
p = cell(numel(T), 1);
for idT = 1:numel(T) 
    p{idT} = plot(ax1, coeffpc1(:, idT));
    p{idT}.Color = colors(idT, :);
    p{idT}.Marker = 's';
    p{idT}.LineWidth = .5;
    p{idT}.MarkerSize = 8;
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    manualerrbar(1:5, errpc1{idT}', 'Color', colors(idT, :));
end
pm = plot(ax1, mcoeffpc1);
pm.DisplayName = 'Mean';
pm.Color = [0, 0, 0, 0.75];
ax1.XTick = 1:nfeatures;
ax1.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax1.XTickLabelRotation = 45;
ax1.YLim = [-1, 1];
ylabel(ax1, 'PC1 coefficients');
legend(ax1, [p{:}, pm]);

% * PC2 coefficients
f2 = figure('Name', 'IntraByTemp_PC2Coeff'); ax2 = axes(f2); hold on
p = cell(numel(T), 1);
for idT = 1:numel(T) 
    p{idT} = plot(ax2, coeffpc2(:, idT));
    p{idT}.Color = colors(idT, :);
    p{idT}.Marker = 's';
    p{idT}.LineWidth = .5;
    p{idT}.MarkerSize = 8;
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    manualerrbar(1:5, errpc2{idT}', 'Color', colors(idT, :));
end
pm = plot(ax2, mcoeffpc2);
pm.DisplayName = 'Mean';
pm.Color = [0, 0, 0, 0.75];
ax2.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax2.XTickLabelRotation = 45;
ax2.YLim = [-1, 1];
ylabel(ax2, 'PC2 coefficients');
legend(ax2, [p{:}, pm]);

% * Projection in PC space
f3 = figure('Name', 'IntraByTemp_ProjPC1PC2_Centered'); ax3 = axes(f3); hold on
s = cell(numel(T), 1);
for idT = 1:numel(T) 
    x = projmpc{idT}(:, 1);
    y = projmpc{idT}(:, 2);
    
    s{idT} = scatter(ax3, x, y);
    s{idT}.Marker = '.';
    s{idT}.MarkerFaceColor = colors(idT, :);
    s{idT}.MarkerEdgeColor = colors(idT, :);
    s{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
end
ax3.XLim = [-5, 5];
ax3.YLim = [-5, 5];
axis square
xlabel(ax3, 'Projection on PC1');
ylabel(ax3, 'Projection on PC2');
legend(ax3, [s{:}], 'Location', 'southeast');

% * PDF of projections on PC1
f4 = figure('Name', 'IntraByTemp_PDFProjPC1_Centered'); ax4 = axes(f4); hold on
for idT = 1:numel(T)
    p = plot(ax4, bins, pdfprojpc1(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
end
xlabel(ax4, 'Projections on PC1');
ylabel(ax4, 'pdf');
ax4.XLim = [-5, 5];
ax4.YLim = [0, 0.5];
axis square

% * PDF of projections on PC1
f5 = figure('Name', 'IntraByTemp_PDFProjPC2_Centered'); ax5 = axes(f5); hold on
for idT = 1:numel(T)
    p = plot(ax5, bins, pdfprojpc2(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
end
xlabel(ax5, 'Projections on PC2');
ylabel(ax5, 'pdf');
ax5.XLim = [-5, 5];
ax5.YLim = [0, 0.5];
axis square

% --- Display (shuffled)
% * Variance explained (shuffled)
f0s = figure('Name', 'IntraByTemp_VarExplained_shuffle'); ax0s = axes(f0s); hold(ax0s, 'on');
b = bar(ax0s, explained_s);
ax0s.XTickLabel = {'PC1', 'PC2', 'PC3', 'PC4', 'PC5'};
ylabel(ax0s, 'Variance explained (shuffled) [%]');
leg = split(num2str(T));
leg = cellfun(@(x) [x '°C'], leg, 'UniformOutput', false);
legend(ax0s, leg);
axis square
axis([0.5, 5.5, 0, 60]);
for idb = 1:size(explained_s, 1)
    % Colors
    b(idb).FaceColor = 'flat';
    b(idb).CData = repmat(colors(idb, :), [size(explained_s, 2), 1]);
    
    % Error bars
    groupwidth = min(b(idb).BarWidth, nbars/(nbars + 1.5));
    x = (1:ngroups) - groupwidth/2 + (2*idb-1) * groupwidth / (2*nbars);
    manualerrbar(x, errexplained_s{idb}(:, 1:ngroups)', 'Color', [0.01, 0.01, 0.01]);
    
    b(idb).DisplayName = [num2str(T(idb)) '°C'];
end
legend(ax0s, b);

% * PC1 coefficients (shuffled)
f1s = figure('Name', 'IntraByTemp_PC1Coeff_shuffle'); ax1s = axes(f1s); hold on
p = cell(numel(T), 1);
for idT = 1:numel(T) 
    p{idT} = plot(ax1s, coeffpc1_s(:, idT));
    p{idT}.Color = colors(idT, :);
    p{idT}.Marker = 's';
    p{idT}.LineWidth = .5;
    p{idT}.MarkerSize = 8;
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    manualerrbar(1:5, errpc1_s{idT}', 'Color', colors(idT, :));
end
pm = plot(ax1s, mcoeffpc1_s);
pm.DisplayName = 'Mean';
pm.Color = [0, 0, 0, 0.75];
ax1s.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax1s.XTickLabelRotation = 45;
ax1s.YLim = [-1, 1];
ylabel(ax1s, 'PC1 coefficients (shuffled)');
legend(ax1s, [p{:}, pm]);

% * PC2 coefficients (shuffled)
f2s = figure('Name', 'IntraByTemp_PC2Coeff_shuffle'); ax2s = axes(f2s); hold on
p = cell(numel(T), 1);
for idT = 1:numel(T) 
    p{idT} = plot(ax2s, coeffpc2_s(:, idT));
    p{idT}.Color = colors(idT, :);
    p{idT}.Marker = 's';
    p{idT}.LineWidth = .5;
    p{idT}.MarkerSize = 8;
    p{idT}.DisplayName = ['T=' num2str(T(idT)) '°C'];
    manualerrbar(1:5, errpc2_s{idT}', 'Color', colors(idT, :));
end
pm = plot(ax2s, mcoeffpc2_s);
pm.DisplayName = 'Mean';
pm.Color = [0, 0, 0, 0.75];
ax2s.XTickLabel = {'IBI', 'p_{turn}', 'p_{flip}', '\sigma_{turn}', 'd'}';
ax2s.XTickLabelRotation = 45;
ax2s.YLim = [-1, 1];
ylabel(ax2s, 'PC2 coefficients (shuffled)');
legend(ax2s, [p{:}, pm]);

% * Projection in PC space (shuffled)
f3s = figure('Name', 'IntraByTemp_ProjPC1PC2_Centered_shuffle'); ax3s = axes(f3s); hold on
for idT = 1:numel(T) 
    x = projmpc_s{idT}(:, 1);
    y = projmpc_s{idT}(:, 2);
    
    s = scatter(ax3s, x, y);
    s.Marker = '.';
    s.MarkerFaceColor = colors(idT, :);
    s.MarkerEdgeColor = colors(idT, :);
    s.DisplayName = ['T=' num2str(T(idT)) '°C'];
end
ax3s.XLim = [-5, 5];
ax3s.YLim = [-5, 5];
axis square
xlabel(ax3s, 'Projection on PC1 (shuffled)');
ylabel(ax3s, 'Projection on PC2 (shuffled)');

% * PDF of projections on PC1 (shuffled)
f4s = figure('Name', 'IntraByTemp_PDFProjPC1_Centered_shuffle'); ax4s = axes(f4s); hold on
for idT = 1:numel(T)
    p = plot(ax4s, bins, pdfprojpc1_s(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
end
xlabel(ax4s, 'Projections on PC1 (shuffled)');
ylabel(ax4s, 'pdf');
ax4s.XLim = [-5, 5];
ax4s.YLim = [0, 0.8];
axis square

% * PDF of projections on PC2 (shuffled)
f5s = figure('Name', 'IntraByTemp_PDFProjPC2_Centered_shuffle'); ax5s = axes(f5s); hold on
for idT = 1:numel(T)
    p = plot(ax5s, bins, pdfprojpc2_s(:, idT));
    p.Color = colors(idT, :);
    p.DisplayName = ['T=' num2str(T(idT)) '°C'];
end
xlabel(ax5s, 'Projections on PC2 (shuffled)');
ylabel(ax5s, 'pdf');
ax5s.XLim = [-5, 5];
ax5s.YLim = [0, 0.8];
axis square