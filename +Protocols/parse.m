function [durations, temperatures] = parse(protocol_path)
% Protocols.parse. This function reads a protocol file for ThermoMaster and fetch left/right
% target temperatures and the duration for each temperature conditions.
% Supports only two digits temperatures, with N different temperature
% gradients.
%
% INPUTS :
% ------
% protocol_path : path to the protocol file.
%
% OUTPUTS :
% -------
% timings : Nx1 vector containing wait time after a temperature target is
% set, in seconds.
% temperatures : Nx2 array containing left temperature target on first
% column and right temperature target on right column.

fid = fopen(protocol_path);

c_0 = 0;

% Get useful lines
while 1
    
    read_string = fgetl(fid);
    
    if read_string == -1 % end of file
        break
    end
    
    if contains(read_string, 'targets:') && ~strcmp(read_string(1) , '#')
        c_0 = c_0 + 1;
        lines{c_0} = read_string;
    end
    
    if contains(read_string, 'wait:') && ~strcmp(read_string(1) , '#')
        c_0 = c_0 + 1;
        lines{c_0} = read_string;
    end
    
end

% Determine if lines are part of protocol
c = 0;
for l = 1:length(lines) - 1
    
    if contains(lines{l + 1}, 'wait:')
        c = c + 1;
        lin1 = lines{l};
        lin2 = lines{l + 1};
        temperatures(c, :) = [str2double(lin1(9:10)) str2double(lin1(12:end))];
        t(c, 1) = str2double(lin2(6:end));
    end
end

durations = t*1e-3;     % convert to seconds

fclose(fid);
end