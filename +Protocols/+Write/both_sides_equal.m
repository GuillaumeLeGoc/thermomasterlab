% 2018-04-18 Write ThermoMaster protocols. Both sides equal
clear;

% Protocol definition
% -------------------------------------------------------------------------

% File
% ----
output_path = '/home/ljp/Science/Projects/ThermoMaster/Data/Protocols/';

% Temperatures
% ------------
T_baseline = 28;                        % Temperature in baseline condition
T_stim = 18;                            % Temperature in stim condition
n_T = 4;                                % Number of temperature to set

% Timings
% -------
t_baseline = 10;                        % Wait time in baseline condition (minutes)
t_stim = 20;                        % Wait time in gradient condition (minutes)
n_cycles = 1;

% Options
% -------
randomize = 'y';                        % Shuffle T

% Commands definition
% -------------------   
cam_start = 'camera:start\n';                                       % start recording images
cam_stop = 'camera:stop\n';                                         % stop recording images
create_dir = 'data:create directory\n';                             % create Run directory
set_T = @(Tl, Tr) ['targets:' num2str(Tl) ':' num2str(Tr) '\n'];    % set left and right targets
wait = @(wait_time) ['wait:' num2str(wait_time*60*1000) '\n'];      % wait wait_time minutes

% File name
% ---------
filename = [output_path 'both_side_equals_Tbase=' num2str(T_baseline) '_' ...
    num2str(t_baseline) 'min_Tstim=' num2str(T_stim)  '_' num2str(t_stim) 'min.protocol'];

% Write protocol
% -------------------------------------------------------------------------
if ~exist(filename, 'file')
    fid = fopen(filename, 'w');              % Open file
else
    error('File already exists.');
end

targets_string = cell(numel(T_stim, 1));
for c = 1:numel(T_stim)
    targets_string{c} = set_T(T_stim(c), T_stim(c));
end

% Randomize gradients
% -------------------
switch randomize
    case 'y'
        targets_string = targets_string(randperm(numel(targets_string)));            % Randomize targets
end

% Write protocol to file
% ----------------------
fprintf(fid, '# Protocol %s', ['Both sides always equal, ' num2str(t_baseline) ' min @ ' num2str(T_baseline) '°C' ...
    ', ' num2str(t_stim) ' min @ ' num2str(T_stim) '°C\n']);
fprintf(fid, '\nprint:Starting protocol...\n');
fprintf(fid, create_dir);
fprintf(fid, '\n# Start images recording\n');
fprintf(fid, cam_start);
for c = 1:n_cycles
    fprintf(fid, '\n# Set baseline condition\n');
    fprintf(fid, set_T(T_baseline, T_baseline));
    fprintf(fid, '\n# Wait\n');
    fprintf(fid, wait(t_baseline));
    fprintf(fid, '\n# Set stimulation condition\n');
    fprintf(fid, targets_string{c});
    fprintf(fid, '\n# Wait\n');
    fprintf(fid, wait(t_stim));
end

fprintf(fid, '\n# Stop images recording\n');
fprintf(fid, cam_stop);
fprintf(fid, 'print:Protocol ended');