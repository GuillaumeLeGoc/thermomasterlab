% 2018-02-12 Write ThermoMaster protocols. Baseline - Gradient -Baseline -
% ...
clear;

% Protocol definition
% -------------------------------------------------------------------------

% File
% ----
output_path = '/home/ljp/Science/Projects/ThermoMaster/Data/Protocols/';

% Temperatures
% ------------
T_baseline = 28;                      % Temperature in baseline condition
T1 = T_baseline + 10;                  % Temperatures in gradient condition
T2 = T_baseline + 15;
T3 = T_baseline - 10;
T4 = T_baseline - 15;

T_gradient = [T1 T2 T3 T4];
% T_gradient = [T1 T2];
dT1 = abs(T1 - T_baseline);
dT2 = abs(T2 - T_baseline);

% n_cycles = 1;

% Timings
% -------
t_baseline = 10;                        % Wait time in baseline condition (minutes)
t_gradient = 20;                        % Wait time in gradient condition (minutes)

% Options
% -------
randomize = 'y';                    % Shuffle T
dupli_left_right = 'y';             % Duplicate each gradient with its left-right mirror

if ~exist('n_cycles', 'var')
    switch dupli_left_right
        case 'y'
            n_cycles = 2*numel(T_gradient);
        case 'n'
            n_cycles = numel(T_gradient);
    end
end

% Commands definition
% -------------------
cam_start = 'camera:start\n';                                 % start recording images
cam_stop = 'camera:stop\n';                                   % stop recording images
create_dir = 'data:create directory\n';                       % create Run directory
set_T = @(Tl, Tr) ['targets:' num2str(Tl) ':' num2str(Tr) '\n']; % set left and right targets
wait = @(wait_time) ['wait:' num2str(wait_time*60*1000) '\n'];      % wait wait_time minutes

% File name
% ---------
filename = [output_path 'bl=' num2str(T_baseline) '-' num2str(t_baseline) 'min_dT1=' ...
    num2str(dT1) '_dT2=' num2str(dT2) '-' num2str(t_gradient) 'min.protocol'];

% Write protocol
% -------------------------------------------------------------------------
if ~exist(filename, 'file')
    fid = fopen(filename, 'w');              % Open file
else
    error('File already exists.');
end

% Duplicate each gradient
% -----------------------
switch dupli_left_right
    case 'y'
        targets_string = cell(n_cycles, 1);
        for c = 1:numel(T_gradient)
            targets_string{c} = set_T(T_baseline, T_gradient(c));
            targets_string{c + numel(T_gradient)} = set_T(T_gradient(c), T_baseline);
        end
        
    case 'n'
        targets_string = cell(numel(T_gradient, 1));
        for c = 1:numel(T_gradient)
            targets_string{c} = set_T(T_baseline, T_gradient(c));
        end
end

% Randomize gradients
% -------------------
switch randomize
    case 'y'
        targets_string = targets_string(randperm(numel(targets_string)));            % Randomize targets
end

% Write protocol to file
% ----------------------
fprintf(fid, '# Protocol %s', ['bl=' num2str(T_baseline) '-' num2str(t_baseline) 'min_dT1=' ...
    num2str(dT1) '_dT2=' num2str(dT2) '-' num2str(t_gradient) 'min\n']);
fprintf(fid, '\nprint:Starting protocol...\n');
fprintf(fid, create_dir);
fprintf(fid, '\n# Start images recording\n');
fprintf(fid, cam_start);
for c = 1:n_cycles
    fprintf(fid, '\n# Set baseline condition\n');
    fprintf(fid, set_T(T_baseline, T_baseline));
    fprintf(fid, '\n# Wait\n');
    fprintf(fid, wait(t_baseline));
    fprintf(fid, '\n# Set gradient condition\n');
    fprintf(fid, targets_string{c});
    fprintf(fid, '\n# Wait\n');
    fprintf(fid, wait(t_gradient));
end
fprintf(fid, '\n# Set baseline condition\n');
fprintf(fid, set_T(T_baseline, T_baseline));
fprintf(fid, '\n# Wait\n');
fprintf(fid, wait(t_baseline));
fprintf(fid, '\n# Stop images recording\n');
fprintf(fid, cam_stop);
fprintf(fid, 'print:Protocol ended');