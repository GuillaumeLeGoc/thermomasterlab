function [time, temperatures] = readTempLogger(path_to_file)
% Reads .log file from ThermoMaster calibration. The file is the output of
% the software included in the USB NI thermocouple reader.

data = readtable(path_to_file, 'HeaderLines', 8, 'FileType', 'text');

time_txt = table2array(data(:,1));
temperatures = table2array(data(:, 2));

time = NaN(length(time_txt), 1);

for idx_time = 1:length(time_txt)
    tmp_str = time_txt(idx_time);
    t = datevec(tmp_str);
    tmp_time = t(4)*60*60 + t(5)*60 + t(6);
    time(idx_time) = tmp_time;
end

time = time - time(1);
end