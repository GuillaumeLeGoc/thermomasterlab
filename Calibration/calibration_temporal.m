% 2018-02-12 Plots the mean temperature at 5 different position in the fish
% pool, after a given time till the end. Adds the theoritical curves.

clear
%close all
clc

base = '/home/guillaume/Science/Projects/ThermoMaster/Data/';
calib_path = [base 'Calibration_V2' filesep];

Lx = 100;           % Bath length (mm)
T_look = 30;        % Look at temperature every T seconds
dT_mean = 5;        % averages temperature between T_look and T_look + dT_mean

protocols = {[28 28], [34 28], [22 28]};

positions = 1:5;

x = linspace(5, Lx - 5, length(positions));

marker_size = 10;

max_time = 200;     % limit all temperatures log at first  246 points

% 28-28 > 33-28
% -------------------------------------------------------------------------
Tstart = protocols{1};
Tend = protocols{2};

Tstart_left = Tstart(1);
Tstart_right = Tstart(2);
Tend_left = Tend(1);
Tend_right = Tend(2);

figure;
hold on

for position = positions

    temp_filename = [calib_path 'Pos' num2str(position) '_' ...
    num2str(Tstart_left) '-' num2str(Tstart_right) '_to_' ...
    num2str(Tend_left) '-' num2str(Tend_right) '.log'];

    [t, T] = readTempLogger(temp_filename);
    
    t = t(1:max_time);
    T = T(1:max_time);
    
    sample_rate = mean(diff(t));
    T_look_sample = round(T_look./sample_rate);
    dT_mean_sample = round(dT_mean./sample_rate);
    
    c = 0;
    tmp_T = NaN(length(1:T_look_sample:length(t)), 1);
    for t_look = 1:T_look_sample:length(t)
        c = c +1;
        if (t_look + dT_mean) > length(T)
            sub_Temperature = T(t_look:end);
        else
            sub_Temperature = T((t_look:(t_look + dT_mean)));
        end
        tmp_T(c) = mean(sub_Temperature);
    end
    
    T_plateau(position, :) = tmp_T;
    
end

cmap = jet(size(T_plateau, 2));

timepoint = -T_look;
a = (Tend_right - Tend_left)/100;
b = Tend_left;
x_y = linspace(0, 100, 1000);
y = a.*x_y + b;
for i = 1:size(T_plateau, 2)
    timepoint = timepoint + T_look;
    curve_name = [num2str(timepoint) 's'];
    plot(x, T_plateau(:, i), '+-', 'Color', cmap(i, :), 'DisplayName', curve_name, ...
        'MarkerSize', marker_size, 'LineWidth', 3);
end
plot(x_y, y, 'r--', 'LineWidth', 2, 'DisplayName', 'Model');
xlabel('Position [mm]');
ylabel('Temperature [°C]');
legend;
titre = ['Thermal gradient, protocol : ' num2str(Tstart_left) ':' num2str(Tstart_right) '>' num2str(Tend_left) ':' num2str(Tend_right)];
title(titre);
set(gca, 'FontSize', 30);
grid on

% 28-28 > 23-28
% -------------------------------------------------------------------------
Tstart = protocols{1};
Tend = protocols{3};

Tstart_left = Tstart(1);
Tstart_right = Tstart(2);
Tend_left = Tend(1);
Tend_right = Tend(2);

figure;
hold on

for position = positions

    temp_filename = [calib_path 'Pos' num2str(position) '_' ...
    num2str(Tstart_left) '-' num2str(Tstart_right) '_to_' ...
    num2str(Tend_left) '-' num2str(Tend_right) '.log'];

    [t, T] = readTempLogger(temp_filename);
    
    t = t(1:max_time);
    T = T(1:max_time);
    
    sample_rate = mean(diff(t));
    T_look_sample = round(T_look./sample_rate);
    dT_mean_sample = round(dT_mean./sample_rate);
    
    c = 0;
    tmp_T = NaN(length(1:T_look_sample:length(t)), 1);
    for t_look = 1:T_look_sample:length(t)
        c = c +1;
        if (t_look + dT_mean) > length(T)
            sub_Temperature = T(t_look:end);
        else
            sub_Temperature = T((t_look:(t_look + dT_mean)));
        end
        tmp_T(c) = mean(sub_Temperature);
    end
    
    T_plateau(position, :) = tmp_T;
    
end

cmap = jet(size(T_plateau, 2));

timepoint = -T_look;
a = (Tend_right - Tend_left)/100;
b = Tend_left;
x_y = linspace(0, 100, 1000);
y = a.*x_y + b;
for i = 1:size(T_plateau, 2)
    timepoint = timepoint + T_look;
    curve_name = [num2str(timepoint) 's'];
    plot(x, T_plateau(:, i), '+-', 'Color', cmap(i, :), 'DisplayName', curve_name, ...
        'MarkerSize', marker_size, 'LineWidth', 3);
end
plot(x_y, y, 'r--', 'LineWidth', 2, 'DisplayName', 'Model');
xlabel('Position [mm]');
ylabel('Temperature [°C]');
legend;
titre = ['Thermal gradient, protocol : ' num2str(Tstart_left) ':' num2str(Tstart_right) '>' num2str(Tend_left) ':' num2str(Tend_right)];
title(titre);
set(gca, 'FontSize', 30);
grid on