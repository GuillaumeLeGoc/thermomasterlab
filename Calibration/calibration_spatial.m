% Plots the mean temperature at 5 different position in the fish
% pool, after a given time till the end. Adds the theoritical curves.

clear
close all
clc

base = '/home/guillaume/Science/Projects/ThermoMaster/Data/';
calib_path = [base 'Calibration_V2' filesep];

Lx = 100;   % Bath length (mm)
X = 3;      % Compute mean temerature after X minutes till the end

protocols = {[28 28], [34 28], [22 28]};

positions = 1:5;

x = linspace(10, Lx - 10, length(positions));
x_y = linspace(0, Lx, length(positions));

marker_size = 10;

colors = getColors;

% Prepare figure
figure;
hold on
pl = plot(NaN(10, 1), '-.');
pl.LineWidth = 4;
pl.Color = [0 0 0];
pl2 = plot(NaN(10, 1), '+');
pl2.LineWidth = 4;
pl2.Color = [0 0 0];
legend('Asked', 'Measured', 'AutoUpdate', 'off')

% 28-28 > 34-28
% -------------------------------------------------------------------------
Tstart = protocols{1};
Tend = protocols{2};

Tstart_left = Tstart(1);
Tstart_right = Tstart(2);
Tend_left = Tend(1);
Tend_right = Tend(2);

T_plateau = NaN(length(positions), 1);

for position = positions

    temp_filename = [calib_path 'Pos' num2str(position) '_' ...
    num2str(Tstart_left) '-' num2str(Tstart_right) '_to_' ...
    num2str(Tend_left) '-' num2str(Tend_right) '.log'];

    [t, T] = readTempLogger(temp_filename);

    sample_rate = mean(diff(t));
    X_s = round(60.*X./sample_rate);
    
    T_plateau(position) = mean(T((end-X_s):end));
end

a = (Tend_right - Tend_left)/100;
b = Tend_left;
y = a.*x_y + b;

p0 = plot(x, T_plateau, '+', 'MarkerSize', marker_size);
p1 = plot(x_y, y, '-.');
p0.Color = colors(2, :);
p0.LineWidth = 4;
p1.Color = colors(2, :);
p1.LineWidth = 3;

% 28-28 > 22-28
% -------------------------------------------------------------------------
Tstart = protocols{1};
Tend = protocols{3};

Tstart_left = Tstart(1);
Tstart_right = Tstart(2);
Tend_left = Tend(1);
Tend_right = Tend(2);

T_plateau = NaN(length(positions), 1);

for position = positions

    temp_filename = [calib_path 'Pos' num2str(position) '_' ...
    num2str(Tstart_left) '-' num2str(Tstart_right) '_to_' ...
    num2str(Tend_left) '-' num2str(Tend_right) '.log'];

    [t, T] = readTempLogger(temp_filename);

    sample_rate = mean(diff(t));
    X_s = round(60.*X./sample_rate);
    
    T_plateau(position) = mean(T((end-X_s):end));
end

a = (Tend_right - Tend_left)/100;
b = Tend_left;
y = a.*x_y + b;
% subplot(3,1,2);
hold on
p0 = plot(x, T_plateau, '+', 'MarkerSize', marker_size);
p1 = plot(x_y, y, '-.');
p0.Color = colors(1, :);
p0.LineWidth = 4;
p1.Color = colors(1, :);
p1.LineWidth = 3;

% 33-28 > 28-28
% -------------------------------------------------------------------------
Tstart = protocols{2};
Tend = protocols{1};

Tstart_left = Tstart(1);
Tstart_right = Tstart(2);
Tend_left = Tend(1);
Tend_right = Tend(2);

T_plateau = NaN(length(positions), 1);

for position = positions

    temp_filename = [calib_path 'Pos' num2str(position) '_' ...
    num2str(Tstart_left) '-' num2str(Tstart_right) '_to_' ...
    num2str(Tend_left) '-' num2str(Tend_right) '.log'];

    [t, T] = readTempLogger(temp_filename);

    sample_rate = mean(diff(t));
    X_s = round(60.*X./sample_rate);
    
    T_plateau(position) = mean(T((end-X_s):end));
end

a = (Tend_right - Tend_left)/100;
b = Tend_left;
y = a.*x_y + b;

hold on
p0 = plot(x, T_plateau, '+', 'MarkerSize', marker_size);
p1 = plot(x_y, y, '-.');
p0.Color = colors(5, :);
p0.LineWidth = 4;
p1.Color = colors(5, :);
p1.LineWidth = 3;

xlabel('Position [mm]');
ylabel('Temperature [°C]');

title('Calibration of behavioral assay, 3 minutes after');
set(gca, 'FontSize', 24);