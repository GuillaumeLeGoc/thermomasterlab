function [track_path, stamp_path, fish_numbers, fish_ages] = getGradientList(Tleft, Tright, Tbase, exp_list_file)
% [track_path, stamp_path, fish_numbers, fish_ages] = GETGADIENTLIST(Tgradient, Tbase, exp_list_file)
% GETCHEMICALSLIST returns a list of paths to tracking and stamps files
% corresponding to TEMPERATURE and CHEMICALS at specified CONCENTRATION 
% found in the chemicals experiment list. If no concentration specified, 
% all concentrations are returned.
%
% INPUTS :
% ------
% Tleft : left temperature during gradient
% Tright : right temperature during gradient
% Tbase : temperature during baseline
% exp_list (optional) : path to the experiment list file.
%
% OUTPUTS :
% -------
% track_path : list of paths to tracking file
% stamp_path : list of path to stamps file
% fish numbers : number of fish in the experiment
% fish_ages : ages of fish in  the experiment

% --- Defaults values
cwd = strcat(pwd, filesep, 'Data', filesep);
if ~exist('exp_list_file', 'var')
    exp_list_file = strcat(cwd, 'Gradient_list.txt');
end

% --- Definitions
make_trackname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');
make_stampname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');

% --- Preparation
exp_list = readtable(exp_list_file, 'DatetimeType', 'text');
track_path = cell(size(exp_list, 1), 1);
stamp_path = cell(size(exp_list, 1), 1);
fish_numbers = cell(size(exp_list, 1), 1);
fish_ages = cell(size(exp_list, 1), 1);
c = 0;

% --- Search for experiments with the given temperature
for idx_dat = 1:size(exp_list, 1)
    
    dat = exp_list.Date{idx_dat};
    
    if strcmp(dat(1), '#')
        continue;
    end
    
    runs = exp_list.RunNumber(idx_dat);
    Tbases = exp_list.Baseline(idx_dat);
    Tlefts = exp_list.TemperatureLeft(idx_dat);
    Trights = exp_list.TemperatureRight(idx_dat);
    
    numbers = exp_list.FishNumber(idx_dat);
    age = exp_list.Age(idx_dat);
    
    if ischar(runs)
        runs = eval(strcat('[', runs, ']'));
        temperatures = eval(strcat('[', temperatures, ']'));
        concentrations = eval(strcat('[', concentrations, ']'));
        numbers = eval(strcat('[', numbers, ']'));
    end
    
    % Find all experiments matching specs
    idxTempL = find(Tlefts == Tleft);
    idxTempR = find(Trights == Tright);
    idxBase = find(Tbases == Tbase);
    
    % Find experiments that meet all three
    idx = intersect(intersect(idxTempL, idxTempR), idxBase);    
    
    if ~isempty(idx)
        
        for id = 1:numel(idx)
            c = c + 1;
            track_path{c, 1} = make_trackname(dat, runs(idx(id)));
            stamp_path{c, 1} = make_stampname(dat, runs(idx(id)));
            fish_numbers{c, 1} = numbers(idx(id));
            fish_ages{c, 1} = age;
        end
        
    end
end

% --- Cleanup
track_path(cellfun(@isempty, track_path)) = [];
stamp_path(cellfun(@isempty, stamp_path)) = [];
fish_numbers(cellfun(@isempty, fish_numbers)) = [];
fish_ages(cellfun(@isempty, fish_ages)) = [];
end