function [track_path, stamp_path, chem_units, fish_numbers, fish_ages] = getChemicalsList(temperature, chemical, concentration, exp_list_file)
% [track_path, stamp_path, chem_units, fish_numbers, fish_ages] = GETCHEMICALSLIST(temperature, chemical, concentration, exp_list_file)
% GETCHEMICALSLIST returns a list of paths to tracking and stamps files
% corresponding to TEMPERATURE and CHEMICALS at specified CONCENTRATION 
% found in the chemicals experiment list. If no concentration specified, 
% all concentrations are returned.
%
% INPUTS :
% ------
% temperature : target pool temperature
% chemicals : target chemicals
% concentration : target concentration (in % for ethanol, in mM for others)
% exp_list (optional) : path to the experiment list file.
%
% OUTPUTS :
% -------
% track_path : list of paths to tracking file
% stamp_path : list of path to stamps file
% chem_units : list of chemical concentration units
% fish numbers : number of fish in the experiment
% fish_ages : ages of fish in  the experiment

% --- Defaults values
cwd = strcat(pwd, filesep, 'Data', filesep);
if ~exist('exp_list_file', 'var')
    exp_list_file = strcat(cwd, 'Chemicals_list.txt');
end

% --- Definitions
make_trackname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');
make_stampname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');

% --- Preparation
exp_list = readtable(exp_list_file, 'DatetimeType', 'text');
track_path = cell(size(exp_list, 1), 1);
stamp_path = cell(size(exp_list, 1), 1);
chem_units = cell(size(exp_list, 1), 1);
fish_numbers = cell(size(exp_list, 1), 1);
fish_ages = cell(size(exp_list, 1), 1);
c = 0;

% --- Search for experiments with the given temperature
for idx_dat = 1:size(exp_list, 1)
    
    dat = exp_list.Date{idx_dat};
    
    if strcmp(dat(1), '#')
        continue;
    end
    
    runs = exp_list.RunNumber{idx_dat};
    temperatures = exp_list.Temperature{idx_dat};
    chemicals = split(exp_list.Chemicals{idx_dat}, ',');
    units = split(exp_list.Units{idx_dat}, ',');
    concentrations = exp_list.Concentration{idx_dat};
    
    numbers = exp_list.FishNumber{idx_dat};
    age = exp_list.Age(idx_dat);
    
    if ischar(runs)
        runs = eval(strcat('[', runs, ']'));
        temperatures = eval(strcat('[', temperatures, ']'));
        concentrations = eval(strcat('[', concentrations, ']'));
        numbers = eval(strcat('[', numbers, ']'));
    end
    if isempty(concentration)
        concentration = concentrations;
    end
    
    % Find all experiments matching specs
    idxTemp = find(temperatures == temperature);
    idxChem = find(contains(chemicals, chemical, 'IgnoreCase', true));
    idxConc = find(concentrations == concentration);
    
    % Find experiments that meet all three
    idx = intersect(intersect(idxTemp, idxChem), idxConc);    
    
    if ~isempty(idx)
        
        for id = 1:numel(idx)
            c = c + 1;
            track_path{c, 1} = make_trackname(dat, runs(idx(id)));
            stamp_path{c, 1} = make_stampname(dat, runs(idx(id)));
            chem_units{c, 1} = units{idx(id)};
            fish_numbers{c, 1} = numbers(idx(id));
            fish_ages{c, 1} = age;
        end
        
    end
end

% --- Cleanup
track_path(cellfun(@isempty, track_path)) = [];
stamp_path(cellfun(@isempty, stamp_path)) = [];
chem_units(cellfun(@isempty, chem_units)) = [];
fish_numbers(cellfun(@isempty, fish_numbers)) = [];
fish_ages(cellfun(@isempty, fish_ages)) = [];
end