function rearranged = rearrangeData(X)
% Removes trailing NaNs and appends NaNs to the end of each rows of X.

n_seq = size(X, 1);
tmp = cell(n_seq, 1);
maxn = 0;
for seq = 1:size(X, 1)
    first_nonan = find(~isnan(X(seq, :)), 1, 'first');
    last_nonan = find(~isnan(X(seq, :)), 1, 'last');
    tmp{seq} = X(seq, first_nonan:last_nonan);
    maxn = max(maxn, numel(tmp{seq}));
end
rearranged = NaN(n_seq, maxn);
for seq = 1:size(X, 1)
    rearranged(seq, 1:numel(tmp{seq})) = tmp{seq};
end
end