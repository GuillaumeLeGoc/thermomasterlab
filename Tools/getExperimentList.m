function [track_path, stamp_path, fish_numbers, fish_ages] = getExperimentList(temperature, age, exp_list_file)
% [track_path, stamp_path, fish_numbers, fish_ages] = GETEXPERIMENTLIST(temperature, exp_list_file)
% GETEXPERIMENTLIST returns a list of paths to tracking and stamps files
% corresponding to TEMPERATURE found in the uniform experiment list.
%
% INPUTS :
% ------
% temperature : target pool temperature
% age (optional) :, target fish age (dpf), empty for all
% exp_list (optional) : path to the experiment list file.
%
% OUTPUTS :
% -------
% track_path = list of paths to tracking file
% stamp_path = list of path to stamps file

% --- Defaults values
cwd = strcat(pwd, filesep, 'Data', filesep);
if ~exist('age', 'var')
    age = [];
end
if ~exist('exp_list_file', 'var')
    exp_list_file = strcat(cwd, 'Uniform_list.txt');
end

% --- Definitions
make_trackname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'Tracking_Result', 'tracking.txt');
make_stampname = @(d, r) fullfile(cwd, d, ['Run ' sprintf('%02i', r)], 'stamps.mat');

% --- Preparation
exp_list = readtable(exp_list_file, 'DatetimeType', 'text');
track_path = cell(size(exp_list, 1), 1);
stamp_path = cell(size(exp_list, 1), 1);
fish_numbers = cell(size(exp_list, 1), 1);
fish_ages = cell(size(exp_list, 1), 1);
c = 0;

% --- Search for experiments with the given temperature
for idx_dat = 1:size(exp_list, 1)
    
    dat = exp_list.Date{idx_dat};
    
    if strcmp(dat(1), '#')
        continue;
    end
    
    if iscell(exp_list.RunNumber)
        runs = exp_list.RunNumber{idx_dat};
        temperatures = exp_list.Temperature{idx_dat};
        numbers = exp_list.FishNumber{idx_dat};
    else
        runs = exp_list.RunNumber(idx_dat);
        temperatures = exp_list.Temperature(idx_dat);
        numbers = exp_list.FishNumber(idx_dat);
    end
        ages = exp_list.Age(idx_dat);
    
    if ~isempty(age)
        if ages ~= age
            continue;
        end
    end
    
    if ischar(runs)
        runs = eval(strcat('[', runs, ']'));
        temperatures = eval(strcat('[', temperatures, ']'));
        numbers = eval(strcat('[', numbers, ']'));
    end
    
    idx = find(temperatures == temperature);
    
    if ~isempty(idx)
        
        for id = 1:numel(idx)
            c = c + 1;
            track_path{c, 1} = make_trackname(dat, runs(idx(id)));
            stamp_path{c, 1} = make_stampname(dat, runs(idx(id)));
            fish_numbers{c, 1} = numbers(idx(id));
            fish_ages{c, 1} = ages;
        end
    end
end

% --- Cleanup
track_path(cellfun(@isempty, track_path)) = [];
stamp_path(cellfun(@isempty, stamp_path)) = [];
fish_numbers(cellfun(@isempty, fish_numbers)) = [];
fish_ages(cellfun(@isempty, fish_ages)) = [];
end