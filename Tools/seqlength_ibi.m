% Look at interbout interval dependence on sequence length

% close all
clear
clc

% --- Files
seqfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences.mat'];

% --- Temperature
T = [18, 22, 26, 30, 33];

% --- Parameters
nbins = 25;
pixsize = 0.09;
framerate = 25;

% --- Get data
D = load(seqfile);

% --- Prepare figures
colors = rainbow(numel(T));
flen = figure; axlen = axes(flen); hold(axlen, 'on'); axis square;
fibi = figure; axibi = axes(fibi); hold(axibi, 'on'); axis square;
fvar = figure; axvar = axes(fvar); hold(axvar, 'on'); axis square;

% --- Processing
e0 = cell(numel(T), 1);
e1 = cell(numel(T), 1);
for idT = 1:numel(T)
    
    % - Get data
    ibi = D.interboutintervals{idT}/framerate;
    
    % - Get mean interbout interval and trajectory lenghts
    mibi = nanmean(ibi);
    boutloc = ~isnan(ibi);
    seqlen = sum(boutloc);
    
    % Get trajectory lengths pdf
    binlen = linspace(0, 800, 25);
    [pdflen, centerlen] = computePDF(binlen, seqlen, 'mode', 'edges');
    
    % Get mean interbout interval as a function of trajectory length
    [bins_seq_ibi, epb_ibi, bmibi] = BinsWithEqualNbofElements(seqlen, mibi, nbins, nbins + 3);
    ibi_binmean = mean(bmibi, 2);
    erro_ibi = std(bmibi, 1, 2)./sqrt(epb_ibi);
    
    % Get variance of means depending on trajectory length
    [binnedlen, edges] = discretize(seqlen, nbins);                 % regroup in bins
    varibi = accumarray(binnedlen', mibi', [], @var, NaN);	% variance over bins
    centers = edges(1:end-1) + diff(edges)/2;
    
    % 1/N curve
    n1curve = varibi(1).*centers(1)./centers;
    
    % - Display
    lineopt = struct;
    lineopt.DisplayName = ['T = ' num2str(T(idT)) '°C'];
    lineopt.Color = colors(idT, :);
    lineopt.MarkerSize = 4;
    lineopt.LineWidth = 2;
    shadopt = struct;
    shadopt.FaceAlpha = 0.275;
    
    pname = ['T=' num2str(T(idT)) '°C'];
    plot(axlen, centerlen, pdflen, 'Color', colors(idT, :), 'DisplayName', pname);
    set(0, 'CurrentFigure', fibi);
    e0{idT} = errorshaded(bins_seq_ibi, ibi_binmean, erro_ibi, 'line', lineopt, 'patch', shadopt);
    e1{idT} = plot(axvar, centers, varibi, 'Color', colors(idT, :), 'DisplayName', pname);
    plot(axvar, centers, n1curve, 'Color', [0.25 0.25 0.25 0.25]);
end

axlen.XLabel.String = 'number of bout in sequence';
axlen.YLabel.String = 'pdf';
legend(axlen)
axibi.XLabel.String = 'number of bout in sequence';
axibi.YLabel.String = 'bin-average of <\delta{t}>';
legend(axibi, [e0{:}]);
axvar.XLabel.String = 'number of bout in sequence';
axvar.YLabel.String = 'Var(<\delta{t}>)';
legend(axvar, [e1{:}]);
axvar.XScale = 'log';
axvar.YScale = 'log';