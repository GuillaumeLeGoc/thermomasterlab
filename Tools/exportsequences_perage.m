% Export data in matfile.

clear

% * Temperatures
T = [18, 22, 26, 30, 33];
framerate = 25;
pixsize = 0.09;
age = 7;

% * Files
outfile = [pwd filesep 'Data' filesep 'Matfiles' filesep 'allsequences_' num2str(age) 'dpf.mat'];

% * Filters
filt.seq_length = 25;       % min sequence length for each fish in seconds
% filt.bout_freq = .1;        % minimum average bout frequency
filt.n_bouts = 10;          % min number of bouts
filt.minx = 0;
filt.maxx = Inf;

% * Displacement fix
theta_threshold = 10;    % deg
displ_factor = 1.6;

% * Init.
xpos = cell(numel(T), 1);
ypos = cell(numel(T), 1);
interboutintervals = cell(numel(T), 1);
displacements = cell(numel(T), 1);
dtheta = cell(numel(T), 1);
bouttime = cell(numel(T), 1);
framerates = cell(numel(T), 1);

for idT = 1:numel(T)
        
    fprintf(['Gathering data for T = ', num2str(T(idT)), '°C ']); tic
    
    % Build file names
    [ftrack, fstamp, nfish] = getExperimentList(T(idT), age);
    
    % Init. arrays
    allx = cell(size(ftrack, 1), 1);
    ally = cell(size(ftrack, 1), 1);
    alli = cell(size(ftrack, 1), 1);
    alld = cell(size(ftrack, 1), 1);
    allr = cell(size(ftrack, 1), 1);
    allt = cell(size(ftrack, 1), 1);
    allf = cell(size(ftrack, 1), 1);
    
    % Get features from each experiment
    for idexp = 1:size(ftrack, 1)
        
        [exp_xpo, exp_ibi, exp_dsp, exp_reo, exp_fra, ~, framerate, exp_ypo] = Tracks.getFeatures(ftrack(idexp), fstamp(idexp), filt, false);
        
        nseq = size(exp_ibi, 2);
        
        % Loop over sequence
        x = cell(nseq, 1);
        y = cell(nseq, 1);
        i = cell(nseq, 1);
        d = cell(nseq, 1);
        r = cell(nseq, 1);
        t = cell(nseq, 1);
        toclean = false(nseq, 1);
        for s = 1:nseq
            
            % Position
            trajx = exp_xpo(:, s);
            trajx(isnan(trajx)) = [];
            trajx = trajx.*pixsize;
            x{s} = trajx;
            
            trajy = exp_ypo(:, s);
            trajy(isnan(trajy)) = [];
            trajy = trajy.*pixsize;
            y{s} = trajy;
            
            % Interbout interval
            trajibi = exp_ibi(:, s);
            trajibi(isnan(trajibi)) = [];
            trajibi = trajibi./framerate;
            i{s} = trajibi;
            
            % Turn angle
            trajreo = exp_reo(:, s);
            trajreo(isnan(trajreo)) = [];
            r{s} = trajreo;
            isturn = abs(trajreo) > theta_threshold;
            trireo = trajreo;
            trireo(~isturn) = 0;
            trireo(trireo < 0) = -1;
            trireo(trireo > 0) = +1;
            % filter out if not all three bout types are present
            if numel(unique(trireo)) < 3
                toclean(s) = true;
                continue;
            end
            
            % Displacement
            trajdsp = exp_dsp(:, s);
            trajdsp(isnan(trajdsp)) = [];
            trajdsp = trajdsp.*pixsize;
            trajdsp(isturn) = trajdsp(isturn)./displ_factor;
            d{s} = trajdsp;
            
            % Time vector of bouts
            trajt = exp_fra(:, s);
            trajt(isnan(trajt)) = [];
            trajt = trajt./framerate;
            t{s} = trajt;
            
        end
            
        % Cleanup
        x(toclean) = [];
        y(toclean) = [];
        i(toclean) = [];
        d(toclean) = [];
        r(toclean) = [];
        t(toclean) = [];
        
        % Match number of bouts by filling with NaNs
        x = fixCellSize(x);
        y = fixCellSize(y);
        i = fixCellSize(i);
        d = fixCellSize(d);
        r = fixCellSize(r);
        t = fixCellSize(t);
        
        % Concatenate & store
        allx{idexp} = cat(2, x{:});
        ally{idexp} = cat(2, y{:});
        alli{idexp} = cat(2, i{:});
        alld{idexp} = cat(2, d{:});
        allr{idexp} = cat(2, r{:});
        allt{idexp} = cat(2, t{:});
        allf{idexp} = framerate;
        
        fprintf('.');
    end
    
    % Match number of bouts & concatenate
    allx = fixCellSize(allx, 1);
    allx = cat(2, allx{:});
    ally = fixCellSize(ally, 1);
    ally = cat(2, ally{:});
    alli = fixCellSize(alli, 1);
    alli = cat(2, alli{:});
    alld = fixCellSize(alld, 1);
    alld = cat(2, alld{:});
    allr = fixCellSize(allr, 1);
    allr = cat(2, allr{:});
    allt = fixCellSize(allt, 1);
    allt = cat(2, allt{:});
    allf = mean(cat(2, allf{:}));
    
    xpos{idT} = allx;
    ypos{idT} = ally;
    interboutintervals{idT} = alli;
    displacements{idT} = alld;
    dtheta{idT} = allr;
    bouttime{idT} = allt;
    framerates{idT} = allf;
    
    fprintf('\tDone (%2.2fs.)\n', toc);
end

META = 'ntemperature x 1 cell. nbout x nsequences arrays. Trailing NaNs to match sizes. x, y, dsp in mm, time in secs. Turn displacements fixed.';
save(outfile, 'xpos', 'ypos', ...
    'interboutintervals', 'displacements', 'dtheta', ...
    'bouttime', 'framerates', 'META');
